;----------------------------------------------------------------------------------------
; *  V A R I A B L E S  *			; $ff91
;----------------------------------------------------------------------------------------
		org	48896
ticks		equ 48896   
spr		equ 48897	;2
vidas		equ 48899    
fila		equ 48900
colu		equ 48901
frame		equ 48902
senpro		equ 48903
dirmovsp	equ 48904
flagpa		equ 48905	; Pasar de pantalla
dead		equ 48906
last_p		equ 48907	;2 copia de fila y colu
dir_spr		equ 48909   ;2 posicion del spr.en pantalla
d_bu_spr	equ 48911	;2 direccion en el buffer del prota
bytevesal	equ 48913
keypush		equ 48914
keypush2	equ 48915
proparado	equ 48916
skipf_p		equ 48917
flagespa	equ 48918
repe_fx		equ 48919
long_fx		equ 48920
inmunidad	equ 48921
control		equ 48922	;2
cont_inmu	equ 48924
accion		equ 48925
obje		equ 48926
num_tile	equ 48927
al_tile		equ 48928
an_tile		equ 48929
fil_dat		equ 48930
col_dat		equ 48931
poti_im		equ 48932	;2
poti_ia		equ 48934	;2
panta		equ 48936	;2 el registro L valdr� el valor de aqu� en la rutina mapping.El byte anterior dejar a 0
final		equ 48938
timer		equ 48939
alto		equ 48940	; Utiles para la impresion de los atributos del prota
				; ancho	    equ 48941	; "
enemenliana	equ 48941	; enemigo enganchado a liana
framev		equ 48942	; frame vertical
liana		equ 48943	; liana en la posici�n
enganchao	equ 48944	; Enganchado a una liana si o no
portaluz	equ 48945	; Llevas la luz contigo
activ_eyes	equ 48946
latigo		equ 48947
gemas		equ 48948	; gemas a recoger en cada nivel para pasarlo = 4
skipf_co	equ 48949  ; skipf del cofre para que se vea la animaci�n.
nivel		equ 48950	; Nivel terminado.
obstaculo	equ 48951	;
desconener	equ 48952	; Descontar energia o no del prota.
parpapro	equ 48953	; variable para saber si el prota parpadea despu�s del choque con enemigos. Inmunidad
enemobst	equ 48954
tipobst		equ 48955
vchoqEnem	equ 48956
ghostenem	equ 48958	; variable para ayudarnos a hacer desaparecer los enemigos anim�ndonos.
coluenem	equ 48959
filaenem	equ 48960
paraframeen	equ 48961
sinluz		equ 48962
barra		equ 48963	; 2 bytes barra energia. Char en la barra
offbarra	equ 48965	; offset de la barra de energia para saber donde est�s.
vchoqEnemesp	equ 48966
mosttiempo	equ 48967	; tiempo para quitar el cartel de special level
moretiempo	equ 48968	; "
moltotempo	equ 48969	; "
level		equ 48970	; N.nivel que usamos en contrase�as y nivelesyenergia.asm
; Hasta 49151 181bytes libres
sec		equ 48971	; Contrareloj �ltimos niveles
min		equ 48972	; "
valtemp		equ 48973	; "
