;-------------------------------------------------------------------------------------------------------
;-------------------------------------------------------------------------------------------------------
; Rutina de cambio y creaci�n de datos de enemigos en tabEnemigos.asm
;-------------------------------------------------------------------------------------------------------



; Especifica de Tulum puesto que se crean los datos apartir de los valores del prota. 
; Y los enemigos son siempre los mismos

; ENTRADA : VALORES COLU EN B Y FILA EN C desde luzpantallaGest.asm

creadatenem:
; Valores para Maya
	ld	ix,ENEMIGOS
	call	bcreadatenem
; calculo los valores para ix21 a ix24 de los enemigos en la tabla ENEMIGOS
	call	avolcamiento_enem

; Valores para Olmeca
	ld	ix,ENEMIGOS+churrodbsenemigos	; etiqueta con el valor del churro
	call	bcreadatenem
; calculo los valores para ix21 a ix24 de los enemigos en la tabla ENEMIGOS
	jp	avolcamiento_enem
bcreadatenem:
	ld	[ix+4],255	; ticks para la animaci�n
	ld	[ix+6],0	; Inicializo el valor de frame
	ld	[ix+2],b
	ld	[ix+3],c
	ret