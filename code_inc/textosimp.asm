;----------------------------------------------------------------------------------------
;   Rutina de impresion de textos de Intro, final falso y The End
;----------------------------------------------------------------------------------------
Intro_Tulum:
	call	parar_musica		; gestmelodias.asm
	xor	a		; Fx de GOLPE METALICO PARA TECLA PULSADA EN MENU
	ld	[paraponerenB+1],a
	call	llamar_efecto
	call	delay
; Borrar el men� principal con el efecto de Paco
	xor	a
	ld	b,12
	ld	hl,$5960
	ld	de,$5980
loophlp:
	push	bc
	ld	b,32
	push	hl
	push	de
loophl:
	ld	[hl],a
	ld	[de],a
	inc	hl
	inc	de
	djnz	loophl
;
	push	hl
	push	bc
	ld	b,7
	call	pausanivelfinish	; call delay
	pop	bc
	pop	hl
;
	pop	de
	pop	hl
	ld	c,32
	ex	de,hl
	add	hl,bc
	ex	de,hl
	sbc	hl,bc
	pop	bc
	djnz	loophlp
	call	ruborco
;----------------------------------------------------------------------------------------
; Imprimo Intro
	call	imprim_intros	; en impmenuymarcadores.asm

Imprime_TextoIntro:
	ld	de,texto_intro
        ld	c,0
	ld	b,128	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,6	; Fila  10
        rst	16
	xor	a	; columna 0
        rst	16
lineaIntro:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineaIntro
;----------------------------------------------------------------------------------------
; colorear fondo texto
;
colorearintro:
	ld	hl,$58c0	;$5940
	ld	de,$58c1	;$5941
	ld	b,65	; caracteres a imprimir
bcolerarintro:
	ld	[hl],5
	ldi
	ld	[hl],71
	ldi
	djnz	bcolerarintro
;
; Poner m�sica INTRO
	ld	a,7		; Musica INTRO
	ld	[poncancionenA+1],a
	jp	llamar_musica	; en rucontroles.asm


;----------------------------------------------------------------------------------------
; Impresi�n de textos Coatlicue
;----------------------------------------------------------------------------------------
Imprime_TextoCoatlicue:
	ld	de,texto_coatlicue
        ld	c,0
	ld	b,64	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,20	; Fila  22
        rst	16
	xor	a	; columna 0
        rst	16
lineaCoatlicue:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineaCoatlicue
;----------------------------------------------------------------------------------------
; colorear fondo texto
;
colorearcoatlicue:
	ld	hl,$5a80
	ld	de,$5a81
	ld	bc,63	; caracteres a imprimir
bcolerarcoatlicue:
	ld	[hl],70
	ldir
	ret

;----------------------------------------------------------------------------------------
; Impresi�n de textos final de juego
;----------------------------------------------------------------------------------------
Imprime_Textofinaljuego:
	ld	de,texto_finaljuego
        ld	c,0
	ld	b,6
an_lineafinaljuego:
	push	bc
	ld	b,12	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,[f_final]	; Fila  22
        rst	16
	ld	a,20	; columna 20
        rst	16
lineafinaljuego:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineafinaljuego
	ld	a,[f_final]
	inc	a
	ld	[f_final],a
	pop	bc
	djnz	an_lineafinaljuego
;----------------------------------------------------------------------------------------
; colorear fondo texto
;
colorearfinaljuego:
	ld	hl,$5814
	ld	de,$5815
	ld	b,6
an_bcolerarfinaljuego:
	push	bc
	push	de
	push	hl
	ld	bc,11	; caracteres a imprimir
;bcolerarfinaljuego:
	ld	[hl],70
	ldir
	pop	hl
	pop	de
	ld	c,32
	ex	de,hl
	add	hl,bc
	ex	de,hl
	add	hl,bc
	pop	bc
	djnz	an_bcolerarfinaljuego
;----------------------------------------------------------------------------------------
; Texto leyenda The End
;----------------------------------------------------------------------------------------
	ld	de,texto_theend
        ld	c,0
	ld	b,11	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,20	;
        rst	16
	ld	a,21	; columna 22
        rst	16
lineatheend:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineatheend
;----------------------------------------------------------------------------------------
; Colorear
coloreartheend :
	ld	hl,$5a95
	ld	de,$5a96
	ld	b,6	; caracteres a imprimir
bcoloreartheend:
	ld	[hl],4
	ldi
	ld	[hl],6
	ldi
	djnz	bcoloreartheend
	ld	[hl],4
;----------------------------------------------------------------------------------------
; Muevo 3 bits a la izquierda el texto del The End
;----------------------------------------------------------------------------------------
muevebytes:
	push	hl
	push	bc
	ld	hl,$509F
	ld	b,3
loopmb:
	push	hl
	push	bc
	ld	b,08
loopbmb:	
	push	bc
	push	hl
	xor	a
	ld	b,10
	sla	[hl]
	dec	hl
contmb	
	rl	[hl]
	dec	hl
	djnz	contmb

	pop	hl
	pop	bc
	inc	h
	djnz	loopbmb
	pop	bc
	pop	hl
	djnz	loopmb
	pop	bc
	pop	hl
	ret
f_final	defb	0

texto_intro:
	defb "    You are trapped in TULUM    "
	defb "   Refuge Temple of Coatlicue   "
	defb "Plaged with danger and treasures"
	defb "Escape with them or perish there"

texto_coatlicue:
	defb "My girl Coyolxauhqui, chases you"
	defb "Run away as fast as possible. !!"

texto_finaljuego:
	defb "You managed "
	defb " to escape. "
	defb "Coyolxauhqui"
	defb " curses you "
	defb "'We'll meet "
	defb "  again...' "

texto_theend:
	defb "- The End -"