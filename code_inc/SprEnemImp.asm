
;----------------------------------------------------------------------------------------
; Calculo de la posic.en pant. y en el buffer del enemigo o la plataforma 
;----------------------------------------------------------------------------------------
avolcamiento_enem:
        call    im_pix_e      ; hl=dir_spr POSIC.ARCH.PANT         SprProtaImp.asm
	jp	c_dirbuff_e
;----------------------------------------------------------------------------------------
; Calculo de la direcci�n del prota en el archivo de pantalla
;----------------------------------------------------------------------------------------
; ix+2 colu en chars, ix+3 fila en pixels SALIDAS: HL y dir_spr_e
im_pix_e:							; 45869
        ld      l,[ix+3]	;fila en scannes o pixels
        ld      a,[ix+2]	;colu en chars (para usar pixels pasar previam a char)
        call    tabla4metal
	ld	[ix+22],h
	ld	[ix+21],l
	ret
;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLU en chars   SALIDA:Pos. del sprite en el buffer interm. en HL 
;----------------------------------------------------------------------------------------
c_dirbuff_e:							; 45883
	xor	a		; acarreo a 0
	ld	e,a		; 1/4
        ld	a,[ix+3]	; 4/13
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	ld	d,a		; 1/4
	ld	a,[ix+2]	; 4/13 
	add	a,e
	ld	e,a
	ld	hl,buffpant-$400	;$ad00  3/10 buffer tama�o marcador de arriba 1024= $400
	add	hl,de           ; 3/11
	ld	[ix+24],h
	ld	[ix+23],l
	ret			; 3/10. Total = 108 t-states.

Antes_imp_ene:			; Antiguo cambiaranchoenem:	
;
; Modifico el valor del registro B en SprEnemImp.asm para que sea el ancho del sprite actual.
;

; Cu�ndo el valor es fijo como es este caso, este cacho no tiene sentido.
; Lo mantengo para no modificar los db's de los enemigos ni la rutina.
	exx
	ld	hl,chganchospen+1
	ld	a,[ix+9]	; para tulum los enemigos siempre 2 de ancho. Ojo la rutina de la luz en pantalla lo usa.
	ld	[hl],a		; O incluso COMENTAR TODAS ESTAS LINEAS ? VER
	exx

;----------------------------------------------------------------------------------------
; IMPRESION DE SPRITE.
;----------------------------------------------------------------------------------------
imp_ene:							; 55725
	push	hl		; Guardo entre otros registros C
	push	de		; necesario en la vuelta de rutinas de salto etc.etc.
	push	bc
;        ld      de,[dir_spr_e]	; direccion del sprite en la videoram
	ld	d,[ix+22]
	ld	e,[ix+21]
;	ld	hl,[spr_e]	; direccion de sprite a imprimir
	ld	h,[ix+12]
	ld	l,[ix+11]
evaluamov_e:
;	ld	a,[altosc_e]	; dato que vendr� de los db's de la tabla
	ld	c,[ix+8]
;	ld      c,a             ; paso el valor a c
loopenem:
        push    de
;	ld	b,[ix+9]	; ancho
chganchospen:			; Etiqueta para indicar la direccion de hl para cambiar 
	ld	b,2		; el valor del registro B ( 1,2 ) Se usa en otras rutinas.
buloopenem:			; 
        ld      a,[de]          ; Coge lo de la pantalla
        and     [hl]            ; Borro lo que no sean 1. Enmascaramiento
        inc     hl
        or      [hl]            ; Le sumo el sprite
        ld      [de],a		; guarda en pantalla
        inc     hl
        inc     e		; aumenta posici�n pantalla 1 a la derecha
        djnz    buloopenem
        pop     de		; posicion de pantalla original
        inc     d		; aqu� viene la rutina de bajar scan de toda la vida
        ld      a,d
        and     7
        jr      nz,sal2ene
        ld      a,e
        add     a,32
        ld      e,a
        jr      c,sal2ene
        ld      a,d
        sub     8
        ld      d,a
sal2ene:
        dec     c
        jr      nz,loopenem
	pop	bc		; Saco C y el resto de registros
	pop	de
	pop	hl
        ret



;----------------------------------------------------------------------------------------
; Rutina de c�lculo direc.Atrib    Entrada dir_spr con valor de la posic.en la  Videoram
;----------------------------------------------------------------------------------------
c_dirAtr_enem:			
;				 copia de calc_attr_prota:	
;	ld	hl,[dir_spr]	; 5/16 Cargo HL con el valor de la direccion VideoRam
;
; el valor de hl hay que traerlo aqu� antes de llamar a la rutina
;
	srl	h		; 2/8
	srl	h		; 2/8
	srl	h		; 2/8 Desplazo 3 veces a la derecha el valor del bit Alto
	ld	a,$50		; 2/7 Valor fijo
	or	h		; 1/4 Sumo h desplazada con A que vale 0101 = $50
	ld	h,a		; 1/4 hl ya contiene el valor de la direcc. de atributos
	ret

;----------------------------------------------------------------------------------------
; Calculo de la dir.enm. en el buffer de attributos.   fila pixels columna en char
;----------------------------------------------------------------------------------------

c_dirbuffAtr_enem:							
	ld	hl,buffattpant	; $bd00	antes $fd90 buffer de atributos.
calc_dir_buff_ilum_delenem
	ld	a,[ix+3]	; 5/19 fila
	and	248		; 2/7 anulando valores intermedios para el calculo
	sub	32		; tama�o del marcador superior
	jr	z,sumacoluatt_enem
	ld	c,a		; 1/4
	xor	a		; 1/4
	sla	c		; 2/8 desplazo la fila 2 veces a
	rla			; 1/4 izquierda rotando b
	sla	c		; 2/8 para que el resultado de dividir /8
	rla			; 1/4 y luego multiplicar x32 sea lo mismo.
	ld	b,a		; 1/4 fila en bc
	add	hl,bc
sumacoluatt_enem:
	ld	c,[ix+2]
	ld	b,0		; 2/7 
	add	hl,bc		; 3/11 hl = direcc.en Buffer Atrib. 103t-states
	ret


