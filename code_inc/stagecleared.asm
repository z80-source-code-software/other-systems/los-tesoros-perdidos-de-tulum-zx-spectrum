;-----------------------------------------------------------------------------------
; Efecto de NIVEL TERMINADO
;-----------------------------------------------------------------------------------
finaldenivel:
	call	imp_solo_fondo	; Borro al prota
	
	call	delay
	ld	a,59			; Valor numtile de la puerta entre abierta.
	call	chgtileenpantadelcofre	; En compobjetos.asm
	call	delay
	ld	a,60
	call	chgtileenpantadelcofre	; En compobjetos.asm
	call	delay
	ld	a,10
	call	chgtileenpantadelcofre	; En compobjetos.asm



; Aqui debo poner el jingle  de nivelterminado:
	ld	a,2			; Fx de PORTAZO
	ld	[paraponerenB+1],a
	call	llamar_efecto
; poner aqu� animaciones de pasar por la puerta
	ld	a,8
	ld	[pausacolor],a
	call	fadeout_color		; Fade Out de colores

	ret

delay:
	ld	b,200
pausanivelfinish:
; repetici�n de todo
	push	bc
	ld	a,64
	ld	[delaypausa],a
pausaniveltime:
; bucle 1 de pausa
	ld	b,255
pausafadetime:
	djnz	pausafadetime
; bucle 2 de pausa
	ld	hl,delaypausa
	rl	[hl]
	jr	nc,pausaniveltime
	pop	bc
	djnz	pausanivelfinish
	xor	a	; anulo el m�s que probable acarreo de las operaciones anteriores
	ret
