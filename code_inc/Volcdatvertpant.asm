;----------------------------------------------------------------------------------------
; Volcado de datos en la tabla local de vertices desde la tabla de vertices en pantalla
;----------------------------------------------------------------------------------------

volcadoverticesenpan:
	ld	hl,TabVerticesPant-154
	ld	bc,154
	ld	a,[panta]
	ld	d,a
loopvolcdatvert:
	add	hl,bc
	ld	a,[hl]
	cp	d
	jr	nz,loopvolcdatvert
	inc	hl
	ld	de,TablaposicYXvertices
	dec	bc
	ldir
	ret

