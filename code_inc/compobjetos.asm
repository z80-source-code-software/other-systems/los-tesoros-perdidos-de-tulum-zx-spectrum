;----------------------------------------------------------------------------------------
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	ix4		ix5
;	    Act.en Mar,
;		ix6

tabla_objetos_enpantalla:					; 34894
	db	0,0,0,0,0,0,0;,0,0	; Diamante 1
	db	0,0,0,0,0,0,0;,0,0	; Diamante 2
	db	0,0,0,0,0,0,0;,0,0	; Diamante 3
	db	0,0,0,0,0,0,0;,0,0	; Diamante 4
	db	0,0,0,0,0,0,0;,0,0	; Cofre, valores para ix+6= 19 gema. si no 0=luna,1=sol,2=corazon y 3=llave
	db	0,0,0,0,0,0,0;,0,0	; Puerta 
	db	128
; Act.en pant es un objeto que est� activo en la pantalla, no ha sido recogido.
; n.tile = 0 es el de vaciar para borrar objetos de 2x2
; Act.en Mar es un objeto que se encuentra en el marcador y no est� ya en pantalla





;----------------------------------------------------------------------------------------
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	   ix4		ix5

; Tipo de Objeto = 0,1,2,3
;	si n.tiles= 19	diamantes entonces cada una de las piedras preciosas
;	si n.tiles= 17 � 18 cofre cerrado o abierto entonces:  0 luna, 1 sol, 2 corazon, 3 llave 
; Todos los objetos pueden estar ocultos en  el cofre.
;	Si el valor de ix+5 del churro del cofre es 19 quiere decir que has  escondido un 
;       un diamante en �l. 






comp_objetos:						; 34991
	push	hl
	push	ix
	call	Ccomp_objetos
	call	Ccomp_objetos_ilum	; Comprobar si hay diamantes parcialmente iluminados.
	pop	ix
	pop	hl
	ret

Ccomp_objetos:						; 35001
	ld	ix,tabla_objetos_enpantalla
bcomp_objetos:
	ld	a,[ix+0]	; Valor de la pantalla 
	rla	
	ret	c		; si el valor es 128 termina
	ld	a,[ix+1]	; La puerta de paso de nivel estar� a 0 hasta que la abras.
	and	a
	jr	z,Otroobjeto	; objeto desactivado
	ld	a,[ix+2]
	cp	19
	jp	c,El_cofre
;-----------------------------------------------------------------------------------
; Diamantes. Entrada fila y colu. Salida calculo limites del prota dejandolos en DE Y HL 
;
        ld	a,[fila]	
	inc	a
        ld	d,a		; d= fila +1px
        add	a,13
	ld	e,a		; e= fila +13px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
	inc	a
        ld	l,a	        ; l= colu +1    (ANCHO)
	ld	a,[ix+3]	
	ld	b,a		; b= col_dat
	inc	a
	ld	c,a             ; c= col_dat+1
;
;comprobamos Columnas
;
        ld	a,c		
	cp	h
	jr	c,Otroobjeto
	cp	l		
	jr	z,verfilaobje	
	ld	a,b
	cp	l
	jr	z,verfilaobje
	jr	nc,Otroobjeto
verfilaobje:

	ld	a,[ix+4]	
	ld	b,a		; b= fil_dat
	add	a,16
        ld	c,a             ; c= fil_dat+alto Sp
;
;comprobamos Filas
;
	ld	a,c		
	cp	d
	jr	c,Otroobjeto
	ld	a,b
	cp	e
	jr	c,SonidoObjetoalcogerlo
Otroobjeto:
	ld	de,07
	add	ix,de
	jr	bcomp_objetos
SonidoObjetoalcogerlo:
	ld	a,3
	ld	[paraponerenB+1],a
	call	llamar_efecto		; llamar_efecto en 27590

;
; Borrado en buffers, pantalla. Activaci�n en marcador, etc.
;
	ld	[ix+1],0	; Desactivar el objeto en la tabla

; Si es la puerta cerrada no la borres.
	ld	a,[ix+2]
	cp	24
	jr	z,mirarobjeIto
;
; Borrar objeto en buffer y calculo e poti_im
;
	xor	a			; n.tile vacio = 0 para borrar en pantalla y poner en buffer
	call	chgtileenpantadelcofre

mirarobjeIto:
;
; Nos preguntamos que objeto es el que est�s recogiendo y actuar.
;
	ld	a,[ix+2]		; 34448
	cp	24			; puerta cerrada 24 puertaabierta
	jr	z,pasardenivel
	cp	19			; 19 = gemas
	jp	z,incrementargemas
	cp	21
	jr	z,iluminarescena	; 21 = sol
	jr	c,apagarescena		; 20 = luna
	cp	23
	jp	c,usarllave		; 22 = llave
;
; Aumentar corazon
;
	; Aumentar la energia y devolver el control
	call	Aumentador_Energia	; En barraenergia.asm
	jp	Otroobjeto
;
; Iluminamos la escena total de la pantalla
;
iluminarescena:
; los tiles de la pantalla	
	ld	hl,buffattpant	; $bd00	 antes $fd90
	ld	de,$5880	; Buffer de atrributos a videroam
	ld	bc,511
	ldir
; Pinto los tiles del buffer de iluminaci�n. Llamando a la rutina en SprProtaEyes.asm
	call	llenadoBufferIluminacion
	jp	Otroobjeto
;
; Apagamos la escena total de la pantalla
;
apagarescena:
; Actuamos como si la hubieramos arrojado			35145
	ld	a,[accion]		
	set	0,a			; Activo el bit de disparo: luz, l�tigo.
        ld      [accion],a
	xor	a
	ld	[portaluz],a
	ld	hl,noiluminar		;  Desactivo en el engine la iluminaci�n alrededor del prota
	ld	[verIluminacion+1],hl	
	inc	a
	ld	[latigo],a
; La rutina que comprueba si puedes volver a coger una luz se encuentra en luzpantallagest y comprueba que ix+1 sea 0 para poder cogerla
; tengo que cambiar su valor. Ya que puede llegar el caso de que cojas una luz despu�s de abrir el cofre y ver que est� la luna en su
; interior lo que provocar�a que no pudieras cogerla nunca.
	push	ix
	ld	ix,DISPARO_LUZ		;luzgestytab.asm
	ld	[ix+1],0
	pop	ix
	call	apagavelamarcador	; GestionLuzmarcador.asm
	ld	hl,nomirarllama		; Desactivo la llamada en el engine
	ld	[vermirarllama+1],hl	; para no imprimir la llama del marcador
; Activo el salto a mirar que puedas tener que imprimir los ojos del prota 
	ld	hl,miraroscuridad
	ld	[mirarenlaoscuridad+1],hl
; Pinto el attr del fondo del prota para que no se quede el halo de luz al desactivar portaluz
	call	pincolordelfondo1	; En SprProtaVolcAttr.asm
	call	pincolordelfondo2
; Apago los tiles de la pantalla	
	ld	hl,$5880	; Linea 0 de la videoRAM de la pantalla del juego
	ld	de,$5881	; Videoram a negro
	ld	bc,511
	ld	[hl],0
	ldir
; Ilumino la luz del nivel y los tres tiles de encima
	call	Imp_attr_de_la_luz_de_pantalla	; LuzPantallaGest.asm
; Lleno el buffer de iluminaci�n con 0 tambi�n.
	call	llenadoBufferIluminacion	; sprprotaeyes.asm
	call	Coloca_valor_buffer_iluminado
	jp	Otroobjeto

pasardenivel:							; 35225
	ld	a,[gemas]
	cp	4
	jp	c,Otroobjeto
;
; Miramos si el salto est� activo. Si es as� ir a Otroobjeto
;
	ld	a,[accion]
	rra
	rra
	call	c,ajustesalto		; en movimientos.asm para anular el salto si lo hay.
	xor	a
	ld	[latigo],a
	ld	[activ_eyes],a
	inc	a
	ld	[flagpa],a
	ld	[portaluz],a
	ld	hl,iluminar		; Activo en el engine la iluminaci�n alrededor del prota
	ld	[verIluminacion+1],hl	
; Anulamos la posible luz que est� en movimiento porque la hayas disparado
	push	ix
	ld	ix,DISPARO_LUZ		; luzgestytab.asm
	ld	[ix+1],0		; luz no enviada
	pop	ix
; En caso de que pases el nivel con los ojos activos, debes devolver el cuerpo al prota
	call	restaurarprota		; sprprotaeyes.asm
; Desctivo el salto a mirar que puedas tener que imprimir los ojos del prota 
	ld	hl,nomiraroscuridad
	ld	[mirarenlaoscuridad+1],hl
; Activo la llamada en el engine para imprimir la llama del marcador
	ld	hl,mirarllama		
	ld	[vermirarllama+1],hl	
	call	valoresVela

	call	finaldenivel		; stagecleared.asm

	ld	a,[panta]		; comprobamos la variable panta para saber en que punto andamos
	cp	19
	jp	z,FINALFALSO		; SI 128 hurry up !
	cp	24
	ret	nz
	ld	a,1
	ld	[final],a
	jp	FINALJUEGO






;
; Incremento la variable de gemas que has recogido
;
incrementargemas:
	ld	a,2			; Aumenta Energ�a del prota 35291
	ld	[Paumentar_Energia+1],a	
	call	Aumentador_Energia	; Barraenergia.asm
	ld	a,4			; Restauro valor inicial
	ld	[Paumentar_Energia+1],a	
	
	ld	[ix+6],1		; Activa el objeto en el marcador.
	ld	a,[gemas]
	inc	a
	ld	[gemas],a
	cp	4
	call	z,abrirpuerta
;
; Tipo de diamante a colorear en el marcador.
;
	ld	a,[ix+5]
	and	a
	jp	z,Ponerdiamante0enmarcador
	cp	2
	jp	c,Ponerdiamante1enmarcador
	jp	z,Ponerdiamante2enmarcador
	jp	Ponerdiamante3enmarcador
;
; Hemos cogido los 4 diamantes
;
abrirpuerta:
; M�sica / Efecto de abrir puerta.
	ld	a,8		; PONGO LA DEL INTRO. ANTES 14 PARA ABRIR PUERTA QUE ES LA DE DA�O
	ld	[paraponerenB+1],a
	call	llamar_efecto

	
	push	ix
	ld	ix,tabla_objetos_enpantalla+35	; el valor del inicio del churro de la puerta
	ld	[ix+1],1		; Activo el mirar si la puerta se ha abierto
	ld	a,24			; Valor numtile de la puerta abierta.
	call	chgtileenpantadelcofre
	pop	ix
	ret
;
; Hemos cogido la llave			*** PENDIENTE DE RESOLVER LAS LLAMADAS DEBIDO AL INCREMENTO DE GEMAS
;
usarllave:
; M�sica / Efecto de aparecer algo dentro del cofre.	35364
	xor	a
	ld	[paraponerenB+1],a
	call	llamar_efecto

	push	ix
	ld	ix,tabla_objetos_enpantalla+35	; el valor del inicio del churro de la puerta
	ld	a,[gemas]
	cp	4
	jr	z,llamaacerrarpuerta
; Abrimos la puerta del nivel
	ld	[ix+1],1
; Pongo el tile de la puerta abierta
	ld	a,4		; tengo que quitar las gemas de la pantalla
	ld	[gemas],a
	ld	a,24
	ld	[ix+2],a	; cambio en la tabla tambi�n
	jr	Usadopapuertaabiertaycerrada
; Cerramos la puerta del nivel
llamaacerrarpuerta:
; M�sica / Efecto
	ld	a,2
	ld	[paraponerenB+1],a
	call	llamar_efecto

	ld	[ix+1],0		; Desctivo el mirar si la puerta se ha abierto
; Borro las gemas recogidas del marcador
	call	Borradiamantesenmarcador	; Salida con el RET en GestionObjetos.asm
; Pongo el tile de la puerta cerrada y tengo que volver a colocar los diamantes en la pantalla.
	call	VolcarGemas	
	ld	a,24
	ld	[ix+2],a	; NO LO PONGO en la tabla tambi�n porque no funcinar�a la rutina ppal despu�s para poder comprobar que objeto es debido a la comprobacion del n� tile 19 en el inicio de la rutina.

	ld	a,10
Usadopapuertaabiertaycerrada:
	call	chgtileenpantadelcofre
	pop	ix
	jp	Otroobjeto





















;-----------------------------------------------------------------------------------
; Cofres		Utilizar� ix+6 para saber el estado
; Cerrado y Abierto.
; Entrada fila y colu  Salida calculo los limites del prota dejandolos en DE Y HL 
;
; 1. El cofre comprueba si le has golpeado para cambiarse por el tile de su interior
;	Activado bit 7 de ix+6
; 2. El cofre comprueba si se encuentra abierto para imprimir el 2.frame 
;	Activado bit 0 de ix+6
; 3. sino comprueba si est�s a su lado golpeando.
El_cofre:
	ld	a,[ix+6]		; Utilizo bit7 y bit0.
	rla
	jp	c,objetoenelinterior	; Se ha activado el objeto de su interior ?
	rra
	rra
	jr	c,cam_frame_delcofre	;imprimecofreabierto	; Has golpeado por primera vez el cofre ?
compruebasigolpeaselcofre:
;
; Ralentizar movimiento de apertura del cofre.
;					35444
	ld	a,[skipf_co]
	cp	10
	jp	c,aumento_skipf_co
	xor	a
	ld	[skipf_co],a
;
; El cofre se comprueba de distinta forma.Obst�culo que romper. 
;
        ld	a,[fila]	
        ld	d,a		; d= fila
        add	a,16
	ld	e,a		; e= fila +13px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
	inc	a
        ld	l,a	        ; l= colu +1    (ANCHO)
;
; comprobamos filas
;
        ld	a,[ix+4]	
	ld	b,a		; b= fil_dat
	add	a,16
        ld	c,a             ; c= fil_dat+alto Sp
	cp	d
	jp	c,Otroobjeto	; Mirar siguiente del churro
	ld	a,b
	cp	e
	jp	nc,Otroobjeto	; Mirar siguiente del churro
;
;comprobamos Columna 2 del cofre y prota debe estar mirando a izquierdas
;
	ld	a,[ix+3]
	dec	a
	ld	b,a		; b= col_dat -1
	add	a,3
	ld	c,a             ; c= col_dat+1
	cp	h
	jr	nz,Otroladodelcofre
miraprotamirandoaizquierdas:		; 34513
; colu2 del cofre y fila coinciden con el prota.
	ld      a,[dirmovsp]
	cp	3		; Si prota no mira a la izquierda, retornamos
	jp	nz,Otroobjeto	; Mirar siguiente del churro
	ld	a,[flagespa]	; Flag de latigazos 0 off 1 y 2 on
	and	a		; Si prota no usa el l�tigo retornamos
	jp	z,Otroobjeto	; Mirar siguiente del churro
actiix6dereizq:
; Activamos ix+6 seg�n su procedencia.
	ld	a,[ix+6]
	cp	64	
	jr	z,cam_ti_int_cofre
	rra
	jr	c,cam_frame_delcofre
; Activo el que se imprima el cofre abierto
imprimecofreabierto:
	set	0,[ix+6]	
	jp	Otroobjeto	; Mirar siguiente del churro
cam_ti_int_cofre:
	set	7,[ix+6]
	jp	Otroobjeto

Otroladodelcofre:			; 34567
;
;comprobamos Columna 1 del cofre y prota debe estar mirando a derechas
;
	ld	a,b	; b= ix+3 => col_dat -1 
	cp	l       ; l= colu +1 
	jp	nz,Otroobjeto	; Mirar siguiente del churro

miraprotamirandoaderechas:
; colu1 del cofre y fila coinciden con el prota.
	ld      a,[dirmovsp]
	cp	1		; Si prota no mira a la izquierda, retornamos
	jp	nz,Otroobjeto	; Mirar siguiente del churro
	ld	a,[flagespa]	; Flag de latigazos 0 off 1 y 2 on
	and	a		; Si prota no usa el l�tigo retornamos
	jp	z,Otroobjeto	; Mirar siguiente del churro
	jr	actiix6dereizq
aumento_skipf_co:
	inc	a
	ld	[skipf_co],a
	jp	Otroobjeto
;
; Cambio del frame del cofre o del objeto de su interior
;
cam_frame_delcofre:
; Cambio el tile del cofre al abierto y lo imprimo. 
	ld	a,18		; Frame 1 del cofre. Osea cofre abierto
	ld	[ix+2],a	; cambio en la tabla tambi�n
	call	chgtileenpantadelcofre
; Activo que ya est� cambiado el tile a abierto.
	ld	[ix+6],64
	jp	Otroobjeto	; Mirar siguiente del churro
;
objetoenelinterior:
; M�sica / Efecto de aparecer algo dentro del cofre.
	ld	a,15
	ld	[paraponerenB+1],a
	call	llamar_efecto


; imprimo el objeto nuevo por el cofre y lo activo
	ld	a,[ix+5]
	cp	19
	jr	z,gemaencofre
	cp	2
	jr	z,esuncorazon
	jr	nc,eslallave	; = 3
	rra
	jr	c,eselsol	; = 1
; es la luna			; = 0
; M�sica / Efecto de aparecer algo dentro del cofre.
	ld	a,11
	ld	[paraponerenB+1],a
	call	llamar_efecto
	ld	a,20
	ld	[ix+2],a	; necesito que tambi�n est� en A
	jr	todoslosobjetosdentrodelcofre
eselsol:
	ld	a,12
	ld	[paraponerenB+1],a
	call	llamar_efecto
	ld	a,21
	ld	[ix+2],a	; necesito que tambi�n est� en A
	jr	todoslosobjetosdentrodelcofre
eslallave:
	ld	a,7
	ld	[paraponerenB+1],a
	call	llamar_efecto
	ld	a,22
	ld	[ix+2],a	; necesito que tambi�n est� en A
	jr	todoslosobjetosdentrodelcofre
esuncorazon:
	ld	a,5		; coger obejto de dentro
	ld	[paraponerenB+1],a
	call	llamar_efecto
	ld	a,23
	ld	[ix+2],a	; necesito que tambi�n est� en A
	jr	todoslosobjetosdentrodelcofre
gemaencofre:
	ld	[ix+5],3	; pongo 3 para la gema a subir al marcador
	ld	[ix+2],a	; le digo que es una gema y no es un cofre.
todoslosobjetosdentrodelcofre:
	call	chgtileenpantadelcofre
	jp	Otroobjeto	; Mirar siguiente del churro











;----------------------------------------------------------------------------------------
; Rutina para colocar los diamantes en pantalla en caso de que uses MAL la llave.
;----------------------------------------------------------------------------------------

VolcarGemas:
; La tabla de Objetos donde est�n los diamantes, los restauro en ella y en pantalla.

	push	ix
	ld	ix,tabla_objetos_enpantalla
lazocolocogemas:
	ld	a,[ix+0]
	rla				; 128 Fin tabla
	jr	c,fincolocargemas
	ld	a,[ix+2]		; num.tile
	cp	19
	jr	nz,otragemaacolocar
; A ya tiene el valor 19 de la gema
	call	chgtileenpantadelcofre
	ld	[ix+1],1		; vuelves a activar el objeto para recogerlo
	ld	[ix+6],0		; desactivo gema en el marcador
otragemaacolocar:
	ld	de,07
	add	ix,de
	jr	lazocolocogemas
fincolocargemas:
	xor	a			; Valor de gemas recogido  = 0
	ld	[gemas],a	
	pop	ix
	ret
















;-----------------------------------------------------------------------------------
;
; Cambio de los tiles en pantalla. Tile de Borrado o Tiles de cambio del cofre ...
;
chgtileenpantadelcofre:
	ld	[num_tile],a		; Entrada en A el numero de tile a cambiar
	ld	a,[ix+4]
	ld	[fil_dat],a
	ld	a,[ix+3]
	ld	[col_dat],a
	call	search_dat		; En bupaim.asm. Salida tile a imprimir en 'de' poti_im
	call	c_dirbufftile		; colocaci�n en buffer de los objetos
;
; Borrar/Cambiar objeto en pantalla	
;
	ld	a,[ix+4]
	rra
	rra
	rra	
	and	31
	ld	[fil_dat],a		; fila_dat en char
	ld	b,a
	ld	a,[ix+3]
	ld	[col_dat],a		; columna
	ld	c,a
	ld	de,[poti_im]
	call	im_char
;
; restauro valores de la vela del marcador
; 
	jp	valoresVela
;-----------------------------------------------------------------------------------