;--------------------------------------------------------------------------
; Rutina de Controles
CONTROLES:
	ld	a,247
	in	a,[#fe]
        rra
        jr	nc,teclas
        rra
	jr      c,CONTROLES

mando_de_juegos:
;JOYSTICK:
        ld	hl,JOYSTICK
	ld	[control],hl
;	jr	SON_MENU
	jp	Espera_tecla

teclas:
	ld	hl,KEYBOARD
	ld	[control],hl

	jp	Espera_tecla
