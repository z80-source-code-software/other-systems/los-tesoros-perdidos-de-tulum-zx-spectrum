
; Descomprimo el menu directamente en pantalla
descomprimenu:
	nop
	nop

	ld	hl,menutulumpd2		; En datostilesypant.asm 
	ld	de,44288		; Uso el Buffer de pantalla para descomprimir
	ld	bc,412			;  bytes del titulo comprimido.
	call	dzx7			; descompresor
	ld      de,44288		; direccion donde se ha descomprimido
	ld      hl,txttitulopd
	call    llamimprim

	ld	hl,menutulumpar2	; En datostilesypant.asm 
	ld	de,44288		; Uso el Buffer de pantalla para descomprimir
	ld	bc,1208			;  bytes del titulo comprimido.
	call	dzx7			; descompresor
	ld      de,44288		; direccion donde se ha descomprimido
	ld      hl,txttitulopar
	call    llamimprim
	
	
	ld	hl,menutulumpi		; En datostilesypant.asm 
	ld	de,44288		; Uso el Buffer de pantalla para descomprimir
	ld	bc,2253			;  bytes del titulo comprimido.
	call	dzx7			; descompresor
	ld      de,44288		; direccion donde se ha descomprimido
	ld      hl,txttitulopi
	call    llamimprim
	


	ld	hl,menutulumpab		; En datostilesypant.asm 
	ld	de,44288		; Uso el Buffer de pantalla para descomprimir
	ld	bc,796			;  bytes del titulo comprimido.
	call	dzx7			; descompresor
	ld      de,44288		; direccion donde se ha descomprimido
	ld      hl,txttitulopab
	jp   llamimprim






menutulumpdp:

	DEFB	  3,248
	DEFB	  3,240
	DEFB	  7,224
	DEFB	 15,192
	DEFB	 31,128
	DEFB	123,  0
	DEFB	254,  0
	DEFB	248,  0

	DEFB	224,  0
	DEFB	128,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  0

txttitulopi	db	192,13,1,0
txttitulopar	db	80,16,14,0
txttitulopd	db	48,10,22,10
txttitulopdp	db	16, 2,30,16
txttitulopab	db	64,16,14,16



; Se descomprimen en el buffer de pantalla que empieza en 44288 #ad00 Que tiene 4608bytes
