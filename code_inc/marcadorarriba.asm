marcadorarriba:
	incbin "gfx_inc\marcadorarriba.zx7"	; 
; Descomprimo el marcador en un trozo del buffer de pantalla. Voy a ponerlo en 64000=FA00
descomprimemarcadorarriba:
	ld	hl,marcadorarriba	
	ld	de,64000	; $fa00	Direcci�n en el buffer de pantalla donde descomprimirse.
	ld	bc,907		; 905	marcadorarribaOLD	; bytes del titulo comprimido.
	jp	dzx7		; descompresor