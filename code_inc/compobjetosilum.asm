;----------------------------------------------------------------------------------------
; EN LA RUTINA COMPOBJETOS.ASM
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	ix4		ix5

; Solo vamos a comprobar si en alg�n momento hay una parte de un diamante iluminado.
Ccomp_objetos_ilum:
	nop
	nop
	push	ix
	push	hl
	push	de
	push	bc
	call	BCcomp_objetos_ilum
	pop	bc
	pop	de
	pop	hl
	pop	ix
	ret
BCcomp_objetos_ilum
	
	ld	ix,tabla_objetos_enpantalla
bcomp_objetos_ilum:
	ld	a,[ix+0]	; Valor de la pantalla 
	rla	
	ret	c		; si el valor es 128 termina
	ld	a,[ix+1]
	and	a
	jr	z,notileilum	; si el diamante no est� activo, no lo mires
	ld	a,[ix+2]
	cp	19
	jr	nz,notileilum
; Comp. s�l diamantes. Entrada fila y colu. B y L para llamar a coordficu 
;
	ld	a,[ix+3]	
	ld	l,a		; l= col_tile
	xor	a		; acarreo a 0
	ld	a,[ix+4]
	rra
	rra
	rra
	ld	b,a             ; b= fil_tile en char
	call	coordficu

	push	hl		; hl valor en la videoram ATTR del diamante
	pop	de		; guardo el valor en de
	ld	a,[hl]
	and	a
	jr	nz,tileilum
	inc	l
	ld	a,[hl]
	and	a
	jr	nz,tileilum
	ld	bc,30
	add	hl,bc
	ld	a,[hl]
	and	a
	jr	nz,tileilum
	and	a
	jr	z,notileilum
tileilum:
	ld	bc,32
	ex	de,hl		; devuelvo el valor a hl
	ld	[hl],7
	inc	l
	ld	[hl],5
	dec	l
	add	hl,bc
	ld	[hl],5
	inc	l
	ld	[hl],7
notileilum:
	ld	de,07
	add	ix,de
	jr	bcomp_objetos_ilum
	




apagadiamxluzlanz:

	push	ix
	push	hl
	push	de
	push	bc
	ld	ix,tabla_objetos_enpantalla	; 
bapagadiam:
	ld	a,[ix+0]
	cp	128
	jr	z,finrutinapagadiamxl
	bit	6,a
	jr	z,otroapagadiamxluz
	ld	a,[panta]			; coloco el valor que ten�a antes
	ld	[ix+0],a
; Es un diamante y lo ha iluminado la luz al lanzarla.
	ld	a,[ix+3]	
	ld	l,a		; l= col_tile
	xor	a		; acarreo a 0
	ld	a,[ix+4]
	rra
	rra
	rra
	ld	b,a             ; b= fil_tile en char
	call	coordficu	; hl = videoramAttr del diamante.
; pongo el attributo con fondo amarillo osea lo apago.
	ld	bc,32
	ld	[hl],0
	inc	l
	ld	[hl],0
	dec	l
	add	hl,bc
	ld	[hl],0
	inc	l
	ld	[hl],0
otroapagadiamxluz:
	ld	de,07
	add	ix,de
	jr	bapagadiam
finrutinapagadiamxl:
	pop	bc
	pop	de
	pop	hl
	pop	ix
	ret

