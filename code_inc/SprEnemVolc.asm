;----------------------------------------------------------------------------------------
; Rutina de volcado del fondo de pantalla que ocupa el sprite.
;----------------------------------------------------------------------------------------
vuelcafondosprenem:			; Se usa por varias rutinas (ej. luzpantallagest
	push	hl
	push	de
	push	bc
	call	c_vuelcafondosprenem
	pop	bc
	pop	de
	pop	hl
	ret
c_vuelcafondosprenem:
	ld	h,[ix+24]
	ld	l,[ix+23]
	ld	d,[ix+22]
	ld	e,[ix+21]
;
; Necesitamos cargar en C el tama�o sprite  ancho x (altosc o altochar (x8))  
; para ello rotamos el ancho y sabremos cuantas veces sumar el alto en escannes
;
	ld	c,[ix+8]	; altosc
	ld	b,[ix+9]	; ancho en char
acompanchoen:
	dec	b
	jr	z,companchoen
	ld	a,[ix+8]	; altosc del sprite
	add	a,c		; 
	ld	c,a		; guardo el dato en c para despu�s
	jr	acompanchoen
;
; Ahora necesito saber que ancho tiene para discriminar entre subrutinas.
; Los enemigos tienen 1 o 2 de ancho.
;
companchoen:
	ld	b,0		; B es aqu� 0
	ld	a,[ix+9]	; saco el valor del ancho
	cp	2
	jr	z,enem2ancho
	jr	c,enem1ancho
enem3ancho:
;
; sprite de 3 ancho
;
;lazoavolcen3:
	ldi
	ldi
	ldi
; m�s optimizacion metal
	ret	po	; Hay Paridad cuando bc=0
	ld	a,c
;	and	a
;	ret	z
	ld	c,29		;posicion imediata debajo del scann anterior
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	dec	e
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,enem3ancho	;lazoavolcen3  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,enem3ancho	;lazoavolcen3    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      enem3ancho	;lazoavolcen3       ;cambio de caracter

enem2ancho:
;
; sprite de 2 ancho
;
;lazoavolcen2:
	ldi
	ldi
; m�s optimizacion metal
	ret	po	; Hay Paridad cuando bc=0
	ld	a,c
;	and	a
;	ret	z
	ld	c,30		;posicion imediata debajo del scann anterior
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,enem2ancho	;lazoavolcen2  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,enem2ancho	;lazoavolcen2    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      enem2ancho	;lazoavolcen2       ;cambio de caracter
;
; sprite de 1 ancho
;
enem1ancho:
;lazoavolcen1:
	ldi
; m�s optimizacion metal
	ret	po	; Hay Paridad cuando bc=0
	ld	a,c
;	and	a
;	ret	z
	ld	c,31		;posicion imediata debajo del scann anterior
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	inc	d
        ld      a,d
        and     7
        jr      nz,enem1ancho	;lazoavolcen1  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,enem1ancho	;lazoavolcen1    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      enem1ancho	;lazoavolcen1       ;cambio de caracter

