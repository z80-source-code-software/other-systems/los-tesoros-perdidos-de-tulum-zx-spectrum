; 
vuelcafondosprprota_colu2:
	ld	hl,[dir_spr]
	inc	hl
	ld	[dir_spr],hl
	ld	hl,[d_bu_spr]
	inc	hl
	jr	bvuelcafondosprprota
vuelcafondosprprota_colu:
	ld	hl,[d_bu_spr]	; Direcci�n en el buffer general del fondo viene de c_dirbuff
bvuelcafondosprprota:
	ld	de,[dir_spr]	; viene de im_pix	
	ld	bc,16		; total ancho 1* alto2 *8
lazoavolc1:
	ldi
; m�s optimizacion metal
	ret	po		; Hay Paridad cuando bc=0
	ld	a,c
	ld	c,31		;posicion imediata debajo del scann anterior
; por supuesto para cualquier sprite de 1 de ancho
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
;	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,lazoavolc1  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,lazoavolc1    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      lazoavolc1       ;cambio de caracter

;----------------------------------------------------------------------------------------
; Rutina de volcado del fondo de pantalla que ocupa el sprite en el buffer donde mezclarlo
;----------------------------------------------------------------------------------------
volcarymezclar_fondo_spr_mov:
;volcar_fondo_spr:
	ld	de,buffer_spr	; Buffer donde mezclar el fondo y el prota
	ld	hl,[d_bu_spr]	; viene de c_dirbuff
	ld	b,16		; total alto scannes
loop_volcar_fondo_spr:
	push	bc
	ldi
        ldi
	ld	bc,30
	add	hl,bc
	pop	bc
	djnz	loop_volcar_fondo_spr
;	ret

;----------------------------------------------------------------------------------------
; Rutina de mezclar fondo y sprite en el buffer_spr
;----------------------------------------------------------------------------------------

;mez_fon_spr_mov:
	ld	hl,buff_des_spr	; Buffer que contiene al sprite desplazado
	ld	de,buffer_spr	; buffer donde mezclar el fondo previo con el sprite 
; Creo que si pusieramos b=32 y lo repitieramos sin hacer caso a c funcionaba igual y ahorramos estados.
	ld      c,16		; 2/7  altosc
loop_volcar_spr:
	ld	b,2		; 2/7   
buloop_volcar_spr:
        ld      a,[de]		; 2/7  Coge lo de la pantalla
        and     [hl]		; 2/7  Borro lo que no sean 1. Enmascaramiento
        inc     hl		; 1/6
        or      [hl]		; 2/7  Le sumo el sprite
        ld      [de],a		; 2/7  guarda en pantalla
        inc     hl		; 1/6
        inc     e		; 1/4  aumenta posici�n pantalla 1 a la derecha
        djnz    buloop_volcar_spr	; 3/13 , 2/8   57estadossino y 52estasosisi
	dec     c		; 1/4
        jr      nz,loop_volcar_spr	; 3/12 , 7/7
        ret			; 3/10
;----------------------------------------------------------------------------------------
; Rutina de volcado del fondo de pantalla en la videoram
;----------------------------------------------------------------------------------------

imp_solo_fondo:
	ld	hl,[d_bu_spr]	; Direcci�n en el buffer general del fondo viene de c_dirbuff
	ld	de,[dir_spr]	; viene de im_pix	
paracambiarBC:			; Desde movimientos.asm y engine 
	ld	bc,32		; PARA 1pixel *2chars = 2     PARA 4pixels*2chars = 8
lazosolofondo:
	ldi
	ldi

; m�s optimizacion metal
	ret	po		; Hay Paridad cuando bc=0
	ld	a,c
	ld	c,30		;posicion imediata debajo del scann anterior
; por supuesto para cualquier sprite de 2 de ancho
	add	hl,bc
	ld	c,a
	dec	de	;con esto queda resuelto el que la colu provoque un acarreo
	dec	e
	inc	d
        ld      a,d
        and     7
        jr      nz,lazosolofondo  ;baja una l�nea dentro del caracter
        ld      a,#20
        add     a,e
        ld      e,a
        jr      c,lazosolofondo    ;cambio de tercio
        ld      a,d
        sub     8
        ld      d,a
        jr      lazosolofondo       ;cambio de caracter

