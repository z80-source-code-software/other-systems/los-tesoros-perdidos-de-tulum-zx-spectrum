;----------------------------------------------------------------------------------------
; impresion de menu  y marcador
;----------------------------------------------------------------------------------------
        org     24300
;-----------------------------------------------------------------------
; Delay de espera para los letreros
;-----------------------------------------------------------------------
contador_sp_level:
	ld	hl,mosttiempo
loop_contador_sp_level:
	dec	[hl]
	jr	nz,loop_contador_sp_level
	ld	hl,moretiempo
	dec	[hl]
	jr	z,cont_loop_contador_sp_level
	ld	a,255
	ld	[mosttiempo],a
	jr	contador_sp_level
cont_loop_contador_sp_level:	
	ld	a,255
	ld	[mosttiempo],a
	ld	[moretiempo],a
	ld	a,[moltotempo]
	and	a
	jr	z,fincontador_sp_level
	dec	a
	ld	[moltotempo],a
	jr	contador_sp_level
fincontador_sp_level:
	ld	a,2
	ld	[moltotempo],a
	ret

;-----------------------------------------------------------------------
; Rutina de pulsaci�n de tecla y soltado
Espera_tecla:
        xor     a
        in      a,(#fe)
        cpl
        and     #1f
        jr      z,Espera_tecla
	ret
;
; Soltado de teclas
;
Soltar_tecla:
	xor	a
        in      a,(#fe)
        cpl
        and     #1f
	jr	nz, Soltar_tecla
	ret
;----------------------------------------------------------------------------------------
; Rutina de borrado de pantalla completo
ruborco:
        ld      hl,#4000
        ld      de,#4001
        ld      [hl],l
        ld      bc,6144+767
        ldir
        ret



;----------------------------------------------------------------------------------------
imprimenu:

;
	call	ruborco
; Cambio a p�gina 1			
	di				; 33723
	ld	b,1			; por ejemplo, si tenemos las pantallas en la p�gina 3
	call	setrambank		; rutina paginacion.asm
; El titulo se ha comprimido para tener espacio por lo que tenemos que llamar antes al descompresor
	call	descomprimenu		; menudetulum2
; Cambio a p�gina 0
	ld	b, 0
	call setrambank
	ei



	ld	de,menutulumpdp
	ld      hl,txttitulopdp
	call	llamimprim


	call	Imprime_TextoMenu	; Rutina en textomenu.asm
	ret
;----------------------------------------------------------------------------------------
; Impresion en caracteres
llamimprim:
impdatmenu:
        ld     a,[hl]
        ld     [al_tile],a
        inc    hl
        ld     a,[hl]
        ld     [an_tile],a
        inc    hl
        ld     a,[hl]
        ld     c,a                        ; COLUMNA
        ld     [col_dat],a
        inc    hl
        ld     a,[hl]
        ld     b,a                        ; FILA
        ld     [fil_dat],a
;        ret
;----------------------------------------------------------------------------------------
;RUTINA DE COORD.POR CARACT DE MH B=FILA y C=COLUMNA EN CARACTERES
;----------------------------------------------------------------------------------------
im_char:
        ld     h,#40
        ld     a,b
        and    24
        add    a,h
        ld     h,a
        ld     a,b
        and    7
        rrca
        rrca
        rrca
        add    a,c
        ld     l,a
;        ret	c�digo 201 ; valor que pongo desde gestacciones.asm gracias a la etiq.
;----------------------------------------------------------------------------------------
imprbloques:    ;  Para que el c�digo lo pueda modificar desde gestacciones.asm 201=ret 
        ld     a,[al_tile]	; 4/13 58= ld a,[nn] debo restaurar esta orden posteriormente.
        ld     c,a              ; 1/4  ALTURA
        ld     a,[an_tile]	; 4/13
        ld     b,a		; 1/4

i_arriba:
	ex     af,af'		; 1/4  respaldo de B
	push   hl		; 3/11
i_bu:	
	ld     a,[de]		; 2/7
        ld     [hl],a		; 2/7
        inc    de		; 1/6
        inc    l		; 1/4
        djnz   i_bu		; 3/13 , 2/8
        pop    hl		; 3/11
        inc    h		; 1/4
        ld     a,h		; 1/4	
        and    7		; 2/7
        jr     nz,i_sal2	; 3/12 , 7/7
        ld     a,l		; 1/4
        add    a,32		; 2/7
        ld     l,a		; 1/4
        jr     c,i_sal2		; 3/12 , 7/7
        ld     a,h		; 1/4
        sub    8		; 2/7
        ld     h,a		; 1/4
i_sal2:

	ex     af,af'		; 1/4  saco el valor de B=an_tile
	ld     b,a		; 1/4

	dec    c		; 1/4
        jr     nz,i_arriba	; 3/12 , 7/7
;----------------------------------------------------------------------------------------
impattr_gen:
;B=FILA y L=COLUMNA EN CARACTERES
        ld     a,[fil_dat]
        ld     b,a
        ld     a,[col_dat]
        ld     l,a
;llamada a RUTINA DE COORD. DE ATRIBUTOS  DATOS SPRPROTAIMP.ASM
        call   coordficu
i_at_nor:
	xor	a		; acarreo a 0
        ld     a,[al_tile]
	rra		; DIVIDIMOS EL ALT_TILE ENTRE 8 PARA QUE EL VALOR SEA EN CHARS.
	rra		; De esta forma y no con SRL A
	rra		; trasladamos el acarreo
	and	31	; que aqu� anulamos. Con esto ahorramos bytes y 5 estados al menos
i_atb:
	ex	af,af'			; Guardo el Alto en af'
        push   hl                      ; LA POSICION DE HL EN A.ATTR
        ld     a,[an_tile]
        ld     b,a
i_atb1:
        ld     a,[de]
        ld     [hl],a
        inc    de
        inc    l
        djnz   i_atb1
        pop    hl                      ; SACAMOS LA POSICION ATTR
        ld     c,$20
        add    hl,bc

	ex	af,af'			; Saco el Alto de af'
	dec	a			; 
	jr	nz,i_atb		; 3/12
	ret
;FIN IMPRESION DE MARCADORES



;----------------------------------------------------------------------------------------
;Rutina de calc.coord. de atributos B=FILA y L=COLUMNA EN CARACTERES
;----------------------------------------------------------------------------------------
; Se usa para imprimir marcadores, menus, imagenes
coordficu:
	        ld      h,#58
	        xor     a
	        srl     b
	        rra
	        srl     b
	        rra
	        srl     b
	        rra
	        ld      c,a
	        add     hl,bc
	        ret             ;19ciclos / 72 t-states



;----------------------------------------------------------------------------------------
;SUBRUTINA DE IMPRESION DE MARCADORES Y TILES
;----------------------------------------------------------------------------------------
impmarcadores:
; empezamos:                      
;impresion marcador de arriba
 	call	descomprimemarcadorarriba	; en marcadorarriba.asm 
        ld	de,64000				; direccion donde se ha descomprimido
        ld	hl,txtmarca
        call	llamimprim
;impresion marcador de abajo
 	call	descomprimemarcadorabajo		; en marcadorabajo.asm 
        ld	de,64000				; direccion donde se ha descomprimido
        ld	hl,txtmarca2
	call	llamimprim
	jp	guardarcolormarcabajo		; guardo el valor attr de abajo en su bufer
						; porque lo necesito restaurar al pillar la luz	
;impresion final del juego
imprim_fin_juego:
	ld	hl,theend		; En datostilesypant.asm 
	ld	de,44288		; Uso el Buffer de pantalla para descomprimir
	ld	bc,3364			;  bytes del titulo comprimido.
	call	dzx7			; descompresor
	ld      de,44288		; direccion donde se ha descomprimido
	ld      hl,txtfinal
	call	llamimprim		; Imprimo imagen
	jp	Imprime_Textofinaljuego	; Imprimo el texto


imprim_hurryup:
	ld	hl,coatlicue	; 
	ld	de,44288		; Uso el Buffer de pantalla para descomprimir
	ld	bc,2584	; 2450			;  bytes del titulo comprimido.
	call	dzx7			; descompresor
	ld      de,44288		; direccion donde se ha descomprimido
	ld      hl,txthurryup
	call	llamimprim		; Imprimo imagen
	jp	Imprime_TextoCoatlicue	; Imprimo el texto

imprim_gameover:
        ld	de,gameover_gfx
        ld	hl,txtgameover
	jp	llamimprim

imprim_intros:
	ld	de,intro1
	ld	hl,txtintro1
	call	llamimprim
	ld	de,intro2
	ld	hl,txtintro2
	jp	llamimprim

marcabenazul:
	ld	hl,$5A80
	ld	de,$5A81
	ld	bc,127
	ld	a,1
	ld	[hl],a
	ldir	
	ret


txtmarca       defb 32,32,0,0
txtmarca2      defb 32,32,0,20
txtgameover    defb 16,10,12,10
txthurryup	defb 160,17,7,0
txtfinal	defb 192,21,0,0
txtintro1	defb 32,19,7,16		;20
txtintro2	defb 40,9,15,11		;15






chgsetchar:

	ld hl,setletras-256	;60000-256     ; fuente menos 32*8.
        ld (23606),hl       ; apuntar a la nueva fuente.
setpoints:
	ld a,22         ; c�digo de control ASCII para AT.
        rst 16          ; 
        ld a,21		; coordenada y
        rst 16          ; 
        ld a,1		; coordenada x
        rst 16          ;     
	ret


; Set de letras para TULUM
setletras:

	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 24, 52, 60, 16, 24,  0, 24,  0
	DEFB	 36, 18, 50, 22, 36,  0,  0,  0
	DEFB	 68, 70,124, 36,126, 32, 36,  0
	DEFB	  8, 22, 57, 20, 78, 60,  0,  8
	DEFB	 97, 82, 36,  8, 22, 37, 66,  0
	DEFB	 28, 96, 32, 18, 74, 68, 58,  0
	DEFB	 16,  8, 24, 16, 16,  0,  0,  0
	DEFB	  6,  4, 12,  4, 12,  4,  2,  0
	DEFB	 48, 16,  8, 16, 24, 16, 32,  0
	DEFB	  8, 42, 28, 42,  8,  0,  0,  0
	DEFB	  0,  0,  8,  8, 62,  8,  8,  0
	DEFB	  0,  0,  0,  8,  4, 12, 24,  0
	DEFB	  0,  0,  0,  0, 60,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0, 24, 24,  0
	DEFB	  1,  3,  6, 12, 16, 32, 64,  0
	DEFB	 56, 68, 78, 82, 98, 35, 30,  0
	DEFB	  8, 20, 20,  6,  4,  4, 14,  0
	DEFB	 60, 66,  2,  5, 12, 16, 62,  0
	DEFB	 62, 17,  6,  4,  6, 17, 62,  0
	DEFB	  2,  7, 10, 18, 34, 31, 10,  0
	DEFB	127, 34, 32, 30,  1, 81, 62,  0
	DEFB	 62, 64, 78, 65, 97, 69, 62,  0
	DEFB	127,  1,  6,  4,  8, 20, 32,  0
	DEFB	 62, 73, 65, 62, 65, 73, 62,  0
	DEFB	 62, 65, 65, 63,  1,  9, 62,  0
	DEFB	 24, 24,  0, 24, 24,  0,  0,  0
	DEFB	 24, 24,  0,  0, 12,  4,  8,  0
	DEFB	  8, 16, 32, 64, 32, 16,  8,  0
	DEFB	127,127,  0,  0, 85,  0,  0,  0
	DEFB	 16,  8,  4,  2,  4,  8, 16,  0
	DEFB	 62, 33,  1,  1,  6,  0,  4,  0

	DEFB	 28, 55, 41, 69,105, 18, 12,  0
	DEFB	 62, 65, 65,127, 85,127, 34,  0
	DEFB	 62, 81,113, 86,113, 81, 62,  0
	DEFB	 62, 81,112, 80,112, 81, 62,  0
	DEFB	 62, 81,113, 81,113, 81, 62,  0
	DEFB	 62, 81,124, 80,112, 81, 62,  0
	DEFB	 62, 81,124, 80,112, 80, 48,  0
	DEFB	 62, 81,112, 82,113, 81, 62,  0
	DEFB	 50, 81,127, 81,113, 81, 50,  0
	DEFB	 28, 20, 28, 20, 28, 20, 28,  0
	DEFB	  6,  5,  7, 37, 71, 69, 62,  0
	DEFB	 56, 41, 42, 60, 60, 42, 25,  0
	DEFB	112, 80,112, 80,112, 81,126,  0
	DEFB	114, 85,127, 85,113, 81,114,  0
	DEFB	113, 81,121, 85,115, 81,113,  0
	DEFB	 62, 75, 73, 71, 65, 65, 62,  0
	DEFB	126, 81,113, 81,126, 80,112,  0
	DEFB	 62, 81,113, 81,119, 85, 62,  0
	DEFB	126, 81,113, 81,126, 82,113,  0
	DEFB	 62, 97, 88, 54, 13, 71, 62,  0
	DEFB	127, 85, 28, 20, 28, 20, 28,  0
	DEFB	113, 81,113, 81,113, 81, 62,  0
	DEFB	 66, 33,113,105, 62, 26, 12,  0
	DEFB	 34, 65, 65, 73, 93, 85, 42,  0
	DEFB	 99, 82, 56, 20, 14, 37, 66,  0
	DEFB	 66, 33,113, 41, 30,  4, 28,  0
	DEFB	 63, 69, 14, 20, 56, 81,126,  0
	DEFB	127, 85, 64, 64, 64, 65,127,  0
	DEFB	 64, 96, 32, 24,  4,  2,  1,  0
	DEFB	127, 85,  1,  1,  1, 65,127,  0
	DEFB	  0,  8, 20, 34, 65,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0, 34,127,  0

	DEFB	 96, 16, 24,  0,  0,  0,  0,  0
	DEFB	  0, 60,  2, 61, 67, 81, 62,  0
	DEFB	 96, 96,126, 65, 99, 65,126,  0
	DEFB	  0,  0, 30, 97, 64,101, 62,  0
	DEFB	  3,  3, 63, 65, 99, 65, 63,  0
	DEFB	  0, 62, 97,127, 72, 64, 62,  0
	DEFB	 12, 24, 24,118, 24,  8, 24,  0
	DEFB	  0, 62, 67,105, 63,  1, 62,  0
	DEFB	 96, 96, 76,114, 98, 66, 98,  0
	DEFB	 24,  0, 24, 24, 28, 24, 24,  0
	DEFB	 12,  0, 12, 12, 12,  4, 24,  0
	DEFB	 96, 32,100, 88,112, 72,100,  0
	DEFB	 48, 16, 48, 16, 48, 48, 62,  0
	DEFB	  0, 52, 42,106,106, 98, 74,  0
	DEFB	  0, 88, 40,100,100, 36,100,  0
	DEFB	  0,  0, 56, 68, 76, 68, 56,  0
	DEFB	  0, 90, 36,100, 88, 64, 64,  0
	DEFB	  1, 26, 34, 38, 26,  3,  2,  0
	DEFB	  0, 64, 44,112, 32, 96, 32,  0
	DEFB	  0, 60, 64,120,  4, 84, 56,  0
	DEFB	  0, 48,120, 16, 48, 16, 48,  0
	DEFB	  0,  0, 70,100, 36,108, 52,  0
	DEFB	  0,  0, 97, 17, 26, 40,  4,  0
	DEFB	  0,  0, 34,106, 42, 20, 34,  0
	DEFB	  0,  0, 34, 84,  8, 21, 34,  0
	DEFB	  0, 68, 68, 56,  4, 20, 56,  0
	DEFB	  0, 62, 18,  4,  8, 20, 58,  0
	DEFB	 12, 24, 16, 32, 16, 16, 12,  0
	DEFB	 24, 16, 24,  0, 24, 16, 24,  0
	DEFB	 48, 24,  8,  4,  8,  8, 48,  0
	DEFB	  0, 48,121, 71,  6,  0,  0,  0
	DEFB	  8, 20, 34, 93,  0,  0,  0,  0
