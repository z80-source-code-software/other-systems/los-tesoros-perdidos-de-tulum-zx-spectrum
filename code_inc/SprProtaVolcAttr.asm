;----------------------------------------------------------------------------------------
; Calculo del attr del tile de fondo del prota en el buffer attr. Fila2 y colu2 son
; fila en pix y colu en chars,del tile a continuacion. SALIDA: hl direcci�n en el buffer.
;----------------------------------------------------------------------------------------

Pincolorfondoaderprota:			; Desde Engine linea 445
; Comprobamos que llevas la luz para continuar
	ld	a,[portaluz]
	rra
	ret	nc

pincolordelfondo1:
	ld	a,[colu]
	push	af
;	add	a,1
	inc	a
	ld	[colu],a
	call	im_pix
	call	calc_dirbuffattr_prota
	call	Volcar_attr_verticales
	pop	af
	ld	[colu],a
	jp	im_pix

Pincolorfondoaizqprota:
; Comprobamos que llevas la luz para continuar
	ld	a,[portaluz]
	rra
	ret	nc

pincolordelfondo2:
	call	calc_dirbuffattr_prota
	jp	Volcar_attr_verticales

Pincolorfondoabaprota:
; Comprobamos que llevas la luz para continuar
	ld	a,[portaluz]
	rra
	ret	nc
abPincolorfondoabaprota:			;54320

	ld	a,[fila]
	push	af
	add	a,16
	jr	cont_bajo_Pincolorfondoaraprota

Pincolorfondoaraprota:
; Comprobamos que llevas la luz para continuar
	ld	a,[portaluz]
	rra
	ret	nc

	ld	a,[fila]
	push	af
	and	248
cont_bajo_Pincolorfondoaraprota:
	ld	[fila],a
	call	im_pix
	call	calc_dirbuffattr_prota
	ldi
	ldi
	pop	af
	ld	[fila],a
	jp	im_pix
;--------------------------------------------------------------------------------------------------
; Calcular la direccion en buffer atributos con respecto a su direcci�n de pantalla calculada previamente
;--------------------------------------------------------------------------------------------------
calc_dirbuffattr_prota:
        ld     hl,buffattpant	; $bd00	antes $fd90	Inicio buffer attr
calc_dir_buff_ilum_delprota:	; Necesario para calcular la direcc.buffer de iluminaci�n y no 
        ld     a,[fila]		; duplicar esta rutina innecesariamente. Se llama desde SproProtaEyes.asm
	and    248		; Para ajustar el valor a Fila absoluta o char.
        sub    32		; para saber si est�s en la fila 0 del area de juego.
        jr     z,sumacoluatt_prota
        ld     e,a
        xor    a
;PARA CALCULAR LOS CHARS DEBEMOS 1� DIVIDIR ENTRE 8 LOS SCANNES FILA Y DESPUES
;MULTIPLICAR POR 32 (CADA FILA TIENE 32) ES LO MISMO QUE SI MULTIPLICAMOS 4 DE PRIMERAS.
        sla    e		; MULTIPLICAMOS POR 4 (DOS VECES CORRER A LA IZQ. DE
        rla
        sla    e
        rla
        ld     d,a
        add    hl,de		; YA TENEMOS EL VALOR DE LA FILA SUMADO
sumacoluatt_prota:
        ld     a,[colu]
        ld     e,a
        ld     d,0
        add    hl,de		; SUMO LA COLUMNA	Salida hl= direccion en el buffer de atributos.
;--------------------------------------------------------------------------------------------------
; Utilizo la que hay en sprprotaImp. Antes utilizaba reg.C y L para el c�lculo desde fila y colu
; como fila en scannes y colu en char pero esta otra es m�s r�pida usando ya dir_spr
;Direccion		Byte VideoRam			Direccion	Byte Atributo
; $4000			0100 0000			 $5800		0101 1000
; $4800			0100 1000			 $5900		0101 1001
; $5000			0101 0000			 $5A00		0101 1010
;--------------------------------------------------------------------------------------------------
	ex	de,hl		; 1/4 Guardo la direccion buffer Attrib. en  DE
	call	calc_attr_prota	; Rutina de calculo en la videoram. La uso en otras ocasiones. SprProtaImp.asm
	ex	de,hl		; 1/4 Intercambiamos los valores denuevo.
	ret			; 3/10 Total 20ciclos/ 73estados
;
; Ahora vamos a volcar los atributos de los caracteres del prota. Ser�n 3 por si est�s en una liana.
;
Volcar_attr_verticales:		; hl=buffer Attrib de=VideoramAttr
	ld	a,[hl]		;  7/ 1b
	ld	[de],a		;  7/ 1b   Primer caracter.
	ld	bc,$20		; 10/ 3b
	add	hl,bc		; 11/ 1b
	ex	de,hl		;  4/ 1b
	add	hl,bc		; 11/ 1b
	ex	de,hl		;  4/ 1b
	ld	a,[hl]		;  7/ 1b
	ld	[de],a		;  7/ 1b   Segundo caracter o pies
	add	hl,bc		; 11/ 1b
	ex	de,hl		;  4/ 1b
	add	hl,bc		; 11/ 1b
	ex	de,hl		;  4/ 1b
	ld	a,[hl]		;  7/ 1b
	ld	[de],a		;  7/ 1b   Tercero o suelo o pies en liana	112 estados.	17 bytes
	ret

