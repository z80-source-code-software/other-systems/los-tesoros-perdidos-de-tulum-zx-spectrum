;----------------------------------------------------------------------------------------
; Gesti�n de localizaci�n de camino. Algoritmo de Dijkstra
;----------------------------------------------------------------------------------------
seed			db	0	; 58038 Variable para la rutina aleatoria	
pesotemporal		db	0	; 58039
pesoacumulado		db	0	; 5
vertestudio		db	0	; 5
sentvertestudio		db	0	; 5
varcompvertice		db	0	; 5   Variable de v�rtice en Ruta
punteroapesotemporal	dw	0	; 5
punteroRutalocalizada	dw	0	; 5
punterotablavertices	dw	0	; 5
punterocreaRuta		dw	0	; 5
copiaix19enem		db	0	; 5   para la salida de la rutina
varcopiasix		db	0	; 5	; 0,1,2 valores (ver salida de la rutina)
verticeinicial		db	0	; para guardar el v�rtice mientras c�lculo el sentido.
localizaruta:				
;----------------------------------------------------------------------------------------
; Busqueda del v�rtice m�s cercano al enemigo.	58359
	push	bc
	push	de
	push	hl
	push	ix
; datos a guardar
	ld	a,[ix+19]
	ld	[copiaix19enem],a
	xor	a
	ld	[varcopiasix],a
;----------------------------------------------------------------------------------------
; Comprobar que v�rtices est�n iluminados y pasar el dato a TablaposicYXvertices	
	ld	hl,TablaposicYXvertices
	ld	b,9
loop_local_yx_vert:
	push	hl
	inc	l
	ld	a,[hl]		; Y
	ld	d,a
	inc	l
	ld	a,[hl]		; X
	ld	l,d		; paso Y a L que es donde la necesito. A= colu
        call    tabla4metal
; hl = valor del v�rtice en la videoram
	call	c_dirAtr_enem
; hl = valor del v�rtice en la pant.atributos.
	ld	a,[hl]
	and	a
	jr	z,bpon1entab	; Si hay oscuridad. Activa obst�culos y da la vuelta
;pon1entab:
	ld	a,1
bpon1entab:
	pop	hl
	ld	[hl],a		; Paso el dato para esos valores Y,X
	inc	l
	inc	l
	inc	l
	djnz	loop_local_yx_vert
;----------------------------------------------------------------------------------------
; Si despu�s de revisarlo no hay ning�n v�rtice iluminado. Devuelve el control sin buscar camino.
	ld	b,9
	ld	c,0
	ld	hl,TablaposicYXvertices
loop_comp_01:
	ld	a,[hl]
	rra
	jr	c,sumaAA
bloop_comp_01:
	inc	l
	inc	l
	inc	l
	djnz	loop_comp_01
	jr	schearchficovert0
sumaAA:
	rla	
	add	a,c
	ld	c,a
	jr	bloop_comp_01
schearchficovert0:
	ld	a,c
	and	a
	jr	nz,schearchficovert1
	ld	[ix+0],0	; Enemigo a andar
	jp	finrut_x_vert_off
schearchficovert1:
; En la tabla TablaposicYXvertices tendr�n 0 si no est� iluminado el v�rtice y 1 si lo est�
;----------------------------------------------------------------------------------------
	ld	hl,TablaposicYXvertices
	ld	de,TablaposicYXvertices
	push	de
	ld	c,[ix+3]		; fila en scann
;----------------------------------------------------------------------------------------
; Fila del enemigo cercana a un v�rtice
; VerticeInicio:
	ld	b,9
siguebuscvertice:
	push	bc
	ld	a,[de]			; valor de v�rtice iluminado SI O NO
	rra
	jr	c,absiguebuscvertice	; 0 est� a oscuras, 1 est� iluminado
	inc	e
	jr	buscaotrovalorfila
absiguebuscvertice:
	inc	e
	ld	a,[de]			; valor Y en la tabla TablaposicYXvertices
	sub	c			; restamos el valor de la fila del enemigo
	jr	nc,bsiguebuscvertice	; hay acarreo la resta es negativa
	cpl
	inc	a
bsiguebuscvertice:
	ld	b,a
	ld	a,[varcery]
	cp	b
	jr	z,buscaotrovalorfila
	jr	c,buscaotrovalorfila
	ld	a,b
	ld	[varcery],a		; Valor de la fila del v�rtice m�s cercano
	ex	de,hl
	ld	[puntposxyvert],hl
	ex	de,hl
buscaotrovalorfila:
	inc	e
	inc	e
	pop	bc
	djnz	siguebuscvertice
;----------------------------------------------------------------------------------------
; Columna del enemigo cercana a un v�rtice
; hl contiene la direcci�n del v�rtice cuya fila es m�s cercana al enemigo.
	ld	hl,[puntposxyvert]
	ld	a,[hl]			; Posic.en la tabla del valor Y del v�rtice m�s cercano
	ld	[varcery],a		; lo guardo ah� que ya no lo utilizar�
	ld	d,a
	ld	hl,TablaposicYXvertices
	ld	b,9
siguebuscvertice2:
	push	bc
	inc	l
	ld	a,[hl]
	cp	d
	jr	nz,nsiguebusccolu
bsiguebuscvertice2:
	ld	b,[ix+2]		; columna en char
	inc	l
	ld	a,[hl]
	sub	b		; restamos el valor de la columna del enemigo
	jr	nc,bbsigu	; hay acarreo la resta es negativa
	cpl
	inc	a
bbsigu:
	ld	b,a		; guardo la diferencia entre ambos puntos
	ld	a,[varcerx]
	cp	b
	jr	c,nsiguebusccolu2	; Si hay acarreo es que hay m�s distancia.No actualices
	ld	a,b
	ld	[varcerx],a
	dec	l
	ld	[puntposxyvert],hl
nsiguebusccolu:
	inc	l
nsiguebusccolu2:
	inc	l
	pop	bc
	djnz	siguebuscvertice2
;----------------------------------------------------------------------------------------
; valorescerc YX son la distancia hacia el v�rtice m�s cercano Y = scanns X = chars
	ld	hl,CreandoRuta		; Actualizo el puntero de la tabla.		
	inc	hl			; As� es m�s f�cil volcar despu�s los datos de una en la otra.
	inc	hl
	ld	[punterocreaRuta],hl	; Puntero de la ruta final que incremento a la par que el otro
	pop	de			; Valor guardado del inicio de la tabla		VER DE QUITAR SI NO LO USO DESPUES
	ld	hl,[puntposxyvert]	; Valores Y,X del v�rtice iniciar al que ir.
	push	hl			; guardo del valor del puntero
	xor	a			; Anulo acarreo para hacer la resta
	sbc	hl,de			; hl = valor de bytes desde el inicio hasta el v�rtice
	ld	a,l
	dec	a			; le quito 1 antes de hacer la divisi�n por 3
div3pvert:
	sub	3	
	jr	c,bdiv3pvert		; para a< 3 entonces h=0
	jr	z,abdiv3pvert
	inc	h
	jr	div3pvert
abdiv3pvert:
	inc	h
bdiv3pvert:
	ld	a,h			; En h el valor del v�rtice al que apunta puntposxyvert	58132
	ld	hl,RutaLocalizada
	ld	[hl],a			; Aqui metemos los valores en la tabla de inicio de camino
	ld	[verticeinicial],a	; ex	af,af'			; Guardo el valor del v�rtice Inicial.
	inc	hl
	ld	[punteroRutalocalizada],hl	; Puntero de la ruta que ir� increment�ndose
	pop	hl			; Saco el valor del puntero putposxyvert.
;---
	ld	b,[ix+2]		; columna en char ENEMIGO  58470
	ld	c,[ix+3]		; fila en scann ENEMIGO
; Pongo los limites si o si del primer vertice donde ir.
	PUSH	HL
	ld	a,[hl]			; valor Y del v�rtice
	ld	[ix+17],a		; limite Y del v�rtice
	inc	l
	ld	a,[hl]
	ld	[ix+16],a
; discriminar la busqueda seg�n como est� el enemigo	128 el enemigo se ha pegao con la oscuridad
	ld	a,[ix+10]
	rla	
	jr	c,a_enembuscaenHORIZONTAL_unvertice
	
	ld	a,[ix+15]
	and	a
	jr	z,enembuscaenHORIZONTAL_unvertice
;-----------------------------------------------------------
; Enemigo busca el vertice y �l est� en movimiento vertical  58493
enembuscaenVERTICAL_unvertice:
	pop	hl
	ld	a,[hl]			; valor Y del v�rtice
	cp	c
	jr	c,vertice_arr_delenem_envertical
	jr	z,vertice_enY_delenem_envertical
;vertice_aba_delenem_envertical:
	ld	a,2			; a = 2 sentido abajo
	jr	vertice_aba_delenem	; No mires si tienes que saltar, muevete abajo por la liana

vertice_enY_delenem_envertical:
	inc	l
	ld	a,[hl]			; valor de la columna
; aqui puedo ver si est� a la izquierda o a la derecha para poner valores de salto en B tengo el char del enemigo
	cp	b
	jr	z,vertice_enXY_delenem_envertical
	jr	sigue_vertice_arr_delenem_envertical
vertice_enXY_delenem_envertical:
	ld	a,[ix+19]		; sentido el que lleves
	jr	vertice_aba_delenem

vertice_arr_delenem_envertical:
	inc	l
	ld	a,[hl]			; valor de la columna
; aqui puedo ver si est� a la izquierda o a la derecha para poner valores de salto en B tengo el char del enemigo
	cp	b
	jr	z, vertice_arr_delenem
sigue_vertice_arr_delenem_envertical:
	jr	c,SaltoVertice_izq_delenem
; Si tiene que saltar a un lado va a tener que cambiar ix+15 y la cantidad de frames
	ld	a,4			; salto a la derecha
	jp	abb_vertice_arr_delenem_horizontal	; mismo c�digo en parte horizontal m�s abajo

SaltoVertice_izq_delenem:
	ld	a,5			; salto a la izquierda
	jp	abb_vertice_arr_delenem_horizontal

vertice_arr_delenem:
	xor	a			; a = 0 sentido arriba
vertice_aba_delenem:
	ld	[ix+19],a
	ld	[sentvertestudio],a
	ld	[varsent1vert],a	; para localizarliana
	ld	[ix+15],1		; enemigo en vertical
	ld	[ix+6],0		; frameskip en la vertical lo pongo por si anteriormente se cambio en la horizontal
	ld	[ix+7],1		; framestotal
	jp	randsemilla
;-----------------------------------------------------------------
; vuelvo a poner el dato de si viene el enemigo de tropezar en zona oscura a 0
; como vienes de desaparecer por que estabas en vertical. Vamos a cambiar tu sentido inicial tambi�n
; asi la rutina no har� cosas como poner al enemnigo a lo moowalker
a_enembuscaenHORIZONTAL_unvertice
	ld	[ix+10],0	; enemigo en zona oscura	
	ld	[ix+15],0	; enemigo en horizontal
	ld	a,1		; y mirando a la derecha siempre
	ld	[copiaix19enem],a


; Enemigo busca el vertice y �l est� en movimiento horizontal	
enembuscaenHORIZONTAL_unvertice:	
	POP	HL
	ld	a,[hl]			; valor Y del v�rtice
	cp	c
	jr	c,vertice_arr_delenem_enhorizontal
; El v�rtice est� a su altura o por debajo.
elenemigovaenhorizontal:
	ld	[ix+7],3		; framestotal en la horizontal lo pongo por si anteriormente se cambio en la vertical
; lo siguiente lo puedo poner como a,ix+16
	inc	l
	ld	a,[hl]
	cp	b
	jr	c,vertice_izq_delenem_horizontal
vertice_der_delenem_horizontal:
	ld	a,1			; A=1 sentido a la derecha
	call	look_sent_horiz_enem
	jr	finaliza_vertice_delenem_horizontal
vertice_izq_delenem_horizontal:
	ld	a,3			; a=3 sentido a la izquierda
	call	look_sent_horiz_enem
finaliza_vertice_delenem_horizontal:
	ex	af,af'			; Saco el Valor de C guardado en look_sent_horiz_enem
	ld	[ix+19],a
	ld	[sentvertestudio],a
	xor	a			
	ld	[varcopiasix],a		; Para enemigo andando
	ld	[EnemAnimSN],a		; Si est� andando ya no estar� animandose
	jp	randsemilla

EnemAnimSN	defb	0			; Variable para usar solo aqui

look_sent_horiz_enem:
	ld	c,a
	ex	af,af'
; Pon valor en EnemAnimSN
	ld	a,[ix+20]		; Para saber si llegas animando una desaparici�n.
	and	a
	jr	z,versienemanimabaa0
	ld	[ix+20],0		; ix+20 lo pongo a 0 ya que estaba a 1 ->animandose
	ld	a,1
	jr	versienemanimabaa1
versienemanimabaa0:
	xor	a
versienemanimabaa1:
	ld	[EnemAnimSN],a		; Para Setframe2 o 3
;	
	ld	a,[copiaix19enem]	; valor anterior ix+19
	cp	c			; si el valor ix+19 nuevo coincide y est� en la horizontal
	jr	z,mismosentidoponloaandar	; pon lo a andar solamente
; sino dale la vuelta
	cp	1
	jr	nz,look_averthorizond
look_averthorizoni:
	ld	a,[EnemAnimSN]		; Si 0 pon frame3 si 1 es que estabas animando antes, luego frame2
	and	a
	jp	z,Setframe3_Sent_enem_horiz_izq
	jp	Setframe2_Sent_enem_horiz_izq
look_averthorizond:
	ld	a,[EnemAnimSN]		; Si 0 pon frame3 si 1 es que estabas animando antes, luego frame2
	and	a
	jp	z,Setframe3_Sent_enem_horiz_der
	jp	Setframe2_Sent_enem_horiz_der
mismosentidoponloaandar:
; como tienes el mismo sentido que llevabas comprueba si vienes de animar y ponlo a andar
	ld	a,c			; C= 1 o 3 der o izq
	cp	1
	jr	z,look_averthorizond
	jr	look_averthorizoni

; El vertice est� por encima del enemigo y este anda en horizontal.
vertice_arr_delenem_enhorizontal:
	inc	l
	ld	a,[hl]			; columna del vertice
; aqui puedo ver si est� a la izquierda o a la derecha para poner valores de salto en B tengo el char del enemigo
	cp	b
	jr	z,vert_arr_delenem_enHoriz_mismacolumna		; si est� en la misma columna
	jr	c,SaltoVertice_izq_delenem_enhorizontal
	ld	a,4			; salto a la derecha
	jr	abb_vertice_arr_delenem_horizontal
SaltoVertice_izq_delenem_enhorizontal:
	ld	a,5			; salto a la izquierda
abb_vertice_arr_delenem_horizontal:
	call	vertsaltaunlao
bb_vertice_arr_delenem_horizontal:
	ld	[ix+19],a
	ld	[sentvertestudio],a
	ld	[ix+6],0		; frame
	ld	[ix+7],3		; framestotal
	ld	[ix+15],0		; enemigo en horizontal
	jr	randsemilla
vert_arr_delenem_enHoriz_mismacolumna:
	xor	a
	ld	[ix+19],a
	ld	[varsent1vert],a
	call	Set_enemarraba_ambos
	ld	[ix+15],1
	ld	a,8			; Para mirar lianas
	ld	[varcopiasix],a
	jr	randsemilla
vertsaltaunlao:
; Al v�rtice se llega saltando.
	ld	c,a
	call	Enem_preparar_salto
	ld	a,4			; Para enemigo saltando
	ld	[varcopiasix],a
	ld	a,c			; recupero C para ix+19 y sentvertestudio
	ret

;----------------------------------------------------------------------------------------
; Busqueda aleatoria del v�rtice final para crear el camino.		
;
; Rutina basada en la RAND de www.z80.info y modificada		
randsemilla:
	ld	a, [ix+30]		; SEED
	ld	b, a 
	ld	a,r
	xor	b
	rrca
	rrca
	xor	0x1f
	add	a, b
	sbc	a, 32
	and	15
	cp	9
	jr	c,metervalorsemilla
	and	7
metervalorsemilla:
	ld	[seed], a
	ld	c,a			; valor v�rtice final en C
	ld	a,[verticeinicial]	;	ex	af,af'			; saco valor vertice inicial y guardo el final en AF'
	ld	b,a			; guardar en B vertice Inicial
	cp	c
	jr	nz,brandsemilla
	ex	af,af'			; Vuelvo a guardar en AF' vertice inicial y AF el final que no vale
	jr	randsemilla
brandsemilla:
;----------------------------------------------------------------------------------------
; V�rtice Inicial. Estudio de los v�rtices desde el Inicial hasta el final      
; Colocar peso temporal y definitivo. Colocar sentido en tabla temporal y en la tabla del v�rtice
; A y B = v�rtice Inicial
; El v�rtice inicial tiene un peso definitivo y temporal de 0
	ld	[vertestudio],a		; V�rtice en Estudio
	call	calculapunteroapesotemporal
	ld	[hl],0			; peso temporal 0 del v�rtice Incial
	inc	hl			; sentido en la tabla de peso temporal del v�rtice
	ld	a,[sentvertestudio]
	ld	[hl],a
	ld	a,b			; Pongo el peso definitivo del vertice de inicio a 0 
	call	calculapunteroapesodefinitivo	; en la tabla de pesos definitivos.. Vtice inicial guardado en B
	xor	a
	ld	[hl],a			; peso definitivo del v�rtice de Inicio
	ld	[pesoacumulado],a	; Guardo el valor nuevo de pesoacumulado (en este caso 0 )
; Compruebo los v�rtices adyacentes y cu�l ser� el de menor peso nuevo ANTES DE FIJAR el v�rtice 0
	ld	a,b			; vertice inicial guardado previamente en B
	call	ponpunterotablavertices	; HL = direc.del v�rtice actual
	ld	[punterotablavertices],hl
	call	fijarverticeysentido	; Fijamos el v�rtice y le indicamos el sentido en la tabla del v�rtice
	call	ververticesadyacentes	; Partiendo del primer v�rtice, colocar los datos temporales de sus adyacentes.
	call	verticemenorpeso	; Tomar el menor de los valores temporales y lo hacemos definitivo.
loop_conv_pesos_temp_a_def:
	ld	a,[vertestudio]
	call	ponpunterotablavertices	; HL = direc.del v�rtice actual
	ld	[punterotablavertices],hl
	call	comp_vertice_fijado	; Mirar si ese v�rtice estaba fijado de antes.
	jr	nz,cont_loop_saltar_conv_pesos_temp_a_def	; En caso de que lo est� comprueba otro v�rtice	
	call	fijarverticeysentido	; Fijamos el v�rtice y le indicamos el sentido en la tabla del v�rtice
	ld	a,[vertestudio]		; Peso temporal lo hago definitivo
	call	calculapunteroapesotemporal
	ld	a,[hl]
	ex	af,af'
	ld	a,[vertestudio]
	call	calculapunteroapesodefinitivo
	ex	af,af'
	ld	[hl],a			; peso temporal a definitivo en la tabla de este �ltimo
	ld	a,[vertestudio]		; ver A previamene por si no es necesario este comando.
	call	ActualizapunterosRuta	; Antes -> primeraruta

cont_loop_saltar_conv_pesos_temp_a_def:
; Comprueba si el nuevo v�rtice es el v�rtice final y sal del bucle
	ld	a,[seed]
	cp	c
	jr	z,ultimoverticeencontrado
	ld	hl,[punterotablavertices]
	call	ververticesadyacentes	; Comprobaci�n de v�rtices adyacentes
	call	verticemenorpeso
	jr	loop_conv_pesos_temp_a_def
ultimoverticeencontrado:
;----------------------------------------------------------------------------------------
; Salida de la rutina despu�s de crear el camino.
;----------------------------------------------------------------------------------------
	call	ververticescorrectos
	ld	a,[seed]
	ld	[ix+30],a		; para comprobar en la gesti�n si ha llegado al �ltimo vertice
	ld	a,[varcopiasix]		; Seg�n el sentido del primer v�rtice as� pongo ix+0 (0,4,8)
	ld	[ix+0],a		; Pongo los enemigos a andar, saltar o subir por liana	
	call	restvalorestablocales
finrut_x_vert_off:
	pop	ix			; FIN RUTINA
	pop	hl
	pop	de
	pop	bc
	ret	

;----------------------------------------------------------------------------------------
; Restauraci�n de valores en las tablas -- Tablocalvertices.asm        58955
;----------------------------------------------------------------------------------------
restvalorestablocales:
	ld	hl,RutaLocalizada	
	ld	de,RutaLocalizada+1
	ld	[hl],128
	ld	bc,8
	ldir
	inc	l
	ld	[hl],0
	inc	l
	ld	[hl],0
	inc	l
	ld	d,h
	ld	e,l
	inc	de
	ld	[hl],255
	ld	bc,29
	ldir
	ld	[hl],128
	ld	bc,18
	ldir
	ld	hl,seed		; 57981 direcci�n de memoria de variables de aqui a reiniciar. (Al inicio)
	ld	b,14
restdirmemvar:
	ld	[hl],0
	inc	l		
	djnz	restdirmemvar
	ld	b,9		; n� de v�rtices Restauraci�n de los valores de v�rtice fijado SI o NO en cada v�rtice
	ld	de,14		; n� bytes por v�rtice
	ld	hl,43525	; ultimo byte del v�rtice 0
finrestvaloresvertices:
	ld	[hl],0
	add	hl,de
	djnz	finrestvaloresvertices
	ret
;----------------------------------------------------------------------------- 59018
; 
ververticescorrectos:
	call	decrempunteroruta	; Puntero en el �ltimo valor
	ld	a,[seed]		; Valor del v�rtice �ltimo
	ld	c,a			; Meter aqui ese valor en la tabla Ruta Creada y su sentido
	ld	hl,[punterocreaRuta]	; Cargo el puntero.
	ld	[hl],a			; coloco el n� de v�rtice correcto en la ruta.
	inc	hl
	push	hl
	call	calculapunteroapesotemporal
	inc	hl			; sentido
	ld	a,[hl]
	pop	hl
	ld	[hl],a			; coloco el sentido del v�rtice correcto en la ruta. Aunque no lo necesito para la SEED
	call	decrempunteroruta	
; �ltimo v�rtice o SEED en tabla CreadaRuta
bververticescorrectos:
	ld	a,[pesoacumulado]
	and	a
	jr	z,fincreandolaRuta	; Si pesoacumulado es 0 es que hemos terminado
	ld	a,c			; C= v�rtice a mirar
	call	ponpunterotablavertices	; hl = puntero en la tabla de vertices
	ld	b,6
; Ahora hay que buscar el v�rtice que estamos tratando con el anterior para saber cu�l es el peso entre ambos.
	inc	hl
sigvertcorrecto:
	push	bc
	ld	a,[hl]			; sent + vertadyac
	and	15			; dejo los 4 bits bajos del vertice= n�v�rtice
	cp	c			; comprobar si es el mismo v�rtice
	jr	z,consigvertcorrecto	; si eres el mismo v�rtice NO lo trates.
; Comprobar si ese v�rtice est� fijado. Si no lo est� saltarlo
	push	af			; necesito en A el v�rtice para la comprobaci�n
	ex	af,af'			; y despu�s tambi�n en af' ese VERTICE ADYACENTE 	
	pop	af
	push	hl
	call	ponpunterotablavertices	; HL = direc.del v�rtice adyac que miramos
	call	comp_vertice_fijado
	pop	hl
; A tiene 0 o 1 al volver de comp vertice fijado. Si est� fijado continua con el siguiente
	jr	z,consigvertcorrecto	; Si lo est�, ya lo has guardado en af'
; Ver si es el que corresponde a la ruta mirando el peso entre ambos.
	inc	hl
	ld	a,[hl]
	ld	e,a			; peso entre ambos en E
	ld	a,[pesoacumulado]
	sub	e
	ld	e,a			; guardo el resultado en E
	ex	af,af'			; saco el v�rtice que estamos tratando para saber si coincide la resta con su peso definitivo
	ld	d,a
	push	hl
	call	calculapunteroapesodefinitivo
	ld	a,[hl]
	cp	e
	jr	z,eseeselverticecorrecto
	pop	hl			; No, es el v�rtice correcto
	jr	consigvertcorrecto2
eseeselverticecorrecto:			; Si, es el vertice correcto
	pop	hl
	ld	[pesoacumulado],a
	ld	a,1
	ld	[varcompvertice],a	
	ld	a,d			; n� v�rtice en A que debo guardar como parte de la ruta
	ld	hl,[punterocreaRuta]		; Cargo el puntero.
	ld	[hl],a			; coloco el n� de v�rtice correcto en la ruta.
	inc	hl
	push	hl
; C tiene el valor del v�rtice "A" y A tiene el valor del v�rtice "B". Buscamos en la tabla del v�rtice "A"
; la correspondencia con "B" atrav�s de su sentido. Y grabamos este
	call	correspondenciaAB	; <<<<<<<<<<<<<<< aqui est� el problema. El sentido es de v�rtice A a B
	pop	hl
	ld	[hl],a			; coloco el sentido del v�rtice correcto en la ruta.
	pop	bc
	jr	consigvertcorrecto3
consigvertcorrecto:
	inc	hl
consigvertcorrecto2:
	inc	hl
	pop	bc
	djnz	sigvertcorrecto
consigvertcorrecto3:
; Aqui debo sacar hl para continuar con la rutina principal. Si es o no el v�rtice, que hacer despu�s de saberlo
; se me ocurre usar una variable para saberlo Si es 0 entonces coge otro v�rtice de Rutalocalizada. Si es 1
	ld	a,[varcompvertice]
	and	a
	jr	z,noeraverticederuta
	xor	a
	ld	[varcompvertice],a	; actualizo valor
	ld	c,d			; nuevo v�rtice
	call	decrempunteroruta
	jr	bververticescorrectos	; Ahora saltamos arriba. Al inicio de nuevo.
noeraverticederuta:
	ld	hl,[punteroRutalocalizada]	; 
	dec	hl
	ld	[punteroRutalocalizada],hl
	ld	a,[hl]			; hl= punteroRutaLocalizada, nuevo v�rtice a mirar si es que no hay camino por all�
	ld	c,a
	jr	bververticescorrectos	; Ahora saltamos arriba. Al inicio de nuevo.
decrempunteroruta:
	ld	hl,[punterocreaRuta]	; Decrementar punteros para que est�n en el lugar correcto.
	dec	hl
	dec	hl
	ld	[punterocreaRuta],hl
	ret
fincreandolaRuta:
	ld	hl,[punterocreaRuta]	; Incrementar el puntero para que est�n en el primero.  58718
	inc	hl
	inc	hl
	ld	[punterocreaRuta],hl
;----------------------------------------------------------------------------------------
; Aqu� traslada el valor de la tabla CreandoRuta a la tabla propia del enemigo.
; El puntero de ruta YA apunta al primer dato que enviar y est� en HL
;----------------------------------------------------------------------------------------
	ld	a,[ix+18]			; 59166
	and	a
	jr	nz,rutenem2
rutenem1:
	ld	de,TAB_RUTA_ENEM1
	jr	brutenem2
rutenem2
	ld	de,TAB_RUTA_ENEM2
brutenem2:
	push	de
	ld	bc,18
	ldir	
	pop	de
	ret
;----------------------------------------------------------------------------------------
; Entrada HL= puntero al v�rtice a determinar
;----------------------------------------------------------------------------------------
ververticesadyacentes:
	ld	a,[hl]			; N� vertice 	
	ld	c,a
	ld	b,6
	inc	hl
sigvertadyac:
	push	bc
	ld	a,[hl]			; sent + vertadyac
	and	15			; dejo los 4 bits bajos del vertice= n�v�rtice
	cp	c
	jr	z,consigvertadayac	; si eres el mismo v�rtice NO lo trates.
; Comprobar si ese v�rtice est� fijado previamente para NO considerarlo
	push	af			; necesito en A el v�rtice para la comprobaci�n
	ex	af,af'			; y despu�s tambi�n en af' ese VERTICE ADYACENTE 	
	pop	af
	push	hl
	call	ponpunterotablavertices	; HL = direc.del v�rtice adyac que miramos
	call	comp_vertice_fijado
	pop	hl
; A tiene 0 o 1 al volver de comp vertice fijado. Si est� fijado continua con el siguiente
	jr	nz,consigvertadayac	; Si no lo est�, ya lo has guardado en af'
; fin de comprobaci�n de v�rtice fijado o no.
	ld	a,[hl]			; sent + vertadyac
	and	240			; 4 bits altos = Sentido 
	rra
	rra
	rra
	rra
	ld	[sentvertestudio],a	; guardo el sentido al vertice adyacente
; hl+1 = peso temporal del vertice adyacente al que estamos estudiando
	inc	hl
	ld	a,[hl]			; peso
; sumamos el peso acumulado con v�rtice ady. que estamos mirando
	ld	c,a
	ld	a,[pesoacumulado]
	add	a,c			; sumo el peso acumulado con el peso del v�rtice adyacente
	ld	c,a			; compararemos con C para saber si es menor del que ya hay.
	ex	af,af'			; devuelvo a A el VERTICE ADYACENTE
	push	hl			; guardo el valor porque voy a cambiar HL en la llamada
	call	calculapunteroapesotemporal	; tabla de pesos temporales
	ld	a,[hl]			; valor en la tabla de peso temporal de ese v�rtice
	cp	c			; PESO ACUMULADO EN C para comparar con el que est�s mirando
	jr	c,salvertadyac
	ld	[hl],c			; guardo el peso en la tabla de pesos temporales porque es 
	inc	hl			; menor que el que hab�a previamente.
	ld	a,[sentvertestudio]
	ld	[hl],a			; guardo el sentido del v�rtice adyacente del que he guardado su peso
; luego al comprobar que peso definitivo es el menor, necesitar� saber su sentido para hacer este v�rtice adyacente, definitivo
salvertadyac:
	pop	hl
	dec	hl
consigvertadayac:
	inc	hl
	inc	hl
	pop	bc
	djnz	sigvertadyac
	ret
;----------------------------------------------------------------------------------------
; Hallar el vertice cuyo peso temporal sea el m�s peque�o en la tabla de pesos temporales
verticemenorpeso:
; inicializo valores de las variables locales pesotemporal
	ld	a,255			; n�mero m�ximo para hallar el n�mero m�s peque�o
	ld	[pesotemporal],a
	ld	c,a
	ld	b,9
	ld	de,43512-14		; direcci�n del primer v�rtice menos 14bytes.
	ld	hl,TablaPesoTemporal
siguebuscpesomenor:
; Mirar si el v�rtice est� sin fijar. 
	push	hl			; puntero en tabla de pesos temporales
	ld	[punteroapesotemporal],hl	
; Hl contiene el indice de ese puntero.		
	push	ix
	ex	de,hl		
	ld	de,14			; bytes de un v�rtice
	add	hl,de
	ex	de,hl			; de = nuevo v�rtice
	push	de
	pop	ix
	ld	a,[ix+13]		; posici�n en el v�rtice de VERTICE FIJADO
	and	a
	jr	nz,buscaotrovalorpesotemporal	; verticeYAfijado
; V�rtice no fijado
	ld	hl,[punteroapesotemporal]
	ld	a,[hl]			; valor del churro actual
	and	a
	jr	z,buscaotrovalorpesotemporal	; 0 es el valor del peso temporal del primer v�rtice
	cp	c			; comprobamos numeros
	jr	nc,buscaotrovalorpesotemporal	; si acarreo A < C
	ld	[pesotemporal],a	; V�rtice m�s peque�o
	ld	c,a
	ld	[etiq_para_hl+1],hl	; guardo la direcci�n del v�rtice de menor peso
buscaotrovalorpesotemporal:
	pop	ix
	pop	hl			; saco el puntero de la tabla de pesos temporales
	inc	l
	inc	l
	djnz	siguebuscpesomenor
	ld	a,[pesotemporal]
	ld	[pesoacumulado],a
;----------------------------------------------------------------------------------------
; HL le pongo el punteroapesotemporal que hemos visto es el menor
; restamos la tablapesotemporales y nos dar� el valor del v�rtice en A
etiq_para_hl:
	ld	hl,$0000		; valor que le dar� en la etiq_para_hl
	ld	[punteroapesotemporal],hl	
	ld	de,TablaPesoTemporal
	XOR	A			; ACARREO A 0
	sbc	hl,de				
	ld	a,l				
	rra				; N� de v�rtice m�s peque�o
	ld	c,a			; Guardo en C NUEVO v�rtice de menor peso
	ld	[vertestudio],a
	ret
;----------------------------------------------------------------------------------------
; Rutina de busqueda del sentido entre dos v�rtices A y B
;----------------------------------------------------------------------------------------
correspondenciaAB
	ld	a,d			; v�rtice A, en el registro D tengo el v�rtice B
	push	de
	call	ponpunterotablavertices	; hl direcci�n del v�rtice A
	pop	de
	inc	hl
	ld	b,6
bcorrespondenciaAB:
	ld	a,[hl]			; sent + vertadyac
	and	15			; dejo los 4 bits bajos del vertice= n�v�rtice
	cp	c
	jr	z,cont_bcorrespondenciaAB
	inc	hl
	inc	hl
	djnz	bcorrespondenciaAB	; si lo has encontrado continua
cont_bcorrespondenciaAB:
	ld	a,[hl]			; sent + vertadyac
	and	240			; dejo los 4 bits altos del vertice= SENTIDO
	rra
	rra
	rra
	rra
	ret				; A= sentido
;----------------------------------------------------------------------------------------
; Incremento puterocreaRuta y guardo en RutaLocalizada el valor del siguiente v�rtice encontrado
ActualizapunterosRuta:
	ld	hl,[punterocreaRuta]
	inc	hl
	inc	hl
	ld	[punterocreaRuta],hl	; Puntero de la ruta final que incremento a la par que el otro
	ld	hl,[punteroRutalocalizada]
	ld	[hl],a
	inc	hl
	ld	[punteroRutalocalizada],hl	; Puntero de la ruta que ir� increment�ndose
	ret
;----------------------------------------------------------------------------------------
; Coloco en el puntero el valor de la direccion al v�rtice
; Entrada A= v�rtice a buscar en la tabla de v�rtices. Salida HL= puntero
ponpunterotablavertices:
	ld	hl,TablaIndiceVertices
	add	a,a
	add	a,l
	ld	l,a			; hl valor en la tabla de indices de la posici�n donde est� la direcci�n
	ld	e,[hl]
	inc	hl
	ld	d,[hl]
	ex	de,hl			; hl contiene la direcci�n del v�rtice
	ret
;----------------------------------------------------------------------------------------
; Entrada A= v�rtice a buscar en la tabla. Salida HL= puntero
calculapunteroapesotemporal:
	ld	hl,TablaPesoTemporal
	add	a,a
	add	a,l
	ld	l,a
	ret
;----------------------------------------------------------------------------------------
; Entrada A= v�rtice a buscar en la tabla. Salida HL= puntero
calculapunteroapesodefinitivo:
	ld	hl,TablaPesoDefinitivo
	add	a,l
	ld	l,a
	ret
;----------------------------------------------------------------------------------------
; Fijar v�rtice al poner su peso definitivo. HL = puntero a la tabla de indices de v�rtices
fijarverticeysentido:
	push	hl
	ld	de,13
	add	hl,de
	ld	[hl],1			; Fijo el v�rtice
	pop	hl
	ret
;----------------------------------------------------------------------------------------
; Comprobar v�rtice fijado. HL = puntero a la tabla de indices de v�rtices
comp_vertice_fijado:
	push	hl
	ld	de,13
	add	hl,de
	ld	a,[hl]			; 0 NO FIJADO Y 1 FIJADO
	and	a
	pop	hl
	ret