;----------------------------------------------------------------------------------------
FINALJUEGO:
	call	ruborco
; Cambio a p�gina 1			
	di				; 
	ld	b,1			; 
	call	setrambank		; rutina paginacion.asm
; El titulo se ha comprimido para tener espacio por lo que tenemos que llamar antes al descompresor
	call	imprim_fin_juego
; Cambio a p�gina 0
	ld	b, 0
	call setrambank
	ei
; Poner texto
	ld	a,6			; Musica del final del juego
	ld	[poncancionenA+1],a
	call	llamar_musica		; El cacho anterior en rucontroles.asm
	call	Soltar_tecla	
	jp	Inicio_vr	


;----------------------------------------------------------------------------------------
FINALFALSO:
	call	ruborco
; Cambio a p�gina 3	La imagen est� en la p�gina de las melodias.			
	di				;
	ld	b,3			;
	call	setrambank		; rutina paginacion.asm
	call	imprim_hurryup	; Mostrar el finalfalso		; Inicializar m�sica. NO CAMIBAR DE PAGINA. 
; Cambio a p�gina 0
	ld	b, 0
	call setrambank
	ei
; PONER TEXTO
	ld	a,5			; Musica del hurryup
	ld	[poncancionenA+1],a
	call	llamar_musica		; El cacho anterior en rucontroles.asm
	call	Soltar_tecla	
	call	Espera_tecla		; Espera teclas
	jp	ruborco

;----------------------------------------------------------------------------------------
MUERTO:
	call	imp_spr_mov		; 
	call	Soltar_tecla	
	ld	a,8
	ld	[pausacolor],a
	call	fadeout_color		; Fade Out de colores
	call	ruborco
	call	imprim_gameover
; Inicializar m�sica gameover 
	ld	a,4			; Musica del hurryup
	ld	[poncancionenA+1],a
	call	llamar_musica		; El cacho anterior en rucontroles.asm
	call	Soltar_tecla	
	call	Espera_tecla	; Espera teclas
	jp	Inicio_vr	
