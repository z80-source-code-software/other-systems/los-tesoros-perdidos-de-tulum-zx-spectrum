
; Comprobaci�n de obst�culos. En el buffer de atributos.

;--------------------------------------------------------------------------------------------------
obst_arr:
;--------------------------------------------------------------------------------------------------
	ld	a,[last_p+1]	; [fila]
       	cp	34              ; MAX.POSIC.PANTALLA A arriba
       	jr	nc,acomp_CHR_ARRBA	; ANTES YSIAR
	xor	a
	ld	[senpro],a
	inc	a
       	ld	[flagpa],a      ; Banderin activo de paso de pantallas
       	ld	a,128
	ld	[fila],a
       	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
	ld	a,[colu]
       	ld	[last_p],a
	jp	hayobs
;--------------------------------------------------------------------------------------------------
obst_aba:
;--------------------------------------------------------------------------------------------------
	ld	a,[last_p+1]	; [fila]
       	cp	152		;134             ; MAX.POSIC.PANTALLA A abajo
       	jr	c,YSIAB
	ld	a,1
       	ld	[flagpa],a      ; Banderin activo de paso de pantallas
	inc	a
	ld	[senpro],a
	ld	a,32
       	ld	[fila],a
       	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
	ld	a,[colu]
       	ld	[last_p],a
	jp	hayobs
YSIAB:                          ; Comprobar el suelo bajo tus pies
	ld	a,[fila]
	add	a,16		; 
	ld	[last_p+1],a
acomp_CHR_ARRBA:
	ld	a,[colu]
	ld	[last_p],a
;--------------------------------------------------------------------------------------------------
comp_CHR_ARRBA:
	call	calc_dirbuffattr	; al final de este archivo
; primer CHR arriba/abajo del prota 
	ld	a,[hl]
	bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
	jp	nz,hayobs
; segundo CHR arriba/abajo del prota 
	inc	hl
	ld	a,[hl]
	bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
	jp	nz,hayobs
	jp	Nohayobs
;--------------------------------------------------------------------------------------------------
obst_de:                                ; A la derecha   40873
;--------------------------------------------------------------------------------------------------
	ld	a,[colu]
	cp	31              ; MAX.POSIC.PANTALLA A derecha
       	jr	NZ,YSIDE
	ld	a,1
	ld	[colu],a
       	ld	[last_p],a
       	ld	[flagpa],a      ; Banderin activo de paso de pantallas
	ld	[senpro],a
	ld	a,[fila]
       	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
	jp	hayobs
YSIDE:					; 40903
	ld	a,[colu]
	add	a,2
	ld	[last_p],a
	jr	acomp_CHR_DERIZ
;--------------------------------------------------------------------------------------------------
obst_iz:                                ; la izquierda
;--------------------------------------------------------------------------------------------------
	ld	a,[colu]
	and	a	;	cp	1
	jr	NZ,YSIIZ
	ld	a,1
       	ld	[flagpa],a      ; Banderin activo de paso de pantallas
	ld	a,3		; 3
	ld	[senpro],a
	ld	a,[fila]
       	ld	[last_p+1],a    ; Inicializan VALORES CUANDO MUERES
	ld	a,29
	ld	[colu],a
       	ld	[last_p],a
	jp	hayobs
YSIIZ:
	ld	a,[colu]
	dec	a
	ld	[last_p],a
acomp_CHR_DERIZ:
	ld	a,[fila]
	ld	[last_p+1],a
;--------------------------------------------------------------------------------------------------
comp_CHR_DERIZ:
	call	calc_dirbuffattr
; Calculada la primer posici�n en altura las restantes solo hay que sumar $20 a fila
	ld	a,[hl]
	bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
	jr	Nz,hayobs
;CHR a izq. 8pixels abajo
	ld	bc,$20
	add	hl,bc
	ld	a,[hl]
	bit     6,a             ; comprob.si el flag de brillo atrib.est� activo
	jr	Nz,hayobs
;
; actvamos o desactivamos obstaculos
;
Nohayobs:
	ld	a,1
	ld	[obstaculo],a
	ret
hayobs:
	xor	a
	ld	[obstaculo],a
	ret
;--------------------------------------------------------------------------------------------------
; Calcular la direccion en buffer atributos para comprobaci�n de obstaculos con el prota
;--------------------------------------------------------------------------------------------------

; Salida hl= direcc.en el buffer de atributos, del char a examinar.

calc_dirbuffattr:
        ld     hl,buffattpant	; $bd00 antes $fd90    ; Inicio buffer attr
        ld     a,[last_p+1]; FILA
	and    248	   ; Para ajustar el valor a Fila absoluta o char.
        sub    32          ; para saber si est�s en la fila 0 del area de juego.
        jr     z,sumacoluatt_buffer
        ld     e,a
        xor    a
;PARA CALCULAR LOS CHARS DEBEMOS 1� DIVIDIR ENTRE 8 LOS SCANNES FILA Y DESPUES
;MULTIPLICAR POR 32 (CADA FILA TIENE 32) ES LO MISMO QUE SI MULTIPLICAMOS 4 DE PRIMERAS.
        sla    e           ; MULTIPLICAMOS POR 4 (DOS VECES CORRER A LA IZQ. DE
        rla
        sla    e
        rla
        ld     d,a
        add    hl,de       ; YA TENEMOS EL VALOR DE LA FILA SUMADO
sumacoluatt_buffer:
        ld     a,[last_p]  ; COLU
        ld     e,a
        ld     d,0
        add    hl,de       ; SUMO LA COLUMNA	Salida hl= direccion en el buffer de atributos.
	ld	a,[fila]
	ld	[last_p+1],a
	ld	a,[colu]
	ld	[last_p],a
	ret