;----------------------------------------------------------------------------------------
; BUSQUEDA DE PANTALLA E IMPRESION DE DATOS
;----------------------------------------------------------------------------------------
search_scr:
           ld   a,[panta]
           ld   hl,tabpant    ; DATOS DE LAS DIRECCIONES DONDE ESTAN LAS PANTALLAS.
           add  a,a
           add  a,l
           ld   l,a
           ld   e,[hl]
           inc  hl
           ld   d,[hl]
; Dejo DE en HL= DIR.PANTALLA A IMPRIMIR  dentro de dataspantCo.asm
	   ex   de,hl         
;VALORES SIGUIENT.SERAN LOS TILES QUE COMPONEN LA PANT.Y SU POSC.X en char E Y en scannes
           inc  hl
a_frm:
           ld   a,[hl]
           cp   $ff           ; 2/7 
           ret  z             ; fin rutina al encontrar 255
	   ld   [num_tile],a  ; GUARDAMOS EL TILE QUE QUEREMOS IMPRIMIR (antes ve_tile)
           inc  hl
           ld   a,[hl]
           ld   [fil_dat],a
           inc  hl
           ld   a,[hl]
           ld   [col_dat],a   ; Guardamos la fila en scann y la colu en char del tile
           inc  hl
           push hl            ; GUARDAMOS POSIC.HL QUE APUNTA AL SIGUIENTE TILE A BUSCAR
           call search_dat    ; BUSCAMOS EL TILE A INYECT.EN BUFF_PANT en TABLA DE DATOS.
;
; CALC.DIRECCION EN el BUFFER e inyectamos el tile y su att.
;
	   call c_dirbufftile ; CalcTileDirBuffeInyectile.asm
           pop  hl            ; SACAMOS EL VALOR DEL SIGUIENTE TILE A IMPRIMIR Y SEGUIMOS
           jr   a_frm
;----------------------------------------------------------------------------------------
; SUBRUTINA DE BUSQUEDA DE LOS DATOS QUE COMPONEN LAS PANTALLAS
;----------------------------------------------------------------------------------------
search_dat:
           ld   de,tab_dir_tiles
	   ld	a,[num_tile]
	   add	a,a
	   ld	l,a
	   ld	h,0
	   add	hl,de
	   ld	e,[hl]
	   inc	hl
	   ld	d,[hl]
;encontrado:
           inc  de
           ld   a,[de]         ; ALTO
           ld   [al_tile],a    ; ALTURA DEL TILE EN SCANNES PARA HACER LDIR
           inc  de
           ld   a,[de]         ; ancho
           ld   [an_tile],a    ; ANCHURA DEL TILE EN CARAS PARA HACER LDIR
           inc  de
           ld    [poti_im],de   ; tarda 6/20 mucho menos que lo anterior
           ret
