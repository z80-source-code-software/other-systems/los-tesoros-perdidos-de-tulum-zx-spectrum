;----------------------------------------------------------------------------------------
; Gestion y tabla de la luz de la pantalla. Donde ir�s a reponer la tuya.
;----------------------------------------------------------------------------------------
; 34462


LA_LUZ_DE_PANTALLA:
;	db 0,1,2,56,1,4,3,3,8,1,0,$2c,$7e,$2c,$7e, 2, 72,  0,  0,0, 0, 0, 0,  0, 0, 0
	ds 26
; panta, activo, posxChar,  posyScan, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	    ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9

; NOUSO, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni,	coluprota,	filaprota
; ix10,	 ix11,	    ix12,       ix13,	     ix14,	        ix15,   	ix16

; NOUSO, NOUSO,		NOUSO, NOUSO,	dblowPant, dbhighPant, dblowBuf,    dbhighBuf, Choque
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24	ix25

; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24, 25

;
; A la entrada en la pantalla correspondiente se llama a esta rutina para meter los datos que faltan arriba
;

; Calculo los datos ix22 a 24 y pasamos los datos a las variables colu y fila del prota y de los enemigos.

Calculo_de_luz_de_pantalla:
; Antes meter una rutina que traiga los valores guardados de cada pantalla y meterlos en ix correspondiente
	push	ix
	ld	ix,LA_LUZ_DE_PANTALLA
	call	avolcamiento_enem	; Sacamos los valores para ix+21, +22, +23, +24
; traspaso valores para COLU Y FILA 
	ld	a,[ix+15]		; COLU
	ld	[colu],A		
        ld	[last_p],A
	ld	b,a
	ld	a,[ix+16]		; FILA
        ld	[fila],A	
        ld	[last_p+1],A
	ld	c,a
; traspaso valores a la tabla de enemigos.
	call	creadatenem
	pop	ix
	ret

; 34518
; Rutina de iluminaci�n de la luz en pantalla y los tiles de encima. Uso al inicio del nivel y cu�ndo se apaga la luz por la llave
;
Imp_attr_de_la_luz_de_pantalla:
	push	ix
	xor	a			; acarreo anulado
	ld	ix,LA_LUZ_DE_PANTALLA
	ld	l,[ix+21]		; 
	ld	h,[ix+22]		; db's de la direc.en pantalla
	call	c_dirAtr_enem		; esta en SprEnemImp.asm 
	dec	l
	push	hl
	pop	de			; de = valor en el primer tile de arriba de la luz a colorear
	call	c_dirbuffAtr_enem	; esta en SprEnemImp.asm
; Pintamos. hl contiene el valor en el buffer del primer tile de la luz del nivel
	dec	l			
	ldi				
	ldi
	ldi				; traspaso de los 3 colores del buffer en su posic.en pantalla
; Decremento hl y de con 35 para situarnos en la fila anterior.
	ld	bc,35			; 
	sbc	hl,bc			; 
	ex	de,hl
	sbc	hl,bc
	ex	de,hl
	ldi
	ldi
	ldi				; traspaso de los 3 colores del buffer en su posic.en pantalla
	pop	ix
	ret

;
; Gesti�n de la luz que hay en el nivel. Donde repones la tuya !!!
;
Gest_luz_de_pantalla:
	push	hl		; 	42799
	push	ix
	ld	ix,LA_LUZ_DE_PANTALLA
;
; Ver si la luz est� activa. Habr� pantallas en las que NO. Y tendr�s que guardar la que lleves sin lanzarla
;
	ld	a,[ix+1]
	and	a
	jp	z,bimprimellamapantalla		; Si 0 no hay llama donde reponer la luz en la pantalla.
;
; contador de ticks
;
mirarticksllamapantalla:	
	dec	[ix+4]
	jr	nz,imprimellamapantalla
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
	call	vuelcafondosprenem	; vuelco el fondo de la luz SprEnemVolc.asm
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+7]	; framesTotal
	ld	a,[ix+6]	; frameActual
	cp	b
	jr	z,ponframellamapantalla_0
;
; Incremento el valor del frame
;
	inc	[ix+6]
;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;
	ld	bc,16		; El tama�o es ix+10 como es 1 char 1x1x8 x 2
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
	jr	imprimellamapantalla
ponframellamapantalla_0:	
	ld	[ix+6],0	; frame a 0
	ld	a,[ix+13]	; Inicializo los frames 
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
;
; Imprimo la llama. Pongo en hl el valor del sprite a imprimir
;
imprimellamapantalla:
	call	Antes_imp_ene	; (cambiar el valor del ancho antes de imp_ene en Sprenemimp.asm )
;
; Detectar que el prota est� tocando la llama para activar su vela.
;
	; si prota = vela entonces ver
		; si DISPARO_LUZ ---> IX+1 = 0 ENTONCES NO COGER VELA
			; ELSE SI COGER VELA Y ENTONCES IX+1 PONER A 0 Y ACTIVAR LUZ DEL PROTA
	ld	b,[ix+2]	;columna de la vela
	ld	a,[colu]
	cp	b
	jr	z,verfilallama
	inc	a
	cp	b
	jp	nz,bimprimellamapantalla
verfilallama:
	ld	a,[fila]
	ld	b,[ix+3]
	cp	b
	jp	nz,bimprimellamapantalla
;Estas tocando la llama. Mira si el disparo de luz ha acabado.
	push	ix
	ld	ix,DISPARO_LUZ	;luzgestytab.asm
	ld	a,[ix+1]	; si 1= no ha llegado al limite y no puedes coger denuevo la luz
	rra
	jp	c,abimprimellamapantalla
; la luz se apag� al chocar con una pared y puedes volver a cogerla

; M�sica / Efecto reponer luz
	ld	a,[portaluz]
	and	a
	jr	nz,saltaponersoreluz	; si llevas la luz no restaures el marcador, ni pongas efectos
	ld	a,6
	ld	[paraponerenB+1],a
	call	llamar_efecto
	call	restauracolormarcabajo		; restattrmbajo.asm
saltaponersoreluz:
	ld	[ix+1],0
	xor	a
	ld	[latigo],a		; Desactivo el l�tigo.
	ld	[activ_eyes],a		; Desactivo los ojos
	inc	a
	ld	[portaluz],a		; Coges la luz denuevo 
	ld	hl,iluminar		; Activo en el engine la iluminaci�n alrededor del prota
	ld	[verIluminacion+1],hl	
; Al llevar luz no debes tener activo el mirar en la oscuridad.
	ld	hl,nomiraroscuridad
	ld	[mirarenlaoscuridad+1],hl
	ld	hl,mirarllama		; Activo la llamada en el engine
	ld	[vermirarllama+1],hl	; para imprimir la llama del marcador
abimprimellamapantalla:
	pop	ix
bimprimellamapantalla:
	pop	ix
	pop	hl
	ret

