;----------------------------------------------------------------------------------------
; Tabla de localizaci�n de V�rtices por pantalla
;
; pantalla
; tabla con : 0, las direcciones Y X de los vertices en scann y char
; n�vertice, sentido + vertice adyacente en ese sentido, peso o distancia entre vertices
; Vertice fijado o definitivo
;
;
; NECESITO 154 BYTES POR PANTALLA
;
;----------------------------------------------------------------------------------------



TabVerticesPant:				; 28020

db	0			; Pantalla 0
;		ILUMINADO(SI/NO), Y, X				
	db		0,72,3			;(0)
	db		0,72,8			;(1)
	db		0,72,10			;(2)
	db		0,72,12			;(3)
	db		0,72,18			;(4)
	db		0,72,20			;(5)	0,72,13 salto m�s largo
	db		0,88,21			;(6)
	db		0,88,22			;(7)
	db		0,104,23		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			5
db			$20,			0
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$12,			2
db			$21,			0
db			$30,			5
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$22,			0
db			$31,			2
db			$43,			2
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$14,			6
db			$23,			0
db			$33,			0
db			$43,			0
db			$52,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$15,			2
db			$24,			0
db			$33,			6
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			1
db			$25,			0
db			$34,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$17,			1
db			$26,			0
db			$36,			0
db			$46,			0
db			$55,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			1
db			$27,			0
db			$36,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$38,			0
db			$48,			0
db			$57,			1

; V�rtice fijado
db	0





db	1			; Pantalla 1
;		ILUMINADO(SI/NO), Y, X				
	db		0,64,2			;(0)
	db		0,64,6			;(1)
	db		0,64,11			;(2)
	db		0,80,12			;(3)
	db		0,80,13			;(4)
	db		0,96,14			;(5)	0,72,13 salto m�s largo
	db		0,96,20			;(6)
	db		0,88,21			;(7)
	db		0,80,23		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$10,			0
db			$20,			0
db			$38,			100
db			$41,			2
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$12,			5
db			$21,			0
db			$31,			0
db			$41,			0
db			$50,			2

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$13,			1
db			$22,			0
db			$31,			5
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$14,			1
db			$23,			0
db			$33,			0
db			$43,			0
db			$52,			1
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$15,			1
db			$24,			0
db			$33,			1
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			6
db			$25,			0
db			$35,			0
db			$45,			0
db			$54,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$35,			6
db			$47,			1
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$36,			1
db			$48,			1
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$10,			100
db			$28,			0
db			$37,			1
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0




db	2		; PANTALLA 2

;		ILUM, y,x
	db	0, 104, 6		;(0)
	db	0, 112, 6		;(1)
	db	0, 120, 6		;(2)
	db	0, 136, 2		;(3)
	db	0, 136, 6		;(4)
	db	0, 136,22		;(5)
	db	0, 120,24		;(6)
	db	0, 120,25		;(7)
	db	0, 136,26		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$10,			0
db			$21,			1
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$00,			1
db			$11,			0
db			$22,			1
db			$31,			0
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$01,			1
db			$12,			0
db			$24,			4
db			$32,			0
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$14,			4
db			$23,			0
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$02,			4
db			$15,			16
db			$24,			0
db			$33,			4
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$25,			0
db			$34,			16
db			$46,			1
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$17,			1
db			$26,			0
db			$35,			1
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			2
db			$27,			0
db			$36,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$38,			0
db			$48,			0
db			$57,			2

; V�rtice fijado
db	0






db	3
;		ILUM, y,x
	db	0,  64,24		;(0)
	db	0,  88, 7		;(1) 64,6
	db	0, 136,15		;(2)
	db	0, 136, 2		;(3)
	db	0, 120, 1		;(4)
	db	0, 104, 3		;(5)
	db	0, 104, 5		;(6)
	db	0, 136,28		;(7)
	db	0,  96,20		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$10,			0
db			$20,			0
db			$31,			18
db			$48,			2
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$10,			18
db			$21,			0
db			$36,			2
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$17,			13
db			$22,			0
db			$33,			13
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$12,			13
db			$23,			0
db			$33,			0
db			$43,			0
db			$54,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$13,			2
db			$24,			0
db			$34,			0
db			$45,			2
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			2
db			$25,			0
db			$34,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$35,			2
db			$41,			2
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$32,			13
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$30,			2
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0





db	4

;		ILUM, y,x
	db	0, 136, 8		;(0)
	db	0, 136,15		;(1)
	db	0, 136,22		;(2)
	db	0, 136,28		;(3)
	db	0, 136, 2		;(4)
	db	0, 120, 1		;(5)
	db	0, 120,29		;(6)
	db	0, 136, 5		;(7)
	db	0, 136,25		;(8)



; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			7
db			$20,			0
db			$37,			3
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$12,			7
db			$21,			0
db			$30,			7
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$18,			3
db			$22,			0
db			$31,			7
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$23,			0
db			$38,			3
db			$46,			2
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$17,			3
db			$24,			0
db			$34,			0
db			$44,			0	
db			$55,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$14,			2
db			$25,			0
db			$35,			0
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$33,			2	
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$10,			3
db			$27,			0
db			$34,			3
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$13,			3
db			$28,			0
db			$32,			3
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0







db	5

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 1		;(0)
	db		0,136,24		;(1)
	db		0,120,24		;(2)
	db		0,104,22		;(3)
	db		0,104,16		;(4)
	db		0,104, 3		;(5)
	db		0, 88,16		;(6)
	db		0, 72,14		;(7)
	db		0, 72, 6		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			23
db			$20,			0
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$02,			2
db			$11,			0
db			$21,			0
db			$30,			23
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$21,			2
db			$32,			0
db			$42,			0
db			$53,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$12,			2
db			$23,			0
db			$34,			6
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$06,			2
db			$13,			6
db			$24,			0
db			$35,			13
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$14,			13
db			$25,			0
db			$35,			0
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$24,			2
db			$36,			0
db			$46,			0
db			$57,			2

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$16,			2
db			$27,			0
db			$38,			8
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$17,			8
db			$28,			0
db			$38,			0
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0







db	6

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 1		;(0)
	db		0,136,24		;(1)
	db		0,128,24		;(2)
	db		0,112,26		;(3)
	db		0,104,23		;(4)
	db		0,104, 6		;(5)
	db		0, 96, 6		;(6)
	db		0, 72, 8		;(7)
	db		0, 72,15		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			23
db			$20,			0
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$02,			1
db			$11,			0
db			$21,			0
db			$30,			23
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$21,			1
db			$32,			0
db			$43,			2
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$23,			0
db			$32,			2
db			$43,			0
db			$54,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$14,			0
db			$24,			0
db			$35,			17
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$06,			1
db			$14,			17
db			$25,			0
db			$35,			0
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$25,			1
db			$36,			0
db			$47,			2
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			7
db			$27,			0
db			$36,			2
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$37,			7
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0








db	7	; copia de la seis que no uso. Ajustar solo por probar.

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 2		;(0)
	db		0,136, 8		;(1)
	db		0, 64, 8		;(2)
	db		0, 48, 9		;(3)
	db		0, 48,15		;(4)
	db		0, 48,17		;(5)
	db		0, 48,22		;(6)
	db		0,136,15		;(7)
	db		0,136,27		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			6
db			$20,			0
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$02,			9
db			$17,			7
db			$21,			0
db			$30,			6
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$21,			9
db			$32,			0
db			$43,			1
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$14,			6
db			$23,			0
db			$32,			1
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$14,			0
db			$24,			0
db			$33,			6
db			$45,			2
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			5
db			$25,			0
db			$35,			0
db			$45,			0
db			$54,			2

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$35,			5
db			$48,			11
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			12
db			$27,			0
db			$31,			7
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$37,			12
db			$46,			11
db			$58,			0

; V�rtice fijado
db	0






db	8			; Pantalla 8

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,3			;(0)
	db		0, 64,3			;(1)
	db		0,136,9			;(2)
	db		0, 88,9			;(3)
	db		0,136,15		;(4)
	db		0,104,15		;(5)	0,72,13 salto m�s largo
	db		0,136,22		;(6)
	db		0,136,29		;(7)
	db		0, 88,17		;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$01,			9
db			$12,			6
db			$20,			0
db			$30,			0
db			$48,			20
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$20,			9
db			$31,			0
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$03,			6
db			$14,			6
db			$22,			0
db			$30,			6
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$22,			6
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$05,			4
db			$16,			7
db			$24,			0
db			$32,			6
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$24,			4
db			$35,			0
db			$48,			2
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$17,			7
db			$26,			0
db			$34,			7
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$36,			7
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$10,			20
db			$28,			0
db			$35,			2
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0







db	9			; Pantalla 9

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,21			;(0)
	db		0,112,21			;(1)
	db		0,136,10			;(2)
	db		0,120,10			;(3)
	db		0,104, 9			;(4)
	db		0,104, 5			;(5)	0,72,13 salto m�s largo
	db		0, 88, 5			;(6)
	db		0, 72, 6			;(7)
	db		0, 72,28			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$01,			3
db			$10,			0
db			$20,			0
db			$32,			11
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$20,			3
db			$31,			0
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$03,			2
db			$10,			11
db			$22,			0
db			$32,			0
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$22,			2
db			$33,			0
db			$43,			0
db			$54,			1
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$13,			1
db			$24,			0
db			$35,			4
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$06,			2
db			$14,			4
db			$25,			0
db			$35,			0
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$25,			2
db			$36,			0
db			$47,			1
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			22
db			$27,			0
db			$36,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$37,			22
db			$48,			0
db			$50,			100

; V�rtice fijado
db	0






db	10			; Pantalla 10

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,18			;(0)
	db		0,136,28			;(1)
	db		0, 88,18			;(2)
	db		0,136, 7			;(3)
	db		0,136, 3			;(4)
	db		0, 72, 7			;(5)	0,72,13 salto m�s largo
	db		0,120,29			;(6)
	db		0,112,28			;(7)
	db		0, 96,28			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$02,			6
db			$11,			10
db			$20,			0
db			$33,			11
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$21,			0
db			$30,			10
db			$46,			1
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$20,			6
db			$32,			0
db			$48,			1
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$05,			8
db			$10,			11
db			$23,			0
db			$34,			4
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$13,			4
db			$24,			0
db			$34,			0
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$23,			8
db			$35,			0
db			$45,			0
db			$57,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$31,			1
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$15,			1
db			$27,			0
db			$37,			0
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$32,			1
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0








db	11			; COPIA PANTALLA 10. NO VALE. FASEBONUS

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,18			;(0)
	db		0,136,28			;(1)
	db		0, 88,18			;(2)
	db		0,136, 7			;(3)
	db		0,136, 3			;(4)
	db		0, 72, 7			;(5)	0,72,13 salto m�s largo
	db		0,120,29			;(6)
	db		0,112,28			;(7)
	db		0, 96,28			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$02,			6
db			$11,			10
db			$20,			0
db			$33,			11
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$21,			0
db			$30,			10
db			$46,			1
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$20,			6
db			$32,			0
db			$48,			1
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$05,			8
db			$10,			11
db			$23,			0
db			$34,			4
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$13,			4
db			$24,			0
db			$34,			0
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$23,			8
db			$35,			0
db			$45,			0
db			$57,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$31,			1
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$15,			1
db			$27,			0
db			$37,			0
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$32,			1
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0






db	12			; Pantalla 12

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 4			;(0)
	db		0,136,27			;(1)
	db		0,120,28			;(2)
	db		0, 88,28			;(3)
	db		0,104,26			;(4)
	db		0,104,17			;(5)	0,72,13 salto m�s largo
	db		0, 80,17			;(6)
	db		0,104, 4			;(7)
	db		0, 96, 3			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			23
db			$20,			0
db			$30,			0
db			$40,			0
db			$58,			100

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$21,			0
db			$30,			23
db			$42,			1
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$03,			4
db			$12,			0
db			$22,			0
db			$31,			1
db			$42,			0
db			$54,			1
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$22,			4
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$12,			1
db			$24,			0
db			$35,			9
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$06,			3
db			$14,			9
db			$25,			0
db			$37,			10
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$25,			3
db			$36,			0
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$15,			10
db			$27,			0
db			$37,			0
db			$47,			0
db			$58,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$00,			100
db			$17,			1
db			$28,			0
db			$38,			0
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0






db	13			; Pantalla 13

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 7			;(0)
	db		0,104, 7			;(1)
	db		0,136,12			;(2)
	db		0, 72,12			;(3)
	db		0,136,29			;(4)
	db		0, 72,29			;(5)	0,72,13 salto m�s largo
	db		0,136,23			;(6)
	db		0,120,23			;(7)
	db		0,104,22			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$01,			3
db			$12,			5
db			$20,			0
db			$38,			100
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$20,			3
db			$31,			0
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$03,			8
db			$16,			11
db			$22,			0
db			$30,			5
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$22,			8
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$05,			8
db			$14,			0
db			$24,			0
db			$36,			6
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$24,			8
db			$35,			0
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$07,			2
db			$14,			6
db			$26,			0
db			$32,			11
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$26,			2
db			$37,			0
db			$47,			0
db			$58,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$17,			1
db			$28,			0
db			$30,			100
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0






db	14			; Pantalla 14

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,15			;(0)
	db		0, 80,15			;(1)
	db		0,136,21			;(2)
	db		0,104,21			;(3)
	db		0,136,25			;(4)
	db		0,112,25			;(5)	0,72,13 salto m�s largo
	db		0, 72,16			;(6)
	db		0,104,26			;(7)
	db		0,104,28			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$01,			7
db			$12,			6
db			$20,			0
db			$30,			0
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$20,			7
db			$31,			0
db			$46,			1
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$03,			4
db			$14,			4
db			$22,			0
db			$30,			6
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$22,			4
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$05,			3
db			$14,			0
db			$24,			0
db			$32,			4
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$24,			3
db			$35,			0
db			$47,			1
db			$58,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$31,			1
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$35,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$15,			1
db			$28,			0
db			$38,			0
db			$48,			0
db			$50,			100

; V�rtice fijado
db	0







db	15			; Pantalla 15

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,19			;(0)
	db		0,120,19			;(1)
	db		0,104,19			;(2)
	db		0,136,21			;(3)
	db		0,136,23			;(4)
	db		0,136,26			;(5)	0,72,13 salto m�s largo
	db		0,136,29			;(6)
	db		0,112,29			;(7)
	db		0, 88,29			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$01,			2
db			$13,			2
db			$20,			0
db			$30,			0
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$02,			2
db			$11,			0
db			$20,			2
db			$31,			0
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$21,			2
db			$32,			0
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$14,			2
db			$23,			0
db			$30,			2
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$15,			3
db			$24,			0
db			$33,			2
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			3
db			$25,			0
db			$34,			3
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$07,			3
db			$16,			0
db			$26,			0
db			$35,			3
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$08,			3
db			$17,			0
db			$26,			3
db			$37,			0
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$27,			3
db			$38,			0
db			$40,			100
db			$58,			0

; V�rtice fijado
db	0








db	16			; Pantalla 16

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,25			;(0)
	db		0,136,27			;(1)
	db		0,136,29			;(2)
	db		0,120,27			;(3)
	db		0,104,27			;(4)
	db		0, 96,27			;(5)	
	db		0, 80,28			;(6)
	db		0, 72,29			;(7)
	db		0,136,28			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			2
db			$20,			0
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$03,			2
db			$18,			1
db			$21,			0
db			$30,			2
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$22,			0
db			$38,			1
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$04,			2
db			$13,			0
db			$21,			2
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$05,			1
db			$14,			0
db			$23,			2
db			$34,			0
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$24,			1
db			$35,			0
db			$46,			1
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$35,			1
db			$47,			1
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$36,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$12,			1
db			$28,			0
db			$31,			1
db			$48,			0
db			$58,			0

; V�rtice fijado
db	0







db	17			; Pantalla 17		FASE BONUS

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 3			;(0)
	db		0,136, 5			;(1)
	db		0,136,23			;(2)
	db		0,120, 5			;(3)
	db		0,120,24			;(4)
	db		0,104, 4			;(5)	
	db		0, 64, 2			;(6)
	db		0,112,25			;(7)
	db		0,104, 2			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			2
db			$20,			0
db			$30,			0
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$03,			2
db			$12,			18
db			$21,			0
db			$30,			2
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$22,			0
db			$31,			18
db			$44,			1
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$21,			2
db			$33,			0
db			$43,			0
db			$55,			1
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$17,			1
db			$24,			0
db			$32,			1
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$13,			1
db			$25,			0
db			$38,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$28,			5
db			$36,			0
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$34,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$06,			5
db			$15,			2
db			$28,			0
db			$38,			0
db			$40,			100
db			$58,			0

; V�rtice fijado
db	0







db	18			; Pantalla 18		NO ES	FASE BONUS  PERO NO HABR�N ENEMIGOS

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 3			;(0)
	db		0,136, 5			;(1)
	db		0,136,23			;(2)
	db		0,120, 5			;(3)
	db		0,120,24			;(4)
	db		0,104, 4			;(5)	
	db		0, 64, 2			;(6)
	db		0,112,25			;(7)
	db		0,104, 2			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			2
db			$20,			0
db			$30,			0
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$03,			2
db			$12,			18
db			$21,			0
db			$30,			2
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$22,			0
db			$31,			18
db			$44,			1
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$21,			2
db			$33,			0
db			$43,			0
db			$55,			1
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$17,			1
db			$24,			0
db			$32,			1
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$13,			1
db			$25,			0
db			$38,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$28,			5
db			$36,			0
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$34,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$06,			5
db			$15,			2
db			$28,			0
db			$38,			0
db			$40,			100
db			$58,			0

; V�rtice fijado
db	0







db	19			; Pantalla 17	 FASE BONUS

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 3			;(0)
	db		0,136, 5			;(1)
	db		0,136,23			;(2)
	db		0,120, 5			;(3)
	db		0,120,24			;(4)
	db		0,104, 4			;(5)	
	db		0, 64, 2			;(6)
	db		0,112,25			;(7)
	db		0,104, 2			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			2
db			$20,			0
db			$30,			0
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$03,			2
db			$12,			18
db			$21,			0
db			$30,			2
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$22,			0
db			$31,			18
db			$44,			1
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$21,			2
db			$33,			0
db			$43,			0
db			$55,			1
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$17,			1
db			$24,			0
db			$32,			1
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$13,			1
db			$25,			0
db			$38,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$28,			5
db			$36,			0
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$27,			0
db			$34,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$06,			5
db			$15,			2
db			$28,			0
db			$38,			0
db			$40,			100
db			$58,			0

; V�rtice fijado
db	0







db	20			; Pantalla 20

;		ILUMINADO(SI/NO), Y, X				
	db		0,136, 5			;(0)
	db		0,136, 3			;(1)
	db		0,136, 1			;(2)
	db		0,120, 3			;(3)
	db		0,104, 3			;(4)
	db		0, 96, 3			;(5)	
	db		0, 80, 2			;(6)
	db		0, 72, 1			;(7)
	db		0, 72, 3			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$10,			0
db			$20,			0
db			$31,			2
db			$40,			0
db			$58,			100

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$03,			2
db			$10,			2
db			$21,			0
db			$32,			2
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$11,			2
db			$22,			0
db			$32,			0
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$04,			2
db			$13,			0
db			$21,			2
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$05,			1
db			$14,			0
db			$23,			2
db			$34,			0
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$15,			0
db			$24,			1
db			$35,			0
db			$45,			0
db			$56,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$15,			1
db			$26,			0
db			$36,			0
db			$46,			0
db			$57,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$16,			1
db			$27,			0
db			$37,			0
db			$48,			2
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$37,			2
db			$40,			100
db			$58,			0

; V�rtice fijado
db	0





db	21			; Pantalla 21

;		ILUMINADO(SI/NO), Y, X				
	db		0, 48, 4			;(0)
	db		0, 64, 5			;(1)
	db		0, 72, 5			;(2)
	db		0, 88, 1			;(3)
	db		0, 96, 3			;(4)
	db		0,136, 3			;(5)	
	db		0,136, 1			;(6)
	db		0,136, 8			;(7)
	db		0,136,13			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			1
db			$20,			0
db			$30,			0
db			$48,			100
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$22,			1
db			$31,			0
db			$41,			0
db			$50,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$01,			1
db			$12,			0
db			$22,			0
db			$33,			3
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$02,			3
db			$13,			0
db			$23,			0
db			$36,			7
db			$44,			2
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$14,			0
db			$25,			5
db			$34,			0
db			$44,			0
db			$53,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$04,			5
db			$17,			5
db			$25,			0
db			$36,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$03,			7
db			$15,			2
db			$26,			0
db			$36,			0
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			5
db			$27,			0
db			$35,			5
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$37,			5
db			$48,			0
db			$50,			100

; V�rtice fijado
db	0






db	22			; Pantalla 22

;		ILUMINADO(SI/NO), Y, X				
	db		0,136,17			;(0)
	db		0,136,15			;(1)
	db		0, 88,15			;(2)
	db		0,136, 2			;(3)
	db		0, 96, 2			;(4)
	db		0, 80, 3			;(5)	
	db		0, 80, 5			;(6)
	db		0, 64, 5			;(7)
	db		0, 48, 4			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$10,			0
db			$20,			0
db			$31,			2
db			$40,			0
db			$58,			100

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$02,			6
db			$10,			2
db			$21,			0
db			$33,			13
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$21,			6
db			$32,			0
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$04,			5
db			$11,			13
db			$23,			0
db			$33,			0
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$14,			0
db			$23,			5
db			$34,			0
db			$45,			1
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			2
db			$25,			0
db			$34,			1
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$07,			2
db			$16,			0
db			$26,			0
db			$35,			2
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$26,			2
db			$37,			0
db			$47,			0
db			$58,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$17,			1
db			$28,			0
db			$38,			0
db			$40,			100
db			$58,			0

; V�rtice fijado
db	0







db	23			; Pantalla 23

;		ILUMINADO(SI/NO), Y, X				
	db		0, 56, 2			;(0)
	db		0, 72, 1			;(1)
	db		0,136, 1			;(2)
	db		0,136, 7			;(3)
	db		0,120, 7			;(4)
;	db		0,104, 8			;(5)
;	db		0,104, 9			;(6)
;	db		0,104,11			;(7)
;	db		0,104,12			;(8)
	db		0,136,13			;(5)
	db		0,120,13			;(6)
	db		0,136,18			;(7)
	db		0,136,25			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$10,			0
db			$20,			0
db			$31,			1
db			$40,			0
db			$58,			100

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$22,			8
db			$31,			0
db			$40,			1
db			$51,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$01,			8
db			$13,			6
db			$22,			0
db			$32,			0
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$04,			2
db			$15,			6
db			$23,			0
db			$32,			6
db			$43,			0
db			$53,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$14,			0
db			$23,			2
db			$34,			0
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$06,			2
db			$17,			5
db			$25,			0
db			$33,			6
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$25,			2
db			$36,			0
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			7
db			$27,			0
db			$35,			5
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$37,			7
db			$48,			0
db			$50,			100

; V�rtice fijado
db	0








db	24			; Pantalla 24

;		ILUMINADO(SI/NO), Y, X				
	db		0, 48, 8			;(0)
	db		0, 64, 9		;(1)
	db		0, 80, 9			;(2)
	db		0, 96,11			;(3)
	db		0,136,11			;(4)
	db		0,136,20			;(5)	
	db		0,136,29			;(6)
	db		0,128,20			;(7)
	db		0, 80, 6			;(8)

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			1
db			$20,			0
db			$30,			0
db			$48,			10
db			$50,			0

; V�rtice fijado
db	0

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$11,			0
db			$22,			2
db			$31,			0
db			$41,			0
db			$50,			1

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$01,			2
db			$13,			2
db			$22,			0
db			$38,			3
db			$42,			0
db			$52,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$13,			0
db			$24,			5
db			$33,			0
db			$43,			0
db			$52,			2
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$03,			5
db			$15,			9
db			$24,			0
db			$34,			0
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$07,			2
db			$16,			9
db			$25,			0
db			$34,			9
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$16,			0
db			$26,			0
db			$35,			9
db			$46,			0
db			$56,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$17,			0
db			$25,			2
db			$37,			0
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$12,			3
db			$28,			0
db			$38,			0
db			$48,			0
db			$50,			10

; V�rtice fijado
db	0
