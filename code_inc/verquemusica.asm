
	
quemusicapongo:	
	ld	hl,tabmusicas
	ld	a,[panta]
	add	a,a
	add	a,l
	ld	l,a
	inc	l
	ld	a,[hl]
;bajoquemusica:
	ld	[poncancionenA+1],a	; Valor de A en la etiqueta de rucontroles
	call	llamar_musica		; En rucontroles.asm
	ret

; hacer una tabla con las pantallas y los temas	
tabmusicas:
	db	0,1
	db	1,1
	db	2,1
	db	3,1
	db	4,1
	db	5,1
	db	6,3	; fase bonus
	db	7,1	
	db	8,1
	db	9,1
	db	10,1
	db	11,3	; fase bonus
	db	12,2
	db	13,2
	db	14,2
	db	15,3	; fase bonus
	db	16,2
	db	17,2	;
	db	18,2	; sin enemigos
	db	19,3	; fase bonus
	db	20,2	
	db	21,2
	db	22,2
	db	23,2
	db	24,2