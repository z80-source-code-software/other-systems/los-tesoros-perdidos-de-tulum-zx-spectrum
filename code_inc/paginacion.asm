;-------------------------------------------------------------------------------------------
; Rutina de utopian
;
; Entrada:
;   B: p�gina de RAM a poner

setrambank:
	ld	a,[23388]	; variable de sistema con el valor anterior
	and	$f8             ; mantener los bits altos (son la ROM y otros)
	or	b               ; poner la p�gina de RAM
	ld	bc,$7ffd        ; puerto para escribir
	ld      [23388],a       ; actualizamos la variable de sistema
	out     (c),a           ; y p'alante
	ret
