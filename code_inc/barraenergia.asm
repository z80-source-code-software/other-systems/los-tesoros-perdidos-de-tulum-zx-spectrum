;--------------------------------------------------------------------------------------------------------
;Rutina de borrado de barra de energ�a
;--------------------------------------------------------------------------------------------------------
; Tama�o barra de energia desde 24,4 a 24,27 en chars. total 23 chars de energ�a.
; Inicio barra #407B final (muerte) en #4063

quita_energia:
		ld	a,[desconener]
		and	a
		jr	z,da_o_1	; Si 0 te has chocado con un enemigo
;Si tocas los pinchos
		ld	b,2		; Da�o 2
		xor	a
		ld	[desconener],a
		jr	bucleda_o
;Si tocas a los enemigos:
da_o_1:
		ld	b,1
bucleda_o:
		call	desp_un_pixel	; Da�o 1
		call	desp_un_pixel	; Da�o 1
		ld	hl,offbarra
		dec	[hl]
		call	z,bofb
		djnz	bucleda_o
		ret
bofb:
		ld	a,8
		ld	[offbarra],a
		ld	hl,[barra]	; posic. del char de la barra actual
		dec	l
                ld	a,l
                cp	$63		; Si la barra ha llegado al final $4064
                jr	z,bbofb
                ld	[barra],a
                ld	a,h
                ld	[barra+1],a
                ret
bbofb:
;
; Activamos la muerte del protagonista.
;
		xor	a
                ld	[vidas],a       ; Vida = 0. Tas morio
                ret
desp_un_pixel:
		push	hl
		ld	hl,[barra]	; posic. del char de la barra actual
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		sla	[hl]
		inc	h
		pop	hl
		ret
;--------------------------------------------------------------------------------------------------------
; Rutina de aumento de barra de energ�a al coger las gemas
;--------------------------------------------------------------------------------------------------------
Aumentador_Energia:
		ld	a,8
		ld	[offbarra],a
Paumentar_Energia:
		ld	b,4	; que son los char's que se aumentan al recoger seg�n que objetos.
		ld	de,[barra]
asiaumen_tando:
		ld	hl,tab_aum_barra
		push	bc
		push	de
		ld	b,8
siaumen_tando:
		ld	a,e
		cp	$7c		; m�xima energia de la barra
		jr	z,finaumentador

		ld	a,[hl]
		ld	[de],a
		inc	d
		inc	hl
		djnz	siaumen_tando
		pop	de
		pop	bc
		inc	e		; siguiente char a aumentar
		ld	a,e
		ld	[barra],a	; guardo la nueva posicion.
		djnz	asiaumen_tando
		ld	hl,barra
		dec	[hl]		; para ajustar el valor de la barra a lo aumentado
		ret

finaumentador:
		pop	de
		pop	bc
		ld	hl,barra
		dec	[hl]		; para ajustar el valor de la barra a lo aumentado
		ret
;--------------------------------------------------------------------------------------------------------

Colocar_barra_Energia:
		ld	de,$4064	; minima energia
		ld	hl,[barra]
		xor	a
		sbc	hl,de		; valor en la resta de lo que aumentamos para crear la barra de energ�a.
		ld	a,l
		inc	a
		ld	[Paumentar_Energia+1],a
		ex	de,hl
		ld	[barra],hl
		call	Aumentador_Energia
		ld	a,4
		ld	[Paumentar_Energia+1],a
		ret

;--------------------------------------------------------------------------------------------------------

; Rutina que comprueba si la fase que acabas de hacer es una fase especial de bonus
; Tabenemavolcar en Tabenemxpant.asm Indico los enemigos en pantalla las pantallas 6,11,17 y 19 son las fase bonus
Compfasebonus:
		ld	a, [panta]
		cp	6
		jr	z,siesfasebonus
		cp	11
		jr	z,siesfasebonus
		cp	15
		jr	z,siesfasebonus
		cp	19
		ret	nz
;		jr	z,siesfasebonus
;		ret
siesfasebonus:
; Si has terminado una fase bonus, sube la energ�a al m�ximo.
		ld	hl,$407C	
		ld	[barra],hl
		ret





; 
; datos de la barra a imprimir
;
tab_aum_barra:
	DEFB	  0,0,247,247,222,222,0,0
