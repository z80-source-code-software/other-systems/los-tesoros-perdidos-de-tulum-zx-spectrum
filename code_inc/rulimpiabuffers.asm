;----------------------------------------------------------------------------------------
limpiobuffer
; tabla de objetos
	ld	hl,tabla_objetos_enpantalla
	ld	de,tabla_objetos_enpantalla+1
	ld	bc,41		; 53
	ld	[hl],0
	ldir



; vacia el buffer de tiles iluminados en pantalla.
	ld	hl,bufferdeiluminacion		;$aa80 antes $de00
	ld	de,bufferdeiluminacion+1	; $aa81 antes $de01
	ld	[hl],0
	ld	bc,511		
	ldir
; Rutina para limpiar el buffer de pantalla y pintar el buffer de atributos de amarillo 
	ld	hl,buffpant			; $ad00 antes $ed90
	ld	de,buffpant+1			; $ad01 antes $ed91
	ld	[hl],0
	ld	bc,4096		
	ldir
;pintabufferattr:				; $bd00  color de fondo amarillo
        ld	[hl],6
        ld	bc,511
        ldir             
	ret
