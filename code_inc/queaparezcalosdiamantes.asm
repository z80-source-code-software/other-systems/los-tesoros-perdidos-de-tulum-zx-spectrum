;----------------------------------------------------------------------------------------
; EN LA RUTINA COMPOBJETOS.ASM
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	ix4		ix5

; Solo vamos a comprobar si en alg�n momento hay una parte de un diamante iluminado.
Ccomp_objetos_ilum:
	push	ix
	push	hl
	push	de
	push	bc
	call	BCcomp_objetos_ilum
	pop	bc
	pop	de
	pop	hl
	pop	ix
	ret
BCcomp_objetos_ilum
	
	ld	ix,tabla_objetos_enpantalla
bcomp_objetos_ilum:
	ld	a,[ix+0]	; Valor de la pantalla 
	rla	
	ret	c		; si el valor es 128 termina
	ld	a,[ix+1]
	and	a
	jr	z,bnotileilum	; si el diamante no est� activo, no lo mires
	ld	a,[ix+2]
	cp	19
	jr	nz,bnotileilum
; Comp. s�l diamantes. Entrada fila y colu. B y L para llamar a coordficu 
;
	ld	a,[ix+3]	
	ld	l,a		; l= col_tile
	xor	a		; acarreo a 0
	ld	a,[ix+4]
	rra
	rra
	rra
	ld	b,a             ; b= fil_tile en char
	call	coordficu
	push	hl		; hl valor en la videoram ATTR del diamante
	push	hl
	ld	bc,32
	ld	a,[hl]
	and	a
	jr	nz,tileilum
	inc	l
	ld	a,[hl]
	and	a
	jr	nz,tileilum
	pop	hl
	add	hl,bc
	ld	a,[hl]
	and	a
	jr	nz,tileilum
	and	a
	jr	z,notileilum
tileilum:
	ld	a,[ix+0]
	and	a
	jr	z,sitileilum	; si es 0 ese diamante ya ha aparecido antes, no hagas el efecto.
	call	queaparezcadiamante

sitileilum:
	pop	hl		; Alguna parte hay iluminada. Asi que revela el diamante
	ld	[hl],7
	inc	l
	ld	[hl],5
	dec	l
	add	hl,bc
	ld	[hl],5
	inc	l
	ld	[hl],7
	jr	bnotileilum
notileilum:
	pop	hl
bnotileilum:
	ld	de,07
	add	ix,de
	jr	bcomp_objetos_ilum


; Efecto de diamante apareciendo.

queaparezcadiamante:
	ld	[ix+0],0
	ld	hl,delay+1
	ld	[hl],8

	call	delay			; en stagecleared.asm
	ld	a,61			; Valor numtile de la puerta entre abierta.
	call	chgtileenpantadelcofre	; En compobjetos.asm
	call	delay
	ld	a,62
	call	chgtileenpantadelcofre	; En compobjetos.asm
	call	delay
	ld	a,63
	call	chgtileenpantadelcofre	; En compobjetos.asm
	call	delay
	ld	a,19
	call	chgtileenpantadelcofre	; En compobjetos.asm


; Aqui debo poner :
	ld	a,4			; Fx de diamante3 para usar aqu�.
	ld	[paraponerenB+1],a
	call	llamar_efecto

	ld	hl,delay+1
	ld	[hl],200
	ret
	