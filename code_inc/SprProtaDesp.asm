;----------------------------------------------------------------------------------------
; Rutina de desplazamiento de sprite       
; Vamos a desplazar el sprite en un buffer antes de volcarlo con el fondo a pantalla o a otro buffef
;----------------------------------------------------------------------------------------
;							64 bytes que son 2x3x8 + mask
; A la derecha
desp_spr_der:
	xor	a
        ld      hl,buff_des_spr
	ld	b,16
bparsaltoder:
        scf		;1/4	1b
        rr      [hl]	;4/15	2b
        inc     hl	;1/6	1b
        rra		;1/4	1b
        srl     [hl]	;4/15	2b
        inc     hl	;1/6	1b
        rla		;1/4	1b
        rr      [hl]	;4/15	2b
        inc     hl	;1/6	1b
        rra		;1/4	1b
        rr      [hl]	;4/15	2b
        inc     hl	;1/6	1b	;16bx24=384bytes 100tstatesx24=2400tstates
	djnz	bparsaltoder	;3/13 x23 = 299 + 100tstatesx24=2400tstates =2799 + 8 = 2807 tstates
	ret		; dif 2807-2400=407tstates x desplaz. para 3=1221 tst  para 6= 2442 tstates
; A la izqquierda
desp_spr_izq:
	xor	a
	ld      hl,buff_des_spr+63
	ld	b,16
bparsaltoizq:
	sla     [hl]	; sprite3
	dec	hl
	rra		; acarreo sprite3 en bit 7,a
;El flag de acarreo lo activo para mask3
	scf		
	rl	[hl]	; mask3
	dec	hl
	rla		; acarreo mask3 en bit 0,a
;En el flag de acarreo tengo el acarreo del sprite3
	rl	[hl]	; sprite2
	dec	hl
	rra		; acarreo sprite2 en bit 7,a
	rl	[hl]	; mask2
	dec	hl
;En el flag de acarreo tengo el acarreo del sprite2
	djnz	bparsaltoizq
	ret
