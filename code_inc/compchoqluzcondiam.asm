;----------------------------------------------------------------------------------------
; Comprobar si la luz lanzada toca un diamante.
; DE contiene el valor de la luz o de su estela al entrar aqu�.
;
; Salida de la rutina: suma al valor ix+0 de la tabla_objetos_enpantalla el valor 64 si la luz lanzada choca con una parte del diamante. 
locchocoluz:
	push	ix
	push	hl
	push	de
	push	bc
	ld	ix,tabla_objetos_enpantalla	; 
blocchocoluz:
	ld	a,[ix+0]
	cp	128
	jr	z,finrutinalocchocoluz
	bit	6,a
	jr	nz,miraotrolocchocoluz
	ld	a,[ix+2]
	cp	19
	jr	nz,miraotrolocchocoluz
; Es un diamante y no ha sido pintado a�n.
	ld	a,[ix+3]	
	ld	l,a		; l= col_tile
	xor	a		; acarreo a 0
	ld	a,[ix+4]
	rra
	rra
	rra
	ld	b,a             ; b= fil_tile en char
	call	coordficu
	ld	a,h
	cp	d
	jr	nz,miraotrolocchocoluz
	ld	a,l
	cp	e
	jr	z,sihaychocoluz
; vamos a mirar la fila inmediata superior
	ex	de,hl
	ld	bc,32
	xor	a
	sbc	hl,bc
	ex	de,hl
	ld	a,h
	cp	d
	jr	nz,miraotrolocchocoluz
	ld	a,l
	cp	e
	jr	z,sihaychocoluz

miraotrolocchocoluz:
	push	de
	ld	de,07
	add	ix,de
	pop	de
	jr	blocchocoluz
sihaychocoluz:
	ld	a,64
	add	a,[ix+0]
	ld	[ix+0],a
	jr	miraotrolocchocoluz
finrutinalocchocoluz:
	pop	bc
	pop	de
	pop	hl
	pop	ix
	ret

