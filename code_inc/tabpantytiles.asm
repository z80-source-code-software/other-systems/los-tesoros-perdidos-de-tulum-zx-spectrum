;----------------------------------------------------------------------------------------
; Tablas de direcciones del mapeado pantalla y de tiles. Agrupadas por falta de espacio
;----------------------------------------------------------------------------------------


;TABLA DE DIRECCIONES DE SALIDA DE PANTALLA. MAPEADO 
;----------------------------------------------------------------------------------------

;
; Creo todas las direcciones de cada pantalla para el valor de senpro. 0,1,2,3 
; Se necesitan 4 bytes por pantalla lo cual es una locura, pero si hay problema 
; con el espacio--> comprimes la tabla y la descomprimes en el buffer antes y ya est�.
;

TABSEN:					; valor inicial de panta =
	defb 1,1,1,1			; 0
	defb 2,2,2,2			; 1
	defb 3,3,3,3			; 2
	defb 4,4,4,4			; 3
	defb 5,5,5,5			; 4
	defb 6,6,6,6			; 5
	defb 7,7,7,7			; 6
	defb 8,8,8,8			; 7
	defb 9,9,9,9			; 8
	defb 10,10,10,10		; 9
	defb 11,11,11,11		;10
	defb 12,12,12,12		;11
	defb 13,13,13,13		;12
	defb 14,14,14,14		;13
	defb 15,15,15,15		;14
	defb 16,16,16,16		;15
	defb 17,17,17,17
	defb 18,18,18,18
	defb 19,19,19,19
	defb 20,20,20,20
	defb 21,21,21,21
	defb 22,22,22,22
	defb 23,23,23,23
	defb 24,24,24,24
	defb 0,0,0,0			; para volver a empezar


; Tabla de las direcciones de los tiles
;----------------------------------------------------------------------------------------

tab_dir_tiles:
dw	37104	;57344	; 0
dw	37143	;57383	; 1
dw	37182	;57422	; 2
dw	37221	;57461	; 3
dw	37260	;57500	; 4
dw	37299	;57539	; 5
dw	37338	;57578	; 6
dw	37359	;57599	; 7
dw	37398	;57638	; 8
dw	37437	;57677	; 9
dw	37476	;57716	;10
dw	37515	;57755	;11
dw	37554	;57794	;12
dw	37593	;57833	;13
dw	37632	;57872	;14
dw	37671	;57911	;15
dw	37710	;57950	;16
dw	37749	;57989	;17
dw	37788	;58028	;18
dw	37827	;58067	;19
dw	37866	;58106	;20
dw	37905	;58145	;21
dw	37944	;58184	;22
dw	37983	;58223	;23
dw	38022	;58262	;24
dw	38061	;58301	;25
dw	38073	;58313	;26
dw	38085	;58325	;27
dw	38124	;58364	;28
dw	38163	;58403	;29
dw	38202	;58442	;30
dw	38241	;58481	;31
dw	38280	;58520	;32
dw	38319	;58559	;33
dw	38358	;58598	;34
dw	38397	;58637	;35
dw	38436	;58676	;36
dw	38475	;58715	;37
dw	38514	;58754	;38
dw	38553		;39
dw	38628		;40
dw	38703		;41
dw	38850		;42
dw	38925		;43
dw	39000		;44
dw	39075		;45
dw	39114		;46
dw	39153		;47
dw	39192		;48
dw	39231		;49
dw	39270		;50
dw	39309		;51
dw	39348		;52
dw	39387		; 53 simbolo diosa.
dw	39444		; 54
dw	39483		; 55 quedan libres 8bytes
dw	39630		; 56
dw	39669		; 57
dw	39708		; 58
dw	39720		; 59	puerta con prota0
dw	39759		; 60	"
;dw	39798		; 61 diamante apareciendo
;dw	39837		; 62	"
;dw	39876		; 63	"

