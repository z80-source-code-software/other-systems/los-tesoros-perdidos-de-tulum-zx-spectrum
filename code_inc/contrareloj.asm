;----------------------------------------------------------------------------------------
;   Rutina contrareloj
;sec=255
;min=1
;valtemp=100
;----------------------------------------------------------------------------------------
certina:			; 41248
	push	ix
	push	hl
	push	de
	push	bc
	ld	a,[min]
	and	a
	jr	z,loopcertina2

	ld	a,[sec]
	dec	a
	ld	[sec],a
	jr	nz,fincertina

	ld	a,51
	ld	[sec],a
	ld	a,[min]
	dec	a
	ld	[min],a
	jr	fincertina
loopcertina2:
	ld	a,1
	ld	[min],a
	ld	a,51
	ld	[sec],a

; decrementar valores unidades
	ld	a,[valtemp]
	dec	a
	ld	[valtemp],a	; En A el valor del conversor
	jr	z,an_fincertina
	call	conversornum	; llamo a convnum.asm   el conversor n�merico
; imprimo en pantalla los valores de centenas decenas y unidades
	ld	a,22	; codigo AT
        rst	16
	ld	a,21	; marcador abajo a la izquierda
        rst	16
	ld	a,01	; Columna
	rst	16
;	ld	a,[centenas]
;	add	a,48
	ld	a,'T'
	rst	16
;	ld	a,[decenas]
;	add	a,48
	ld	a,'-'
	rst	16
	ld	a,[unidades]
	add	a,48
	rst	16
	ld	hl,$5aa1
	ld	[hl],5+64
	inc	hl
	ld	[hl],1
	inc	hl
	ld	[hl],5+64
fincertina:
	pop	bc
	pop	de
	pop	hl
	pop	ix
	ret
an_fincertina:
; Bajar la barra de energia
	ld	a,14				; Efecto de da�o
	ld	[paraponerenB+1],a
	call	llamar_efecto
	ld	a,1
	ld	[desconener],a
	call	quita_energia
	ld	a,10
	ld	[valtemp],a
	jr	fincertina

