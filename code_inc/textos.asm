;----------------------------------------------------------------------------------------
;   Rutina de impresion de textos. Usando la ROM
;----------------------------------------------------------------------------------------

Imprime_TextoMenu:
	ld	de,texto_keyboard
        ld	c,0
	ld	b,11	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,11	; Fila  y columna 11,11 
        rst	16
        rst	16
lineaKeyboard:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineaKeyboard
; Joystick
	ld	b,11	; contador de carac. a imprimir
	ld	a,22	; codigo AT
        rst	16
	ld	a,13	; Fila marco superior
        rst	16
	ld	a,11	; Columna
	rst	16
lineaJoystick:
	ld	a,[de]  ; cargo el primer caracter de Joystick
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineaJoystick
;
; colorear fondo texto
;
coloreartexto:
	ld	hl,colormenu
	ld	de,#596B
	ld	bc,11	; caracteres a imprimir
	ldir
	dec	hl
	ld	bc,11	; caracteres a imprimir
	ld	de,#59AB
	ldir
	ret




;----------------------------------------------------------------------------------------
;   Rutina de impresion de textos. Usando la ROM
;----------------------------------------------------------------------------------------


Imprime_fasebonus:
	push	hl
	push	de
	push	bc
chg_text:
	ld	de,texto_fasebonus
	ld	b,13	; contador de carac. a imprimir
	call	mismaposic_tex
lineafasebonus:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineafasebonus
;
; colorear fondo texto
;
	ld	de,#5969+$20
	ld	hl,color_fasebonus
	ld	bc,13	; caracteres a imprimir
	jr	fin_Imprime_level



mismaposic_tex:
	ld	a,22	; codigo AT
        rst	16
	ld	a,12	; Fila  y columna 12 
        rst	16
	LD	A,09    ; columna 9
        rst	16
	ret

Imprime_level:			; 27807

	push	hl
	push	de
	push	bc
	ld	de,texto_nivel
	ld	b,9	; contador de carac. a imprimir
	call	mismaposic_tex

lineanivel:
	ld	a,[de]  ; cargo el primer caracter
        rst	16	; chimpun
        inc	de	; incremento la posi pa cargar el siguiente
        djnz	lineanivel

	ld	a,[panta]
	call	conversornum	; convnumjj.asm Convertir n� nivel en dato a imprimir
	ld	a,[decenas]	; Para que coincida en el inicio en char de los c�digos n�mericos.
	add	a,48
	rst	16	
	ld	a,[unidades]
	add	a,48
	rst	16
	
;
; colorear fondo texto
;
	ld	de,#5969+$20
	ld	hl,color_nivel
	ld	bc,11	; caracteres a imprimir
fin_Imprime_level:
	ldir
	pop	bc
	pop	de
	pop	hl
	ret

texto_keyboard:
;	defb "1. TECLADO."
	defb "1. KEYBOARD"
texto_joystick:
	defb "2. JOYSTICK"
colormenu:
	db	67,3,67,67,3,67,3,67,3,67,3
	db	67,3,67,3,67,3,67,3,67,3,67

texto_fasebonus:
;	defb " FASE  BONUS "
	defb "SPECIAL LEVEL"
color_fasebonus:
	db	2,3,6,65,4,5,7+64,0,2,3+64,6,3,4

texto_nivel:
;	defb "- Nivel -"
	defb "- Stage -"
color_nivel:
	db	2,0,3,5,2,4,6,0,2,7,7,7
