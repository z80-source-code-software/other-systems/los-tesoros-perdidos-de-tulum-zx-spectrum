;----------------------------------------------------------------------------------------
; Calculo de la direcci�n del prota en el archivo de pantalla
;----------------------------------------------------------------------------------------
im_pix:
        ld      a,[fila]	;fila en scannes o pixels
        ld      l,a
        ld      a,[colu]	;colu en chars (para usar pixels pasar previam a char)
        call    tabla4metal
        ld      [dir_spr],HL    
; Dejar en caso de usar lasrutinas independientemente de la llamada desde avolcamiento.asm
;        ret

;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS LA FILA Y LA COLUMNA
;ENTRADA:FILA pixels Y COLUMNA en chars    SALIDA:Posicion del sprite en el buffer intermedio 
;----------------------------------------------------------------------------------------
c_dirbuff:
	xor	a		; acarreo a 0
	ld	e,a		; 1/4
        ld	a,[fila]	; 4/13
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	rra			; 1/4
	rr	e		; 2/8
	ld	d,a		; 1/4
	ld	a,[colu]	; 4/13 
	add	a,e
	ld	e,a
	ld	hl,buffpant-$400	; $ad00 3/10 buffer antes en $ed90 tama�o marcador de arriba 1024= $400
	add	hl,de           ; 3/11
	ld	[d_bu_spr],hl	; 
        ret			; 3/10. Total = 108 t-states.

;----------------------------------------------------------------------------------------
; IMPRESION DE SPRITE en videoram
;----------------------------------------------------------------------------------------
imp_spr_mov		; Segunda parte antigua de MOV

	ld      de,[dir_spr]	; 6/20 direccion del sprite en la videoram
	ld	hl,buffer_spr	; Buffer mezclado de fondo + sprite
evaluamov:
        ld      bc,32		;
looprota:
        push    de		; 3/11
	ldi
	ldi
	pop     de		; 3/11 posicion de pantalla original
        inc     d               ; 1/4  aqu� viene la rutina de bajar scan de toda la vida
        ld      a,d		; 1/4
        and     7		; 2/7
        jr      nz,blooprota	; 3/12 , 7/7
        ld      a,e		; 1/4
        add     a,32		; 2/7
        ld      e,a		; 1/4
        jr      c,blooprota	; 3/12 , 7/7
        ld      a,d		; 1/4
        sub     8		; 2/7
        ld      d,a		; 1/4
blooprota:
        ld	a,b
	or	c
        jr      nz,looprota	; 3/12 , 7/7
;	ret
;----------------------------------------------------------------------------------------
;Rutina de Impresion de atributos del prota.
;----------------------------------------------------------------------------------------
;impatrib:						41545

; Comprobamos que llevas la luz para continuar
	ld	a,[portaluz]
	rra
	ret	nc		; 
;
	call	calc_attr_prota	; La Subrutina la uso en otras ocasiones.
	ld	a,[alto]
	ld      b,a             ; alto del prota
sifattrp:
        push    hl
;;	ld	a,[ancho]
        ld      c,2		; ld c,a  ancho del prota
EtiqColProta:
	ld      a,6            ; COLOR
siattrpro:
        ld      [hl],a
        inc     hl
        dec     c
        jr      nz,siattrpro
        pop     hl
        ld      de,32
        add     hl,de
        djnz    sifattrp
        ret
;----------------------------------------------------------------------------------------
; Rutina de c�lculo direc.Atrib    Entrada dir_spr con valor de la posic.en la  Videoram
;----------------------------------------------------------------------------------------
calc_attr_prota:	
	ld	hl,[dir_spr]	; 5/16 Cargo HL con el valor de la direccion VideoRam
	srl	h		; 2/8
	srl	h		; 2/8
	srl	h		; 2/8 Desplazo 3 veces a la derecha el valor del bit Alto
	ld	a,$50		; 2/7 Valor fijo
	or	h		; 1/4 Sumo h desplazada con A que vale 0101 = $50
	ld	h,a		; 1/4 hl ya contiene el valor de la direcc. de atributos
	ret
