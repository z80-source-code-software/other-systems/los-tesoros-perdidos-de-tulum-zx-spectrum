;----------------------------------------------------------------------------------------
; RUTINA DE JOYSTICK y CURSORES Adaptada
; entrada: valores 1,3,5,7 = arriba,derecha,abajo,izquierda ; salida: valor en E
;----------------------------------------------------------------------------------------
LEER:
;----------------------------------------------------------------------------------------
; miramos si hemos elegido teclas o joystick
	ld	hl,[control]    ; 33813     Valor que se nutre en RUCONTROL.ASM
        jp	[hl]
;----------------------------------------------------------------------------------------
; RUTINA DE JOYSTICK			rutina ok pero sin comprobar si pulsabas ARRIBA
;----------------------------------------------------------------------------------------
JOYSTICK:
	ld	e,0
	ld	a,231
	in	a,[#fe]
	xor	31		; enmascaro las 5 teclas (6 a 0)
	rra			; space = salta
	rl	e		; 
	rra			; arriba
	rl	e		
	rra			; abajo
	rl	e
	rra			; derecha
	rl	e
	rra			; izquierda
	rl	e
	jr	keyfight





;----------------------------------------------------------------------------------------
; RUTINA DE TECLADO
;----------------------------------------------------------------------------------------
KEYBOARD:			; 33491

	ld	e,0
	ld	a,#7f
	in	a,[#fe]
	xor	1		; Enmascaro la tecla Space
	rra
	rl	e		; en el bit 4 de e

	ld	a,#fb
	in	a,[#fe]
	xor	1		; Enmascaramos la tecla arriba
	rra	
	rl	e		; bit 3

	
	ld	a,#fd
	in	a,[#fe]
	xor	1		; Enmascaro la tecla A
	rra
	rl	e		; Bit 2 valor abajo

	
	ld	a,#df
	in	a,[#fe]
	xor	3	        ; ENMASCARAMOS LAS TECLAS O y P
	rra
	rl	e		; Bit 1 valor derecha
	rra
	rl	e		; Bit 0 valor izquierda


keyfight:
	ld	a,#7f		; semifila teclas 'space...
	in	a,[$FE]		; para la luz o el l�tigo
	bit	2,a
	jr	nz,key_exit
	set	6,e

key_exit:
	bit	4,e
	jr	z,indekey

	ld	a,[keypush2]
	and	16
	cp	16
	jr	z,estabaspulsando
indekey:
	ld	a,e
	ld	[keypush2],a
	jr	finestas
estabaspulsando:
	res	4,e
	ld	a,e
finestas:
	ld	[keypush],a	
	ret
