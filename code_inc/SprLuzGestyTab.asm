; MISMA RUTINA QUE SPRLUZGESTYTAB.ASM PERO AQU� PARA RESOLVER QUE LOS DIAMANTES NO SE APAGAN
; HE COMENTADO DOS LINEAS 438 439 PORQUE ILUMINABA DOS TILES POR ENCIMA. AUNQUE SIGUE EL BUG
; MIRAR PORQUE A LA IZQUIERDA NO FUNCIONA. O VOLVER AL ANTERIOR
;----------------------------------------------------------------------------------------
; Gestion y tabla del disparo de la luz 
;----------------------------------------------------------------------------------------

; Intentar� que use el mismo tipo de datos para poder usar la rutina de enemigos

DISPARO_LUZ:
	db 0  ,0, 0,  0,1, 2,3,3, 8, 1, 16,  0,$be,  0,$be, 0,  0,  0,  0,0, 0,  0,  0,  0, 0, 0
; Defb's con los siguientes datos
;	db 0  ,0, 0,  0,1, 2,3,3, 8, 1, 16,  0,$be,  0,$be, 0,  0,  0,  0,0, 0,  0,  0,  0, 0 ;chinchillader
					; $bec0 vela derechas $bed0 vela izquierdas
; panta, activo, posx,  posy, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
; tama�o1frame, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni, direccion, limite1
;	ix10,	ix11,		ix12,  ix13,		ix14,	   ix15,	ix16
; limite2, codanim, sentido, desplaz,	dblowPant, dbhighPant, dblowBuff, dbhighBuff, ChoqueObstaculo
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24	ix25

; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24, 25

; enemigos en horizontal
;	db  30,1, 7, 72,1,6,2,2, 8, 2, 32,$20,$bd,$20,$bd, 0, 12,  3, 64, 0, 0,   0, 0,  0, 0 ;murcielagoder (velocidad 3 a 6)



; Notas:
; En principio los valores son los mismos que para los enemigos

; El valor de ix+0=panta se va a utilizar tambi�n como acumulador de la distancia hasta un obstaculo
; el �ltimo va a ser el que indique si choca la luz con un obstaculo para 
; que podamos meter la animaci�n de luz despareciendo. Que podemos machacar los datos de la luz,con estos.

; Lo primero que haremos cu�ndo disparemos ser� meter los datos de la posici�n del prota como posici�n
; de la luz. Es decir que los valores ix2 e ix3 ser�n practicamente colu y fila. 
; Y por supuesto panta = panta y activo = cambiar� de valor (nos puede valer para saber si puede lanzar)
; o no ya que si en el transcurso de movimiento de la luz, recoges otra luz no puedes volver a disparar.
; Es decir, SI EL VALOR ES 0 quiere decir que la luz est� moviendose por tanto NO PUEDES VOLVER A DISPARAR
; SI HAS COGIDO OTRA LUZ, INDEPENDIENTEMENTE DE QUE LA LLEVES ENCIMA POR QUE HAYAS RECOGIDO UNA NUEVA.


;******************************************************************************************************



































;
; Rutina de colocaci�n de valores en el churro LUZ
;
Actualiza_churro_luz:
	push	ix
	push	hl
	ld	ix,DISPARO_LUZ	
	ld	[ix+1],1	; Activa el disparo de la luz
; Si vas a la derecha la posicion X de la luz es fila +1. Si vas a la izquierda, fila -1
	ld	a,[dirmovsp]
	and	a		; 0 = arriba
	jr	z,luzarriba
	cp	2
	jr	z,luzabajo	; 2= abajo
	jr	c,luzaderecha	; 1= derechas, 3 = izquierdas
;luzaizquierda:
	ld	[ix+19],1	; luz sentido a la izquierda
	ld	hl,gfxprotavelaizq
	ld	[ix+11],l	; db low de la luz
	ld	[ix+13],l
	ld	[ix+12],h	; db high de la luz
	ld	[ix+14],h
	jr	calculoposLuz
luzarriba:
	ld	[ix+15],1	; Vertical
	ld	[ix+19],1	; luz sentido a arriba
	ld	hl,gfxprotavelader
	ld	[ix+11],l	; db low de la luz
	ld	[ix+13],l
	ld	[ix+12],h	; db high de la luz
	ld	[ix+14],h
	jr	calculoposLuz
luzabajo:
	ld	[ix+15],1	; Vertical
	ld	[ix+19],0	; luz sentido a abajo
	ld	hl,gfxprotavelader
	ld	[ix+11],l	; db low de la luz
	ld	[ix+13],l
	ld	[ix+12],h	; db high de la luz
	ld	[ix+14],h
	jr	calculoposLuz
luzaderecha:
	ld	[ix+19],0	; luz sentido a la derecha
	ld	hl,gfxprotavelader
	ld	[ix+11],l	; db low de la luz
	ld	[ix+13],l
	ld	[ix+12],h	; db high de la luz
	ld	[ix+14],h

calculoposLuz:
	ld      a,[fila]	;fila en scannes o pixels
	ld      l,a
	ld	[ix+3],a
	ld	a,[colu]
	inc	a
	ld	[ix+2],a	; Guardo la posici�n X en el churro LUZ
	call    tabla4metal	; HL contiene el valor en pantalla de la posici�n de inicio de la luz    
	ld	[ix+21],l	; low y high de la posic. en pantalla
	ld	[ix+22],h
	call	c_dirbuff_e	; Direcc.en buffer. ix+23 ix+24 igual enemigos SprEnemImp.asm
;
; Ahora tengo que saber cu�l es la posici�n que tienes en el buffer de atributos para 
; localizar la pared donde pueda tropezar la luz al lanzarla. Y crear sus limites
;
	call	c_dirbuffAtr_enem	; hl contiene el valor de la luz en el buffer de Atributos.
;
; Has disparado en Vertical u Horizontal ?
;
	ld	a,[ix+15]
	rra
	jr	c,ponlimitesverticales
;
; tengo que poner los limites a los que va a llegar la luz ix+16(derecha) o ix+17(izquierda)
;
	ld	a,[ix+19]	; que sentido llevas ?
	and	a		;  0 derecha 1 izquierda PARA PONER INC HL o DEC HL en la rutina
	jr	z,calc_lim_H_luz
; Meto $2b o $23 para usar s�lo una rutina y que INC o DEC el valor de HL
	ld	a,$2b		; hexadecimal de DEC HL
	ld	[calc_lim_H2_luz],a
	jr	calc_lim_H2_luz
calc_lim_H_luz:
	ld	a,$23		; hexadecimal de INC HL
	ld	[calc_lim_H2_luz],a
;
; Calculo la distancia de donde arrojas la luz hasta la pared buscando el attr=67
;
calc_lim_H2_luz:
	dec	hl
	ld	a,[hl]
	cp	67		; color magenta+BRILLO de los suelos y paredes
	jr	z,fincalc_limH2_luz
	inc	[ix+0]
	jr	calc_lim_H2_luz
fincalc_limH2_luz:
	ld	b,[ix+0]
;
; Calculo la distancia de donde arrojas la luz hasta la pared.
;
	ld	a,[ix+19]	; que sentido llevas ?
	and	a		;  0 derecha 1 izquierda
	jr	z,calc_lim_1_luz
	ld	a,[ix+2]	; calc_lim_2_luz
Decalc_lim_4_luz:
	sub	b
	ld	[ix+17],a
	jr	fin_Actualiza_churro_luz
calc_lim_1_luz:			; calc_lim_1_luz
	ld	a,[ix+2]
Decalc_lim_3_luz:
	add	a,b
	ld	[ix+16],a
fin_Actualiza_churro_luz:
	pop	hl
	pop	ix
	ret
;
; tengo que poner los limites a los que va a llegar la luz VERTICALES
;
ponlimitesverticales:					; 42877
	ld	a,[ix+19]	; que sentido llevas ?
	and	a		;  0 abajo 1 arriba para saber si hay que sumar 32 o -32
	jr	z,calc_lim_V_luz
; El registro bc= + - 32 seg�n bajes o subas por la liana.
	ld	bc,-32		; valor para restar o sumar a hl
	jr	calc_lim_V2_luz	
calc_lim_V_luz:
	ld	bc,32		; valor para restar o sumar a hl
;
; Calculo la distancia de donde arrojas la luz hasta la pared buscando el attr=67
;
calc_lim_V2_luz:			; ix+17
	add	hl,bc
	ld	a,[hl]
	cp	67		; color magenta+BRILLO de los suelos y paredes
	jr	z,fincalc_limV2_luz
	inc	[ix+0]
	jr	calc_lim_V2_luz
fincalc_limV2_luz:
	ld	a,[ix+0]	; Distancia en caracteres para calcular el limite.
	rla			; El valor de la fila al contrario que la columna
	rla			; est� en scannes.
	rla			; Multiplico x8 para descantar correctamente
	ld	b,a
	ld	a,[ix+19]	; que sentido llevas ?
	and	a		;  0 abajo 1 arriba	para saber donde poner el limite.
	jr	z,calc_lim_3_luz
	ld	a,[ix+3]
	jr	Decalc_lim_4_luz
calc_lim_3_luz:	
	ld	a,[ix+3]
	jr	Decalc_lim_3_luz













;
; Rutina Principal
;
GESTLUZ_DISPARADA:
	push	hl		; Guardo sin saber si es necesario	42799
	push	ix
	ld	ix,DISPARO_LUZ
;
; ix+1 si est� NO activa desactiva el disparo de la luz. Esto es por si pasas de nivel mientras est� activa el disparo de la luz
;
	ld	a,[ix+1]	; 1 ha sido lanzada, 0 la llevas contigo
	and	a
	jp	z,abimprimeluz	

;
; contador de ticks
;
mirarticksluz:	
	dec	[ix+4]
	jr	nz,iluminartilesdencima;imprimeluz
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a

	call	vuelcafondosprenem	; vuelco el fondo de la luz SprEnemVolc.asm
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+7]	; framesTotal
	ld	a,[ix+6]	; frameActual
	cp	b
	jr	z,ponframeluz_0
;
; Incremento el valor del frame
;
	inc	[ix+6]

;
; Si la luz disparada es en vertical no continues ya que s�lo es un frame
;
	ld	a,[ix+15]
	rra
	jr	c,iluminartilesdencima	; Has disparado en Vertical u Horizontal ?


;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;
	ld	bc,16		; El tama�o es ix+10 como es 1 char 1x1x8 x 2
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
	jr	iluminartilesdencima;imprimeluz
ponframeluz_0:	
	ld	[ix+6],0	; frame a 0
	ld	a,[ix+13]	; Inicializo los frames 
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
	call	oscurecetilesconlaluz	; SprProtaEyes.asm
	ld	a,[ix+15]
	and	a
	jr	z,luz_desplazandose_horizontal
	jr	luz_desplazandose_vertical
; Ilumino los tiles que hay en la fila anterior a por donde va a pasar la luz al ser lanzada.
iluminartilesdencima:
	call	iluminatilesconlaluz	; SprProtaEyes.asm
;
; Imprimo la luz. Pongo en hl el valor del sprite a imprimir
;
imprimeluz:
	call	Antes_imp_ene	; (cambiar el valor del ancho antes de imp_ene en Sprenemimp.asm )
bimprimeluz:
	pop	ix
	pop	hl
	ret
;
; Reseteo de valores
;
abimprimeluz:
	call	vuelcafondosprenem	; Borro la vela
	ld	[ix+0],0		; Resetear valor de la distancia del spr a los limites proximos.
	ld	[ix+1],0		; Desactivo que hayas lanzado la luz.
	ld	[ix+6],3		; Reseteo el valor del frame actual
	ld	[ix+15],0		; Dejar luz en horizontal siempre.

	ld	hl,nomirarluz		; 
	ld	[vermirarluz+1],hl	; Desactivo la llamada en el engine

	jp	bimprimeluz
;
luz_desplazandose_horizontal:
;
; Comprobamos los limites necesito ayudarme del sentido del enemigo
;
; 0 derecha 1 izquierda
	ld	a,[ix+19]
	and	a		; derecha?
	jp	z,luz_horizontal_derecha
; 
; mov_luz_horizontal_izquierda. Comprobaci�n de limite2
;
	ld	b,[ix+17]
	ld	a,[ix+2]	; posX
	cp	b
	jr	z,abimprimeluz	; luz_fin_limite1y2_mov_horiz
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
	dec	a
	jp	luz_ll_a_ambos
; 
; mov_luz_horizontal_derecha. Comprobaci�n de limite1
;
luz_horizontal_derecha
	ld	b,[ix+16]
	ld	a,[ix+2]	; posX
	cp	b
	jr	z,abimprimeluz	; luz_fin_limite1y2_mov_horiz
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
	inc	a
luz_ll_a_ambos:
	ld	[ix+2],a	; nueva posX
	call	avolcamiento_enem	;calc_nueva_pos_enem
	jp	imprimeluz




luz_desplazandose_vertical:

;
; Comprobamos los limites necesito ayudarme del sentido del enemigo
;
; 0 abajo 1 arriba
	ld	a,[ix+19]
	and	a		; abajo
	jp	z,luz_vertical_abajo
; 
; mov_luz_vertical_arriba. Comprobaci�n de limite2
;
	ld	b,[ix+17]
	ld	a,[ix+3]	; posY
	cp	b
	jr	z,abimprimeluz	; luz_fin_limite3y4_mov_vertical
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
	sub	8
	jp	luz_ll_a_ambos2
; 
; mov_luz_horizontal_derecha. Comprobaci�n de limite1
;
luz_vertical_abajo:
	ld	b,[ix+16]
	ld	a,[ix+3]	; posY
	cp	b
	jr	z,abimprimeluz	; luz_fin_limite3y4_mov_vertical
; en caso contrario no hemos llegado al limite.Pintamos monigote en su nueva posicion.
	add	a,8
luz_ll_a_ambos2:
	ld	[ix+3],a	; nueva posY
	call	avolcamiento_enem	;calc_nueva_pos_enem
	jp	imprimeluz



;
; Rutinas de iluminacion de los tiles al ser arrojada la luz
;

iluminatilesconlaluz:
; la idea es imprimir los colores de los 3 tiles en la horizontal inmediata superior a la llama
; usando el buffer de atributos normal.

	push	ix	; **==	--->	Esta linea y la siguiente se podr�n eliminar <---  ==**
	ld	ix,DISPARO_LUZ
	call	c_dirbuffAtr_enem	; calculo la posici�n en el buffer. Resultado en hl
	push	hl
	ex	de,hl
; ix+21 lowpant ix+22 highpant
	ld	l,[ix+21]
	ld	h,[ix+22]
	call	c_dirAtr_enem		; calculo la posici�n en la videoram Atributos.
	ex	de,hl
; hl= buff.atr de la luz y de=videoram atr.de la luza
	call	locchocoluz		; Compruebo si la luz lanzada choca con un diamante.
	ldi		; Pinto la luz del valor que haya all�. ---------
; ahora a pintar los tiles de arriba
	pop	hl	; saco el valor calculado en c_dirbuffAtr_enem
; hl contiene el valor de la luz en el buffer de atributos. Necesitamos el inmediato superior menos
; una posici�n. Asi que
	ld	bc,33
;;	xor	a	; quito el acarreo
;;	sbc	hl,bc	; Primer valor a iluminar su attr. en el buffer del paso de la luz.
; de = direc.Atributos en pantalla de la luz
	ex	de,hl	; dejo en de=buffer y hl=videoram para restar bc
	inc	bc
	xor	a	; quito el acarreo
	sbc	hl,bc	; Primer valor a iluminar su attr. en pantalla por el paso de la luz.
	ex	de,hl	; intercambio los valores
	call	locchocoluz		; Compruebo si la luz lanzada choca con un diamante.
	ldi
	ldi
	ldi
	pop	ix	; ***==--- >>>> anular si se anula el push
	ret

oscurecetilesconlaluz:
; Y para borrarlos, vamos a usar los valores del buffer de iluminaci�n. As� borramos cu�ndo pasen
; por la zona oscura y con el anterior se iluminar�n.


; Entrada hl= inicio del buffer de iluminacion
	ld	hl,bufferdeiluminacion	; $aa80 antes $de00	 En SprEnemImp.asm misma rutina c_dirbuffAtr_enem
	call	calc_dir_buff_ilum_delenem	;  Resultado en hl
	push	hl			; hl=valor en buffer de iluminacion
	ex	de,hl			; guardo el valor en de
; ix+21 lowpant ix+22 highpant
	ld	l,[ix+21]
	ld	h,[ix+22]
	call	c_dirAtr_enem		; calculo la posici�n en la videoram Atributos.
	ex	de,hl			; de= valor en videoramAttr , hl= buffer_de_iluminacion
; Borrado del attr de la luz al cambiar de posici�n. 
	ldi
; Borrado de los attr'S de los 3 tiles de encima de la luz al cambiar de posici�n.
; de = direc.Atributos en pantalla de la luz, debo restar 33
	ld	bc,34
	ex	de,hl	; de= videoram de la luz
	xor	a	; quito el acarreo
	sbc	hl,bc	; Primer tile iluminado a borrar.
	ex	de,hl	; de= videoram del primer tile a borrar
	pop	hl	; hl=valor en buffer de iluminacion
	inc	hl
	sbc	hl,bc	; borramos
	ldi
	ldi
	ldi
	ret
