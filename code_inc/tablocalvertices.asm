;----------------------------------------------------------------------------------------
; Tabla de localizaci�n de V�rtices atrav�s del algoritmo de Dijkstra
;----------------------------------------------------------------------------------------
; N�mero de v�rtice :		Valor del v�rtice a analizar
; Sentido +  v�rtice adyacente:	sentido en el que est� el v�rtice adyacente al analizado. 
;				Sino hay ponemos el mismo v�rtice para no analizarlo.
; Peso:				Distancia del v�rtice principal al adyacente.
; Peso definitivo:		Menor valor definitivo
; Peso temporal:		valor temporal mientras no sea definitivo.
; V�rtice fijado:		0 sin analizar 1 analizado y fijado su valor def. de peso
; En principio s�lo vamos a intentarlo con 9 v�rtices en pantalla
; Terminar� la rutina cu�ndo est�n analizados los 9 v�rtices y se haya creado el 
; camino entre los puntos inic. y final
; La direcci�n de la tabla debe ser fija y ha de crearse tambi�n una tabla fija de 
; los valores de los v�rtices a analizar
;_________________________________________________notas

; las posiciones de los vertices tienen que ir de la forma
; y x	; ADEMAS DEBEN IR DE MENOR A MAYOR POSICION


	org	$a990				; 43408
; 
TablaIndiceVertices:	
	dw	43512		;(0)
	dw	43526		;(1) 
	dw	43540		;(2) 
	dw	43554		;(3) 
	dw	43568		;(4) 
	dw	43582		;(5) 
	dw	43596		;(6) 
	dw	43610		;(7) 
	dw	43624		;(8) 

; Ruta localizada acumula los v�rtices de menor peso entre los puntos A y Z (9 v�rtices)
RutaLocalizada:
	defb	128,128,128,128,128,128,128,128,128		; 43426


; Puntero de posici�n en la tabla x,y para cercania del enemigo a un v�rtice
puntposxyvert:							; 43435		
	defb	0,0

varcery:
	defb	255	; Distancia en scanns del enemigo al v�rtice m�s cercano
varcerx:
	defb	255	; "		en chars		"

; pesos temoporales con el sentido que llevas para volcarlos cu�ndo sea definitifo en la tabla de indices
TablaPesoTemporal:
	db	255,255		;(0) + sentido			43439
	db	255,255		;(1) + "
	db	255,255		;(2) + "
	db	255,255		;(3) + "
	db	255,255		;(4) + "
	db	255,255		;(5) + "
	db	255,255		;(6) + "
	db	255,255		;(7) + "
	db	255,255		;(8) + "
; 
TablaPesoDefinitivo:						
	db	255		;(0)				43457
	db	255		;(1)
	db	255		;(2)
	db	255		;(3)
	db	255		;(4)
	db	255		;(5)
	db	255		;(6)
	db	255		;(7)
	db	255		;(8)


; Variable fija para la rutina de creaci�n de ruta
varrotura	db	128					; 43466



; La ruta se cre desde el v�rtice �ltimo (seed) hasta el incial. De aqu� la volcaremos a la tabla del enemigo. 
; Cada enemigo tiene su propia ruta. Y CreandoRuta debe ser reseteado en cada creaci�n de ruta a su valor inicial.

CreandoRuta:			; Ruta desde v�rtice Incial a Final. M�ximo 9 v�rtices. 
	db	128,128		;(0) + sentido			43467
	db	128,128		;(1) + "
	db	128,128		;(2) + sentido
	db	128,128		;(3) + "
	db	128,128		;(4) + sentido
	db	128,128		;(5) + "
	db	128,128		;(6) + sentido
	db	128,128		;(7) + "
	db	128,128		;(8) + "


;		ILUMINADO(SI/NO), Y, X				43485
TablaposicYXvertices:

	db		0,72,3			;(0)
	db		0,72,8			;(1)
	db		0,72,10			;(2)
	db		0,72,12			;(3)
	db		0,72,18			;(4)
	db		0,72,20			;(5)	0,72,13 salto m�s largo
	db		0,88,21			;(6)  43503
	db		0,88,22			;(7)
	db		0,104,23		;(8)





;	org	$a9f9						; 43512

; n� v�rtice		Sentido/vert.ady.	Peso
db	0
db			$00,			0
db			$11,			5
db			$20,			0
db			$30,			0
db			$40,			0
db			$50,			0

; V�rtice fijado
db	0							; 43525

; n� v�rtice		Sentido/vert.ady.	Peso
db	1
db			$01,			0
db			$12,			2
db			$21,			0
db			$30,			5
db			$41,			0
db			$51,			0

; V�rtice fijado
db	0							; 43539


; n� v�rtice		Sentido/vert.ady.	Peso
db	2
db			$02,			0
db			$12,			0
db			$22,			0
db			$31,			2
db			$43,			2
db			$52,			0
; V�rtice fijado
db	0							; 43553


; n� v�rtice		Sentido/vert.ady.	Peso
db	3
db			$03,			0
db			$14,			6
db			$23,			0
db			$33,			0
db			$43,			0
db			$52,			2
; V�rtice fijado
db	0							; 43567


; n� v�rtice		Sentido/vert.ady.	Peso
db	4
db			$04,			0
db			$15,			2
db			$24,			0
db			$33,			6
db			$44,			0
db			$54,			0
; V�rtice fijado
db	0							; 43581


; n� v�rtice		Sentido/vert.ady.	Peso
db	5
db			$05,			0
db			$16,			1
db			$25,			0
db			$34,			2
db			$45,			0
db			$55,			0

; V�rtice fijado
db	0							; 43595


; n� v�rtice		Sentido/vert.ady.	Peso
db	6
db			$06,			0
db			$17,			1
db			$26,			0
db			$36,			0
db			$46,			0
db			$55,			1

; V�rtice fijado
db	0							; 43609


; n� v�rtice		Sentido/vert.ady.	Peso
db	7
db			$07,			0
db			$18,			1
db			$27,			0
db			$36,			1
db			$47,			0
db			$57,			0

; V�rtice fijado
db	0							; 43623


; n� v�rtice		Sentido/vert.ady.	Peso
db	8
db			$08,			0
db			$18,			0
db			$28,			0
db			$38,			0
db			$48,			0
db			$57,			1

; V�rtice fijado
db	0							; 43637