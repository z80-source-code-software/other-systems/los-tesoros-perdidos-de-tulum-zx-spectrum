;----------------------------------------------------------------------------------------
; Rutina que quita la llama del marcador al ser arrojada.
;----------------------------------------------------------------------------------------

; Aqui lo que haremos ser� poner los atributos de la vela a 0 SIMPLE y efectivo
; Posici�n de los attributos en la videoram de poner a 0. Son cuatro
; 23196,23197,23228 y 23229 en hex #5a9c, #5a9d, #5abc y #5abd
apagavelamarcador:
	call	marcabenazul
	ld	hl,#5a9c	; 3/10
	ld	[hl],0		; 3/10
	inc	l		; 1/4
	ld	[hl],0		; 3/10
	ld	l,#bc		; 2/7
	ld	[hl],0		; 3/10
	inc	l		; 1/4
	ld	[hl],0		; 3/10		
	ret			; 3/10		total = 75estados

;----------------------------------------------------------------------------------------
; Rutina de colocaci�n de la llama del marcador y gesti�n de la animaci�n
;----------------------------------------------------------------------------------------

; Los datos aqu� son la posici�n en la videoram donde pintar la vela y es #541C (20,28)
; Van a ser 8 frames y la impresi�n ser� como un tile cualquiera pero haciendo la gesti�n
; como si fuera un sprite osea con ticks para que la llama tenga una cadencia. Ojo que 
; hay que imprimir los atributos de la vela. Porque cambian durante la animaci�n.


; las variables siguientes solo se usan en momentos puntuales. Impresi�n de : 
; menu, marcadores, final, gameover y durante el juego en LLAMADA (paso de pantallas)
; asi que dejo estos valores fijos mientras juegas; metiendolos al inicializar una pantalla

valoresVela:
	ld	a,16
        ld      [al_tile],a
	ld	a,2
        ld      [an_tile],a
	ld	a,28
        ld      [col_dat],a
        ld      a,20
        ld      [fil_dat],a		; datos bc los necesita la rutina im_char
	ret




; Gesti�n de animaci�n de la llama en el marcador  --> llamadelmarcador = #0000
; activo,ticks, frameskip, frameActual, framestotal, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni
; ix0,	  ix1,	ix2,	   ix3,	        ix4,	      ix5,	ix6,		ix7,	  ix8

	; ix0,ix1,ix2,ix3,ix4,ix5,ix6,ix7

churrodbsllama:	
	db   1, 4,  7,  7,$6c,$7e,$6c,$7e
Gest_llama:
	push	hl
	push	ix
	ld	ix,churrodbsllama
;
; contador de ticks
;
mirarticksllama:	
	dec	[ix+0]
	jr	nz,salGest_llama	
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+1]		
	ld	[ix+0],a
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+3]	; framesTotal
	ld	a,[ix+2]	; frameActual
	cp	b
	jr	z,ponframellama_0
;
; Incremento el valor del frame
;
	inc	[ix+2]
;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;
	ld	bc,36		; El tama�o es 4 char 2x2x8 + 4 atributos
	ld	l,[ix+4]	; dblowAnim
	ld	h,[ix+5]	; dbhighAnim
	add	hl,bc
	push	hl
	pop	de		; traspaso a de el valor de la llama a imprimir
	ld	[ix+4],e
	ld	[ix+5],d	; guardo el valor para el pr�ximo frame
	jr	imprimellama
ponframellama_0:	
	ld	[ix+2],0	; frame a 0
	ld	e,[ix+6]	; Inicializo los frames 
	ld	[ix+4],e	; low
	ld	d,[ix+7]
	ld	[ix+5],d	; high
;
; Imprimo la llama del marcador
;
imprimellama:
        ld      c,28			; COLUMNA	char
        ld      b,20                    ; FILA		char
; de= valor de la llama a imprimir
	call	im_char			; Esta rutina de coord.chars necesita la entrada de BC
salGest_llama:
	pop	ix
	pop	hl
	ret
