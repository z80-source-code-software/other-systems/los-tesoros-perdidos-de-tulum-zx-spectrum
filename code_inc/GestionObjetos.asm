;-----------------------------------------------------------------------------------
; Gestion de Objetos en pantalla y marcador
;-----------------------------------------------------------------------------------
Borradiamantesenmarcador:
	ld	hl,	$582e	; primer diamante del marcador a poner a 0 su attr
	ld	[hl],0
	inc	l
	ld	[hl],0
	inc	l
	ld	[hl],0
	inc	l
	ld	[hl],0
	xor	a		; Pongo el contador a 0
	ld	[gemas],a
	ret
Ponerdiamante0enmarcador:
	ld	hl,	$582e	; primer diamante del marcador a poner su attr
	ld	[hl],3
	ret
Ponerdiamante1enmarcador:
	ld	hl,	$582f	; primer diamante del marcador a poner su attr
	ld	[hl],5
	ret
Ponerdiamante2enmarcador:
	ld	hl,	$5830	; primer diamante del marcador a poner su attr
	ld	[hl],4
	ret
Ponerdiamante3enmarcador:
	ld	hl,	$5831	; primer diamante del marcador a poner su attr
	ld	[hl],6
	ret
;----------------------------------------------------------------------------------------
; Colocamos objetos en pantalla al iniciarla
;----------------------------------------------------------------------------------------
; En el futuro la tabla_Luz_y_Objetos la pondremos en el buffer de pantalla para descomprimirla.
ponerobjetos:
	ld	de,62
	ld	hl,tabla_Luz_y_Objetos -62	;tabla_Objetos -06
	ld	a,[panta]
	ld	b,a
bponerobj:
	add	hl,de
	ld	a,[hl]			
	cp	128 			; = 128 entonces fin tabla
	ret	z
	cp	b			; pantalla
	jr	nz,bponerobj
; Objetos de pantalla encontrado. Pasar luz primero	
	ld	de,LA_LUZ_DE_PANTALLA
	ld	bc,26
	ldir
; Pasamos las 6 tiras de db's a la tabla de objetos en pantalla
	ld	de,tabla_objetos_enpantalla
	ld	b,6
traspobjetostabla:
	push	bc
	ld	bc,6
	ldir
	inc	de
	pop	bc
	djnz	traspobjetostabla
;	ret
fasebonus:
	ld	ix,LA_LUZ_DE_PANTALLA
	ld	a,[ix+1]
	and	a
	ret	nz			; Si hay 0 es pantalla fase bonus
; Activar que la pantalla se le apaguen los tiles y la luz cambie su posici�n si no la llevas.
	ld	hl,apagar		;  No apagamos el area de juego.
	ld	[verapagarluces+1],hl	;  
	ld	hl,nomirarenem		;  Y NO ponemos los enemigos.
	ld	[vermirarenemigos+1],hl	;  
	ld	a,1
	ld	[sinluz],a
	ret