;----------------------------------------------------------------------------------------
; Rutina de animaci�n de enemigos al comenzar el nivel.
;----------------------------------------------------------------------------------------

;----------------------------------------------------------------------------------------
; Sprites APARECIENDO
;	org	$c580				; 50560
;	include "gfx_inc\Apaenemigos.asm"
; Maya		Frame0	c580	Frame1	c5c0	Frame2	c600
; Maya		Frame3 (frame en pausa) c640
; Olmeca	Frame0	c680	Frame1	c6c0	Frame2	c700
; Olmeca	Frame3 (frame en pausa)	c740

; Frame de los enemigos en modo estatua
frame3maya	dw	$c640
frame3olmeca	dw	$c740
priene	db	0	; variables para saber si actuan o no los enemigos
segene	db	0

GEST_ANIM_ENEM:
;
; Valores 0,1 o 2.	0 est� contando, 1 imprime animaci�n, 2 ha sido golpeado imprime SOLO el frame3
;
	ld	a,[ix+25]	
	and	a
	jr	nz,a_imp_frames_anim	; Si Activo imprime frames de animaci�n
	cp	2
	jr	z,contador_AnimenGolpeado

contador_Animenem:
	dec	[ix+4]
	jp	nz,verotroenemigo
	ld	[ix+4],255	; Contador denuevo activo
	sra	[ix+26]		; Seguimos restando al iniciar el nivel valores (0,1,2,4,8,...
	jp	nz,verotroenemigo
	ld	a,[ix+27]	; restauro valor alto del contador
	ld	[ix+26],a
	ld	a,[ix+18]	; codanim 
	and	a		; compruebo si 0 o 1 y de paso el acarreo a 0
	jr	nz,AnimenemIA2
	ld	a,[ix+5]	; Actualizo el skipframe del db's en ticks	
salircontador_Animenem:
	ld	[ix+4],a
	ld	[ix+25],1	; Activo la animaci�n del enemigo
	jp	verotroenemigo
AnimenemIA2:
	ld	a,[ix+5]
	rla			; multiplico por dos el frameskip para el segundo enemigo.
	jr	salircontador_Animenem

contador_AnimenGolpeado:
	dec	[ix+4]
	jp	nz,verotroenemigo

	ld	[ix+25],1	; Poner a contar
	jp	etiq_desde_gestionanimac_enem	; Gesionenemigos.asm Imprimimos animaci�n 0 en la parte reanimarenem




;
; Miramos si los enemigos van a actuar o no. A la rutina que pone los valores en priene y segene
; se le llama desde LLAMADA en el engine. Al iniciar el nivel. 
;
a_imp_frames_anim:
	ld	a,[ix+18]	; codanim
	and	a
	jr	z,verprienenanim
	ld	a,[segene]
	and	a
	jp	z,verotroenemigo
	jr	imp_frames_anim
verprienenanim:
	ld	a,[priene]
	and	a
	jp	z,verotroenemigo
;
; Comenzamos a imprimir los frames de la animaci�n
;
imp_frames_anim:
;
; contador de ticks
;
	dec	[ix+4]
	jp	nz,imprimemismanimenemigo
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
;
; Vuelco fondo del sprite
;
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+7]	; framesTotal
	ld	a,[ix+6]	; frameActual
	cp	b
	jr	z,ponframeAnimEnemigo_0
;
; Incremento el valor del frame
;
	inc	[ix+6]
;
; Si el enemigo ha sido golpeado. S�lo debe imprimirse quieto.
;
	ld	a,[ix+25]
	cp	2			; 2 enemigo paralizado o en la fase 3 de aparici�n
	jr	z,imprimemismanimenemigo


;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;
				; cacho de metalbrain
	ld      c,64	        ; tama�o del frame fijo en Tulum (antes ix+10 ) ahora ese db's lo uso para otra cosa
	ld	a,127		; cualquier numero mayor es tomado como negativo
	cp	c		; si es mayor habr� acarreo
	sbc     a,a		; si acarreo 127- 127 -1 = 255, si no acarreo 127-127=0
	ld	b,a		; b=255 con acarreo (negativo) b=0 positivo
				; fin cacho de metelbrain	
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
	jr	imprimemismanimenemigo
ponframeAnimEnemigo_0:		; En realidad hay que dejar el �ltimo frame activado. 
; Por si pega el prota con el l�tigo al enemigo y hay que dajarlo parado.
	ld	[ix+6],0	; frame 0
;
; Los frames del enemigo los cambio por enemigo a andar.
;
	ld	a,[ix+13]
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
	ld	[ix+25],0	; Valor Enemigo terminado de animar.
;paranoactivarenemigo:
	ld	[ix+1],1	; Activado el enemigo
	jp	verotroenemigo
;
; Imprimo la misma animaci�n enemigo. Pongon en hl el valor del sprite a imprimir
;
imprimemismanimenemigo:
	call	Antes_imp_ene	; (cambiar el valor del ancho antes de imp_ene en Sprenemimp.asm )
	jp	verotroenemigo
;
; Comprobaci�n de enemigos a gestionar
;
Compenempant:
	ld	hl,Tabenemavolcar	; Tabla en tabenemxpant.asm
	ld	de,03
	ld	a,[panta]
	ld	b,a
bCompenenmpant:
	ld	a,[hl]
	cp	b
	jr	z,enemencdepant
	add	hl,de
	jr	bCompenenmpant
enemencdepant:
	inc	hl
	ld	a,[hl]
	and	a
	jr	z,primerenema0
; Primer enemigo a 1:
	ld	a,1
	jr	segenemver
primerenema0:
	xor	a
segenemver:
	ld	[priene],a
	inc	hl
	ld	a,[hl]
	and	a
	jr	z,segunenema0
; Segundo enemigo a 1:
	ld	a,1
	jr	segenemver2
segunenema0:
	xor	a
segenemver2:
	ld	[segene],a
	ret
