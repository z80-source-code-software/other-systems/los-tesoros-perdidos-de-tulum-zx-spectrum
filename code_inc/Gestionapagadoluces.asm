;----------------------------------------------------------------------------------------
; Rutina que va a ir apagando los tiles de la escena de juego.
;----------------------------------------------------------------------------------------
; Buffer de apagado en #a780


gestapagaluces:			; 59170
	push	hl
	push	de
	push	bc
	ld	hl,$5880	; Inicio de los attr. en la pantalla de juego 
	ld	bc,512
loopgestaapaluces:
	ld	a,[hl]		; si 0 no hagas nada
	and	a
	jr	z,cont_en_sig_tile
	ld	a,h
	add	a,$4f		; 58+4f = a7. Byte alto del buffer de apagado
	ld	d,a
	ld	e,l		; DE contiene la direcci�n en el buffer de apagado.
	ex	de,hl		; intercambio DE =buf.ilu HL =buf.apa
	dec	[hl]		; decremento el valor en el buffer de apagado.
	ld	a,[hl]
	and	a
	jr	z,ponvalor0enattrpantallas
	cp	128
	jr	nz,an_cont_en_sig_tile	; Si no es 128 continua
ponvalor1enattrpantallas:		; Si lo es pon 1 en attr de pantalla
	ex	de,hl		; intercambio HL= buff.ilu DE= buff.apa
	ld	[hl],1
	jr	cont_en_sig_tile
ponvalor0enattrpantallas:
	ld	[hl],255	; dejo en el buffer de apagado el valor de inicio para descontar.
	ex	de,hl		; intercambio HL= buff.ilu DE= buff.apa
	ld	[hl],0
	jr	cont_en_sig_tile
an_cont_en_sig_tile:
	ex	de,hl		; intercambio HL= buff.ilu DE= buff.apa
cont_en_sig_tile:
	inc	hl
	dec	bc
	ld	a,b
	or	c
	jr	nz,loopgestaapaluces
	pop	bc
	pop	de
	pop	hl
	jp	Imp_attr_de_la_luz_de_pantalla
