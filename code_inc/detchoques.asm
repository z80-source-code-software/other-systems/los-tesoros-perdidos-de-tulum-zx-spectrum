;-----------------------------------------------------------------------------------
; RUTINAS DE DETECCION DE CHOQUES
;-----------------------------------------------------------------------------------
;
; Entrada fila y colu  Salida calculo los limites del prota dejandolos en DE Y HL 
;

det_choques_enemigos:

;calc_pxy_pro:
        ld	a,[fila]	
	add	a,3
        ld	d,a		; d= fila +3px
        add	a,13
	ld	e,a		; e= fila +24px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
;
; limites para ajustar el golpeo de la espada si se produce o el choque
;
	ld	a,h
	inc	a	        ; l= colu +1    (ANCHO)
	ld	l,a	        ; l= colu +2   (ANCHO) para espadazo
;
; Tamiz de comprobacion con enemigos. Tabla ENEMIGOS IX+2 ES X e IX+3 ES Y
;
	ld	a,[ix+2]	
	ld	b,a		; b= col_enem
	ld	c,a             ; c= col_enem para enem.de un solo caracter
;
; de que ancho es el enemigo?
;
	inc	c		; c= col_enem para enem.de dos caracteres
;
;comprobamos Columnas
;
        ld	a,c		
	cp	h
	ret	c
        ld	a,b
	cp	l
	jr	z,verfila_enem_prota
        ret	nc
verfila_enem_prota:
        ld	a,[ix+3]
	ld	c,a		; lo guardo para calcular su valor seg�n la altura del enemigo
	add	a,2
	ld	b,a		; b= fil_enem +2px
;
; como de alto es el enemigo ?
;
	ld	a,c		; Recupero para sumar el valor
	add	a,14
	ld	c,a             ; c= fil_enem+alto Sp
;
;comprobamos Filas
;
	ld	a,c		
	cp	d
	ret	c
	ld	a,b
	cp	e
	ret	nc
;
; Comprobamos si pulsas disparo
;
	ld	a,[accion]
	bit	0,a
	jr	z,enemy_w
;
; Paralizas al enemigo
;
; poner fx de enemigo paralizado.
;
; activo la variable de choque de mandoble con espada al enemigo
;
	xor	a
	ld	[vchoqEnem],a
	inc	a
	ld	[vchoqEnemesp],a
	ld	[ix+4],255		; pongo el skipframe al m�ximo como para animar.
	ret
enemy_w:

 	ld	a,1			; flag para el parpadeo del prota y que no lo puedan matar.
	ld	[inmunidad],a		; El siguiente enemigo que haya en pantalla.
	ld	a,14				; Efecto de da�o
	ld	[paraponerenB+1],a
	call	llamar_efecto

;
; Quitar barra energia
;
	call	quita_energia
;
; activo la variable de choque y anulo la de matar al enemigo
;
	xor	a
	ld	[vchoqEnemesp],a
	inc	a
	ld	[vchoqEnem],a
	ret
;-----------------------------------------------------------------------------------
; RUTINAS DE COLOCACI�N DE DATOS DE LA MUERTE DE LOS ENEMIGOS A MANOS DEL PROTA.
;-----------------------------------------------------------------------------------
ponmuerteEnemigo:
;
; desactivo la variable de choque
;
	xor	a
	ld	[vchoqEnemesp],a


;----------------------------------------------------------------------------------------
; Misma r�tina que enem_trop_oscura. El enemigo ha chocado con la oscuriradad 
;----------------------------------------------------------------------------------------
;enem_trop_oscura:
	ld	[ix+4],255	; ticks
	ld	[ix+0],2
	ld	[ix+32],0
	ld	[ix+31],0
;	jp	verotroenemigo
;-------------------------------


;
; Activamos la animacion de la muerte del enemigo y sus nuevos datos
;
;	ld	[ix+1],0	; desactivar enemigo.
	ld	[ix+25],2	; bit choque activado
; Frame de los enemigos en modo estatua
;frame3maya	dw	$c640			;frame3olmeca	dw	$c740	
	ld	[ix+11],$40	; dblow
	ld	a,[ix+18]
	and	a
	jr	nz,ponmuerteolmeca
	ld	[ix+12],$c6	; dbhigh
	jp	Antes_imp_ene
ponmuerteolmeca:
	ld	[ix+12],$c7	; dbhigh
	jp	Antes_imp_ene

;-----------------------------------------------------------------------------------
;FIN DETECCION CHOQUES