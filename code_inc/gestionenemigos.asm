;----------------------------------------------------------------------------------------
;
; Rutinas de Gestion de enemigos.
;
;----------------------------------------------------------------------------------------
;
; Maya derecha		0,1,2,3		c780,c7c0,c800,c840
; Maya izquierda	0,1,2,3		c880,c8c0,c900,c940
; Olmeca derecha	0,1,2,3		c980,c9c0,ca00,ca40
; Olmeca izquierda	0,1,2,3		ca80,cac0,cb00,cb40
; Maya sube/baja	0,1		cb80,cbc0
; Olmeca sube/baja	0,1		cc00,cc40
;
; Sprites APARECIENDO
; Maya		Frame0	c580	Frame1	c5c0	Frame2	c600
; Maya		Frame3 (frame en pausa) c640
; Olmeca	Frame0	c680	Frame1	c6c0	Frame2	c700
; Olmeca	Frame3 (frame en pausa)	c740
;----------------------------------------------------------------------------------------
GESTENEM:			; 56188
	push	hl		; Guardo sin saber si es necesario
	push	de
	push	ix
	ld	ix,ENEMIGOS
BGESTENEM:
;----------------------------------------------------------------------------------------
; enemigo activo S/N
;----------------------------------------------------------------------------------------
	ld	a,[ix+1]	
	and	a
	jp	z,GEST_ANIM_ENEM	; Si 0 VE A ANIMAR el enemigo
;----------------------------------------------------------------------------------------
; Centro de acciones
;----------------------------------------------------------------------------------------
	ld	a,[ix+0]
	and	a		; si 0 caminar
	jr	z,mirarticks
	rra			; si 1 cae
	jp	c,cayendoenem
	rra			; si 2 reaparece el enemigo
	jp	c,reanimatorenem
	rra			; si 4 salta
	jp	c,CONT_SALTANDO_ENEM
	rra			; si 8 subiendo por liana
	jp	c,localizaliana
	rra			; si 16 localizar hueco
	jp	c,enemenganchadoaliana
	call	localizaruta	; si 32 localiza nueva ruta.
	jp	verotroenemigo
;---------------------------------------------------------------------------------------
; contador de ticks
;---------------------------------------------------------------------------------------
mirarticks:	
	dec	[ix+4]
	jp	nz,imprimemismoenemigo
;
; pongo el skipframe del db's en ticks
;
	ld	a,[ix+5]		
	ld	[ix+4],a
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
;
; Comprobamos si es el �ltimo frame
;
	ld	b,[ix+7]	; framesTotal
	ld	a,[ix+6]	; frameActual
	cp	b
	jr	z,ponframeEnemigo_0
	inc	[ix+6]		; Incremento el valor del frame
;
; Ahora con el tama�o del frame calculamos el frame a imprimir. Sumandolo a dblowAnim
;
; cacho de metalbrain
	ld      c,64	        ; tama�o del frame fijo en Tulum (antes ix+10 ) ahora ese db's lo uso para otra cosa
	ld	a,127		; cualquier numero mayor es tomado como negativo
	cp	c		; si es mayor habr� acarreo
	sbc     a,a		; si acarreo 127- 127 -1 = 255, si no acarreo 127-127=0
	ld	b,a		; b=255 con acarreo (negativo) b=0 positivo
; fin cacho de metelbrain	
	ld	l,[ix+11]	; dblowAnim
	ld	h,[ix+12]	; dbhighAnim
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h	; guardo el valor para el pr�ximo frame
	jp	imprimemismoenemigo
ponframeEnemigo_0:	
	ld	[ix+6],0
;
; Inicializo los frames a pesar de no saber si va a darse la vuelta el sprite.
;
	ld	a,[ix+13]
	ld	[ix+11],a	; low
	ld	a,[ix+14]
	ld	[ix+12],a	; high
	jr	recorriendovertices
;----------------------------------------------------------------------------------------
; discrimino la Direcci�n entre vertical u horizontal. 
;----------------------------------------------------------------------------------------
direccionhorizvert:
	ld	a,[ix+15]
	and	a		; 0 horizontal, 1 vertical
	jp	z,enem_horizontal
	jp	enem_vertical
;
; Imprimo el enemigo. Pongon en hl el valor del sprite a imprimir
;
imprimemismoenemigo:
	call	Antes_imp_ene	; (cambiar el valor del ancho antes de imp_ene en Sprenemimp.asm )
;
; inmunidad Activada ?
;
	ld	a,[inmunidad]
	and	a
	jr	nz,verotroenemigo
;
; Detectar choques de los enemigos con el prota y/o del l�tigo del prota
;
	call	det_choques_enemigos
;
;Si el prota mata al enemigo se activa vchoqEnemesp. Llamamos a rutina de animaci�n del enemigo parado.
;
	ld	a,[vchoqEnemesp]
	and	a
	call	nz,ponmuerteEnemigo

verotroenemigo:
	ld	de,churrodbsenemigos
	add	ix,de
	ld	a,[ix+0]
	rla
	jp	nc,BGESTENEM  ; IX+0 no puede ser igual o superior a 128
FINGESTENEMIGOS:
	pop	ix
	pop	de
	pop	hl
	ret
;----------------------------------------------------------------------------------------
; Detectar al prota y/o acciones que conlleva	56250
;----------------------------------------------------------------------------------------
recorriendovertices:	
	ld	a,[activ_eyes]
	rra
	jr	c,protainvisible
	call	detectar_prota
	and	a
	jr	z,protainvisible
; Detectado y cambiados los limites en la rutina 
;	ld	a,1
;	ld	[ix+32],a		; Si lo detectamos -- Activamos la variable y activamos que lo perseguimos.
	ld	[ix+32],1		; Si lo detectamos -- Activamos la variable y activamos que lo perseguimos.
	ld	[ix+31],1		; Activado persiguiendo al prota
	jr	persiguiendo
protainvisible:
	ld	a,[ix+32]
	and	a
	jr	nz,persiguiendoalprotalohasperdido	; en cambio si lo perseguias entonces lo has perdido
	ld	[ix+31],0		; NO Activado persiguiendo al prota
	jp	route
;----------------------------------------------------------------------------------------
; Modo de actuaci�n del enemigo: en ruta o perseguir		56451
;----------------------------------------------------------------------------------------
persiguiendo:
	call	compvertprota		; Modo persiguiendo. Comprobar si has llegado al prota
	and	a
	jp	z,direccionhorizvert	;	ret	z

; has llegado donde estaba antes el prota. �Por donde se ha ido?
	ld	a,[ix+10]
	and	a
	jr	z,sihallegadoalprota	; Si fila -1 o +1 o =
	cp	1
	jr	nz,sihallegadoalprota	; se ha ido por abajo
	ld	[ix+0],8		; se ha ido por arriba. Ponlo a localizar liana
	jp	imprimemismoenemigo

sihallegadoalprota:						; 56458
	ld	a,[ix+19]		; tambi�n viene aqu� despu�s de comprobarSEED
	cp	3
	jr	z,dar_a_Set_Sent_enem_horiz_der		; Vas a izquierdas? 
	cp	1
	jr	z,dar_a_Set_Sent_enem_horiz_izq
; si estas en vertical imprime el mismo enemigo.
	call	dar_vuelta_enem_en_vertical
	jp	imprimemismoenemigo
dar_a_Set_Sent_enem_horiz_izq:
	call	Set_Sent_enem_horiz_izq			; Setframe3_Sent_enem_horiz_izq
	jp	imprimemismoenemigo
dar_a_Set_Sent_enem_horiz_der:
	call	Set_Sent_enem_horiz_der			; Setframe3_Sent_enem_horiz_der
	jp	imprimemismoenemigo
; 57100 Si despu�s la variable detecprota no est� activa y si lo est� la de perseguir, 
; podemos darle la vuelta al enemigo para buscar al prota
persiguiendoalprotalohasperdido:		; Pero si lo has perdido quiere decir que ha cambiado direcc.o sentido
	call	buscarnuevaruta			; Hay que activar buscar nueva ruta
	ld	[ix+31],0			; No persigues al prota ya.
;	xor	a
;	ld	[ix+32],a
	ld	[ix+32],0
	ld	a,[ix+19]
	cp	1
	jr	z,miraprotaIdoder
	cp	3
	jr	z,miraprotaIdoizq
	jp	verotroenemigo

miraprotaIdoder:
	ld	a,[colu]
	ld	b,a
	ld	a,[ix+2]
	cp	b
	jp	nc,verotroenemigo	; Si ha desaparecido mirando al prota, continua.
	ld	a,[ix+6]		; O da la vuelta si al desaparecer est� detr�s tuyo.
	cp	2
	jr	c,ponframe2_izq_enem
	call	Setframe3_Sent_enem_horiz_izq
	jp	verotroenemigo
ponframe2_izq_enem:
	call	Setframe2_Sent_enem_horiz_izq
	jp	verotroenemigo
miraprotaIdoizq:
	ld	a,[colu]
	ld	b,a
	ld	a,[ix+2]
	cp	b			; Igual que a la derecha pero a la izquierda.
	jp	c,verotroenemigo
	ld	a,[ix+6]
	cp	2
	jr	c,ponframe2_der_enem
	call	Setframe3_Sent_enem_horiz_der
	jp	verotroenemigo
ponframe2_der_enem:
	call	Setframe2_Sent_enem_horiz_der
	jp	verotroenemigo
;-------------------------------------------------------------------------------- 56498
route:
	call	compvertprota	; Modo Ruta. Comprobar si has llegado al v�rtice/prota
	and	a
	jp	z,direccionhorizvert		; no has llegado si hay 0 DEVUELVO CONTROL ARRIBA
	call	comprobarSEED
	and	a
	jp	z,verotroenemigo	; Si a=0 ha habido un cambio en la direcci�n x cualquier motivo
	jp	direccionhorizvert	; Si no continua andando como vas.

comprobarSEED:
	ld	h,[ix+29]
	ld	l,[ix+28]	; puntero de la tabla del enemigo
	ld	a,[hl]		; n� v�rtice
	ld	b,a
	ld	a,[ix+30]	; seed
	cp	b
	jr	nz,enelvertderuta
	call	buscarnuevaruta	; Est�s en SEED. Escoger nueva ruta.
	jp	regAcero	;  RET de alli devuelve el control
; // cambiar el puntero a la ruta y poner los valores del nuevo v�rtice en los limites
enelvertderuta:
	inc	l		; sentido
	ld	a,[hl]		; en el que se encuentra el v�rtice siguiente
	ld	c,a		; guardo en C el sentido
	inc	l
	ld	[ix+28],l	; Actualizo el puntero al v�rtice 
	ld	a,[hl]		; n� v�rtice siguiente
; buscar en la tabla TablaposicYXvertices la posici�n de este v�rtice en pantalla
	ld	hl,TablaposicYXvertices		; En tablocalvertices.asm
	ld	b,a
	add	a,a
	add	a,b
	add	a,l
	ld	l,a		; hl valor de si est� el v�rtice en zona iluminada o no.
	inc	l
	ld	a,[hl]		; posicion Y
	ld	[ix+17],a
	inc	l
	ld	a,[hl]		; posicion X
	ld	[ix+16],a
; Recupero el sentido que tienes que tener para llegar hasta el v�rtice   56475
	ld	a,c		; arriba y abajo entonces cambiar al spr.
	and	a			; 0 arriba
	jr	z,vertArribajoEnem
 	cp	2			; 2 abajo
	jr	z,vertArribajoEnem
	jr	c,vertenhorizontal	; 1
	cp	3			
	jr	z,vertenhorizontal	; 3
; Saltahorizontalizquierda o derecha	; 4 y 5
	ld	[ix+0],4		; salto
	ld	[ix+6],3		; Para el inicio del mov.frames en lo alto del salto
; comprueba que el enemigo est� mirando para la posici�n que debe saltar.
	cp	5
	jr	z,saltaralaizq
saltaralader:
	ld	a,[ix+19]
	cp	1
	call	nz,Set_Sent_enem_horiz_der
	jr	cont_enelvertderuta
saltaralaizq:
	ld	a,[ix+19]
	cp	3
	call	nz,Set_Sent_enem_horiz_izq
cont_enelvertderuta:
	call	Enem_preparar_salto	; necesario valor C para salto
	call	Antes_imp_ene	
	jp	regAcero		; RET de alli devuelve el control
vertArribajoEnem:
	ld	[ix+19],c
	ld	[ix+15],1
; Para arriba, primero saltaremos
	call	Set_enemarraba_ambos
	ld	[ix+0],8		; localizaliana
	jp	regAcero		;  RET de alli devuelve el control
vertenhorizontal:
	ld	[ix+0],0		; a andar
	ld	[ix+7],3		; por si vienes de moverte en vertical
	ld	a,[ix+15]
	cp	1
	jr	z,vienenvertphor
; Comprobaci�n de recorrido en horizontal
	ld	a,[ix+19]		; sentido que llevas t�.
	cp	c
	jp	z,regAuno		; A=1 para saber si continuas en direccionhorivert
	cp	1
	jr	z,vertenhoriz1		; Si el valor ix+19 es 1 ahora ES COMO SI C FUERA 3
	call	Set_Sent_enem_horiz_der	; Si el valor ix+19 es 3 ahora ES COMO SI C FUERA 1
	jp	regAcero
vertenhoriz1:
	call	Set_Sent_enem_horiz_izq
	jp	regAcero
vienenvertphor:
	ld	a,c
	cp	3
	jr	z,vertenhoriz0			; Pon 2.frame
	call	Setframe2_Sent_enem_horiz_der	; Pon 2.frame
	jp	regAcero
vertenhoriz0:
	call	Setframe2_Sent_enem_horiz_izq
	jp	regAcero
;----------------------------------------------------------------------------------------
; Gesti�n de enemigos horizontales				56251
; Comprobamos si has llegado al limite. Para ello necesito ayudarme del sentido del enemigo
; 0 arriba, 1 derecha, 2 abajo 3 izquierda
enem_horizontal:						
	call	vuelcafondosprenem		
	ld	a,[ix+19]
	cp	1	
	jp	z,enem_horizontal_derecha
; mov_enem_horizontal_izquierda. Comprobaci�n de oscuridad
	call	compdarkiz
	cp	1
	jr	c,an_ll_a_todos_2_iz
	jr	z,a_an_ll_a_todos_2_iz
	jp	enem_trop_oscura
a_an_ll_a_todos_2_iz:
	call	Set_Sent_enem_horiz_der		; si hay pared s�lida dar la vuelta al enemigo
	call	buscarnuevaruta			; Buscar otro v�rtice
	jp	verotroenemigo
an_ll_a_todos_2_iz:
	ld	a,[ix+2]	; posX
	dec	a
	ld	[ix+2],a	; nueva posX
	jr	banllatodos2
; mov_enem_horizontal_derecha. Comprobaci�n de limite1
enem_horizontal_derecha
	call	compdarkde			; Comprobaci�n de oscuridad  56317
	cp	1
	jr	c,an_ll_a_todos_2_de
	jr	z,a_an_ll_a_todos_2_de
	jp	enem_trop_oscura
a_an_ll_a_todos_2_de:
	call	Set_Sent_enem_horiz_izq		; si hay pared solida dar la vuelta al enemigo
	call	buscarnuevaruta			; Buscar otro v�rtice
	jp	verotroenemigo
an_ll_a_todos_2_de:
	ld	a,[ix+2]	; posX
	inc	a
	ld	[ix+2],a	; nueva posX
banllatodos2:
	call	avolcamiento_enem
	call	Antes_imp_ene
	call	compsueloenem			; comprobarsuelo del enemigo
	cp	1
	jp	z,verotroenemigo
	ld	[ix+0],1			; enemigo cay�ndose.
	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; Gesti�n de enemigos verticales
; Comprobamos si has llegado al limite. Para ello necesito ayudarme del sentido del enemigo
; 0 arriba, 1 derecha, 2 abajo 3 izquierda
enem_vertical
	call	vuelcafondosprenem		
	ld	a,[ix+19]
	and	a	
	jr	nz,enem_vertical_abajo
enev_vertical_arriba:
; Comprobaci�n de oscuridad
	call	compdarkar
	cp	1
	jr	c,b_enev_vertical_arriba
	jr	z,choqueenvertical
	jp	enem_trop_oscura
b_enev_vertical_arriba:
	ld	a,[ix+3]	; posY
	sub	4
	jr	bavert_ll_a_todos
enem_vertical_abajo:
; Comprobaci�n de oscuridad				56690
	call	compdarkab
	cp	1
	jr	c,b_enev_vertical_abajo
	jr	z,choqueenvertical
	jp	enem_trop_oscura
b_enev_vertical_abajo:
	ld	a,[ix+3]	; posY
	add	a,4
bavert_ll_a_todos:
	ld	[ix+3],a	; nueva posY
	call	avolcamiento_enem
	call	Antes_imp_ene
	jp	enemenganchadoaliana	; desde alli salta a	jp	verotroenemigo
choqueenvertical:
	call	buscarnuevaruta
	jp	enemenganchadoaliana	; desde alli salta a	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; Rutina de comprobaci�n de char's iluminados alrededor del enemigo 
;----------------------------------------------------------------------------------------
compdarkiz:			; oscuridad a izq
	call	compdarkenemigo
	dec	l
antes_de_complospiesenem:
	ld	a,[hl]
	and	a
	jp	z,regAdos	; Si hay oscuridad. Activa obst�culos y da la vuelta
	bit	6,a
	jp	nz,regAuno	; Si es pared s�lida. "
	jp	regAcero
compdarkde:			; oscuridad a der
	call	compdarkenemigo
	inc	l
	inc	l
	jr	antes_de_complospiesenem
compdarkar:			; oscuridad arriba
	call	compdarkenemigo
	ld	bc,&20
	sbc	hl,bc
	jr	antes_de_comp_sig_ch_enem
compdarkab:			; oscuridad debajo
	call	compdarkenemigo
	ld	bc,&40
	add	hl,bc
antes_de_comp_sig_ch_enem:
	ld	a,[hl]
	and	a
	jr	z,regAdos	; Si hay oscuridad. Activa obst�culos y da la vuelta
	bit	6,a
	jp	nz,regAuno	; Si es pared s�lida. "
	jp	regAcero
compdarkenemigo:
	ld	h,[ix+22]
	ld	l,[ix+21]
	jp	c_dirAtr_enem
;----------------------------------------------------------------------------------------
; Subrutina para ajustar la nueva ruta a la tabla del correspondiente enemigo. 
;----------------------------------------------------------------------------------------
buscarnuevaruta:
	ld	[ix+0],32	; Activo de nuevo la busqueda del nuevo v�rtice
	ld	a,[ix+18]	; codanim
	and	a
	jr	z,Srutenem1
; Segundo rutenem2
	ld	de,TAB_RUTA_ENEM2	; Coloco los punteros de la ruta al inicio en TABENEMIGOS.ASM
	jr	bSurtenem1
Srutenem1:
	ld	de,TAB_RUTA_ENEM1
bSurtenem1:
	ld	[ix+28],e
	ld	[ix+29],d
	ret
;----------------------------------------------------------------------------------------
; Rutina de comprobaci�n de char's debajo del enemigo 
;----------------------------------------------------------------------------------------
compsueloenem:
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem
	ld	bc,#40		; bajo los pies
	add	hl,bc
	ld	a,[hl]
	ld	d,a
	inc	l
	ld	a,[hl]
	ld	e,a
	add	a,d
	jr	z,hayoscuridadbajoenemigo	; si la suma de los 2 attr.de debajo es 0 es que no est� iluminado.
	ld	a,d
	bit	6,a
	jp	nz,regAuno	; Si hay suelo continua
	ld	a,e
	bit	6,a
	jp	nz,regAuno	; Si hay suelo continua
	jp	regAcero
hayoscuridadbajoenemigo:
regAdos:
	ld	a,2
	ret
;----------------------------------------------------------------------------------------
; El enemigo est� cayendo por faltar le suelo			56902
;----------------------------------------------------------------------------------------
cayendoenem:							
	call	vuelcafondosprenem		
	call	compsueloenem			; comprobarsuelo del enemigo
	cp	1
	jr	z,cayendopillasuelo		; 1 hay suelo
	jr	nc,enem_trop_oscura	;haydarkbajosuspies		; 2 hay oscuridad
	ld	a,[ix+3]			; 0 no hay suelo bajo sus pies
	add	a,4
	ld	[ix+3],a
	call	avolcamiento_enem
	call	Antes_imp_ene
	jp	verotroenemigo
cayendopillasuelo:
	call	compvertprota
	and	a
	jr	z,vuelveabuscarnruta
	call	comprobarSEED		; Esto est� en ROUTE
	and	a
	jp	z,direccionhorizvert
	jp	verotroenemigo
vuelveabuscarnruta:
	call	buscarnuevaruta
	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; El enemigo ha chocado con la oscuriradad 
;----------------------------------------------------------------------------------------
enem_trop_oscura:
	ld	[ix+10],128	; para decirle a la rutina de gestioncamino que viene de desaparecer.
	ld	[ix+0],2
	ld	[ix+32],0
	ld	[ix+31],0
	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; El enemigo est� desapareciendo y reapareciendo al inicio.		
;----------------------------------------------------------------------------------------
reanimatorenem:
	ld	[ix+26],1			; Cambio los valores del enemigo lento
	ld	[ix+27],1			; Para el r�pido son los mismos.
	ld	a,[ghostenem]
	cp	1
	jr	z,cambioxysecanim
	jp	nc,fincambioxysecanim
	inc	a
	ld	[ghostenem],a
etiq_desde_gestionanimac_enem:
	ld	a,[ix+18]			; que enemigo hacer aparecer ?
	and	a
	jr	z,enemlistoapa0
	ld	[ix+11],$40			; dblowEnem	frame 0 de la desaparici�n
	ld	[ix+12],$c7
	ld	[ix+13],$40			; dblowini, ser� el de aparecer para despu�s
	ld	[ix+14],$c7
	jr	outreanimatorenem1y2
enemlistoapa0:	
	ld	[ix+11],$40			; dblowEnem frame 0 de la desaparici�n/ aparicion
	ld	[ix+12],$c6
	ld	[ix+13],$40			; dblowini, 
	ld	[ix+14],$c6
	jr	outreanimatorenem1y2
cambioxysecanim:
	inc	a
	ld	[ghostenem],a
	ld	a,[ix+18]			; que enemigo hacer aparecer ?
	and	a
	jr	z,enemlistoapa
	ld	[ix+11],$c0			; dblowEnem	frame 0 de la desaparici�n
	ld	[ix+12],$c6
	ld	[ix+13],$c0			; dblowini, ser� el de aparecer para despu�s
	ld	[ix+14],$c6
	jr	outreanimatorenem1y2
enemlistoapa:	
	ld	[ix+11],$c0			; dblowEnem	frame 0 de la desaparici�n
	ld	[ix+12],$c5
	ld	[ix+13],$c0			; dblowini, ser� el de aparecer para despu�s
	ld	[ix+14],$c5
outreanimatorenem1y2:
	call	Antes_imp_ene
	jp	verotroenemigo
fincambioxysecanim:
	call	vuelcafondosprenem
	xor	a
	ld	[ghostenem],a
	ld	[ix+20],1		; variable para saber que est�s animando una desaparici�n.
; En caso de que vengas apareciendo por que le ha golpeado el l�tigo del prota. Entonces pon sentido a derechas.
	ld	a,[ix+25]
	cp	2
	jr	nz,cont_fincambioxysecanim
	ld	[ix+15],0		; pon lo en la horizontal.
	ld	[ix+19],1		; y que mire a la izquierda.
cont_fincambioxysecanim	:
	call	buscarnuevaruta	
; hacer que el enemigo vuelva a la posici�n inicial.
	ld	[ix+1],0		; animar la aparici�n del enemigo
	ld	[ix+4],255
	ld	[ix+6],0		; Inicializo el valor de frame
	ld	[ix+25],0
	ld	[ix+31],0		; enemigo en ruta
	push	ix
	ld	ix,LA_LUZ_DE_PANTALLA
	ld	b,[ix+15]		; COLU
	ld	c,[ix+16]		; FILA
	pop	ix
	ld	[ix+2],b
	ld	[ix+3],c
	call	avolcamiento_enem	; Sacamos los valores para ix+21, +22, +23, +24
	ld	a,[ix+18]
	and	a
	jp	z,verotroenemigo	; valores correctos 1,1 para enemigo 0	
	ld	[ix+26],2
	ld	[ix+27],2
	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; Localizar lianas a la altura del enemigo. Si no las hay, activa ix+0 para saltar
;----------------------------------------------------------------------------------------
enemenganchadoaliana:
	call	c_dirbuffAtr_enem	; 56881
	ld	a,[hl]
	ld	d,a
	inc	l
	add	a,d
	cp	8
	jr	nz,enem_ya_noengancha
	ld	[ix+15],1
	ld	[ix+6],0		; frame
	ld	[ix+7],1		; total frames
	ld	[ix+0],0
	jp	verotroenemigo
enem_ya_noengancha:
	ld	[ix+0],1		; Cayendo, los ticks son m�s r�pidos. Hay que cambiarlos
	ld	[ix+15],0
	ld	[ix+6],0
	ld	[ix+7],3
; toma velocidad cambiar ticks y ver poruq� y poner el enemigo mirando a un lado
; como en call set-sent enem horiz der
	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; Enganchar liana / enganchado liana
;----------------------------------------------------------------------------------------
varsent1vert	defb	0	; variable nutrida en gestioncamino.asm
localizaliana:			; 56864
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem
	ld	a,[varsent1vert]
	and	a
	jr	z,mirarencimliana
; mirar liana debajo los pies
	ld	bc,#40		; bajo los pies
	add	hl,bc
	ld	a,[hl]
	ld	d,a
	inc	l
	ld	a,[hl]
	add	a,d
	cp	8
	jr	z,haylianas	; si la suma de los 2 attr.de debajo es 8 es hay lianas.
nohaylianas:
	call	buscarnuevaruta		; sino hay liana busca nueva ruta
	jp	verotroenemigo
haylianas:
	ld	[ix+0],16		; Enganchate a las lianas
	jp	verotroenemigo
mirarencimliana:
	ld	bc,#20		; sobre la cabeza
	sbc	hl,bc
	ld	a,[hl]
	ld	d,a
	inc	l
	ld	a,[hl]
	add	a,d
	cp	8
	jr	nz,nohaylianas
	ld	[ix+0],16		;ENGANCHATE salta para alcanzar las lianas
	jp	verotroenemigo
;----------------------------------------------------------------------------------------
; Detectar al prota. Tiene que verlo mirando al sitio donde est� y adem�s que no haya obst�culos enmedio.
;----------------------------------------------------------------------------------------
detectar_prota:
	ld	a,[ix+15]
	and	a
	jp	nz,detectar_prota_enliana
	ld	b,[ix+2]
	ld	c,b
	inc	c
	ld	a,[ix+19]
	cp	1
	jr	z,enemirandoderechas
; enemigomirando a la izquierda
; Meto $2b o $23 para usar s�lo una rutina y que INC o DEC el valor de HL
	ld	a,$2b		; hexadecimal de DEC HL
	ld	[searchObstEnem],a
	ld	a,$05		; hexadecimal de DEC B
	ld	[searchObstEnemB],a
	ld	a,[colu]
	cp	b
	jr	c,cont_detectar_prota_
	jr	z,cont_detectar_prota_
	cp	c
	jr	z,cont_detectar_prota_
	jr	enem_noen_linea
enemirandoderechas:
	ld	a,$23		; hexadecimal de INC HL
	ld	[searchObstEnem],a
	ld	a,$04		; hexadecimal de INC B
	ld	[searchObstEnemB],a
	ld	a,[colu]
	cp	b
	jr	z,cont_detectar_prota_
	cp	c
	jr	z,cont_detectar_prota_
	inc	a
	cp	b
	jr	z,cont_detectar_prota_
	jr	c,enem_noen_linea
cont_detectar_prota_:
; el enemigo est� mirando al prota
	ld	a,[ix+3]		; posY	del enemigo
	sub	8
	ld	b,a
	ld	a,[fila]
	ld	c,8	;	
	add	a,c	;
	ld	c,a	;	anterior solo ld c,a
	cp	b
	jr	c,enem_noen_linea
	ld	a,25			; el caracter por debajo de los pies del enemigo.		
	add	a,b
	ld	b,a
	ld	a,c
	cp	b
	jr	nc,enem_noen_linea
enem_en_linea:
	ld	a,[colu]
	ld	c,a
; poner a ix+10=0 si fila misma que fila-1 o fila+1 
	ld	[ix+10],0
; Vamos a ver si hay alg�n obst�culo de por medio. 
	ld	h,[ix+22]
	ld	l,[ix+21]	; hl a la direcci�n del enemigo en la videoram
	call	c_dirAtr_enem	; Hl contiene ahora el valor de esa direcci�n pero en videroramAttr
	ld	a,[ix+2]	; posX del enemigo
	ld	b,a	
searchObstEnem:
; Comprobaci�n de paredes entre el enemigo y el prota buscando el attr=67
	dec	hl
	ld	a,[hl]
	cp	67		; color magenta+BRILLO de los suelos y paredes
	jr	z,enem_noen_linea	; Si hay obst�culo NO ve al PROTA
	ld	a,b
	cp	c
	jr	z,finsearchObsaderEnem
searchObstEnemB:
	inc	b
	jr	searchObstEnem
finsearchObsaderEnem:
; Enemigo SI v� al prota.
; Asignaci�n de limites entre el enemigo y el prota
	ld	a,[colu]
	ld	[ix+16],a
	ld	a,[fila]
	ld	[ix+17],a
	ld	a,1
	ret
enem_noen_linea:
	ld	[ix+10],1		; se ha ido por encima DESARROLLAR ESTO.
	xor	a
	ret
detectar_prota_enliana:
	ld	b,[ix+2]		; colu
	ld	c,[ix+3]		; fila
	ld	a,[colu]
	cp	b
	jr	nz,enem_noen_linea
	ld	a,[fila]
	cp	c
	jr	c,protaenlianaarriba
	ld	a,[ix+19]		; el prota est� debajo del enemigo
	cp	2
	jr	z,finsearchObsaderEnem
	ld	[ix+19],2		; el enemigo andaba para arriba. Dale la vuelta
	jr	finsearchObsaderEnem
protaenlianaarriba:
	ld	a,[ix+19]
	and	a
	jr	z,finsearchObsaderEnem
	ld	[ix+19],0		; el enemigo andaba para abajo. Dale la vuelta
	jr	finsearchObsaderEnem
;----------------------------------------------------------------------------------------
; Comprobar si est� en el v�rtice al que ibas o en la posic. del prota.Limites ix+16 y 17
;----------------------------------------------------------------------------------------
compvertprota:							; 57473 
	ld	a,[ix+2]	; columna
	ld	b,[ix+16]
	cp	b
	jp	nz,regAcero
	ld	a,[ix+3]	; fila
	ld	b,[ix+17]
	cp	b
	jp	nz,regAcero
	jp	regAuno		; si coinciden ambos activar
;----------------------------------------------------------------------------------------
; Cambiar Sentido del enemigo horizontal. Frame 0
;----------------------------------------------------------------------------------------
Set_Sent_enem_horiz_der:
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	[ix+15],0
	ld	[ix+19],1
	ld	[ix+11],$80
	ld	[ix+13],$80
	ld	[ix+12],$c7
	ld	[ix+14],$c7	; maya
	ld	a,[ix+18]
	and	a
	jp	z,Antes_imp_ene
	ld	[ix+12],$c9
	ld	[ix+14],$c9	; olmeca
	jp	Antes_imp_ene
Set_Sent_enem_horiz_izq:
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	[ix+15],0
	ld	[ix+19],3
	ld	[ix+11],$80
	ld	[ix+13],$80
	ld	[ix+12],$c8
	ld	[ix+14],$c8	; maya
	ld	a,[ix+18]
	and	a
	jp	z,Antes_imp_ene
	ld	[ix+12],$ca
	ld	[ix+14],$ca	; olmeca
	jp	Antes_imp_ene
;----------------------------------------------------------------------------------------
; Pon frame 2 de los enemigos APARECIENDO cu�ndo han sido parados por el l�tigo o vienes de animarlos. HORIZONTAL
;----------------------------------------------------------------------------------------
; S�lo puedes golpear a los enemigos cu�ndo andan o saltan, por eso no miro si est�s en la liana
Setframe2_Sent_enem_horiz_der:
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	[ix+25],0	; Para caminar se supone que antes estabas apareciendo o muerto
	ld	[ix+15],0
	ld	[ix+6],2	; Para iniciar la variable frame en mirarticks despu�s
	ld	[ix+19],1
	ld	[ix+11],$00
	ld	[ix+12],$c8	; frame2 maya der
	ld	[ix+13],$80
	ld	[ix+14],$c7	; frame0 maya der
	ld	a,[ix+18]
	and	a
	jp	z,Antes_imp_ene
	ld	[ix+12],$ca	; frame2 olmeca der
	ld	[ix+14],$c9	; frame0 olmeca der
	jp	Antes_imp_ene
retornarchgsenthoriz:
	jp	Antes_imp_ene
Setframe2_Sent_enem_horiz_izq:
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	[ix+25],0	; Para caminar se supone que antes estabas apareciendo o muerto
	ld	[ix+15],0
	ld	[ix+6],2	; Para iniciar la variable frame en mirarticks despu�s
	ld	[ix+19],3
	ld	[ix+11],$00
	ld	[ix+12],$c9	; frame2 maya izq
	ld	[ix+13],$80
	ld	[ix+14],$c8	; frame0 maya izq
	ld	a,[ix+18]
	and	a
	jp	z,Antes_imp_ene
	ld	[ix+12],$cb	
	ld	[ix+14],$ca	; olmeca
	jp	Antes_imp_ene
;----------------------------------------------------------------------------------------
; Pon frame 3 de los enemigos APARECIENDO cu�ndo han sido parados por el l�tigo o vienes de animarlos. HORIZONTAL
;----------------------------------------------------------------------------------------
; S�lo puedes golpear a los enemigos cu�ndo andan o saltan, por eso no miro si est�s en la liana
Setframe3_Sent_enem_horiz_der:
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	[ix+15],0
	ld	[ix+25],0	; Para caminar
	ld	[ix+6],3	; Para iniciar la variable frame en mirarticks despu�s
	ld	[ix+19],1
	ld	[ix+11],$40
	ld	[ix+12],$c8	; frame2 maya der
	ld	[ix+13],$80
	ld	[ix+14],$c7	; frame0 maya der
	ld	a,[ix+18]
	and	a
	jp	z,Antes_imp_ene
	ld	[ix+12],$ca	; frame2 olmeca der
	ld	[ix+14],$c9	; frame0 olmeca der
	jp	Antes_imp_ene
Setframe3_Sent_enem_horiz_izq:
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	[ix+15],0
	ld	[ix+25],0	; Para caminar
	ld	[ix+6],3	; Para iniciar la variable frame en mirarticks despu�s
	ld	[ix+19],3
	ld	[ix+11],$40
	ld	[ix+12],$c9	; frame2 maya izq
	ld	[ix+13],$80
	ld	[ix+14],$c8	; frame0 maya izq
	ld	a,[ix+18]
	and	a
	jp	z,Antes_imp_ene
	ld	[ix+12],$cb	
	ld	[ix+14],$ca	; olmeca
	jp	Antes_imp_ene
;----------------------------------------------------------------------------------------
; El enemigo a tropezado con alg�n tipo de obst�culo. Dar la vuelta
;----------------------------------------------------------------------------------------
dar_vuelta_enem_en_vertical:
	ld	a,[ix+19]
	cp	2
	jr	z,a_Set_Sent_enem_vert_arr	; Bajando ?
	ld	[ix+19],2			; Baja ahora.
	call	Set_enemarraba_ambos
	jp	verotroenemigo
a_Set_Sent_enem_vert_arr:
	ld	[ix+19],0			; Sube ahora.
	call	Set_enemarraba_ambos
	jp	verotroenemigo
Set_enemarraba_ambos:
	ld	a,[ix+18]
	and	a
	jr	z,setenemarrabamaya
setenemarrabaolmeca:
	ld	[ix+11],$00
	ld	[ix+13],$00
	ld	[ix+12],$cc
	ld	[ix+14],$cc
	ret
setenemarrabamaya:
	ld	[ix+11],$80
	ld	[ix+13],$80
	ld	[ix+12],$cb
	ld	[ix+14],$cb
	ret