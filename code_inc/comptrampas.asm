;----------------------------------------------------------------------------------------
; M�xi. 4 trampas por pantalla. La tabla contiene X char Y pixels. Valor de ruptura = 0
tabla_trampas_enpantalla:					
	db	0,0
	db	0,0
	db	0,0
	db	0,0
	db	0
comp_trampas:			
; Comprobar inmunidad del protagonista.			
	ld	a,[inmunidad]
	and	a
	ret	nz		; Si est� activa, no mires si est� pUnch�ndose. Que eres MU de puncharte.

	push	hl
	push	ix
	ld	ix,tabla_trampas_enpantalla
bcomp_trampas:
; Trampas. Entrada fila y colu. Salida calculo limites del prota dejandolos en DE Y HL 
        ld	a,[fila]	
	inc	a
        ld	d,a		; d= fila +1px
        add	a,13
	ld	e,a		; e= fila +13px (ALTO)
        ld	a,[colu]
        ld	h,a		; h= colu
	inc	a
        ld	l,a	        ; l= colu +1    (ANCHO)
	ld	a,[ix+0]	
	ld	b,a		; b= col_tabla
	inc	a
	ld	c,a             ; c= col_tabla+1
;comprobamos Columnas
        ld	a,c		
	cp	h
	jr	c,Otratrampa
	ld	a,b
	cp	l
	jr	z,verfilatrampa
	jr	nc,Otratrampa
verfilatrampa:
        ld	a,[ix+1]	
	ld	b,a		; b= fil_dat
	add	a,16
        ld	c,a             ; c= fil_dat+alto Sp
;comprobamos Filas
	ld	a,c		
	cp	d
	jr	c,Otratrampa
	ld	a,b
	cp	e
	jr	c,Sonidoalpincharte
Otratrampa:
	ld	de,02
	add	ix,de
	ld	a,[ix+0]			; Valor X
	and	a
	jr	nz,bcomp_trampas		; si el valor es 0 --- > termina
	pop	ix
	pop	hl
	ret
Sonidoalpincharte:
	ld	a,14				; Efecto de da�o
	ld	[paraponerenB+1],a
	call	llamar_efecto
	ld	a,1
	ld	[desconener],a			; Descontar energ�a
	ld	[inmunidad],a			; Activar inmunidad del prota
						; para hacer que el prota parpadee
	inc	a
	ld	hl,etiqparpro+1
	ld	[hl],a				; para pinchos que la inmunidad sea menor.
	ld	[parpapro],a			; y tambi�n en la variable. La restauro en el engine.	
	call	quita_energia
	jr	Otratrampa


;----------------------------------------------------------------------------------------
;
; Colocamos las trampas de la pantalla al iniciarla
;

ponertrampas:
	ld	de,9
	ld	hl,tabla_de_trampas -9
	ld	a,[panta]
	ld	b,a
bponertrampas:
	add	hl,de
	ld	a,[hl]			
	cp	128 			; = 128 entonces fin tabla
	ret	z
	cp	b			; pantalla
	jr	nz,bponertrampas
	ld	de,tabla_trampas_enpantalla
	ld	bc,8
	inc	hl			; El n� de la pantalla no lo paso
	ldir
	ret
