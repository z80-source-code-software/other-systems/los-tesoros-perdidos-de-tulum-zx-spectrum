;-------------------------------------------------------------------------------
; Rutina que llama a efectos en el player. El n�mero de efecto se pone en B
; Cambiar B antes de llamar a la rutina
;-------------------------------------------------------------------------------

llamar_efecto:
	di				; 33723
	ld	b,3			; m�sicas en la p�gina 3
	call	setrambank		; rutina paginacion.asm
paraponerenB:
	ld	a,0			; Para el nuevo player se usa A y SET_SAMPLE
	call	SET_SAMPLE
; Cambio a p�gina 0
	ld	b, 0
	call setrambank
	ei
	ret
;-------------------------------------------------------------------------------
; Rutina que llama a efectos en el player. El n�mero de efecto se pone en A
; Cambiar A antes de llamar a la rutina
;-------------------------------------------------------------------------------
llamar_musica:
	di			; deshabilitar interrupciones
	ld	b,3		; ; cambio a p�gina 3	MUSICAS en la p�gina 3
	call	setrambank	; rutina paginacion.asm
	call	INIT_BUFFERS	; En el player antes		call	PLAYER_OFF
poncancionenA:
	ld	a,0			; Cargo la cancion 0
	call	CARGA_CANCION		; Hacemos sonar la canci�n con el player desde la ISR
	ld	b,0
	call	setrambank
	ei
	ret
;-------------------------------------------------------------------------------
; Rutina que para el player y calla la m�sica.
;-------------------------------------------------------------------------------
parar_musica:
	di			; deshabilitar interrupciones
	ld	b,3		; ; cambio a p�gina 3	MUSICAS en la p�gina 3
	call	setrambank	; rutina paginacion.asm
        call	PLAYER_OFF
	ld	b,0
	call	setrambank
	ei
	ret

