;----------------------------------------------------------------------------------------
; Tabla de ENEMIGOS en pantalla. 
;----------------------------------------------------------------------------------------
;----------------------------------------------------------------------------------------
; Sprites APARECIENDO
;	org	$c580				; 50560	include "gfx_inc\Apaenemigos.asm"
; Maya		Frame0	c580	Frame1	c5c0	Frame2	c600
; Maya		Frame3 (frame en pausa) c640
; Olmeca	Frame0	c680	Frame1	c6c0	Frame2	c700
; Olmeca	Frame3 (frame en pausa)	c740
;----------------------------------------------------------------------------------------
; Sprites Enemigos
;	org	$c780	include "gfx_inc\Sprenemigos.asm"	; Tama�o de 1 frame =64 o 40$
; Maya derecha		0,1,2,3		c780,c7c0,c800,c840
; Maya izquierda	0,1,2,3		c880,c8c0,c900,c940
; Olmeca derecha	0,1,2,3		c980,c9c0,ca00,ca40
; Olmeca izquierda	0,1,2,3		ca80,cac0,cb00,cb40
; Maya sube/baja	0,1		cb80,cbc0
; Olmeca sube/baja	0,1		cc00,cc40
churrodbsenemigos	equ	34	; n�bytes por churro de enemigo			
ENEMIGOS:		
; MAYA					; 36353
	db	32	; Accion	ix0
;	 0=caminar, 1=saltar,2=enlaliana,4=localizar al prota,8=localizar hueco,16=engancharse a lianas
	db	0	; activo	ix1
;	 (si o no). A 1 actua. A 0 est� como estatua o hay cambio de sprites etc)
	db	0	; posx		ix2	en char 
	db	0	; posy		ix3	en pixels
	db	1	; ticks		ix4	al inicio y al golpear al enemigo tiene valor 255
	db	4	; frameskip	ix5	en la rutina previa en llamada se actuliza seg�n el nivel
	db	0	; frameActual	ix6
	db	3	; framestotal	ix7
	db	16	; altosc	ix8
	db	2	; anchochars	ix9     para caract ancho del enemigo
	db	0	; PosAlturaPro	ix10	(antiguo tama�o1frame para sumar a dblowAnimMem) 
;	Posici�n en altura del prota con respecto al enemigo. Valores :
;	0 misma altura incluyendo + - 1 char de 
;	1 arriba del enemigo
;	2 abajo del enemigo
	db	$80	; dblowEnem	ix11	Al inicio ser� el de aparecer el enemigo
	db	$c5	; dbhighEnem	ix12	"			"
	db	$40	; dblowEnemIni	ix13	"			"
	db	$c6	; dbhighEnemIni ix14	"			"
	db	0	; direccion	ix15	seg�n direcci�n. direccion ix+15 0 horizontal, 1 vertical	35722
	db	0	; PosProtaX	ix16	(limite1)Posici�n X del prota al mirar al entrar en pantalla
	db	0	; PosProtaY	ix17	(limite2)Posici�n Y del prota al mirar al entrar en pantalla
	db	0	; codanim	ix18
;	c�digo para como deben actuar los enemigos. El listo =0 y el tonto =1.
;	Podr�a intentar que en caso de que el enemigo listo estuviera parado (porque le has golpeado) 
;	cambiar el tonto a listo ese tiempo y luego volver a ser tonto cu�ndo el primero se ha activado.
	db	1	; sentido	ix19									35726
;	sentido en la direcci�n: 0 arriba o derecha  ,  1 abajo o izquierda. 
	db	1	; ANTES desplaz	ix20	Para saber si estoy animando el enem. despu�s de una desaparici�n
	db	0	; dblowPant	ix21
	db	0	; dbhighPant	ix22
	db	0	; dblowBuff	ix23
	db	0	; dbhighBuff	ix24
	db	0	; enemigoMuerto	ix25
	db	1	; moretime	ix26	moretime junto a frameskip ser� el valor del tiempo para 
;	que aparezcan los enemigos o se activen despu�s de haber sido golpeados por el prota. Ejm: 01ffh
;	moretime 01 frameskip ff. Podr�a intentar que los valores de moretime cambi�n desde 04 a 01 seg�n el nivel en el que est�s.
	db	1	; restmoretime	ix27	
	dw	TAB_RUTA_ENEM1		; ix28, +29 Puntero de la tabla ruta del enemigo 2 bytes
	db	0	; valorSEED	ix30
	db	0	;		ix31	Enemigo en ruta o persiguiendo al prota ? 36384
	db	0	; persigue	ix32	Prota detectado.
	db	0	; bytesalenem	ix33	Contador interno del salto del enemigo





; OLMECA				; 36387
	db	32	; Accion	ix0
;	 0=caminar, 1=saltar,2=enlaliana,4=localizar al prota,8=localizar hueco,16=engancharse a lianas
	db	0	; activo	ix1
;	 (si o no). A 1 actua. A 0 est� como estatua o hay cambio de sprites etc)
	db	0	; posx		ix2	en char 
	db	0	; posy		ix3	en pixels
	db	1	; ticks		ix4	al inicio y al golpear al enemigo tiene valor 255
	db	6	; frameskip	ix5	en la rutina previa en llamada se actuliza seg�n el nivel
	db	0	; frameActual	ix6
	db	3	; framestotal	ix7
	db	16	; altosc	ix8
	db	2	; anchochars	ix9     para caract ancho del enemigo
	db	0	; PosAlturaPro	ix10	(antiguo tama�o1frame para sumar a dblowAnimMem) 
;	Posici�n en altura del prota con respecto al enemigo. Valores :
;	0 misma altura incluyendo + - 1 char de 
;	1 arriba del enemigo
;	2 abajo del enemigo
	db	$80	; dblowEnem	ix11	Al inicio ser� el de aparecer el enemigo
	db	$c6	; dbhighEnem	ix12	"			"
	db	$40	; dblowEnemIni	ix13	"			"
	db	$c7	; dbhighEnemIni ix14	"			"
	db	0	; direccion	ix15	seg�n direcci�n. direccion ix+15 0 horizontal, 1 vertical
	db	0	; PosProtaX	ix16	(limite1)Posici�n X del prota al mirar al entrar en pantalla
	db	0	; PosProtaY	ix17	(limite2)Posici�n Y del prota al mirar al entrar en pantalla
	db	1	; codanim	ix18
;	c�digo para como deben actuar los enemigos. El listo =0 y el tonto =1.
;	Podr�a intentar que en caso de que el enemigo listo estuviera parado (porque le has golpeado) 
;	cambiar el tonto a listo ese tiempo y luego volver a ser tonto cu�ndo el primero se ha activado.
	db	1	; sentido	ix19	
;	sentido en la direcci�n: 0 arriba o derecha  ,  1 abajo o izquierda. 
	db	1	; ANTES desplaz	ix20	Para saber si estoy animando el enem. despu�s de una desaparici�n
	db	0	; dblowPant	ix21
	db	0	; dbhighPant	ix22
	db	0	; dblowBuff	ix23
	db	0	; dbhighBuff	ix24
	db	0	; enemigoMuerto	ix25
	db	2	; moretime	ix26	moretime junto a frameskip ser� el valor del tiempo para 
;	que aparezcan los enemigos o se activen despu�s de haber sido golpeados por el prota. Ejm: 01ffh
;	moretime 01 frameskip ff. Podr�a intentar que los valores de moretime cambi�n desde 04 a 01 seg�n el nivel en el que est�s.
	db	2	; restmoretime	ix27	
	dw	TAB_RUTA_ENEM2		; ix28, +29 Puntero de la tabla ruta del enemigo 2 bytes
	db	0	; valorSEED	ix30
	db	0	;		ix31	Enemigo en ruta o persiguiendo al prota ?
	db	0	; persigue	ix32	Prota detectado.
	db	0	; bytesalenem	ix33	Contador interno del salto del enemigo
	db 128
; rutas y sentidos			; 36422
TAB_RUTA_ENEM1:
	db	128,128		;(0) + sentido
	db	128,128		;(1) + "
	db	128,128		;(2) + sentido
	db	128,128		;(3) + "
	db	128,128		;(4) + sentido
	db	128,128		;(5) + "
	db	128,128		;(6) + sentido
	db	128,128		;(7) + "
	db	128,128		;(8) + "
TAB_RUTA_ENEM2:				; 36438
	db	128,128		;(0) + sentido	
	db	128,128		;(1) + "
	db	128,128		;(2) + sentido
	db	128,128		;(3) + "
	db	128,128		;(4) + sentido
	db	128,128		;(5) + "
	db	128,128		;(6) + sentido
	db	128,128		;(7) + "
	db	128,128		;(8) + "


;36458 a 36469 incluido ANTES AQUI LA REST DE DATOS ENEMIGOS SIMPLE
;12 nops
	nop
	nop
	nop
	nop
	nop
	nop

	nop
	nop
	nop
	nop
	nop
	nop


RESTENEMIGOS:		

; Defb's con los siguientes datos
; MAYA					; 36468
	db	32	; Accion	ix0
	db	0	; activo	ix1
	db	0	; posx		ix2	en char 
	db	0	; posy		ix3	en pixels
	db	1	; ticks		ix4	al inicio y al golpear al enemigo tiene valor 255
	db	4	; frameskip	ix5	en la rutina previa en llamada se actuliza seg�n el nivel
	db	0	; frameActual	ix6
	db	3	; framestotal	ix7
	db	16	; altosc	ix8
	db	2	; anchochars	ix9     para caract ancho del enemigo
	db	0	; PosAlturaPro	ix10	(antiguo tama�o1frame para sumar a dblowAnimMem) 
	db	$80	; dblowEnem	ix11	Al inicio ser� el de aparecer el enemigo
	db	$c5	; dbhighEnem	ix12	"			"
	db	$40	; dblowEnemIni	ix13	"			"
	db	$c6	; dbhighEnemIni ix14	"			"
	db	0	; direccion	ix15	seg�n direcci�n. direccion ix+15 0 horizontal, 1 vertical	35722
	db	0	; PosProtaX	ix16	(limite1)Posici�n X del prota al mirar al entrar en pantalla
	db	0	; PosProtaY	ix17	(limite2)Posici�n Y del prota al mirar al entrar en pantalla
	db	0	; codanim	ix18
	db	1	; sentido	ix19									35726
	db	1	; ANTES desplaz	ix20	Para saber si estoy animando el enem. despu�s de una desaparici�n
	db	0	; dblowPant	ix21
	db	0	; dbhighPant	ix22
	db	0	; dblowBuff	ix23
	db	0	; dbhighBuff	ix24
	db	0	; enemigoMuerto	ix25
	db	1	; moretime	ix26	moretime junto a frameskip ser� el valor del tiempo para 
	db	1	; restmoretime	ix27	
	dw	TAB_RUTA_ENEM1		; ix28, +29 Puntero de la tabla ruta del enemigo 2 bytes
	db	0	; valorSEED	ix30
	db	0	;		ix31	Enemigo en ruta o persiguiendo al prota ?
	db	0	; persigue	ix32	Prota detectado.
	db	0	; bytesalenem	ix33	Contador interno del salto del enemigo

; OLMECA				; 36504
	db	32	; Accion	ix0
	db	0	; activo	ix1
	db	0	; posx		ix2	en char 
	db	0	; posy		ix3	en pixels
	db	1	; ticks		ix4	al inicio y al golpear al enemigo tiene valor 255
	db	6	; frameskip	ix5	en la rutina previa en llamada se actuliza seg�n el nivel
	db	3	; frameActual	ix6
	db	3	; framestotal	ix7
	db	16	; altosc	ix8
	db	2	; anchochars	ix9     para caract ancho del enemigo
	db	0	; PosAlturaPro	ix10	(antiguo tama�o1frame para sumar a dblowAnimMem) 
	db	$80	; dblowEnem	ix11	Al inicio ser� el de aparecer el enemigo
	db	$c6	; dbhighEnem	ix12	"			"
	db	$40	; dblowEnemIni	ix13	"			"
	db	$c7	; dbhighEnemIni ix14	"			"
	db	0	; direccion	ix15	seg�n direcci�n. direccion ix+15 0 horizontal, 1 vertical
	db	0	; PosProtaX	ix16	(limite1)Posici�n X del prota al mirar al entrar en pantalla
	db	0	; PosProtaY	ix17	(limite2)Posici�n Y del prota al mirar al entrar en pantalla
	db	1	; codanim	ix18
	db	1	; sentido	ix19	
	db	1	; ANTES desplaz	ix20	0 no animando y 1 est�s animando
	db	0	; dblowPant	ix21
	db	0	; dbhighPant	ix22
	db	0	; dblowBuff	ix23
	db	0	; dbhighBuff	ix24
	db	0	; enemigoMuerto	ix25
	db	2	; moretime	ix26	moretime junto a frameskip ser� el valor del tiempo para 
	db	2	; restmoretime	ix27	
	dw	TAB_RUTA_ENEM2		; ix28, +29 Puntero de la tabla ruta del enemigo 2 bytes
	db	0	; valorSEED	ix30
	db	0	;		ix31	Enemigo en ruta o persiguiendo al prota ?
	db	0	; persigue	ix32	Prota detectado.
	db	0	; bytesalenem	ix33	Contador interno del salto del enemigo
; 36539