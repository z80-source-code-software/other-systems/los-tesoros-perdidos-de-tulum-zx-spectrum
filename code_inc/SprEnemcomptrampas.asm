;----------------------------------------------------------------------------------------
; Uso tabla_trampas_enpantalla la misma que la del prota. X char Y scannes	59437
Enem_comp_trampas:			
	push	ix
	push	hl
	ld	ix,ENEMIGOS
Enem_loop_comp_trampas:
	push	ix
	ld	hl,tabla_trampas_enpantalla
Enem_bcomp_trampas:
; Trampas. Entrada fila y colu. Salida calculo limites del prota dejandolos en DE Y HL 
        ld	a,[ix+2]	; columna del enemigo
        ld	d,a		; d= colu
	inc	a
        ld	e,a	        ; e= colu +1    (ANCHO)
	ld	a,[hl]		; columna de la tabla	
	ld	b,a		; b= col_tabla
	inc	a
	ld	c,a             ; c= col_tabla+1
;comprobamos Columnas
	cp	d
	jr	c,Enem_Otratrampa
	ld	a,b
	cp	e
	jr	z,Enem_verfilatrampa
	jr	nc,Enem_Otratrampa
Enem_verfilatrampa:
	inc	hl
        ld	a,[hl]	; fila del enemigo	
	ld	b,a		; b= fil_tabla
	add	a,16
        ld	c,a             ; c= fil_tabla+alto 16px
;comprobamos Filas
        ld	a,[ix+3]	
	inc	a
        ld	d,a		; d= fila enemigo +1px
        add	a,13
	ld	e,a		; e= fila enemigo +13px (ALTO)
	ld	a,c		
	cp	d
	jr	c,bEnem_Otratrampa
	ld	a,b
	cp	e
	jr	c,Enem_Sonidoalpincharte
Enem_Otratrampa:
	inc	hl
bEnem_Otratrampa:
	inc	hl
	ld	a,[hl]		; Valor X
	and	a
	jr	nz,Enem_bcomp_trampas		; si el valor es 0 --- > termina
Enem_Otratrampa2:
	pop	ix
	ld	de,churrodbsenemigos		; 32 decimal, valores de la tabla enemigos
	add	ix,de
	ld	a,[ix+0]
	rla
	jp	nc,Enem_loop_comp_trampas	; IX+0 no puede ser igual o superior a 128
	pop	ix
	pop	hl
	ret
Enem_Sonidoalpincharte:
; Desaparecer y aparecer enemigo
	ld	[ix+0],2	; Reanimator !!!
	jr	Enem_Otratrampa2

