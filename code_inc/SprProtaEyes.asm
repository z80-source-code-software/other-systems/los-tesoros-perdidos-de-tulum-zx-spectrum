
; -------------------------------------------------------------------------------------------------
; Rutina y tablas para la impresi�n de los ojos del prota en las zonas que no llevas la luz.
; -------------------------------------------------------------------------------------------------
eyes_onoff:
	ld	a,[enganchao]		; Si estas enganchao en la liana no imprimas el attr
	rra				; imprime el valor del buffer iluminado
	jr	nc,antesbeyes
	call	Coloca_valor_buffer_iluminado
	jr	beyes
antesbeyes:	
	ld	a,[senpro]		; si es 2 es que est�s cayendo -> no imprimas attr
	cp	2			; 
	jr	z,beyes			
; Volcamos los attr del buffer_attr
	call	calc_attr_prota
	ex	de,hl
	call	calc_dirbuffattr_prota	;SprProtaVolcAttr4.asm
	ldi	
	ldi				; primera fila del prota y unica para que se vean los ojos
beyes:
; Si los ojos ya est�n activados, comprueba si todav�a est�s en total oscuridad.
	ld	a,[activ_eyes]
	rra
	jp	c,eyes_off
; Comprobamos si hay oscuridad a nuestro alrededor
	call	check_out_darkness
	rra	
	jp	nc,Coloca_valor_buffer_iluminado
; Vamos a poner los ojos en la fila del prota, necesitamos dir_spr
	ld	a,1
	ld	[activ_eyes],a	; Activo los ojos
;-------------------------------------------------------------------------------------------
;	spr_der_OJOS	= $c300 	$c340
;	spr_izq_OJOS	= $c380		$c3c0		
;-------------------------------------------------------------------------------------------	
;
; Coloco el valor de los ojos a la derecha en cada etiqueta del engine y de movimientos 
;
; frame 1. Ojos derecha
	ld	hl,gfxprotaojosder1			; $c340
	ld	[protaparadoderecha+1],hl
	ld	[MovEtiqArrancaDe_1+1],hl
	ld	[MovEtiqChgde_1+1],hl
	ld	[MovEtiqSalde_1+1],hl
	ld	[MovEtiqFinSaltode_1+1],hl
	ld	[MIRADER_DISPARO_LATIGO2+1],hl
; Ojos izquierda
	ld	hl,gfxprotaojosizq1			; ld	l,$c0			; $c3c0
	ld	[protaparadoizquierda+1],hl
	ld	[MovEtiqArrancaIz_1+1],hl
	ld	[MovEtiqChgiz_1+1],hl
	ld	[MovEtiqSaliz_1+1],hl
	ld	[MovEtiqFinSaltoiz_1+1],hl
	ld	[MIRAIZQ_DISPARO_LATIGO2+1],hl
; frame 0. Ojos derecha
	ld	hl,gfxprotaojosder0			; ld	l,$00
	ld	[EngineEtiqDERECHA_0+1],hl
	ld	[MovEtiqArrancaDe_0+1],hl
	ld	[MovEtiqChgde_0+1],hl
	ld	[MovEtiqFinSaltode_0+1],hl
	ld	[MIRADER_DISPARO_LATIGO+1],hl
; Ojos izquierda
	ld	hl,gfxprotaojosizq0			; ld	l,$80
	ld	[EngineEtiqIZQUIERDA_0+1],hl
	ld	[MovEtiqArrancaIz_0+1],hl
	ld	[MovEtiqChgiz_0+1],hl
	ld	[MovEtiqFinSaltoiz_0+1],hl
	ld	[MIRAIZQ_DISPARO_LATIGO+1],hl
;
; Ver que direcci�n llevas para colocar los ojos correspondientes
;
	ld	a,[dirmovsp]		; Discriminamos el sentido del prota.
	cp	1
	jr	nz,retornaOjos
;Ojos_derecha
	ld	hl,gfxprotaojosder0			; ld	l,$00
;
; Salida de ambas partes de la rutina. EyesON/OFF
;
retornaOjos:
;
	ld	[spr],hl
	call	pon_spr_buff		; vuelco spr en buffer desplaz. (engine)
	call	busq_num_desp		; calculo el valor del desplazamiento Valor en A y B
	and	a			; si el valor es 0 no lo desplaces
	jp	z,bcalc_pon_spr_prota	; en el engine. Vuelco el sprite nuevo, lo mezclo y retorno
	ld	a,[dirmovsp]		; Discriminamos el sentido del prota.
	cp	3
	jr	z,LOOPojosiz
LOOPojosde:
	push	bc
	call	desp_spr_der		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPojosde
	jp	bcalc_pon_spr_prota
LOOPojosiz:
	push	bc
	call	desp_spr_izq		; desplazo a la derecha el frame
	pop	bc
	djnz	LOOPojosiz
	jp	bcalc_pon_spr_prota	; en el engine. Vuelco el sprite nuevo, lo mezclo y retorno
;-------------------------------------------------------------------------------------------	
eyes_off:
; Ya est�s a oscuras as� que si no te importa, colorea los ojos con el color del fondo.
; despu�s en el engine apaga el color del fondo al pasar por encima.
;
; Comprobamos si hay oscuridad a nuestro alrededor
;
	call	check_out_darkness
	rra	
	ret	c		; Si hay acarreo es que est� totalmente a oscuras. Entonces...
;
; Desactiva la variable Ojos activos y que aparezca el prota.
;
	xor	a
	ld	[activ_eyes],a	; Desactivo los ojos
;
; Coloco el valor de prota para derecha e izquierda en cada etiqueta del engine y de movimientos 
;	
;-------------------------------------------------------------------------------------------
;	spr_der_parado	= $c280					En parado
;	spr_izq_parado	= $c2c0				
restaurarprota:			; Etiqueta para usarse en compobjetos3.asm
	ld	hl,gfxprotaparadoizq	; $c2c0
	ld	[protaparadoizquierda+1],hl
	ld	hl,gfxprotaparadoder	; $80
	ld	[protaparadoderecha+1],hl
;	spr_latigo_der_0 = $bf00	spr_latigo_izq_0 = $bf80	Usando el latigo
;	spr_latigo_der_1 = $bf40	spr_latigo_izq_1 = $bfc0
	ld	hl,gfxprotalatigoder0	; $bf00	Frame 0 prota a derecha l�tigo
	ld	[MIRADER_DISPARO_LATIGO+1],hl
	ld	hl,gfxprotalatigoder1	; l,$40	Frame 1 prota a derecha l�tigo
	ld	[MIRADER_DISPARO_LATIGO2+1],hl
	ld	hl,gfxprotalatigoizq0	; l,$80	Frame 0 prota a izquierda l�tigo
	ld	[MIRAIZQ_DISPARO_LATIGO+1],hl
	ld	hl,gfxprotalatigoizq1	; l,$c0	Frame 1 prota a izquierda l�tigo
	ld	[MIRAIZQ_DISPARO_LATIGO2+1],hl
;-------------------------------------------------------------------------------------------
;	spr_der_body	= $c000 	$c040
;	spr_izq_body	= $c100		$c140		
; frame 1
	ld	hl,gfxprotader1		; $c040	Frame 1 prota a derecha. Incluyo prota parado.
	ld	[MovEtiqArrancaDe_1+1],hl
	ld	[MovEtiqChgde_1+1],hl
	ld	[MovEtiqSalde_1+1],hl
	ld	[MovEtiqFinSaltode_1+1],hl
	ld	hl,gfxprotaizq1		; inc	h  Frame 1 prota a izquierda. "	"
	ld	[MovEtiqArrancaIz_1+1],hl
	ld	[MovEtiqChgiz_1+1],hl
	ld	[MovEtiqSaliz_1+1],hl
	ld	[MovEtiqFinSaltoiz_1+1],hl
; frame 0
	ld	hl,gfxprotader0		; $c000	 Inicio prota a derecha
	ld	[EngineEtiqDERECHA_0+1],hl
	ld	[MovEtiqArrancaDe_0+1],hl
	ld	[MovEtiqChgde_0+1],hl
	ld	[MovEtiqFinSaltode_0+1],hl
	ld	hl,gfxprotaizq0		; inc	h Inicio prota a izquierda
	ld	[EngineEtiqIZQUIERDA_0+1],hl
	ld	[MovEtiqArrancaIz_0+1],hl
	ld	[MovEtiqChgiz_0+1],hl
	ld	[MovEtiqFinSaltoiz_0+1],hl
;
; Ver que direcci�n llevas para colocar los ojos correspondientes
;
	ld	a,[dirmovsp]		; Discriminamos el sentido del prota.
	cp	1
	jp	nz,retornaOjos		; Si vas a izquierdas hl= $c100
;	spr_der_body	= $c000 	spr_izq_body	= $c100		$c140		
	ld	hl,gfxprotader0		; $c000	 Inicio prota a derecha  ; 	dec	h
	jp	retornaOjos
;
; Saber si el prota est� totalmente oscurecido.
;
check_out_darkness:
        ld      hl,bufferdeiluminacion	; $aa80 antes $de00		Inicio buffer iluminacion
	call	calc_dir_buff_ilum_delprota	; En sprProtaVolcAttr.asm
; hl = valor en el buffer de iluminaci�n del primer char del prota
	ld	bc,31		; 3/10
	ld	a,[hl]		; 2/7
	and	a		; 1/4
	jp	nz,regAcero
	inc	l		; 1/4
	ld	a,[hl]		; 2/7
	and	a		; 1/4
	jp	nz,regAcero
	add	hl,bc		; 3/11
	ld	a,[hl]		; 2/7
	and	a		; 1/4
	jp	nz,regAcero
	inc	l		; 1/4
	ld	a,[hl]		; 2/7
	and	a		; 1/4
	jp	nz,regAcero
	jp	regAuno		; Lo envio a SprProtaCompObst.asm donde pongo a 0 o 1 el registro A
;
; Traspasamos los valores de atributo de la pantalla al buffer de iluminaci�n. Asi este cu�ndo algo est� a oscuras, pondr� un 0 en el buffer.
;
llenadoBufferIluminacion:
	push	hl
	push	de
	push	bc
	ld	hl,$5880	; Linea 0 de la videoRAM de la pantalla del juego
	ld	de,bufferdeiluminacion	; $aa80	 antes $de00	 Buffer de iluminacion
	ld	bc,511
	ldir
	pop	bc
	pop	de
	pop	hl
	ret
;
; Ahora los valores del buffer de iluminaci�n los paso a la videoram. El que est� iluminado, lo estar� y el que no lo estaba si pasabas a oscuras, desaparece
;
Coloca_valor_buffer_iluminado:
        ld      hl,bufferdeiluminacion	; $aa80 Inicio buffer Iluminacion
; Necesario para calcular la direcc.buffer de iluminaci�n y no duplicarla.
	call	calc_dir_buff_ilum_delprota
; de= videoram attr. hl= direccion en buffer de iluminacion.
	ldi
	ldi
	ld	bc,30
	add	hl,bc
	ex	de,hl
	add	hl,bc
	ex	de,hl
	ldi
	ldi
	ret