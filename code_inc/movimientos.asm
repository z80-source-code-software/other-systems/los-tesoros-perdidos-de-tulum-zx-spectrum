; Dia 22/04/2020 modificadas o mejor dicho comentadas las lineas 707 a 719



;----------------------------------------------------------------------------------------
;
; Rutinas y tablas de los movimientos Especiales
;
;----------------------------------------------------------------------------------------

;----------------------------------------------------------------------------------------
; Tablas de los movimientos especiales del protagonista.
;----------------------------------------------------------------------------------------
;
; Tabla de calculo del desplazamiento del frame en buffrotspr y en el salto en buffortspr
;
tabla_des_spr:
		db	0,0
		db	1,1
		db	2,2
		db	3,3
		db	4,0
		db	5,1
		db	6,2
		db	7,3
;
; Tabla de calculo del desplazamiento del frame en el salto en buffortspr
;
tabla_des_spr_salto:
		db	0,3
		db	1,2
		db	2,1
		db	3,0
		db	4,0
		db	5,1
		db	6,2
		db	7,3
;	
; Tabla de cambio de direccion del prota(frames)mov.horizontal y la 2 para mov.vertical 
;	
tabla_chg_spr:	
	db		0,7,3
	db		1,6,2
	db		2,5,1
	db		3,4,0
	db		4,3,3
	db		5,2,2
	db		6,1,1
	db		7,0,0
;
; Tabla de desplazamiento en pixels de la vertical en el salto.
;
tab_sal_ve:		
        db       -4,-3,-3,-2,-2,-1,-1,0,0,0,0,1, 1, 2, 2, 2, 0, 0
;
; Tabla pixeles a desplazar en altura, valor a sumar a fila en B (primer dato x2) y en C el valor del volcado de fondo
;
tabla_pix_salto:
	db	4,12
	db	3,13
	db	2,14
	db	1,15
;----------------------------------------------------------------------------------------
; Protagonista en parado
;----------------------------------------------------------------------------------------
PROTA_PARADO:							;52562
; Si ya lo he cambiado continuo en el final de la rutina
	ld	a,[proparado]
	and	a
	ret	nz
; Mirar si el valor de frame es superior o igual a 3
	ld	a,[frame]		; Si el frame es inferior est� con pies juntos
	cp	4			; por lo tanto no cambio el sprite a parado
	ret	c			; ni lo desplazo
PORFINDESALTO:				; Etiqueta que llamo al finalizar el salto.
; Discriminamos el sentido del prota.
	ld	a,[dirmovsp]
	cp	1
	jr	z,protaparadoderecha	; BAN_DE
; PARADO A IZQUIERDAS____________________________________________
;
protaparadoizquierda:
	ld	hl,gfxprotaparadoizq	; $c2c0	para poner los ojos, pon en hl la direccion del buffer
PARALANZA_IZ:
	call	PARA_ARRANCA
	and	a			; si el valor es 0 no lo desplaces
	jr	z,Antes_fin_parado
	call	DESPLAZOFRAMEIZQ
	jr	Antes_fin_parado
; PARADO A DERECHAS_______________________________________________
;
protaparadoderecha:
	ld	hl,gfxprotaparadoder	; $c280	para poner los ojos, pon en hl la direccion del buffer
PARALANZA_DE:
	call	PARA_ARRANCA
	and	a			; si el valor es 0 no lo desplaces
	jr	z,Antes_fin_parado
	call	DESPLAZOFRAMEDER
Antes_fin_parado:			; Salidas de la rutina: PROTA_parado, arrancar desde PARADO 
	ld	a,1			; y cambio de sentido del protagonista
ARRANCA_fin_parado:
	ld	[proparado],a
	jp	bcalc_pon_spr_prota	; en el engine. Es lo mismo que lo anterior y no lo repito
;----------------------------------------------------------------------------------------
; Protagonista arrancar desde PARADO
;----------------------------------------------------------------------------------------
ARRANCA_IZ:				; ARRANQUE DE IZQUIERDAS_________________________52502
	ld	a,[frame]
	cp	4
	jr	c,MovEtiqArrancaIz_0	; jr	c,BARRANCA_IZ
MovEtiqArrancaIz_1:
	ld	hl,gfxprotaizq1		; $c140
	jr	BBARRANCA_IZ
MovEtiqArrancaIz_0:			;BARRANCA_IZ:
	ld	hl,gfxprotaizq0		; $c100	spr0=parado 
BBARRANCA_IZ:
	call	PARA_ARRANCA
	and	a			; si el valor es 0 no lo desplaces
	jr	z,ARRANCA_fin_parado
	call	DESPLAZOFRAMEIZQ
Antes_fin_parado2:
	xor	a
	jr	ARRANCA_fin_parado
ARRANCA_DE:				; ARRANQUE DE DERECHAS___________________________
	ld	a,[frame]
	cp	4
	jr	c,MovEtiqArrancaDe_0	;
MovEtiqArrancaDe_1:
	ld	hl,gfxprotader1		; $c040
	jr	BBARRANCA_DE
MovEtiqArrancaDe_0:			;BARRANCA_DE:
	ld	hl,gfxprotader0		; $c000	Prota parado a derechas en posicion frame = 4
BBARRANCA_DE:
	call	PARA_ARRANCA
	and	a			; si el valor es 0 no lo desplaces
	jr	z,ARRANCA_fin_parado
	call	DESPLAZOFRAMEDER
	xor	a
	jr	ARRANCA_fin_parado
;----------------------------------------------------------------------------------------
; Parado o arrancado
;-----------------------------------------------------------------------------52556------
PARA_ARRANCA:
	ld	[spr],hl
	call	pon_spr_buff		; vuelco spr en buffer desplaz. (engine)
; Buscar en una tabla las veces a desplazar el sprite para que quede en la posic.de parado
;
busq_num_desp:
	ld	hl,tabla_des_spr	; movimientos.asm			52581
busq_num_desp_salto:
	ld	a,[frame]
	add	a,a
	add	a,l
	ld	l,a
	inc	hl
	ld	a,[hl]
	ld	b,a
	ret
DESPLAZOFRAMEDER:
	push	bc
	call	desp_spr_der		; desplazo a la derecha el frame
	pop	bc
	djnz	DESPLAZOFRAMEDER
	ret
DESPLAZOFRAMEIZQ:
	push	bc
	call	desp_spr_izq		; desplazo a la izquierda el frame
	pop	bc
	djnz	DESPLAZOFRAMEIZQ
	ret
;----------------------------------------------------------------------------------------
; Rutina de cambio de Sentido del protagonista arr y aba
;----------------------------------------------------------------------------------------
chg_sen_spr_AB:									; 52594
	ld	hl,tabla_chg_spr
	ld	a,[framev]
	ld	b,a	; Multiplicaci�n de un numero por 3. Guardar en b como primer valor de la multiplicacion
	sla	a	; El segundo valor de la multip. Se desplaza 1 vez a la izquierda
	add	a,b	; como en una multiplicaci�n normal se suman ambos resultados.
	add	a,l	; Y ya tenemos el valor de la multiplicaci�n en A
	ld	l,a
	inc	hl
	ld	a,[hl]
	ld	[framev],a	; nuevo valor de framev
	ret
;----------------------------------------------------------------------------------------
; Rutina de cambio de Sentido del protagonista izq y der
;----------------------------------------------------------------------------------------
chg_sen_spr:									; 52609
	ld	hl,tabla_chg_spr
	ld	a,[frame]
	ld	b,a	; Multiplicaci�n de un numero por 3. Guardar en b como primer valor de la multiplicacion
	sla	a	; El segundo valor de la multip. Se desplaza 1 vez a la izquierda
	add	a,b	; como en una multiplicaci�n normal se suman ambos resultados.
	add	a,l	; Y ya tenemos el valor de la multiplicaci�n en A
	ld	l,a
	inc	hl
	ld	a,[hl]
	ld	[frame],a	; nuevo valor de frame
	inc	hl
	ld	a,[hl]
	ld	b,a	; Para el bucle de desplazamiento posterior
; Que direcci�n llevabas ?
;	
	ld	a,[dirmovsp]
	cp	1
	jr	z,Antes_ibasaderechas
Antes_ibasaizquierdas:
; Que sprite pinto ahora que tengo que darme la vuelta ?. Usa el nuevo frame
;
	ld	a,[frame]
	cp	4
	jr	c,MovEtiqChgde_0
MovEtiqChgde_1:
	ld	hl,gfxprotader1		; $c040
	jr	bAhoraPon_spr0ader
MovEtiqChgde_0:
	ld	hl,gfxprotader0		; $c000		
bAhoraPon_spr0ader:
	ld	[spr],hl
	call	pon_spr_buff		; vuelco spr en buffer desplaz. (engine)
	ld	a,b			; Mirar Cuanto hay que desplazarlo
	and	a			; si el valor es 0 no lo desplaces
	call	nz,DESPLAZOFRAMEDER
; Ahora vas a derechas
;
	ld	a,1
	ld      [dirmovsp],a		; variable de direccion de movimiento
	jp	bcalc_pon_spr_prota	; en el engine. Es lo mismo que lo anterior y no lo repito
Antes_ibasaderechas:
; Que sprite pinto ahora que tengo que darme la vuelta ?. Usa el nuevo frame
;
	ld	a,[frame]
	cp	4
	jr	c,MovEtiqChgiz_0	; jr c,AhoraPon_spr0aizq
MovEtiqChgiz_1:
	ld	hl,gfxprotaizq1		; $c140
	jr	bAhoraPon_spr0aizq
MovEtiqChgiz_0:	
	ld	hl,gfxprotaizq0		; $c100	
bAhoraPon_spr0aizq	
	ld	[spr],hl
	call	pon_spr_buff		; vuelco spr en buffer desplaz. (engine)
	ld	a,b			; Mirar Cuanto hay que desplazarlo
	and	a			; si el valor es 0 no lo desplaces
	call	nz,DESPLAZOFRAMEIZQ
; Ahora vas a izquierdas
;
	ld	a,3
	ld      [dirmovsp],a		; variable de direccion de movimiento
	jp	bcalc_pon_spr_prota	; en el engine. Es lo mismo que lo anterior y no lo repito
;----------------------------------------------------------------------------------------
; Protagonista se prepara para disparar
;----------------------------------------------------------------------------------------
PREPARA_DISPARO:							; 52816
	ld	a,[accion]
	set	0,a			; Activo el bit de disparo: luz, l�tigo.
        ld      [accion],a

;preparaluz:
; Efecto lanzar la luz
	ld	a,1
	ld	[paraponerenB+1],a
	call	llamar_efecto
; Actualizamos los datos del churro del disparo, con la posici�n del prota
	call	Actualiza_churro_luz	
	ld	hl,mirarluz		; Activo la llamada en el engine para
	ld	[vermirarluz+1],hl	; imprimir la luz al ser arrojada
; Apagamos la luz en el marcador
	call	apagavelamarcador	; GestionLuzmarcador.asm
	ld	hl,nomirarllama		; Desactivo la llamada en el engine
	ld	[vermirarllama+1],hl	; para no imprimir la llama del marcador
	ld      a,[dirmovsp]            ; hacia donde miraba ?
	and	a
	jp	z,MIRAARB_DISPARO_LUZ	; (*)
	cp	2
	jp	z,MIRAARB_DISPARO_LUZ	; (*)
	jr	c,MIRADER_DISPARO_LUZ
; DISPARO a la izquierda	
; Vuelco el frame LANZAR a izquierdas en el buffer
	ld	hl,gfxprotalanzaizq0	; $C180 sprite 0 lanzando a la izquierda
	jp	PARALANZA_IZ
MIRADER_DISPARO_LUZ:
; Vuelco el frame LANZAR a izquierdas en el buffer
	ld	hl,gfxprotalanzader0	; $C080	sprite 0 lanzando a la izquierda
	jp	PARALANZA_DE
MIRAARB_DISPARO_LUZ
	ld	hl,gfxprotaarrba0	; $C200
	jr	saltodasporestelao
MIRAARB_DISPARO_LUZ2:
	ld	hl,gfxprotaarrba1	; $C240
saltodasporestelao:
	ld      [spr],hl		; metemos el valor de hl en spr para imprimir
	call	BBNO_ANIMA		; En el ENGINE
	jp	abPincolorfondoabaprota	; Calculo y pinto el color del fondo del protagonista.



PREPARA_LATIGO:
	ld	a,[accion]
	set	0,a			; Activo el bit de disparo: luz, l�tigo.
        ld      [accion],a

; preparalatigo:							52846
	ld      a,[dirmovsp]            ; hacia donde miraba ?
	and	a
	jr	z,MIRAARB_DISPARO_LATIGO	;(*)
        cp      2
	jr	z,MIRAARB_DISPARO_LATIGO	;(*)
        jr      c,MIRADER_DISPARO_LATIGO
; DISPARO a la izquierda latigo	
; Vuelco el frame LANZAR a izquierdas en el buffer
MIRAIZQ_DISPARO_LATIGO:
	ld	hl,gfxprotalatigoizq0	; $BF80	sprite 0 l�tigo a la izquierda
	jp	PARALANZA_IZ
MIRADER_DISPARO_LATIGO:
; Vuelco el frame LANZAR a izquierdas en el buffer
	ld	hl,gfxprotalatigoder0	; $BF00	sprite 0 l�tigo a la derecha
	jp	PARALANZA_DE
MIRAARB_DISPARO_LATIGO:
	ld	hl,gfxprotaarrbalatigo1
	jr	saltodasporestelao
MIRAARB_DISPARO_LATIGO2:
	ld	hl,gfxprotaarrbalatigo0
	jr	saltodasporestelao


;----------------------------------------------------------------------------------------
; Protagonista se prepara para PARAR DE disparar
;----------------------------------------------------------------------------------------
DISPARADO:								; 52946
	ld	a,[latigo]
	and	a
	jr	nz,DISPARODELATIGO
DISPARADODELALUZ:
; Ralentizar movimientos
;
	ld	a,[skipf_p]
etiq_disparado:				; Etiqueta para modificar el valor de skipf_p
	cp	9			; si disparas en el suelo (9) o saltando (1)
	jr	c,aumento_skipf_disparado
	xor	a
	ld	[skipf_p],a
	inc	a
	ld	[latigo],a		; Activo el l�tigo.

	ld	a,[flagespa]
	inc	a
	ld	[flagespa],a
	cp	2
	jr	z,FINAL_DISPARADOlatigo

	ld      a,[dirmovsp]            ; hacia donde miraba ?
	and	a
	jr	z,MIRAARB_DISPARO_LUZ2	; (*)
	cp      2
        jr      c,MIRADER2_DISPARO
	jr	z,MIRAARB_DISPARO_LUZ2	; (*)
; DISPARO a la izquierda	
; Vuelco el frame LANZAR a izquierdas en el buffer
	ld	hl,gfxprotalanzaizq1	; $C1c0	sprite 1 lanzando a la izquierda
	jp	PARALANZA_IZ
MIRADER2_DISPARO:
; Vuelco el frame LANZAR a izquierdas en el buffer
	ld	hl,gfxprotalanzader1	; $C0c0	sprite 1 lanzando a la izquierda
	jp	PARALANZA_DE
aumento_skipf_disparado:
	inc	a
	ld	[skipf_p],a
	ret


DISPARODELATIGO:
; Ralentizar movimientos
;
	ld	a,[skipf_p]
etiq_disparado2:				; Etiqueta para modificar el valor de skipf_p
	cp	9			; si disparas en el suelo (9) o saltando (1)
	jr	c,aumento_skipf_disparado
	xor	a
	ld	[skipf_p],a
	ld	a,[flagespa]
	inc	a
	ld	[flagespa],a
	cp	2
	jr	z,FINAL_DISPARADOlatigo

	ld      a,[dirmovsp]            ; hacia donde miraba ?
	and	a
	jr	z,MIRAARB_DISPARO_LATIGO2	; (*)
	cp      2
        jr      c,MIRADER_DISPARO_puntaLATIGO
	jr	z,MIRAARB_DISPARO_LATIGO2	; (*)
; DISPARO a la izquierda latigo	
; Vuelco el frame LANZAR a izquierdas en el buffer
	call	Imp_punta_del_latigo	; Imprimo punta l�tigo
MIRAIZQ_DISPARO_LATIGO2:		; Etiqueta para cambiar valor hl
	ld	hl,gfxprotalatigoizq1	; $BFc0	l�tigo a la izquierda
	jp	PARALANZA_IZ
MIRADER_DISPARO_puntaLATIGO:
; Imprimo punta l�tigo
	call	Imp_punta_del_latigo
MIRADER_DISPARO_LATIGO2:		; Etiqueta para cambiar valor hl
; Vuelco el frame LANZAR a izquierdas en el buffer
	ld	hl,gfxprotalatigoder1	; $BF40	l�tigo a la derecha
	jp	PARALANZA_DE


FINAL_DISPARADOlatigo:
; Saltar a borrar punta del l�tigo
	call	Borrar_punta_del_latigo		; al final del todo.
	xor	a
; Disparaste mientras saltabas ?
;
	ld	hl,etiq_disparado+1
	ld	[hl],9			; Restauro el valor 9 luz por si previamente lo cambi� a 1 al disparar saltando
	ld	hl,etiq_disparado2+1
	ld	[hl],9			; Restauro el valor 9 latigo por si previamente lo cambi� a 1 al disparar saltando

	ld	[bytevesal],a
; He restaurado los valores que necesito para anular errores con respecto al salto.
	ld	[flagespa],a
	ld	[proparado],a
        ld      [accion],a
	ld      a,[dirmovsp]            ; hacia donde miraba ?
	and	a
	jp	z,MIRAARB_DISPARO_LUZ	; BBNO_ANIMA
	cp	2
	jp	z,MIRAARB_DISPARO_LUZ	; BBNO_ANIMA
        jp      c,ARRANCA_DE		; PARA PINTAR EL PROTA EN PARADO Y AJUSTAR PARA ANDAR
	jp	ARRANCA_IZ		; PARA PINTAR EL PROTA EN PARADO Y AJUSTAR PARA ANDAR




;----------------------------------------------------------------------------------------
; PREPARACION DE EVENTO DE SALTO	Usamos el frame 1 como salto.
;----------------------------------------------------------------------------------------
preparar_salto:	
; Efecto de salto
	ld	a,10
	ld	[paraponerenB+1],a
	call	llamar_efecto
b_preparar_salto:	
	xor	a			; skipframe a 0
	ld	[skipf_p],a
	ld	[liana],a		; Si saltas quita el posible enganche de la liana
	ld	[enganchao],a
	ld	[framev],a
	ld	a,[dirmovsp]		; Discriminamos el sentido del prota.
	cp	1
	jr	nz,pre_salto_iz
; SALTO MIRANDO A DERECHAS _______________________________________________
;
pre_salto_de:
MovEtiqSalde_1:				; Etiqueta para SprProtaEyes.asm
	ld	hl,gfxprotader1		; $c040	Sprite 1 a derechas para el salto
	ld	a,[frame]
	cp	4			; Si frame menor que 4
	jr	c,PARASALTO_IZ		; desplaza el frame a la izquierda

PARASALTO_DE:
	call	SALTO_A_DER_O_IZQ
	and	a			; si el valor es 0 no lo desplaces
	jp	z,Antes_fin_parado
	call	DESPLAZOFRAMEDER
	jp	Antes_fin_parado
; SALTO MIRANDO A IZQUIERDAS _______________________________________________
;
pre_salto_iz:
MovEtiqSaliz_1:				; Etiqueta para SprProtaEyes.asm
	ld	hl,gfxprotaizq1		; $c140	Sprite 1 a derechas para el salto
	ld	a,[frame]
	cp	4			; Si frame menor que 4
	jr	c,PARASALTO_DE		; desplaza el frame a la derecha
PARASALTO_IZ:
	call	SALTO_A_DER_O_IZQ
	and	a			; si el valor es 0 no lo desplaces
	jp	z,Antes_fin_parado
	call	DESPLAZOFRAMEIZQ
	jp	Antes_fin_parado
SALTO_A_DER_O_IZQ:			; DE AMBOS SALTOS
	ld	[spr],hl
	call	pon_spr_buff		; vuelco spr en buffer desplaz. (engine)
	ld	hl,tabla_des_spr_salto
	jp	busq_num_desp_salto	; calculo el valor del desplazamiento Valor en A y B
;----------------------------------------------------------------------------------------
; EVENTO DE SALTO- Desarrollo Entrada en 'a'= valor  'e'= puls. teclas. 
;----------------------------------------------------------------------------------------
CONT_SALTA:								; 40852
	ld	a,[bytevesal]		; Comprobamos si bytevesal es 18 para anularlo 
	cp	18			; si usas 18 la tabla es la nueva, si usas la
	jp	z,FIN_SALTANDO		; tabla vieja (comentada arriba) tienes que poner 17 
	ld	a,[skipf_p]
	cp	1
	jp	c,aumento_skipf_disparado	; para aprovechar la subrutina anterior
	xor	a
	ld	[skipf_p],a
TIRA_POR_TABLA:
	ld	a,[bytevesal]		; usamos bytevesal para recorrer la tabla de salto en vertical
	ld	hl,bytevesal
	inc	[hl]			; incremento bytesal
	ld      hl,tab_sal_ve		
	add     a,l
        ld      l,a                     ; hl apunta al valor dentro de la tabla
	push	hl			; Lo guardo para usarlo despu�s. Y cargo en A lo que contiene
; Mira si tienes los ojos activos para borrar el rastro
;
	push	hl
	ld	a,[activ_eyes]
	rra
	jr	nc,b_DETIRA_POR_TABLA
	call	Coloca_valor_buffer_iluminado
b_DETIRA_POR_TABLA:	
	pop	hl
	ld	a,[fila]		; Sumamos los pixels a incrementar o decrementar a la fila
	add	a,[hl]			;
	ld	[last_p+1],a		; para las comprobaciones de OBSTACULOS arriba o de abajo	
	ld	a,[hl]			; Tengo que saber si subes o bajas y cuantos pixels subes o bajas
	rla				; Miro a ver si hay acarreo eso querr� decir que es negativo (subes)
	jr	c,sal_Subiendo
; Salto Bajando _____________________________________________________  
;
	rra
	and	a
	jp	z,CONT_TIRANDO		; Para valores de salto 0
	ld	b,a			; Valor de los pixels a subir o bajar en B
	push	bc
	call	obst_aba		; Mirar obstaculo abajo (gracias a last_p+1)
	and	a
	jp	z,CHOQUESALTO		; Anula salto si tropiezas obst�culo (varAltura)
	pop	bc
; B= pixels a saltar. Miro en la tabla para saber los valores a utilizar para sumar a fila y para volcar el fondo SprProtaVolc
	ld	hl,tabla_pix_salto
	call	Ru_tabla_pix_salto	; Al volver 'A' contiene el valor donde apunta hl
	sla	a			; en tabla_pix_salto. Lo multiplicamos por 2 para C 
	ld	c,a			; Valor para imprimir el fondo
	ld	a,[fila]
	ex	af,af'			; Guardo el valor de fila antes de borrar el fondo
	jr	desdeBajando_Subiendo
; Salto Subiendo _____________________________________________________
;
sal_Subiendo:				; 40862
	rra				; Dejo el valor como estaba y ahora tengo que dejarlo sin signo.
	cpl
	inc	a			; Uds pixel que desplaza en el salto
	ld	b,a
	push	bc
	call	obst_arr		; Mirar obstaculo arriba
	and	a
	jp	z,CHOQUESALTO		; Anula salto si tropiezas obst�culo (varAltura)
	pop	bc
; B= pixels a saltar. Miro en la tabla para saber los valores a utilizar para sumar a fila y para volcar el fondo SprProtaVolc
	ld	hl,tabla_pix_salto
	call	Ru_tabla_pix_salto	; Al volver 'A' contiene el valor donde apunta hl
	sla	a			; en tabla_pix_salto. Lo multiplicamos por 2 para C 
	sla	a			; lo multiplico denuevo para borrar restos abajo en la liana.
	ld	c,a			; Valor para imprimir el fondo
	inc	hl
	ld	a,[hl]
	ld	b,a			; Valor para sumar a fila
	ld	a,[fila]
	ex	af,af'			; Guardo el valor de fila antes de borrar el fondo
	ld	a,[fila]
	add	a,b			; Lo sumo a fila
	ld	[fila],a
desdeBajando_Subiendo:
	call	im_pix
	ld	hl,paracambiarBC+1	;--.Modificaci�n del valor 	
	ld	[hl],c			; Lo cojo de arriba. Cambio el valor de BC en la rutina imp_solo_fondo
	call	imp_solo_fondo		; Rutina sprprotavolc.asm
	ld	hl,paracambiarBC+1	;--.Restauraci�n del valor
	ld	[hl],32			; 2  normal y 8 para caida de 4 pixelsDevuelvo su valor habitual. Esto es para no duplicar la rutina.
	ex	af,af'			; recupero el valor de fila antes de borrar el fondo
	pop	hl			; recupero el indice de hl donde apunta a cuanto debo hacer saltar al prota
	add	a,[hl]
	ld	[fila],a
	ld	[last_p+1],a
	call	im_pix
;
; Continuaci�n de la Rutina
;
CONT_TIRANDO:	

; Vamos ahora a comprobar si est�s pulsando alguna tecla de direcci�n.
	ld	a,[keypush]
	bit	3,a
	jp	nz,ENGANCHAR_LIANA		; en el engine. Im_pix,volcar,mezclar

CONT_CONT_TIRANDO:
	bit	6,a				; tecla Accion para l�tigo o luz
	jr	nz,DISPARA_SALTANDO
	bit	1,a
	jr	z,SALTA_IZQUIERDA		; si es NO, vamos a IZQUIERDA
	bit	0,a
	jp	nz,calc_pon_spr_prota
;
; SALTA_DERECHA. Primero ver si pulsabas antes derecha sino cambia el sentido del prota.
;
	ld	a,[dirmovsp]
	cp	1
	jp	nz,chg_sen_spr		; En caso de que haya cambio de sentido ve a rutina movimientos.asm
; Estabas pulsando esa sentido
	ld      a,1
	ld      [dirmovsp],a		; variable de direccion de movimiento
	ld	a,[frame]
	cp	7
	jr	z,BSALTA_DERECHA
	ld	hl,frame
	inc	[hl]
	call	desp_spr_der		; desplazo a la derecha el frame
	jp	calc_pon_spr_prota	; en el engine. Im_pix,volcar,mezclar
BSALTA_DERECHA:
;en caso de que frame = 7 comprobar obstaculos y salida de pantalla
	call	obst_de
	and	a
	jp	z,calc_pon_spr_prota	; ajustesalto
	xor	a
	ld	[frame],a
	call	vuelcafondosprprota_colu
	ld	hl,colu
	inc	[hl]
	call	im_pix
	jp	b_preparar_salto		; Hay que desplazar el frame hasta la posicion 0 antes de saltar
;
; SALTA_IZQUIERDA. Primero comprueba si pulsas o no a la izquierda y luego si ya lo estabas pulsando antes.
;
SALTA_IZQUIERDA:
	bit	0,a
	jp	z,calc_pon_spr_prota	; Si no pulsabas izquierda tampoco, entonces salta en vertical.
	ld	a,[dirmovsp]		; Ver si pulsabas antes izquierda sino cambia el sentido del prota.
	cp	3
	jp	nz,chg_sen_spr		; En caso de que haya cambio de sentido ve a rutina movimientos.asm
; Estabas pulsando esa sentido
	ld      a,3
	ld      [dirmovsp],a		; variable de direccion de movimiento
	ld	a,[frame]
	cp	7
	jr	z,BSALTA_IZQUIERDA
	ld	hl,frame
	inc	[hl]
	call	desp_spr_izq		; desplazo a la derecha el frame
	jp	calc_pon_spr_prota	; en el engine. Im_pix,volcar,mezclar
BSALTA_IZQUIERDA:
;en caso de que frame = 7 comprobar obstaculos y salida de pantalla
	call	obst_iz
	and	a
	jp	z,calc_pon_spr_prota	;ajustesalto
	xor	a
	ld	[frame],a
	call	vuelcafondosprprota_colu2
	ld	hl,colu
	dec	[hl]
	call	im_pix
	jp	b_preparar_salto		; Hay que desplazar el frame hasta la posicion 0 antes de saltar
;
; Si mientras que estabas SALTANDO has pulsado DISPARO entonces ven aqu� y mandalo fuera.
;
DISPARA_SALTANDO:
	ld	a,[bytevesal]
	cp	8
	jp	c,calc_pon_spr_prota	; solo disparas si est�s en lo alto del salto.
	cp	11		; 12			
	jp	nc,calc_pon_spr_prota	; Asi no te metes en el decorado por bajar 4px al caer.
	ld	hl,etiq_disparado+1
	ld	[hl],1			; Valor 1 al disparar saltando. LUZ
	ld	hl,etiq_disparado2+1
	ld	[hl],1			; Valor 1 al disparar saltando. LATIGO

	jp	desdedisparosaltando	; En el engine zona de comprobaci�n pulsaci�n tecla disparo.
;
; Has pulsado la tecla de arriba y tenemos que ver si est�s en una liana
;
ENGANCHAR_LIANA:
	call	salta_a_liana
	ld	a,[liana]
	and	a
	jr	nz,AN_FIN_SALTANDO		; Si est�s enganchado a la liana termina el salto.
; En caso de que no tengas una liana cerca, devuelvo el control para que continue el salto
	ld	a,[keypush]
	jp	CONT_CONT_TIRANDO
AN_FIN_SALTANDO:
	ld	a,4
	ld	[frame],a
	
	ld	a,[fila]
	and	7
	ld	[framev],a		; Actualizo al engancharte, el valor de framev.
;
; Anulo el salto y lo envio al engine donde meter los datos de prota en subida
;
	xor	a
	ld	[accion],a
	ld	[bytevesal],a
	ld	hl,gfxprotaarrba0	; $c200  spr0 de arribajo
	ld	[spr],hl		
	jp	BBNO_ANIMA
;
; salida desde el choque
;
CHOQUESALTO:				; 41066
	pop	bc			; Previamente guardados, se recuperan para
	pop	hl			; quitar de la pila.
	ld	a,[flagpa]
	rra		
	ret	c		; hay cambio de pantalla pero no anulas el salto.
;
; Comprobar que bytesal = 11 si es as� continuar con el salto. No habr�s tropezado 
; el hueco del "pasillo" es de la altura del salto.
;
	ld	a,[bytevesal]
	cp	11	; 12		; comprobar el n�mero exacto
	ret	c
; 
; colocar el sprite si es necesario en una posicion absoluta.
;
FIN_SALTANDO
ajustesalto:				; viene de compobjetos.asm
; imprimo el fondo completo del prota con los datos previos de fila y colu SprProtaVolc.asm
	call	imp_solo_fondo		; imprimo el fondo de todo el enemigo
	ld	a,[fila]		; ajuste de salto en bajada o subida al tropezar
	and	248
	ld	[fila],a
	ld	[last_p+1],a
	call	im_pix		
; Tienes que anular el salto.
	xor	a
	ld	[accion],a
	ld	[bytevesal],a
	ld	[proparado],a		; para colocar la animaci�n en parado
	ld	[liana],a
	ld	[enganchao],a
;
; Seg�n el frame que tengas as� debes poner en spr uno u otro. El valor en "parado" es para los frames 4 en adelante
;
	ld	a,[frame]
	cp	4
	jr	nc,MovEtiqFinSaltode_1	; ponerframe1protadeiz
; Discriminamos el sentido del prota.
;
MovEtiqFinSaltode_0:			; Para llamar desde sprprotaeyes.asm
	ld	hl,gfxprotader0		; $c000
	ld	a,[dirmovsp]
	cp	1
	jp	z,PARALANZA_DE		; En rutina de prota en parado.
MovEtiqFinSaltoiz_0:
; Va a izquierdas
	ld	hl,gfxprotaizq0		; $c100	Para llamar desde sprprotaeyes.asm
	jp	PARALANZA_IZ		; En rutina de prota en parado.

MovEtiqFinSaltode_1:
	ld	hl,gfxprotader1		; $c040
	ld	a,[dirmovsp]
	cp	1
	jp	z,PARALANZA_DE		; En rutina de prota en parado.
MovEtiqFinSaltoiz_1:
; Va a izquierdas
	ld	hl,gfxprotaizq1		; $c140	Para llamar desde sprprotaeyes.asm
	jp	PARALANZA_IZ		; En rutina de prota en parado.
;
; En el salto pulsas ARRIBA para engancharte a la liana. Comprobamos si la hay
;
salta_a_liana:
	call	calc_dirbuffattr_prota	; Busca en el buffer de atributos la direcci�n.
	ld	a,[hl]			; hl = valor atrib. de t� primer chr
	cp	4			; Miro si hay una liana en t� posici�n.
	jr	nz,noenliana
Verotrocolu:
	inc	hl			; 2.chr. Los de abajo (osea los pies) no los miro
	ld	a,[hl]
	cp	4			; Miro si hay una liana en t� posici�n.
	jr	nz,noenliana
; Activo la liana
	ld	a,1
	jr	bnoenliana
noenliana:
	xor	a
bnoenliana:
	ld	[liana],a
	ret
;
; Comprobamos una vez estamos en la liana, si podemos continuar por ella. La llamo desde el motor
;
habemus_liana:
	call	calc_dirbuffattr_prota	; Busca en el buffer de atributos la direcci�n.
	ld	a,[hl]
	cp	4
	jr	nz,nohayliana
	inc	hl
	ld	a,[hl]
	cp	4
	jr	nz,nohayliana		; segundo char del prota
hayliana:
regAuno:
	ld	a,1
	ret
nohayliana:
regAcero:
	xor	a
	ret
Ru_tabla_pix_salto:
	ld	a,[hl]
	cp	b
	ret	z
	inc	hl
	inc	hl
	jr	Ru_tabla_pix_salto

;----------------------------------------------------------------------------------------
; Impresi�n de la punta del l�tigo
;----------------------------------------------------------------------------------------
;churropuntalatigo:
;	db	0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
; NOUSO, NOUSO, posxChar,  posyScan, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	    ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
; NOUSO, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni,	coluprota,	filaprota
; ix10,	 ix11,	    ix12,       ix13,	     ix14,	        ix15,   	ix16
; NOUSO, NOUSO,		NOUSO, NOUSO,	dblowPant, dbhighPant, dblowBuf,    dbhighBuf, Choque
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24	ix25
; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24, 25
; Churro junto al gfx de la punta del l�tigo para aprovechar bytes.
; Calculo los datos ix22 a 24 y del resto de datos del churro
Imp_punta_del_latigo:
; Efecto del l�tigo
	ld	a,9	
	ld	[paraponerenB+1],a
	call	llamar_efecto
; Antes meter una rutina que traiga los valores guardados de cada pantalla y meterlos en ix correspondiente
	push	ix
	push	hl
	ld	ix,churropuntalatigo
	ld	a,[fila]		; traspaso valores de COLU Y FILA 
	add	a,8
	ld	[ix+3],a
	ld	a,[dirmovsp]
	cp	1
	jr	z,puntalatigoder
puntalatigoizq:
	ld	a,[colu]
	dec	a
	ld	[ix+2],a
	jr	c_Calculo_de_la_punta_del_latigo
puntalatigoder:
	ld	a,[colu]
	add	a,2
	ld	[ix+2],a
c_Calculo_de_la_punta_del_latigo:
	call	avolcamiento_enem	; Sacamos los valores para ix+21, +22, +23, +24
	call	Antes_imp_ene	; (cambiar el valor del ancho antes de imp_ene en Sprenemimp.asm )
	pop	hl
	pop	ix
	ret
Borrar_punta_del_latigo:
	push	ix
	ld	ix,churropuntalatigo
	call	vuelcafondosprenem
	pop	ix
	ret