;----------------------------------------------------------------------------------------
;
; Rutinas y tablas varias del enemigo. Ej. Salto, comprobaci�n de obst�culos
;
;----------------------------------------------------------------------------------------
;
; Incluye Pixel a desplazar en alto, Columna a decr o incr . Frame se incrementa en cada pasada
;
tabla_salto_enemigo:			; 
	db	-8,-4,-2,-2
	db	 2, 2, 4, 8
; Maya derecha		0,1,2,3		c780,c7c0,c800,c840
; Maya izquierda	0,1,2,3		c880,c8c0,c900,c940
; Olmeca derecha	0,1,2,3		c980,c9c0,ca00,ca40
; Olmeca izquierda	0,1,2,3		ca80,cac0,cb00,cb40
;----------------------------------------------------------------------------------------
; PREPARACION DE EVENTO DE SALTO	Usamos el frame 1 como INICIO del salto.  57924
;----------------------------------------------------------------------------------------
Enem_preparar_salto:
	ld	l,$40	
	ld	a,c			; B= sentido del nuevo v�rtice.
	cp	4
	jr	z,Enem_pre_salto_der
; SALTO MIRANDO A IZQUIERDAS _______________________________________________
	ld	[ix+19],3
	ld	h,$c9			; Maya a izq.
	ld	a,[ix+18]
	and	a
	jr	z,Enem_p_s_der
	ld	h,$cb			; Olmeca a izq.
	jr	Enem_p_s_der
; SALTO MIRANDO A DERECHAS _______________________________________________
Enem_pre_salto_der:
	ld	[ix+19],1
	ld	h,$c8			; Maya a derechas
	ld	a,[ix+18]
	and	a
	jr	z,Enem_p_s_der
	ld	h,$ca			; Olmeca a derechas
Enem_p_s_der:
	ld	[ix+12],h
	ld	[ix+11],l
	ret
;----------------------------------------------------------------------------------------
; EVENTO DE SALTO de los ENEMIGOS
;----------------------------------------------------------------------------------------
CONT_SALTANDO_ENEM:			;					57968
	ld	a,[ix+33]		; Comprobamos si bytevesal es 8 para terminar
	cp	8
	jp	z,FIN_SALTANDO_enem
	call	vuelcafondosprenem	; vuelco el fondo del enemigo
	ld	a,[ix+19]		; Compruebo si va a la derecha o izquierda.
	and	a
	jr	z,cont_p_imp_m_enem	;salto en vertical cuando tengo que coger una posible liana.
	cp	1
	jr	z,Enem_sal_der
; Salto en Vertical a izquierdas
	ld	a,[ix+33]		; cargar de nuevo bytesalenem
	and	a
	jr	z,i_cont_p_imp_m_enem
	cp	4
	jr	nz,cont_p_imp_m_enem
i_cont_p_imp_m_enem:	
	call	obst_izq_enem
	and	a
	jr	nz,cont_p_imp_m_enem
	ld	a,[ix+2]		; columna
	dec	a			; valor a decrementar en C
	ld	[ix+2],a
	jr	a_cont_p_imp_m_enem
; Salto en Vertical a derechas
Enem_sal_der:
	ld	a,[ix+33]		; cargar de nuevo bytesalenem
	and	a
	jr	z,d_cont_p_imp_m_enem
	cp	4
	jr	nz,cont_p_imp_m_enem
d_cont_p_imp_m_enem:	
	call	obst_der_enem
	and	a
	jr	nz,cont_p_imp_m_enem
	ld	a,[ix+2]		; columna
	inc	a			; valor a incrementar en C
	ld	[ix+2],a
a_cont_p_imp_m_enem:
	call	avolcamiento_enem	; calculo nueva posici�n al cambiar un eje
cont_p_imp_m_enem:
	ld	a,[ix+33]		; cargar de nuevo bytesalenem
	ld	l,a
	inc	[ix+33]			; incrementar bytesalenem
	ld	a,l
	ld      hl,tabla_salto_enemigo
	add     a,l
        ld      l,a                     ; hl apunta al valor dentro de la tabla
	ld	a,[hl]			; Tengo que saber si subes o bajas y cuantos pixels subes o bajas
	rla				; Miro a ver si hay acarreo eso querr� decir que es negativo (subes)
	jr	c,sal_Subiendo_enem
;
; Salto Bajando_enem _____________________________________________________  58057
;
	rra
	ex	af,af'
	call	obst_aba_enem		; Mirar obstaculo abajo 
	and	a
	jr	nz,antesde_FIN_SALTANDO_enem
con_sal_Bajando_enem:
	ex	af,af'
	ld	b,a			; Valor de los pixels a bajar en B
bcon_sal_Bajando_enem:
; BC contienen B= valor a pixels en altura 
	ld	a,[ix+3]
	add	a,b
	ld	[ix+3],a
	jr	desdeBajando_Subiendo_Enem
antesde_FIN_SALTANDO_enem:
	call	avolcamiento_enem
	call	Antes_imp_ene	
	jp	FIN_SALTANDO_enem
;
; Salto Subiendo_enem _____________________________________________________
;
sal_Subiendo_enem:
	rra				; Dejo el valor como estaba y ahora tengo que dejarlo sin signo.
	cpl
	ex	af,af'
	ld	a,[ix+19]		; Si s�lo saltas en vertical entonces mira si hay lianas donde cogerte
	and	a
	jr	nz,con_sal_Subiendo_enem
	call	mirarlianasaltando
	and	a
	jp	nz,salparalianas
con_sal_Subiendo_enem:
	call	obst_arr_enem		; Mirar obstaculo arriba
	and	a
	jr	z,con_sal_subiendo_enem
	ld	b,0
	jr	bcon_sal_subiendo_enem
con_sal_subiendo_enem:
	ex	af,af'
	inc	a			; Uds pixel a subir en B
	ld	b,a
bcon_sal_subiendo_enem:
; BC contienen B= valor a pixels en altura FILA 
	ld	a,[ix+3]
	sub	b
	ld	[ix+3],a
desdeBajando_Subiendo_Enem:
;
; Vamos a intentar imprimir s�lo los frames en determinado momento
;					57760
	ld	a,[ix+33]		; 0 a 7
	dec	a			; En cada pasada se incrementa el valor antes de llegar aqui.
	and	3			; cojo solo los bits 2 y 3
	ld	[paraframeen],a		; guardo para despu�s usarlo como valor en ix+6
	jr	z,ponframe0_sal_enem	;frame0saltando
;
; Calculamos el proximo frame a imprimir. Sumandolo a dblowEnem. By Metalbrain  57814
;
	ld	c,64			; Simpre para tulum
	ld	a,127
	cp	c
	sbc	a,a
	ld	b,a			; fin metal
	ld	l,[ix+11]
	ld	h,[ix+12]
	add	hl,bc
	ld	[ix+11],l
	ld	[ix+12],h
	jr	imprimemismoenemigosaltando	
ponframe0_sal_enem:
	ld	a,[ix+13]
	ld	[ix+11],a
	ld	a,[ix+14]
	ld	[ix+12],a
imprimemismoenemigosaltando:
	call	avolcamiento_enem
	call	Antes_imp_ene	
	jp	verotroenemigo
; salida por encontrar lianas durante el salto
salparalianas:
	ld	[ix+0],16		; Enganchate a las lianas
	ld	[ix+15],1
	call	Set_enemarraba_ambos
	call	avolcamiento_enem
	call	Antes_imp_ene	
	ld	a,[paraframeen]
	ld	[ix+6],a		; Actualizo el valor de frames
	jr	FIN_SALTANDO_enem_porliana
FIN_SALTANDO_enem:			; 57905
	ld	[ix+0],0		; PONER ANDANDO, YA CAERA SI NO HAY SUELO
	ld	[ix+6],3		; Actualizo el valor de frames
	ld	[ix+7],3		; y frametotal si es que vienes de saltar desde una liana
FIN_SALTANDO_enem_porliana:
	ld	[ix+33],0		; Tienes que anular el salto.
	jp	verotroenemigo
;--------------------------------------------------------------------------------------------------
; Comprobaci�n de obst�culos. En el buffer de atributos.
;--------------------------------------------------------------------------------------------------
obst_arr_enem:
;--------------------------------------------------------------------------------------------------
	ld	a,[ix+3]	; [fila] 57928
       	cp	34              ; MAX.POSIC.PANTALLA A arriba
       	jr	nc,YSIAR_enem
	ld	[ix+3],128
	jp	regAuno
YSIAR_enem:
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem	;c_dirbuffAtr_enem
	ld	bc,#20
	xor	a
	sbc	hl,bc
	jr	YSIAR_YSIAB_enem
;--------------------------------------------------------------------------------------------------
obst_aba_enem:
;--------------------------------------------------------------------------------------------------
	ld	a,[ix+3]	; [fila] 57959
       	cp	152		;134             ; MAX.POSIC.PANTALLA A abajo
       	jr	c,YSIAB_enem
       	ld	[ix+3],32
	jp	regAuno
YSIAB_enem:                          ; Comprobar el suelo bajo tus pies
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem	;c_dirbuffAtr_enem
	ld	bc,#20
	add	hl,bc
	add	hl,bc
YSIAR_YSIAB_enem:
	ld	a,[hl]
	bit	6,a
	jp	nz,regAuno
	inc	l
	ld	a,[hl]
	bit	6,a
	jp	nz,regAuno
	jp	regAcero
;--------------------------------------------------------------------------------------------------
obst_der_enem:
;--------------------------------------------------------------------------------------------------
	ld	a,[ix+2]	; 58003
	cp	31              ; MAX.POSIC.PANTALLA A derecha
       	jr	NZ,YSIDE_enem
	ld	[ix+2],1
	jp	regAuno
YSIDE_enem:			
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem	;c_dirbuffAtr_enem
	inc	l
	inc	l
	ld	a,[hl]
	bit	6,a
	jp	nz,regAuno	; Si es pared s�lida. "
	jr	comp_CHR_DERIZ_enem_pies
;--------------------------------------------------------------------------------------------------
obst_izq_enem:                                ; la izquierda
;--------------------------------------------------------------------------------------------------
	ld	a,[ix+2]	; 58036
	and	a	;	cp	1
	jr	NZ,YSIIZ_enem
	ld	[ix+2],29
	jp	regAuno
YSIIZ_enem:
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem	;c_dirbuffAtr_enem
	dec	l
	ld	a,[hl]
	bit	6,a
	jp	nz,regAuno	; Si es pared s�lida. "
;--------------------------------------------------------------------------------------------------
comp_CHR_DERIZ_enem_pies:	; 58065
	ld	bc,#20
	add	hl,bc
	ld	a,[hl]
	bit	6,a
	jp	nz,regAuno	; Si es pared s�lida. "
	jp	regAcero

;----------------------------------------------------------------------------------------
; Comprobaci�n de lianas en el salto
;----------------------------------------------------------------------------------------
mirarlianasaltando:
	ld	h,[ix+22]
	ld	l,[ix+21]
	call	c_dirAtr_enem
	xor	a
	ld	bc,#20			; fila de encima.
	sbc	hl,bc
	ld	a,[hl]
	ld	d,a
	inc	l
	ld	a,[hl]
	add	a,d
	cp	8
	jp	nz,regAcero		; No hay liana saltando
	jp	regAuno			; sihaylianasaltando