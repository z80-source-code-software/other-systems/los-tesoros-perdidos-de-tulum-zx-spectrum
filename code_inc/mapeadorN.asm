;----------------------------------------------------------------------------------------
;  RUTINA DE MAPEADO			33863
;----------------------------------------------------------------------------------------
; Entrada senpro= ARRIBA,DER,ABAJO,IZQ. (0,1,2,3). panta = anterior
; Salida en el contenido hl el nuevo valor de panta

mapping:

; Es necesario que H=0 y L=variable panta. As� que en la etiq.panta hay que dejar el db anterior sin uso
; Eso o poner aqui un ld h,0 desp�es de esta primera linea
	ld	hl,[panta]	; 5/16
	add	hl,hl		; 3/11
	add	hl,hl		; 3/11 multiplicamos el valor 
	ld	de,TABSEN	; 3/10 TABLA CON 256X4 = 1024  POSIBLES DIRECCIONES
	add	hl,de		; 3/11
	ld	a,[dirmovsp]	; 4/13
	add	a,l		; 1/4
	ld	l,a		; 1/4
	jr	nc,bmapping	; 3/12
	inc	h		; 1/4
bmapping:
	ld	a,[hl]		; 2/7 NUEVO VALOR DE LA VARIABLE PANTA
        ld	[panta],a	; 4/13
	ret
;  FIN RUTINA DE MAPEADO	; total estados = 126

