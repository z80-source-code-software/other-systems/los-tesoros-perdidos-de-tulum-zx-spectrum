;-------------------------------------------------------------------------
;Rutina de interrupciones para el player. Activado en RUCONTROLES.ASM
        org    #8c8c		; 35980 
intaddr:			; Etiqueta para cambiar los valores del salto siguiente
        jp     intmusic		; a intmusic o intgame
intmusic:


intgame:
                push hl
                push af
                push bc
                push de
                push ix
                push iy
                ex af, af'
                push af
                ex af, af'
                exx
                push hl
                push de
                push bc
                exx
	

	ld	a,[ticks]
	inc	a
	ld	[ticks],a
	cp	25		; 25	
	jr	c,finisr
	xor	a
	ld	[ticks],a
finisr:
	call	LEER			; rulecturacontroles.asm

; Cambio a p�gina 3 para hacer funcionar la musica			
	ld	a,[23388]		; Cargamos la p�gina que hay en esa posici�n para guardarla
	push	af
	ld	b,3			; cambiamos a p�gina 3
	call	setrambank		; rutina paginacion.asm
;
	call	INICIO			; llamamos al player de wyz
	pop	af
	ld	b,a			; dejamos la p�gina que estaba al cambiarla
	call	setrambank
;

                exx
                pop bc
                pop de
                pop hl
                exx
                ex af, af'
                pop af
                ex af, af'
                pop iy
                pop ix
                pop de
                pop bc
                pop af
                pop hl
	
	ei
        reti
