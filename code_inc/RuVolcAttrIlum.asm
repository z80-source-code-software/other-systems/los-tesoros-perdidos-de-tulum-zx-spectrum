;----------------------------------------------------------------------------------------
; Rutinas de iluminaci�n de tiles en pantalla 
;----------------------------------------------------------------------------------------
; Usaremos d_bu_spr y dir_spr para saber que tile iluminar en cada momento.
; Inicio del buffer de atributos	$fd90	;
; Se ilumina primero los tiles de debajo del prota, luego laterales y luego arriba
; Abajo son 4 los tiles en horizontal: los inmediatos al prota y los dos contiguos a estos
; Laterales son 2 a la derecha y 2 a la izquierda.
; Arriba ser�n 4 los tiles en horizontal como abajo. Pero tambi�n iluminaremos los cuatro
; siguientes y los siguientes y los siguientes. Con la premisa de que siempre ser� si no 
; encontramos algo s�lido.
iluminacion_prota:				; 42078
;
; La iluminaci�n la empezamos por los laterales del prota. Colu=colu-1, fila = fila
;
	ld	a,[colu]
	push	af	; Para restaurar el valor
	dec	a	; 
	ld	[colu],a
	push	af	; Para la iluminaci�n de la parte de arriba del prota
; llamo al calculo de attr en el buffer y en la videoram
	call	im_pix
	call	calc_dirbuffattr_prota		; SprProtaVolcAttr3.asm
; hl= dir_buff_atr, de= dir_videoram_attr
	ld	a,[fila]
	and	7
	cp	0
	jr	z,ilumina2
	ld	b,3
	jr	ilumina_laterales
ilumina2:
	ld	b,2
ilumina_laterales:
	push	bc
	push	hl
	push	de	; guardo las direcciones para usarlas en la parte baja y alto
	ldi
	inc	hl
	inc	hl
	inc	de
	inc	de
	ldi
	pop	hl	; Saco el valor guardado en DE para hl
	ld	bc,32
	add	hl,bc
	ex	de,hl
	pop	hl
	add	hl,bc
	pop	bc
	djnz	ilumina_laterales
; hl y de est�n en la posici�n ahora justo que necesitamos para iluminar la parte de debajo del prota.42135
	ld	a,d	; Comprobamos que no estemos en una fila intermedia por cualquier motivo
	cp	$5a	; e iluminemos sin querer el marcador de abajo, pintando basura.
	jr	nz,siguerutinailu
	ld	a,e	; A veces ocurre y no he encontrado mejor forma que esta.
	cp	$80	; primera columna del marcador.
	jr	nc,noilumsueloprota
siguerutinailu:
	ld	bc,4
	ldir
noilumsueloprota:
; Ahora recuperamos af para iluminar la parte de arriba del prota
	pop	af	; columna sigue estando a la izquierda del prota		41932
	ld	[colu],a
	ld	a,[fila]
	push	af	; la guardo para restaurarla
	sub	8
	ld	[fila],a
; Calculo de attr en el buffer y en la videoram.  ;Salidas hl= dir_buff_atr, de= dir_videoram_attr
	call	im_pix
	call	calc_dirbuffattr_prota		; SprProtaVolcAttr3.asm
	push	hl		; Guardo hl y de para las comprobaciones e iluminaciones siguientes
	push	de
; Comprobamos los tiles superiores y dejamos el resultado en una tabla
	call	mirartilesailuminar
	pop	de
	pop	hl
; hl y dl valores a transferir
	call	iluminastira		; Primer tira de tiles encima del prota
; hl y dl tienen el siguiente valor a iluminar	
	call	compultfilamarca	; Comprobamos si estamos en el limite de iluminacion
	rra				; para no seguir.
	jp	c,Findeiluminarprincipal
	ld	ix,tabladeiluminacion
	ld	a,[ix+0]
	ld	[acumuhueco],a		; Guardo en la variable acumuhueco el primer valor de la tabla de iluminaci�n
	cp	%00001111		; Miro si los 4 chars son s�lidos
	jr	z,Findeiluminarprincipal
	ld	b,a		
	push	bc
	call	iluminastiles		; HL,DE est�n en la fila de encima. Lo ilumino seg�n B
	call	sigtiraaluminar
	pop	bc
; hl y dl tienen el siguiente valor a iluminar	
	call	compultfilamarca	; Comprobamos si estamos en el limite de iluminacion
	rra
	jp	c,Findeiluminarprincipal
	ld	a,[ix+1]
	or	b
	ld	[acumuhueco],a		; acumuhueco + segunda tira
	cp	%00001111		; Miro si los 4 chars son s�lidos. ix+0 + ix+1
	jr	z,Findeiluminarprincipal
	ld	b,a		
	push	bc
	call	iluminastiles		; HL,DE est�n en la fila de encima. Lo ilumino seg�n B. 3�linea
	call	sigtiraaluminar
	pop	bc
	call	compultfilamarca	; Comprobamos si estamos en el limite de iluminacion
	rra
	jp	c,Findeiluminarprincipal
	ld	a,[ix+2]
	or	b
	ld	[acumuhueco],a		; acumuhueco + tercera tira
	ld	b,a		
	call	iluminastiles	
Findeiluminarprincipal:	
	pop	af	; Recupero la fila
	ld	[fila],a
	pop	af	; Recupero la columna
	ld	[colu],a
	call	im_pix
	ret
; bucle de iluminacion y modificaci�n de direcciones bufferattr y videoramattr
iluminastira:					; 42070
	ldi
	ldi
	ldi
	ldi
sigtiraaluminar:
	ex	de,hl
	ld	bc,32+4
	xor	a	; Anulo el acarreo
	sbc	hl,bc
	ex	de,hl
	sbc	hl,bc
	ret
; Iluminar tiles independientes		hl,de valores a transferirse. a= valor acumulado de s�lidos
iluminastiles:
	bit	3,b
	jr	nz,essolido3
	ldi
	jr	verb2
essolido3:
	inc	hl
	inc	de
verb2:
	bit	2,b
	jr	nz,essolido2
	ldi
	jr	verb1
essolido2:
	inc	hl
	inc	de
verb1:
	bit	1,b
	jr	nz,essolido1
	ldi
	jr	verb0
essolido1:
	inc	hl
	inc	de
verb0:
	bit	0,b
	jr	nz,essolido
	ldi
	ret
essolido:
	inc	hl
	inc	de
	ret
;
; Inyecci�n de valores en la tabla de iluminaci�n
;
; hl = dir buffer attr				  42089
mirartilesailuminar:
	ld	de,tabladeiluminacion
	ld	a,[fila]
	cp	40
	jr	c,finmirartilesailuminar
	ld	b,4
lazomirartilesailuminar:
	push	bc
; Ahora compruebo los tiles que acabas de iluminar y si hay alguno s�lido lo inyecto en una tabla
	ld	b,4
	ld	c,0
siguitileilu:
	ld	a,[hl]
	bit	6,a
	jr	Nz,tilesolido	; nz se ilumina tood
	xor	a
bsiguitileilu:
	rr	a		; Paso al acarreo el valor
	rl	c		; en c guardo los 4 tiles como bits
	inc	hl
	djnz	siguitileilu
	ld	a,c
	ld	[de],a		; valor de los 4 bits al primero de los 4 valores de la tabla
	inc	de
	xor	a		; quito el acarreo
	ld	bc,32+4		; 32 columnas +4 incrementos de hl
	sbc	hl,bc		; Posici�n inmediata superior al anterior hl
	pop	bc
	djnz	lazomirartilesailuminar
	ret
tilesolido:
	ld	a,1
	jr	bsiguitileilu
finmirartilesailuminar:
	ld	a,%00001111	; Coloco en el primer valor de la tabla el valor de todos tiles solidos
	ld	[de],a
	ret
;
; Subrutina que comprueba si estamos intentando iluminar la �ltima fila del marcador
;
compultfilamarca:
	ld	a,d
	cp	$58
	jp	nz,regAcero
	ld	a,e
	cp	$60		; $20
	jp	c,regAuno
	jp	regAcero
; Para ahorrar bytes lo envio a movimientos.asm donde pongo a 0 o 1 el registro A
acumuhueco	
	ds	1
tabladeiluminacion:		; 42262
	defb	0,0,0,0