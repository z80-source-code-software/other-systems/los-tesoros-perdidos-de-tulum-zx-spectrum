;----------------------------------------------------------------------------------------
; Rutinas de calculo de direcci�n del buffer para el tile e inyecci�n. Incluye ATTR'S
;----------------------------------------------------------------------------------------
;RUTINA DE CALCULO DE LA DIRECCION EN EL BUFFER INTERMEDIO DADAS
;ENTRADA:  fila en pixels y columna en chars DE LOS TILES A IMPRIMIR
;SALIDA:   POSICION DE INICIO DEL TILE A IMPRIMIR EN EL BUFFER DE PANTALLA
;----------------------------------------------------------------------------------------
c_dirbufftile:
        ld     hl,buffpant	; $ad00 antes $ed90    ; Buffer de pantalla
        ld     a,[fil_dat]
        sub    32          ; para saber si est�s en la fila 0 del area de juego.
        jr     z,sumacoluti
        ld	b,a
	xor	a	;acarreo a 0
        srl	b	;la fila se desplaza 3 veces a  la derecha
        rra		;pasando por A para guardar el posible
        srl	b	;valor del flag de acarreo
        rra
        srl	b
        rra
        ld	c,a	;dejando el valor en bc
        add	hl,bc	;lo sumamos a #ed90
sumacoluti:
        ld     a,[col_dat]
        ld     c,a
        ld     b,0
        add    hl,bc       ; SUMO LA COLUMNA    - FIN CALCULO -
;----------------------------------------------------------------------------------------
; RUTINA DE INYECCION DE TILES EN EL BUFFER DE PANTALLA INTERMEDIO
; HL= DIR.EN EL BUFFER DE PANTALLA ( ) DEL TILE A GUARDAR.
; DE= TILE_A GUARDAR
; BC= ALTO Y ANCHO DEL TILE A GUARDAR (atraves dd al_tile y an_tile)
;----------------------------------------------------------------------------------------
INY_TILE:
        ex     de,hl 
        ld     hl,[poti_im]
        ld     a,[al_tile]
        ld     b,a
iny_bu1:
        push   bc
        ld     a,[an_tile]
        push   de
        ld     c,a
        ld     b,0
        ldir               ; TRANSFIERO LOS x SCANNES (POR ANCHURA) DE HL A DE
        pop    de
        ld     a,#20       ; SUMAMOS 32 PARA INYECTAR EL SIGUIENTE SCANN EN EL BUFFER
        add    a,e
        ld     e,a
        jr     nc,sumh
        inc    d           ; SUMO EL ACARREO A D
sumh:
        pop    bc
        djnz   iny_bu1
        ld     [poti_ia],hl
;HEMOS VOLCADO EL TILE,SEGUN LOS DATOS DE PANT. EN SU POSIC. DENTRO DEL BUFFER INTERMEDIO
;----------------------------------------------------------------------------------------
; Rutina para el calculo del attr del tile en el buffer
; ENTRADA:fila en pixels y columna en chars DE LOS TILES A IMPRIMIR
; SALIDA:POSICION INICIO DE LOS ATTR DEL TILE A IMPRIMIR EN BUFFERat  Version con Reg. A
;----------------------------------------------------------------------------------------
C_DIRBUFFTILEATT:
        ld     hl,buffattpant		; $bd00 antes $fd90     Inicio buffer attr
        ld     a,[fil_dat]
        sub    32          ; para saber si est�s en la fila 0 del area de juego.
        jr     z,sumacoluatt
        ld     e,a
        xor    a
;PARA CALCULAR LOS CHARS DEBEMOS 1� DIVIDIR ENTRE 8 LOS SCANNES FILA Y DESPUES
;MULTIPLICAR POR 32 (CADA FILA TIENE 32) ES LO MISMO QUE SI MULTIPLICAMOS 4 DE PRIMERAS.
        sla    e           ; MULTIPLICAMOS POR 4 (DOS VECES CORRER A LA IZQ. DE
        rla
        sla    e
        rla
        ld     d,a
        add    hl,de       ; YA TENEMOS EL VALOR DE LA FILA SUMADO
sumacoluatt:
        ld     a,[col_dat]
        ld     e,a
        ld     d,0
        add    hl,de       ; SUMO LA COLUMNA
;TENGO EN HL LA POSICION EN EL BUFFER DE ATTR DONDE GUARDAR EL COLOR
;----------------------------------------------------------------------------------------
; RUTINA DE INYECCION DE TILES EN EL BUFFER DE PANTALLA INTERMEDIO para el ATTR
; HL= DIR.EN EL BUFFER DE PANTALLA ( ) DEL TILE A GUARDAR.
; DE= TILE_A GUARDAR
; BC= ALTO Y ANCHO DEL TILE A GUARDAR (atraves dd al_tile y an_tile)
;----------------------------------------------------------------------------------------
iny_tileatt:
        ex     de,hl
        ld     hl,[poti_ia]
        ld     a,[al_tile]
        srl    a
        srl    a
        srl    a           ; DIVIDO ENTRE 8 PARA SABER LOS CHARS DE LA ALTURA
        ld     b,a
iny_bat:
        push   bc
        ld     a,[an_tile]
        push   de
        ld     c,a
        ld     b,0
        ldir               ; TRANSFIERO LOS x SCANNES (POR ANCHURA) DE HL A DE
        pop    de
        ld     a,#20       ; SUMAMOS 32 PARA INYECTAR EL SIGUIENTE SCANN EN EL BUFFER
        add    a,e
        ld     e,a
        jr     nc,sumhat
        inc    d           ; SUMO EL ACARREO A D
sumhat:
        pop    bc
        djnz   iny_bat
        ret
;HEMOS VOLCADO EL TILE X SEGUN LOS DATOS DE LA PANTALLA X EN SU POSICION DENTRO DEL BUFFER INTERMEDIO.