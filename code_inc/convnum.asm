;-----------------------------------------------------------------------------------------
; Entrada en A el n�mero de pantalla. 
; Salida en centenas, decenas y unidades para su posterior impresi�n.
;-----------------------------------------------------------------------------------------

conversornum:
	ld	b,0
cent:
	cp	100
	jr	c,fincentenas
	sub	100
	inc	b
	jr	cent
fincentenas:
	ex	af,af'
	ld	a,b
	ld	[centenas],a
	ld	b,0
	ex	af,af'
dece:
	cp	10
	jr	c,findec
	sub	10
	inc	b
	jr	dece
findec:
	ex	af,af'
	ld	a,b
	ld	[decenas],a
	ex	af,af'
	ld	[unidades],a
	ret

centenas	defb	0
decenas		defb	0
unidades	defb	0
