;----------------------------------------------------------------------------------------
; Nombre del juego:  Los tesoros perdidos de Tulum		06/05/2015
;----------------------------------------------------------------------------------------
	include "code_inc\ImpMenuyMarcador.asm"	; imp.marcadores, menu, final, gameover...
	include "code_inc\marcadorarriba.asm"
	include "code_inc\marcadorabajo.asm"	; 28519
	include "code_inc\rucontroles.asm"	; termina de momento en 27462
	include "code_inc\RuGameOveryFinal.asm"	; 27528
;	include "code_inc\textos.asm"
	include "code_inc\textosesp.asm"
	include "code_inc\convnum.asm"
	include "code_inc\colorespant.asm"	; 
	include "code_inc\tabverticespantalla.asm"	; 28020 a 32299 = 4279 bytes para vertices
	include "code_inc\tabenemxpant.asm"	; Tabla que indica los enemigos que act�n en cada nivel. (20 x 3= 60bytes)
	include "code_inc\menudetulum.asm"	;
	include "code_inc\verquemusica.asm"
; 246 bytes libres
	include "gfx_inc\laluzP.asm"		; 32300
	include "gfx_inc\llamadeveladelmarcador.asm"	; 32364
	include "music\mainmusicas.sym"
	include "gfx_inc\datostilesypant.sym"	; Direcci�n de las etiquetas de las tablas
	include "code_inc\tabpantas.asm"	; org 32652		32512 +116
; 65 bytes libres
	org     32768
;----------------------------------------------------------------------------------------
; INICIO DEL PROGRAMA
;----------------------------------------------------------------------------------------
Inicio_vr:
	xor	a
        out     [254],a
	call	Soltar_tecla
	call	Espera_tecla	; Espera para ver la pant.presentacion MenuyMarcador.asm
	call	chgsetchar
;----------------------------------------------------------------------------------------
; Reseteo de valores, limpieza de buffers, impr.menu, borrado, etc
;----------------------------------------------------------------------------------------
Reset_var:
	call	limpiobuffer	; limpiar solo buffers de pantalla y atributos.
	xor	a
; al volver de limpiobuffer tengo hl,de en el inicio de las variables de ram. poner a 0
	ld	[hl],a
	ld	bc,111
	ldir
;----------------------------------------------------------------------------------------
; Inicializacion de variables
;----------------------------------------------------------------------------------------
	xor	a
	ld	[final],a
	inc	a
	ld	[dirmovsp],a	; 1
	ld	[vidas],a       ; 
	inc	a
	ld	[alto],a
	inc	a
	ld	[senpro],a
	ld	[moltotempo],a
	ld	a,5	
	ld	[parpapro],a	; ticks de parpadeo del prota al activar la inmunidad
	ld	hl,$407C	
	ld	[barra],hl
	ld	a,255
	ld	[mosttiempo],a
	ld	[moretiempo],a
	ld	hl,etchgix2+2	; le pongo 17 para que activemos el tile del cofre en pantalla 10
	ld	[hl],17		; si esto lo usara en m�s cofr�s har�a una rutina
	ld	hl,EtiqColProta+1
	ld	[hl],6		; color del prota amarillo

EngineEtiqInic:
; Puesto en la parte de LLAMADA para iniciar el nivel
	ld	hl,KEYBOARD
	ld	[control],hl
	ld	hl,nomirarluz		; No imprimir la luz al ser arrojada
	ld	[vermirarluz+1],hl
	ld	hl,mirarllama		; 
	ld	[vermirarllama+1],hl	;  tiles de arriba alrededor del prota
	ld	hl,iluminar		;  Iluminaci�n alrededor del prota
	ld	[verIluminacion+1],hl	;  tiles de arriba alrededor del prota
;----------------------------------------------------------------------------------------
; Interrupciones y paseos con el player de WYZ
;----------------------------------------------------------------------------------------
        call	parar_musica	; En rucontroles.asm

	xor	a		; Musica del men�
	ld	[poncancionenA+1],a
	call	llamar_musica	; El cacho anterior en rucontroles.asm

; habilito las interrupciones 2.
	di
	ld	a,$8d		; antes #9c       Comienzo tabla interrup.
        ld	i,a
        im	2
	ei			; al habilitarlas
;----------------------------------------------------------------------------------------
; Men� Principal del juego. Espera pulsaci�n de tecla
;----------------------------------------------------------------------------------------
Menu:

	call    imprimenu       ; En ImpMenuymarcador.asm
	call	CONTROLES	; Elegir control de juego
	call	Espera_tecla	; 
	call	Intro_Tulum	; Textosimp.asm
	call	Espera_tecla	; Espera para empezar a jugar           MenuyMarcador.asm

	ld	a,24	; n� pantalla -1. pantalla24 al inicio
	ld	[panta],a	; 3 Poner siempre valor de la �ltima pantalla realizada. En ella se�alar� la pantalla 0
;----------------------------------------------------------------------------------------
; Bucle principal- Motor del juego              COMIENZA EL JUEGO
;----------------------------------------------------------------------------------------
Inicio_juego:      
	ld	hl,intgame		; puntero de rutina de interrupciones
        ld	[intaddr+1],hl
;----------------------------------------------------------------------------------------
Volcado_de_pantalla:
; Actualizacion de situacion y pantallas. Para el cambio entre pantallas.
        call	parar_musica		; gestmelodias.asm

	call	ruborco			; borro la pantalla
	xor	a
	ld	[flagpa],a		; a= 255 en vuel_plat_act_a_pant
	ld	[frame],a
	ld	[framev],a
	ld	[sinluz],a		; Variable para la fase de bonus
	ld	[activ_eyes],a		; Ojos desactivados
	inc	a
	ld	[portaluz],a		; Llevamos la luz
	ld      [dirmovsp],a		; variable de direccion de movimiento A DERECHAS
	inc	a
	ld	[skipf_p],a

;	ld	a,10
;	ld	[valtemp],a

	call	limpiobuffer	
	call	Borradiamantesenmarcador     ; Borro los diamantes del marcador GestionObjetos.asm
	call	Compfasebonus		; Compruebo si lo el nivel anterior era uno especial.
; Arreglar que si mueres con los ojos activos por oscuridad. Empieces a jugar despu�s con el prota
	ld	hl,nomiraroscuridad	; El prota no mira en la oscuridad
	ld	[mirarenlaoscuridad+1],hl
	call	poner0attrpant			; La pantalla a 0. PantVolcTilesyAtt...
	call	llenadoBufferIluminacion	; El buffer de iluminaci�n a 0 tambi�n. sprprotaeyes.asm
; Poner datos del prota visible en pon_spr_buff por si vienes de morir con los ojos activos.
	call	restaurarprota		; en sprprotaeyes.asm
	ld	hl,noapagar		;  No apagamos el area de juego.
	ld	[verapagarluces+1],hl	;  
	ld	hl,mirarenem		;  Y ponemos los enemigos a caminar.
	ld	[vermirarenemigos+1],hl	;  
	call	restaurardatenem	; Restauramos valores iniciales de los enemigos	 tabenemigos.asm
	call	restvalorestablocales	; restauramos variables de los vertices en gestioncamino.asm y tablocalvertices.asm
; Cambio a p�gina 1			
	di				; 33723
	ld	b,1			; por ejemplo, si tenemos las pantallas en la p�gina 3
	call	setrambank		; rutina paginacion.asm
	call	mapping			; busco pantalla a imprimir. 
	call	search_scr		; busco y coloco los tiles de la pantalla en el buffer
	call	ponerobjetos		; Actualizo las tablas Luz y objetos de la pantalla, en "" Y PONGO LAS PANTALLAS FASE BONUS
	call	ponertrampas		; " las trampas en pantalla
	call	volcadoverticesenpan	; Vuelco los datos de los vertices de pantalla
; Cambio a p�gina 0
	ld	b, 0
	call setrambank
	ei
; comprobamos la variable final para saber en que punto andamos
	ld	a,[final]
	cp	1			; si es 
	jp	z,FINALJUEGO		; ve a final del juego
	ld	a,[sinluz]		; Imprimir Nivel o  fase bonus
	rra
	jr	nc,contlarutllam	; Si no hay luz es una fase de bonus.
	call	poner0attrpant		; PantVolcTilesyAttr.asm para poner attr = 0
	call	Soltar_tecla
	call	Imprime_fasebonus	; Rutina en textofasebonus.asm
	call	contador_sp_level
	jr	contlarutllam2
contlarutllam:	
	call	Soltar_tecla	
	call	Imprime_level
	call	contador_sp_level
contlarutllam2:
	ld	a,8			; Hacer sonido con el player.
	ld	[paraponerenB+1],a	; n�efecto a poner en el registro A en rucontroles.asm
	call	llamar_efecto
;-------------------------------------------------------------------------------------	
	call    impmarcadores		; en ImpMenuymarcador.asm   
	call    volcar_pant		; pantvolctilesyattrsinattr.asm
	call	valoresVela		; valores de la llama del marcador
; Calculo de valores de la luz en pantalla, traspaso valores x y al prota y a los enemigos
	call	Calculo_de_luz_de_pantalla	; LuzPantallaGestyTab.asm y saltamos a rucredatenem.asm
	call	Imp_attr_de_la_luz_de_pantalla
; Comprobamos si los enemigos actu�n o no en este nivel.
	call	Compenempant		; rutina en gestionanimaciones.asm
	call	quemusicapongo		; verquemusica.asm

; Activamos o no mirar el cronometro
;	ld	a,[panta]
;	cp	20
;	jr	c,nomiracr
;	ld	hl,mirarcronometro		;  
;	ld	[vermirarcronometro+1],hl	;  
;	jr	parafinllamada
;nomiracr:
;	ld	hl,nomirarcronometro		;  
;	ld	[vermirarcronometro+1],hl	;  
;parafinllamada:

	ld	hl,gfxprotader0		; $c000	monigote mirando a la derecha
	ld	[spr],hl
	call	pon_spr_buff
;FIN_LLAMADA
; Fin de la colocaci�n del nivel
	call	Colocar_barra_Energia	; Coloca barra de energia en el marcador.
	call	calc_pon_spr_prota
; Loop del juego
GAME:					
	ld	a,[vidas]		; Comprobaci�n de vidas o cambio de pantalla
        and	a
        jp	z,MUERTO		; si hay 0 est�s muerto
	ld	a,[flagpa]		; 1 cambio de pantalla 0 no hay cambio
	rra	
	jp	c,Volcado_de_pantalla
	call	comp_trampas		; Comprobaci�n de Trampas
	call	Gest_luz_de_pantalla	; La luz donde reponer tu vela al arrojarla
vermirarluz:				; la vela al ser disparada o no
	jp	nomirarluz		
mirarluz:
	call	GESTLUZ_DISPARADA
	jr	vermirarenemigos
nomirarluz:
	call	apagadiamxluzlanz	; Ahora apaga los diamantes si los has iluminado por la luz lanzada.

vermirarenemigos:			; Gestiono primero los enemigos
	jp	nomirarenem		
mirarenem:
	call	GESTENEM		; Gesti�n de enemigos
nomirarenem:				; Comprobaci�n de Trampas para los enemigos
	call	Enem_comp_trampas	
verIluminacion:				; Gestiono  iluminaci�n del prota
	jp	iluminar		
iluminar:
	call	iluminacion_prota
noiluminar:
verapagarluces:				; Ir a buffer de apagado -> iuminaci�n de videoram para apagar los tiles.
	jp	noapagar
apagar:
	call	gestapagaluces
noapagar:
vermirarllama:				; Gestionar la llama del marcador
	jp	mirarllama		
mirarllama:				; GestionLlamaMarcador.asm
	call	Gest_llama		
nomirarllama:
	call	comp_objetos		; Comprueba objetos de pantalla
	ld	a,[final]		; Compruebo si has visto el final.
	cp	1
	jp	z,Inicio_vr		; El valor 1 es que has visto el final del juego.
	ld	a,[flagpa]
	cp	1
	jp	z,Inicio_juego		; Si el nivel ha sido completado vuelve al inicio.

;
; Gestiono al prota seg�n la pulsacion de teclas desde la rutina ISR
;
	call	EVENTOS_DE_CONT	
	halt				; sincronizaci�n de la pantalla. impresion prota y enemigos
	call	imp_spr_mov		; SprProtaImp.asm Imprime el prota desde el buffer con el fondo mezclado
mirarenlaoscuridad:			; Gestionar si tienes que 'mirar' en la oscuridad
	jp	nomiraroscuridad
miraroscuridad:
	call	eyes_onoff		; SprProtaEyes.asm
nomiraroscuridad:

;vermirarcronometro:
;	jp	nomirarcronometro
;mirarcronometro:
;	call	certina
;nomirarcronometro:

;
; Inmunidad del protagonista.			
;
	ld	a,[inmunidad]
	and	a
	jp	z,GAME
	call	imp_solo_fondo
	call	imp_spr_mov
	ld	hl,EtiqColProta+1
	dec	[hl]
	ld      hl,parpapro		; Espera de tiempo entre toques con enemigos
        dec     [hl]
	jp	nz,GAME
etiqparpro:
	ld	a,5
	ld	[parpapro],a
	xor	a
	ld	[inmunidad],a
	ld	[vchoqEnem],a
	ld	hl,EtiqColProta+1
	ld	[hl],6
	jp	GAME			; 33218
;----------------------------------------------------------------------------------------
; Mover el SPR del prota al buffer de desplazamiento para su uso. Aprovechamos para Sprite PARADO
pon_spr_buff:
	ld	hl,[spr]		; El valor lo traigo en algunas llamadas en otras no.
	ld	de,buff_des_spr		; $
	ld	bc,64			; para ( 2 x 2 X 8 ) X 2=MASK + SPR
	ldir
	ret


;----------------------------------------------------------------------------------------
; RUTINA DE EVENTOS DE CONTROL 
;	Movimiento en los bits de keypush:	7       6	5	   4     3      2    1   0
;						X	fight   X	space,arriba,abajo,der,izq
; Valores de Senpro = 
;----------------------------------------------------------------------------------------
EVENTOS_DE_CONT:			; 32974
;
; Usamos ACCION con un doble proposito. Comprobar si vienes aqui en un disparo o salto previo
;
	ld      a,[accion]		; 4/13
        rra	                        ; 1/4 Vemos si has disparado
        jp      c,DISPARADO		; 3/10 OJO QUE CAMBIO AQUI . QUE HAS DISPARADO ? LUZ O LATIGO ?*********
        rra				; 1/4 Vemos si estaba el sprite saltando
	jp	c,CONT_SALTA		; 3/10 total=39tstates
;
; Lo Primero es comprobar si cae
;
PRI_EVEN_CONT:
; Si estas enganchado en una liana, no mires el suelo debajo tuyo
	ld	a,[enganchao]
	cp	1
	jr	z,SEG_EVEN_CONT		; Si est�s enganchao, no compruebes lo dem�s.
; comprobamos si hay suelo bajo tus pies
	call	obst_aba		
	and	a
	jr	z,SEG_EVEN_CONT		
; Si no hay suelo miramos si tienes una liana donde cogerte
	ld	a,[liana]		
	and	a
	jr	z,A_SEG_EVEN_CONT
; Comprueba si pulsas 'Q' mientras caes por una liana 
	ld	a,[keypush]
	bit	3,a
	jr	z,A_SEG_EVEN_CONT
; Pillas la liana
	ld	a,1
	ld	[enganchao],a

	call	imp_solo_fondo
	jr	PARA_ENLALIANA

; En caso contrio debes caer
A_SEG_EVEN_CONT:
	ld	a,2			
	ld	[senpro],a
	jp	MIRA_CAER
;
; Lo Segundo es mirar si saltas
;
SEG_EVEN_CONT:				; 33288
	ld	a,2			; Si previamente ca�as debes tener Alto = 3 y el char
	ld	[alto],a		; puede iluminar el suelo con color del prota, lo que no es correcto.
	ld	a,[keypush]
	bit	4,a			; 
	jr	z,TER_EVEN_CONT		; si hay 0 no hay salto
	ld	a,2
	ld	[accion],a		; activo el salto
	jp	preparar_salto
;
; Lo Tercero es comprobar 	(q,a) pero solo si est�s ya en la liana.
;
TER_EVEN_CONT:
;
; Ahora hay que mirar si est�s en una liana
;
	ld	a,[liana]
	and	a
	jr	z,CUA_EVEN_CONT
; ARRIBA 
	ld	a,[keypush]
	bit	3,a
	jr	z,ABAJO		; si hay 0 no SUBE arriba, comprueba si baja
;B_TER_EVEN_CONT:
;
; Ver si pulsabas antes abajo para cambiar el valor de framev
;
	ld	a,[dirmovsp]
	cp	2			; Abajo
	call	z,chg_sen_spr_AB		; En la rutina movimientos.asm
;
; Si estas en la cuerda, sube si no ve a Elprota_salta
;
PARA_ENLALIANA:
	ld	hl,$c200
	ld	[spr],hl		;	 spr1 de arribajo
	jp	MIRA_ARR
;
; Lo Cuarto es comprobar si estas bajando por la cuerda
;

ABAJO
	ld	a,[keypush]
	bit	2,a
	jr	z,CUA_EVEN_CONT		; si hay 0 no hay Abajo
;
; Ver si pulsabas antes arriba para cambiar el valor de framev
;
	ld	a,[dirmovsp]
	and	a		; Arriba
	call	z,chg_sen_spr_AB	; En la rutina movimientos.asm
;
; Si estas en la cuerda, sube si no ve a Elprota_salta
;
	ld	hl,gfxprotaarrba0	;	hl,$c200
	ld	[spr],hl
	jp	MIRA_ABA
;
; Lo Quinto es comprobar si ha pulsado DISPARO,DERECHA,IZQUIERDA
;
CUA_EVEN_CONT:				; 33159
	ld	a,[keypush]		; comprobaciones de pulsacion de tecla DISPARO
	bit	6,a			; tecla Accion para l�tigo o luz
	jr	z,aDERECHA

desdedisparosaltando:			; Etiqueta de movimientos.asm	33166
	ld	a,[sinluz]		; comprobaciones si estas en una fase bonus !!!!
	and	a			; para que no puedas disparar la luz y quedarte sin ella.
	jr	nz,aDERECHA
; en vez de aDERECHA PODRIAMOS HACER UN HUEVO DE PASCUA PARA LOS COFRES EN NIVELES FASE BONUS. 
; 10 pulsaciones de la tecla M y puedes abrir el cofre sin soltar la luz.

; Ver si llevas la luz, si no la llevas continua DERECHA	
	ld	a,[portaluz]
	and	a
	jp	z,PREPARA_LATIGO
; y esta es la primera vez que vienes porque soltaste la luz en un disparo anterior
	xor	a
	ld	[portaluz],a		; Dejamos de llevar la luz
	ld	hl,noiluminar		;  Desactivo en el engine la iluminaci�n alrededor del prota
	ld	[verIluminacion+1],hl	

; Activo el salto a mirar que puedas tener que imprimir los ojos del prota 
	ld	hl,miraroscuridad
	ld	[mirarenlaoscuridad+1],hl
; Pinto el attr del fondo del prota para que no se quede el halo de luz al desactivar portaluz
	call	pincolordelfondo1	; En SprProtaVolcAttr.asm
	call	pincolordelfondo2

; Lleno el buffer de iluminacion con los valores que hay en la videoram_ATTR
	call	llenadoBufferIluminacion	; SprProtaEyes.asm
	jp	PREPARA_DISPARO		; movimientos.asm

;
; Ver ahora que selecci�n tienes hecha: luces,boomerang, dinamita
;
aDERECHA:
	ld	a,[enganchao]		; Si estas enganchao en la liana no mires si
	cp	1			; pulsas a derecha o a izquierda porque no 
	ret	z			; puedes ir en esas direcciones
;
;	Movimientos derecha
;
DERECHA:				;33383
	ld	a,[keypush]
	bit	1,a
	jr	z,IZQUIERDA		; si es NO, vamos a IZQUIERDA
;
; Ver si pulsabas antes derecha sino cambia el sentido del prota.
;
	ld	a,[dirmovsp]
	cp	1
	jp	nz,chg_sen_spr		; En caso de que haya cambio de sentido ve a rutina movimientos.asm
;sipaderecha:; estabas pulsando a la derecha por lo tanto continua
;
; Ahora comprueba si estaba parado para arrancar a andar con el spr_1
;
	ld	a,[proparado]
	and	a
	jp	nz,ARRANCA_DE
EngineEtiqDERECHA_0:
	ld	hl,gfxprotader0		; $c000
	jr	MIRA_DER
;
;	Movimientos izquierda
;
IZQUIERDA:
	bit	0,a			; se pulso izquierda?
	jr	z,SIN_MOV		; si es NO, ESTA PARADO
;
; Ver si pulsabas antes izquierda sino cambia el sentido del prota.
;
	ld	a,[dirmovsp]
	cp	3
	jp	nz,chg_sen_spr		; En caso de que haya cambio de sentido ve a rutina movimientos.asm
;sipaizquierda:; Llegas pulsando a la izquierda por lo tanto continua
;
; Ahora comprueba si estaba parado para arrancar a andar con el spr_1
;
	ld	a,[proparado]
	and	a
	jp	nz,ARRANCA_IZ
EngineEtiqIZQUIERDA_0:
	ld	hl,gfxprotaizq0		; $c100
	jr	MIRA_IZQ
;
; Aqui lleganos si no ha habido ninguna pulsacion
;
SIN_MOV:
	ld	a,[dirmovsp]
	and	a			; si estas parado en la cuerda no imprimas el prota parado
	ret	z
	cp	2			;	"			"
	ret	z			
	ld	a,6			; aqui quitamos el estado
	ld	[senpro],a
	call	PROTA_PARADO		; Imprimimos al prota en parado movimientos.asm
	ret				; Salimos y ya imprimimos arriba despu�s del halt
;----------------------------------------------------------------------------------------
; Actuamos sobre los valores
;----------------------------------------------------------------------------------------
;
; Comprobar si hay movimiento a la derecha
;
MIRA_DER:				; 33238
	ld	a,[dir_spr+1]	; Aqui se encuentra el tercio y la fila en formato videoram
	and	7		; SI es scann intermedio entonces borra el fondo y pon 
	jr	z,BA_MIRA_DER	; al prota en una posici�n total.
	call	imp_solo_fondo	; imprimo el fondo de todo el enemigo
	ld	a,[fila]
	and	248
	ld	[fila],a
	call	im_pix
	jr	BAA_MIRA_DER
BA_MIRA_DER:	
	ld	a,[fila]
	and	248
	ld	[fila],a
BAA_MIRA_DER:
	ld      a,1
	ld      [dirmovsp],a		; variable de direccion de movimiento
	inc	a
	ld	[alto],a
	ld	a,[frame]
	cp	3
	jp	z,CAMBIA_SPR
	cp	7
	jr	nz,DESP_SPR_D
;en caso de que frame = 7 comprobar obstaculos y salida de pantalla
COMP_OB_D:
	call	obst_de
	and	a
	ret	z

	call	Pincolorfondoaizqprota
; Mira si tienes los ojos activos para borrar el rastro
	ld	a,[activ_eyes]
	rra
	jr	nc,bdRESET_FRAM
	call	Coloca_valor_buffer_iluminado
bdRESET_FRAM:	
	ld	hl,colu
	inc	[hl]
;
; Aqui borrar la primera columna del prota, volcando el fondo
;
	call	vuelcafondosprprota_colu
	jr	RESET_FRAM
DESP_SPR_D:
	call	desp_spr_der		; Desplazo el spr en su buffer de desplazamiento
	jp	ACTU_ATRs

;
; Comprobar si hay movimiento a la izquierda
;
MIRA_IZQ:				; 33302
	ld	a,[dir_spr+1]	; Aqui se encuentra el tercio y la fila en formato videoram
	and	7
	jr	z,BA_MIRA_IZQ
	call	imp_solo_fondo	; imprimo el fondo de todo el enemigo
	ld	a,[fila]
	and	248
	ld	[fila],a
	call	im_pix
	jr	BAA_MIRA_IZQ
BA_MIRA_IZQ:	
	ld	a,[fila]
	and	248
	ld	[fila],a
BAA_MIRA_IZQ:
	ld      a,3
	ld      [dirmovsp],a		; variable de direccion de movimiento
	dec	a
	ld	[alto],a
	ld	a,[frame]
	cp	3
	jp	z,CAMBIA_SPR
	cp	7
	jr	nz,DESP_SPR_I
;en caso de que frame = 7 comprobar obstaculos y salida de pantalla
COMP_OB_I:
	call	obst_iz
	and	a
	ret	z

	call	Pincolorfondoaderprota	;SprProtaVolcAttr
; Mira si tienes los ojos activos para borrar el rastro
	ld	a,[activ_eyes]
	rra
	jr	nc,biRESET_FRAM
	call	Coloca_valor_buffer_iluminado
biRESET_FRAM:	
	ld	hl,colu
	dec	[hl]
;
; Aqui borrar la ultima columna del prota, volcando el fondo
;
	call	vuelcafondosprprota_colu2
RESET_FRAM:
	xor	a
	ld	[frame],a
	jp	NO_CAMBIA_FRAME
DESP_SPR_I:
	call	desp_spr_izq		; Desplazo el spr en su buffer de desplazamiento
	jp	ACTU_ATRs
;
; Comprobar si hay movimiento hacia arriba
;
MIRA_ARR:				; 33371
	xor	a
	ld      [dirmovsp],a		; variable de direccion de movimiento
	ld	a,[fila]
	and	7
	ld	[framev],a
	jr	z,verObstaculoarriba
	cp	4
	jr	c,nocambiaframearriba
	ld	hl,gfxprotaarrba1	; $c240
	ld	[spr],hl
nocambiaframearriba:
;
; borrar solo el fondo, volcando directamente en pantalla desde el buffer general 4 pixels a mover
;
	ld	a,[fila]
	push	af
	add	a,16
	ld	[fila],a
	call	im_pix
	call	ModificacionvalorBC
	pop	af
	dec	a			;
	jp	ARR_ABA_JP_BBNO_ANIMA
verObstaculoarriba:
	call	obst_arr
	and	a
	jr	z,altospr
; Comprobamos si podemos continuar para arriba por la liana	
	ld	a,[fila]
	ld	b,a
	sub	4
	ld	[fila],a
	call	habemus_liana		; En movimientos.asm
	ld	c,a			; guardo el valor 0,1 en C
	ld	a,b
	ld	[fila],a
	rr	c			; llevo al acarreo el valor de C
	jr	nc,altospr		; Si 0 es que no hay m�s liana
; fin de comprobacion 
	call	Pincolorfondoabaprota	; Calculo y pinto el color del fondo del protagonista.
;;;;;;;;;; inicio borrado al subir por la liana a oscuras.
; Mira si tienes los ojos activos para borrar el rastro
	ld	a,[activ_eyes]
	rra
	jr	nc,ant_altospr
	call	Coloca_valor_buffer_iluminado
ant_altospr:	
;;;;;;;;;;; borrado al subir por la liana a oscuras
	ld	a,[fila]
	dec	a
	ld      [fila],a
	ld	[last_p+1],a
	jp	calc_pon_spr_prota	
altospr:
	ld	a,2
	ld	[alto],a
	ret
;
; Comprobar si hay movimiento hacia abajo, si estamos cayendo.
;
MIRA_ABA:				; 33483
; En caso de senpro = 2 es que el prota cae.
	ld	a,2
	ld      [dirmovsp],a		; variable de direccion de movimiento
	ld	a,[fila]
	and	7
	ld	[framev],a
	jr	z,verObstaculoabajo
	cp	4
	jr	c,nocambiaframeabajo	
	ld	hl,gfxprotaarrba1	; $c240
	ld	[spr],hl
nocambiaframeabajo:
	call	ModificacionvalorBC	; coloco el fondo de 2 pixels en la trayectoria del prota bajando
;;;;;;;;;; inicio borrado al bajar por la liana a oscuras.
	ld	a,[activ_eyes]		; Mira si tienes los ojos activos para borrar el rastro
	rra
	jr	nc,ant_fincomplianadebajo
	call	Coloca_valor_buffer_iluminado
ant_fincomplianadebajo:	
;;;;;;;;;;; borrado al bajar por la liana a oscuras
	ld	a,[fila]
	inc	a
ARR_ABA_JP_BBNO_ANIMA:
	ld      [fila],a
	ld	[last_p+1],a
	jp	BBNO_ANIMA
verObstaculoabajo:		; 33542
	call	obst_aba
	and	a
	ret	z
; Comprobamos si podemos continuar para abajo por la liana	
	ld	a,[fila]
	ld	b,a
	add	a,08
	ld	[fila],a
	call	habemus_liana		; En movimientos.asm
	ld	c,a			; guardo el valor 0,1 en C
	ld	a,b
	ld	[fila],a
	rr	c			; llevo al acarreo el valor de C
	jr	nc,fincomplianadebajo	; Si 0 es que no hay m�s liana
; fin de comprobacion 
	ld	a,3
	ld	[alto],a
	jr	nocambiaframeabajo
fincomplianadebajo:
	ld	a,2
	ld	[alto],a
	call	ModificacionvalorBC
	jp	BBNO_ANIMA

ModificacionvalorBC:
;--.Modificaci�n del valor 		PARA BORRAR SCANNES
	ld	hl,paracambiarBC+1
	ld	[hl],2			; 1 pixel del fondo del tama�o del sprite
	call	imp_solo_fondo		; Rutina sprprotavolc.asm
;--.Restauraci�n del valor
	ld	hl,paracambiarBC+1
	ld	[hl],32			; dejo valor inicial
	ret


;
; ESTAMOS CAYENDO Y VAMOS A COMPROBAR SI PULSAMOS OTRAS TECLAS.
;
MIRA_CAER:			; 33586
	xor	a
	ld	[enganchao],a
	ld	[liana],a
;  Miramos si al caer tienes posibilidad de activar 'liana'
	call	salta_a_liana	; Rutina en movimientos.asm
	call	imp_solo_fondo	; imprimo el fondo de todo el enemigo
	ld	a,[activ_eyes]		; Mira si tienes los ojos activos
	rra
	jr	nc,b_MIRA_CAER
	call	Coloca_valor_buffer_iluminado
b_MIRA_CAER:	
	ld	a,[fila]
	add	a,4
	ld	[fila],a
	ld	[last_p+1],a
	and	7
	ld	[framev],a	; Nuevo valor de framev que uso en la liana y para borrar rastro del prota
	ld	a,3
	ld	[alto],a
	call	im_pix
	call	imp_spr_mov

;COMP_CAE_ a DERECHA o IZQUIERDA	
	ld	a,[keypush]
	and	3
	cp	3
	jp	z,calc_pon_spr_prota		; si pulsas las dos teclas a la vez, cae.
	bit	0,a
	jr	nz,COMP_CAE_E_IZQUIERDA		; vamos a comp.si pulsas IZQUIERDA
	bit	1,a				; vamos a comp.si pulsas DERECHA
	jp	Z,calc_pon_spr_prota		; si tampoco se pulsa izquierda, cae normal.
;COMP_CAE_E_DERECHA				 33657
	ld	a,[frame]			; antes lo hac�a s�lo si el frame = 7 como en el movimiento
	cp	3				; pero as� evito que se meta en un decorado al caer 
	jp	z,CAMBIA_SPR			; desde cierta altura. Activo la variable obstaculo
	cp	7				; que miro antes de CAE_inc_columna
	jr	nz,CAE_DESP_SPR_D
	call	obst_de				; obstaculos?
	and	a
	jp	z,calc_pon_spr_prota
	call	Pincolorfondoaizqprota
	ld	a,[activ_eyes]			; Mira si tienes los ojos activos
	rra
	jr	nc,CAE_inc_columna
	call	Coloca_valor_buffer_iluminado
CAE_inc_columna:	
	ld	a,[obstaculo]
	and	a
	jp	z,calc_pon_spr_prota
	ld	hl,colu
	inc	[hl]
	call	vuelcafondosprprota_colu	; Aqui borrar la primera columna del prota, volcando el fondo
	jp	RESET_FRAM
CAE_DESP_SPR_D:
	call	desp_spr_der		; Desplazo el spr en su buffer de desplazamiento
	jr	ACTU_ATRs
COMP_CAE_E_IZQUIERDA:
	ld	a,[frame]
	cp	3
	jp	z,CAMBIA_SPR
	cp	7
	jr	nz,CAE_DESP_SPR_I
	call	obst_iz
	and	a
	jp	z,calc_pon_spr_prota
	call	Pincolorfondoaderprota	;SprProtaVolcAttr.ASM
	ld	a,[activ_eyes]		; Mira si tienes los ojos activos 
	rra
	jr	nc,CAE_dec_columna
	call	Coloca_valor_buffer_iluminado
CAE_dec_columna:
	ld	a,[obstaculo]
	and	a
	jp	z,calc_pon_spr_prota
	ld	hl,colu
	dec	[hl]
	call	vuelcafondosprprota_colu2	; Aqui borrar la ultima columna del prota, volcando el fondo
	jp	RESET_FRAM
CAE_DESP_SPR_I:
	call	desp_spr_izq		; Desplazo el spr en su buffer de desplazamiento

;----------------------------------------------------------------------------------------
ACTU_ATRs:
	ld      hl,frame
        inc     [hl]
	jr	bcalc_pon_spr_prota	; no_calc_dir_videoram ya que no ha cambiado.
;
; Esta es la parte encargada de cambiar el prota 
;
CAMBIA_SPR:
	ld      hl,frame
        inc     [hl]
NO_CAMBIA_FRAME:
	ld	hl,[spr]
; Esta es la idea de Metal y es una puta genialidad. Suma $40 y no hace falta que compruebes Na de si hay acarreo o mierdas
	xor	a		;acarreo a 0
	ld	a,l
	xor	$40
	ld	l,a
	ld      [spr],hl		; metemos el valor de hl en spr para imprimir
; La etiqueta es para saltar desde la parte del engine o de movimientos que se refiere a las lianas
BBNO_ANIMA:	; LINEAS 544,569 ENGINE y 633 movimientos. Y que no queremos que actualice spr
	call	pon_spr_buff	
calc_pon_spr_prota:
	call	im_pix			; Calculo dir.en pantalla y dir.buffer pantalla  SprProtaImp.asm 
bcalc_pon_spr_prota:			; SprProtaVolc.asm
	JP	volcarymezclar_fondo_spr_mov

; Final de la rutina de Eventos
;----------------------------------------------------------------------------------------


;
;----------------------------------------------------------------------------------------
; Rutinas de mapeo y tablas de direccion entre pantallas y tiles ;$6228
	include "code_inc\ruLecturaControles.asm"
	include "code_inc\mapeadorN.asm"
	include "code_inc\bupaim.asm"
	include "code_inc\CalcTileDirBuffeInyec.asm"
	include "code_inc\PantVolcTilesyAttrSinAttr.asm"	; 34265  especifico para Tulum
	include "code_inc\GestionObjetos.asm"			; 34375
	include "code_inc\LuzPantallaGest.asm"			; 34462
	include "code_inc\compobjetos.asm"	; 34991
	include "code_inc\comptrampas.asm"	; 35769  Tablas y comprobacion. 
	include "code_inc\rulimpiabuffers.asm"	; 35903
	include "code_inc\paginacion.asm"	; 35950
; libres 32 bytes.  desde 35935 a 35979. *** warning no tocar 
;----------------------------------------------------------------------------------------
; Tabla y rutina de interrupciones
	include "code_inc\rutinainterr.asm"	; 35980
	include "code_inc\Volcdatvertpant.asm"
; libres 8 bytes desde 36088 a 36096.    *** warning no tocar
	include "code_inc\tabinterr.asm"	; 36096
;----------------------------------------------------------------------------------------
; definici�n de los enemigos en las pantallas	
	include "code_inc\tabEnemigos.asm"	; 36353
	include "gfx_inc\sprprotapuntalatigo.asm"	; FIN EN 36579= 29 bytes. no tocar
; Calculo de direcciones en la videoram
	include "code_inc\cdirmemMetalFpixCchar.asm"	; 36608+256+192
	include "code_inc\dzx7mini.asm"			; 37056
;----------------------------------------------------------------------------------------
; definici�n de los Tiles			; 37104 $90f0
	include "gfx_inc\datastiles.asm"	; fin en 39708
	include "code_inc\tabpantytiles.asm"	; tablas de direcciones de los tiles y del mapeado de pantallas.
	include "gfx_inc\gameover4.asm"
	include "code_inc\restdatosenem.asm"
	include "code_inc\restattrmbajo.asm"
	include "code_inc\gestmelodias.asm"
	include "code_inc\stagecleared.asm"
;	include "code_inc\textosimp.asm"	; 
	include "code_inc\textosimpesp.asm"	; 
	include "code_inc\compobjetosilum.asm"	; 41041
;	include "code_inc\contrareloj.asm"
; libres 2123 bytes desde 40756 hasta 42879
;----------------------------------------------------------------------------------------
; Buffer de borrado de iluminaci�n en pantalla 42880 o $a780
	org	$a780
bufferdeapagado:
        defs	512, $ff			; meto el valor ff en las 512 posic.de pantalla de juego
; libres 16bytes
;----------------------------------------------------------------------------------------
; TABLAS vertices desde $a990 hasta $aa80	; 43408
	include "code_inc\tablocalvertices.asm"	; fin 43637 libres 8
;----------------------------------------------------------------------------------------
; Buffer de tiles iluminados de la pantalla	
	org	$aa80				; 43648
bufferdeiluminacion:
	defs	512				; Total 512bytes (2 tercios)
;----------------------------------------------------------------------------------------
; Buffers para desplazamiento del prota.
	org	$ac80				; 44160
buff_des_spr:
	ds	64				; 64 bytes para desplazar el spr para cada frame
buffer_spr:
	ds	64				; 64 bytes para mezclar el fondo con el spr.desplazado
;----------------------------------------------------------------------------------------
; Buffers de pantalla y atributos de la misma
	org	$ad00				; 44288	= $ad00
buffpant:                                       
        ds      4096      ; Buffer de pantalla
buffattpant:                                    ; 48384 = $bd00
        ds      512       ; Buffer de atribut.de pantalla 
; Variables apartir de la $bf00	 48896. Maximo 255 bytes.
	include "code_inc\variables.asm"	; 
; 181 bytes libres
;----------------------------------------------------------------------------------------
; Frames Protagonista
;----------------------------------------------------------------------------------------
SPR_prota:
;	org	$c000				; 49152
	include "gfx_inc\sprprota.asm"            
; Tama�o frame 2x2x8x2=64 bytes o $40h:
;	spr_der_0= $c000	spr_izq_0= $c100		Derecha e Izquierda
;	spr_der_1= $c040	spr_izq_1= $c140		D e I y   tambi�n   Salto
;	spr_lanza_der_0	= $c080		spr_lanza_izq_0	= $c180		disparando la luz
;	spr_lanza_der_1	= $c0c0		spr_lanza_izq_1 = $c1c0
;	spr_ArB_0= $c200					Arriba / Abajo
;	spr_ArB_1= $c240
;	spr_der_parado	= $c280					En parado
;	spr_izq_parado	= $c2c0				
;	spr_der_OJOS	= $c300 	$c340
;	spr_izq_OJOS	= $c380		$c3c0		
; La definici�n del prota termina en $c399. Total 1024 bytes.
	include "gfx_inc\sprprotavela.asm"
	include "gfx_inc\sprprotalatigo.asm"	; FIN en c57f
;----------------------------------------------------------------------------------------
; Sprites APARECIENDO
	org	$c580				; 50560
	include "gfx_inc\Apaenemigos.asm"
; Maya		Frame0	c580	Frame1	c5c0	Frame2	c600
; Maya		Frame3 (frame en pausa) c640

; Olmeca	Frame0	c680	Frame1	c6c0	Frame2	c700
; Olmeca	Frame3 (frame en pausa)	c740
;----------------------------------------------------------------------------------------
; Sprites Enemigos
	org	$c780
	include "gfx_inc\Sprenemigos.asm"	; Tama�o de 1 frame =64 o 40$
; Maya derecha		0,1,2,3		c780,c7c0,c800,c840
; Maya izquierda	0,1,2,3		c880,c8c0,c900,c940
; Olmeca derecha	0,1,2,3		c980,c9c0,ca00,ca40
; Olmeca izquierda	0,1,2,3		ca80,cac0,cb00,cb40
; Maya sube/baja	0,1		cb80,cbc0
; Olmeca sube/baja	0,1		cc00,cc40

;----------------------------------------------------------------------------------------
; Rutinas de movimiento del sprite
	include "gfx_inc\sprprotaarlatigo.asm"
	include "code_inc\movimientos.asm"	;
	include "code_inc\SprProtaCompObst.asm"	;                   
	include "code_inc\SprprotaImp.asm"	; 
	include "code_inc\SprProtaVolc.asm"
	include "code_inc\SprProtaVolcAttr.asm"
	include "code_inc\SprProtaDesp.asm"	;
	include "code_inc\RuVolcAttrIlum.asm"	; 
	include "code_inc\SprProtaEyes.asm"	; 
	include "code_inc\SprLuzGestyTab.asm"	; 
	include "code_inc\GestionLlamaMarcador.asm"
	include "code_inc\SprEnemVolc.asm"	;
	include "code_inc\SprEnemImp.asm"	; 
	include "code_inc\rucredatenem.asm"
	include "code_inc\gestionanimacionenemigos.asm"
	include "code_inc\gestionenemigos.asm"	   ;
	include "code_inc\SprEnemMovimientos.asm"	   ; 
	include "code_inc\gestioncamino.asm"	;
	include "code_inc\SprEnemcomptrampas.asm"
	include "code_inc\Gestionapagadoluces.asm"	   ; 
	include "code_inc\barraenergia.asm"
	include "code_inc\detchoques.asm"
	include "gfx_inc\intro1.asm"
	include "gfx_inc\intro2.asm"
	include "code_inc\compchoqluzcondiam.asm"	; rutina que la llamo desde sprluzgestytab.asm
; libres
