; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  16)
;Char Size:       (  2,   2)
;Frames:             2
;Sort Priorities: Mask, X char, Char line, Y char, Frame number
;Attributes:      At the end
;Mask:            Yes, mask before

; prota l�tigo derechas (Insert graphic label here if needed)
gfxprotalatigoder0:
	DEFB	248,  0
	DEFB	255,  0

	DEFB	240,  7
	DEFB	 63,  0

	DEFB	128, 10
	DEFB	 31,192

	DEFB	  0,111
	DEFB	 63,128

	DEFB	  0, 16
	DEFB	 31, 64

	DEFB	  0,135
	DEFB	 15, 96

	DEFB	  0,177
	DEFB	 31,192

	DEFB	 64, 20
	DEFB	 63,128


	DEFB	 32,142
	DEFB	 95,  0

	DEFB	 64, 23
	DEFB	 15, 32

	DEFB	192, 24
	DEFB	 15, 32

	DEFB	224, 12
	DEFB	 23,128

	DEFB	240,  3
	DEFB	  3, 40

	DEFB	252,  0
	DEFB	  3,104

	DEFB	240,  3
	DEFB	  7, 16

	DEFB	224, 12
	DEFB	239,  0
	

gfxprotalatigoder1:
	
	DEFB	255,  0
	DEFB	255,  0

	DEFB	252,  0
	DEFB	127,  0

	DEFB	248,  3
	DEFB	 31,128

	DEFB	240,  5
	DEFB	 15, 96

	DEFB	240,  7
	DEFB	 31,192

	DEFB	224, 12
	DEFB	 15, 32

	DEFB	240,  3
	DEFB	  7,176

	DEFB	248,  0
	DEFB	 15,224


	DEFB	240,  4
	DEFB	 10,  0

	DEFB	224, 13
	DEFB	  0,245

	DEFB	224, 12
	DEFB	  0,244

	DEFB	240,  4
	DEFB	  3,  0

	DEFB	240,  1
	DEFB	  7, 16

	DEFB	224, 14
	DEFB	 15, 64

	DEFB	241,  4
	DEFB	135, 32

	DEFB	248,  2
	DEFB	  3, 88




; prota l�tigo izquierdas (Insert graphic label here if needed)
gfxprotalatigoizq0:
	DEFB	255,  0
	DEFB	 31,  0

	DEFB	252,  0
	DEFB	 15,224

	DEFB	248,  3
	DEFB	  1, 80

	DEFB	252,  1
	DEFB	  0,246

	DEFB	248,  2
	DEFB	  0,  8

	DEFB	240,  6
	DEFB	  0,225

	DEFB	248,  3
	DEFB	  0,141

	DEFB	252,  1
	DEFB	  2, 40


	DEFB	250,  0
	DEFB	  4,113

	DEFB	240,  4
	DEFB	  2,232

	DEFB	240,  4
	DEFB	  3, 24

	DEFB	232,  1
	DEFB	  7, 48

	DEFB	192, 20
	DEFB	 15,192

	DEFB	192, 22
	DEFB	 63,  0

	DEFB	224,  8
	DEFB	 15,192

	DEFB	247,  0
	DEFB	  7, 48




gfxprotalatigoizq1:
	DEFB	255,  0
	DEFB	255,  0

	DEFB	254,  0
	DEFB	 63,  0

	DEFB	248,  1
	DEFB	 31,192

	DEFB	240,  6
	DEFB	 15,160

	DEFB	248,  3
	DEFB	 15,224

	DEFB	240,  4
	DEFB	  7, 48

	DEFB	224, 13
	DEFB	 15,192

	DEFB	240,  7
	DEFB	 31,  0


	DEFB	 80,  0
	DEFB	 15, 32

	DEFB	  0,175
	DEFB	  7,176

	DEFB	  0, 47
	DEFB	  7, 48

	DEFB	192,  0
	DEFB	 15, 32

	DEFB	224,  8
	DEFB	 15,128

	DEFB	240,  2
	DEFB	  7,112

	DEFB	225,  4
	DEFB	143, 32

	DEFB	192, 26
	DEFB	 31, 64
