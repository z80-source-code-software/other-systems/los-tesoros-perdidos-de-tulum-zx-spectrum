; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 80,  16)
;Char Size:       ( 10,   2)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No

gameover_gfx:
	DEFB	148, 73, 68,148, 73, 68, 73, 68
	DEFB	148, 73,245, 79, 84,245, 79, 85
	DEFB	 79, 84,245, 79,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0, 56, 28
	DEFB	 48, 27,168,  2,195,142,234,236
	DEFB	124,  0, 56, 59,216, 14,243,142
	DEFB	246,238,238, 54, 60,123,168, 12
	DEFB	  1,140,234,227,192, 20, 62,218
	DEFB	  0, 24, 25,140,128,227, 32, 99
	DEFB	 59,154,  0, 24,  1,172,128,254
	DEFB	206,107, 57, 27,248, 24, 25,140
	DEFB	254,252, 38, 99, 16,  3,248, 24
	DEFB	 24, 80,254, 70,238, 99, 40, 26
	DEFB	  0, 12, 48,216,128,162,124,227
	DEFB	144,  3,168, 15,240,  0,234, 67
	DEFB	 56,227,184, 27,168,  3,192,112
	DEFB	234,225,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,247, 15,112,247
	DEFB	 15,112,247, 15,112,247, 20, 65
	DEFB	 68, 20, 65, 68, 20, 65, 68, 20
	DEFB	 68,  4, 68,  4, 68, 68,  4, 68
	DEFB	  4, 68,  6, 70,  6, 70,  6,  6
	DEFB	 70,  6, 70,  6