; *
; Tablas de Colocaci�n de Objetos y luz en Pantalla
; *

; He agrupado dos tablas para usar una s�la rutina de volcado de tablas y para comprimir los datos en
; el futuro. Usar el buffer de pantalla para descomprimirla.

;----------------------------------------------------------------------------------------
; LA_LUZ donde reponer t� vela
;	db 0,1,2,56,1,4,3,3,8,1,0,$2c,$7e,$2c,$7e, 2, 72,  0,  0,0, 0, 0, 0,  0, 0, 0
;
; panta, activo, posxChar,  posyScan, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	    ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
;
; NOUSO, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni,	coluprota,	filaprota
; ix10,	 ix11,	    ix12,       ix13,	     ix14,	        ix15,   	ix16
;
; NOUSO, NOUSO,		NOUSO, NOUSO,	dblowPant, dbhighPant, dblowBuf,    dbhighBuf, Choque
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24	ix25
;
; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24, 25
;
;

;----------------------------------------------------------------------------------------
; tabla_Objetos
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	   ix4		ix5
; Tipo de Objeto = 0,1,2,3
;	si n.tiles= 19	diamantes entonces cada una de las piedras preciosas
;	si n.tiles= 17 � 18 cofre cerrado o abierto entonces:  0 luna, 1 sol, 2 corazon, 3 llave 
; Todos los objetos pueden estar ocultos en  el cofre.
;	Si el valor de ix+5 del churro del cofre es 19 quiere decir que has  escondido un 
;       un diamante en �l. El valor cambiar� siempre a 3 y el 19 pasar� a ix+2
; ejemplo
;	db	 0,1,19, 7, 72,3	;
;	db	 0,1,19,20, 56,0	;
;	db	 0,1,19,21, 88,1	;
;	db	 0,1,19,26, 88,2	;
;	db	 0,0, 0, 0,  0,0	; cofre
;	db	 0,0,24,25,104,0	; puerta abierta

; se vuelca en :

;----------------------------------------------------------------------------------------
; EN LA RUTINA COMPOBJETOS.ASM
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	ix4		ix5

;tabla_objetos_enpantalla:					
;	db	0,0,0,0,0,0,0,0,0	; Diamante 1
;	db	0,0,0,0,0,0,0,0,0	; Diamante 2
;	db	0,0,0,0,0,0,0,0,0	; Diamante 3
;	db	0,0,0,0,0,0,0,0,0	; Diamante 4
;	db	0,0,0,0,0,0,0,0,0	; Cofre, valores para ix+6= 19 gema. si no 0=luna,1=sol,2=corazon y 3=llave
;	db	0,0,0,0,0,0,0,0,0	; Puerta 
;	db	128
; Act.en pant es un objeto que est� activo en la pantalla, no ha sido recogido.
; n.tile = 0 es el de vaciar para borrar objetos de 2x2
; Act.en Mar es un objeto que se encuentra en el marcador y no est� ya en pantalla
; dblow high son para ver como actuar ante la llave en el cofre con los diamantes recogidos
; de momento pendiente de hacer.
;----------------------------------------------------------------------------------------




tabla_Luz_y_Objetos:
; 0
;LUCES_DE_PANTALLA:
	db   0,1, 2, 56,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 2, 72,  0,  0,0, 0, 0, 0,  0, 0, 0
;tabla_Objetos:
	db	 0,1,19,26, 88,3	;
	db	 0,1,19,24, 56,0	;
	db	 0,1,19,21, 88,1	;
	db	 0,1,19, 8, 48,2	;
	db	 0,0, 0, 0,  0,0	; sin cofre
	db	 0,0,24,25,104,0	; puerta 
; 1 
	db   1,1, 2, 48,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 2, 64,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 1,1,19, 2,136,1	;
	db	 1,1,19,26, 88,3	;
	db	 1,1,19,29, 48,0	;
	db	 1,1,19, 8,104,2	;
	db	 1,0, 0, 0,  0,0	; sin cofre
	db	 1,0,24,17, 96,0	; puerta 
; 2 
	db   2,1,22,128,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,22,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 2,1,19,25, 48,1	;
	db	 2,1,19, 5, 40,0	;
	db	 2,1,19,19, 96,3	;
	db	 2,1,19, 1,104,2	;
	db	 2,1,17,22, 64,3	; cofre + llave 3
	db	 2,0,24,20,136,0	; puerta abierta 1,0,24,20,128,0
; 3 
	db   3,1,21,120,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,24, 64,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 3,1,19,29,104,1	;
	db	 3,1,19,29, 56,0	;
	db	 3,1,19,14, 80,2	;
	db	 3,1,19, 8, 48,3	;
	db	 3,1,17,23, 40,1	; cofre + sol
	db	 3,0,24,15,136,0	; puerta 
; 4 
	db   4,1, 4, 56,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 8,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 4,1,19, 1, 80,3	;
	db	 4,1,19,13, 80,0	;
	db	 4,1,19,16, 80,1	;
	db	 4,1,19,29, 80,2	;
	db	 4,1,17, 4, 72,0	; cofre + luna 0
	db	 4,0,24, 8,136,0	; puerta 
; 5
	db   5,1,2,128,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 1,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 5,1,19, 1, 40,1	;
	db	 5,1,19,15, 40,2	;	x,y 29,112
	db	 5,1,19,29, 40,3	;
	db	 5,1,19, 1, 72,0	;
	db	 5,1,17,28,112,1	; cofre + sol 1 x,y27,112
	db	 5,0,24, 3,104,0	; puerta 
; 6- fase bonus. Los cofres no se cogen por que no puedes soltar la luz. Pero ser�a un huevo de pascua guapo
; si hubiera un contador de las veces que golpeas la tecla M. A los 10 pulsaciones que se abra el cofre.
; Se puede ver en el motor principal, QUI_EVEN_CONT 
; 
	db   6,0,4, 40,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 4, 56,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 6,1,19,11,136,0	;
	db	 6,1,19,19,136,1	;
	db	 6,1,19, 1,136,2	;
	db	 6,1,19,29,136,3	;
	db	 6,0,17, 9, 56,2	; SIN COFRE. FASE ESPECIAL
	db	 6,0,24, 7, 88,0	; puerta 
; 7
	db   7,1,2,136,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 3,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 7,1,19,23, 40,3	;
	db	 7,1,19,26, 40,0	;
	db	 7,1,19,20, 80,1	;
	db	 7,1,19,12, 80,2	;
	db	 7,1,17,26, 72,0	; cofre + luna
	db	 7,0,24, 3, 96,0	; puerta 
; 8
	db   8,1,1,128,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e, 1,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 8,1,19, 7, 72,1	;
	db	 8,1,19,10, 72,2	;
	db	 8,1,19,21, 72,3	;
	db	 8,1,19,25, 96,0	;
	db	 8,1,17,10, 48,2	; cofre + corazones
	db	 8,0,24,27,136,0	; puerta 
; 9
	db   9,1,1,128,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,16,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 9,1,19, 1, 40,1	;
	db	 9,1,19,21, 40,0	;
	db	 9,1,19, 1, 64,2;
	db	 9,1,19,29, 96,3	;
	db	 9,0,17, 0,  0,0	; sin cofre
	db	 9,0,24,27,136,0	; puerta 
; 10
	db   10,1,20,128,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,20,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 10,1,19, 9, 40,0	;
	db	 10,1,19,13, 40,1	;
	db	 10,1,19, 4, 88,2;
	db	 10,0,19,29, 40,3	;
etchgix2:
	db	 10,1,17,23,72,19	;	db	 10,1,17, 3, 56,2	; COFRE corazones
	db	 10,0,24,20, 72,0	; puerta 
; 11
	db   11,0,20,128,1,6,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,20,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 11,1,19, 5,136,0	;
	db	 11,1,19,12, 88,1	;
	db	 11,1,19,18, 88,3;
	db	 11,1,19,23, 88,2	;
	db	 11,0,17, 3, 56,1	; sin cofre    FASE ESPECIAL
	db	 11,0,24,23,136,0	; puerta 
; 12
	db   12,1,1,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,1,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 12,1,19, 5, 40,2	;
	db	 12,1,19,27, 72,3	;
	db	 12,1,19,15, 56,1;
	db	 12,1,19,23, 40,0	;
	db	 12,1,17, 7, 80,0	; cofre con la luna.
	db	 12,0,24,29, 48,0	; puerta 
; 13
	db   13,1, 5,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,5,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 13,1,19, 7, 88,2	;
	db	 13,1,19,12, 56,3	;
	db	 13,1,19,17, 40,1;
	db	 13,1,19,24, 40,0	;
	db	 13,0,17, 7, 80,0	; sin cofre 
	db	 13,0,24,27, 48,0	; puerta 
; 14
	db   14,1,13,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,13,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 14,1,19,15, 64,2	;
	db	 14,1,19,29,120,3	;
	db	 14,1,19,18, 40,1;
	db	 14,1,19,23, 80,0	;
	db	 14,0,17, 7, 80,0	; sin cofre 
	db	 14,0,24,29, 40,0	; puerta 
; 15
	db   15,0,17,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,17,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 15,1,19,24, 48,0	;
	db	 15,1,19,22, 64,1	;
	db	 15,1,19,29, 72,2;
	db	 15,1,19,26,104,3	;
	db	 15,0,17, 7, 80,0	; sin cofre 
	db	 15,0,24,29, 48,0	; puerta 
; 16
	db   16,1,25,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,25,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 16,1,19,27, 56,3	;
	db	 16,1,19,29, 56,2	;
	db	 16,1,19,29,104,1;
	db	 16,1,19,29,120,0	;
	db	 16,0,17, 7, 80,0	; sin cofre 
	db	 16,0,24,29,136,0	; puerta 
; 17
	db   17,1, 2,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,2,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 17,1,19, 8, 56,3	;
	db	 17,1,19,22, 56,2	;
	db	 17,1,19, 5, 72,1;
	db	 17,1,19,25, 72,0	;
	db	 17,0,17,17, 96,2	; SIN cofre  FASE ESPECIAL
	db	 17,0,24,15,136,0	; puerta 
; 18
	db   18,1, 2,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,2,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 18,1,19, 1, 40,2	;
	db	 18,1,19, 9, 88,1	;
	db	 18,1,19,21, 88,0;
	db	 18,1,19,29, 40,3	;
	db	 18,0,17,21,136,2	; sin cofre 
	db	 18,0,24,15,136,0	; puerta 
; 19
	db   19,0,29,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,29,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 19,1,19, 8, 56,1	;
	db	 19,1,19,22, 56,3	;
	db	 19,1,19, 5, 72,0;
	db	 19,1,19,25, 72,2	;
	db	 19,0,17, 9,136,1	; cofre sol
	db	 19,0,24,15,136,0	; puerta 
; 20
	db   20,1, 5,128,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,5,136,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 20,1,19, 1, 56,2	;
	db	 20,1,19, 3, 56,1	;
	db	 20,1,19, 1,104,0	;
	db	 20,1,19, 1,120,3	;
	db	 20,0,17,21,136,2	; sin cofre 
	db	 20,0,24, 1,136,0	; puerta 
; 21
	db   21,1, 1, 40,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,1, 48,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 21,1,19, 1, 72,2	;
	db	 21,1,19, 9, 72,1	;
	db	 21,1,19, 6, 96,0	;
	db	 21,1,19,12,104,3	;
	db	 21,0,17,21,136,2	; sin cofre 
	db	 21,0,24,13,136,0	; puerta 
; 22
	db   22,1, 1, 40,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,1, 48,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 22,1,19,10, 40,0	;
	db	 22,1,19, 1, 72,1	;
	db	 22,1,19,12, 88,2	;
	db	 22,1,19,13,136,3	;
	db	 22,1,17, 8, 48,3	; cofre con llave
	db	 22,0,24,17,136,0	; puerta 
; 23
	db   23,1, 3, 40,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,3, 48,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 23,1,19, 6, 40,2	;
	db	 23,1,19,13, 40,3	;
	db	 23,1,19,16, 48,0	;
	db	 23,1,19,23, 88,1	;
	db	 23,0,17, 8, 48,2	; sin cofre 
	db	 23,0,24,25,136,0	; puerta 
; 24
	db   24,1, 1, 40,1,4,3,3, 8, 1,  0,$2c,$7e,$2c,$7e,1, 48,  0,  0,0, 0, 0, 0,  0, 0, 0
	db	 24,1,19,24, 40,2	;
	db	 24,1,19,27, 72,3	;
	db	 24,1,19,11, 40,0	;
	db	 24,1,19, 1,136,1	;
	db	 24,0,17, 8, 48,2	; sin cofre 
	db	 24,0,24,29,136,0	; puerta 
; fin de fichero
	db	128



;----------------------------------------------------------------------------------------
; LA_LUZ donde reponer t� vela
;	db 0,1,2,56,1,4,3,3,8,1,0,$2c,$7e,$2c,$7e, 2, 72,  0,  0,0, 0, 0, 0,  0, 0, 0
;
; panta, activo, posxChar,  posyScan, ticks, frameskip, frameActual, framestotal, altosc, anchochars,
; ix0,	ix1,	ix2,	    ix3,	ix4,	ix5,		ix6,	ix7,	  ix8,  	ix9
;
; NOUSO, dblowEnem, dbhighEnem, dblowEnemIni,dbhighEnemIni,	coluprota,	filaprota
; ix10,	 ix11,	    ix12,       ix13,	     ix14,	        ix15,   	ix16
;
; NOUSO, NOUSO,		NOUSO, NOUSO,	dblowPant, dbhighPant, dblowBuf,    dbhighBuf, Choque
; ix17,	    ix18,	ix19	ix20	ix21		ix22	ix23		ix24	ix25
;
; ix+	     0,1, 2,  3,4,5,6,7, 8, 9, 10, 11, 12, 13, 14,15, 16, 17,18,19,20, 21, 22, 23, 24, 25
;
;

;----------------------------------------------------------------------------------------
; tabla_Objetos
; Contiene: Pantalla, Act.en pant,num.tile, pos. X char, pos. Y pixels, tipo Objeto
;		ix0	ix1	   ix2		ix3	   ix4		ix5
; Tipo de Objeto = 0,1,2,3
;	si n.tiles= 19	diamantes entonces cada una de las piedras preciosas
;	si n.tiles= 17 � 18 cofre cerrado o abierto entonces:  0 luna, 1 sol, 2 corazon, 3 llave 
; Todos los objetos pueden estar ocultos en  el cofre.
;	Si el valor de ix+5 del churro del cofre es 19 quiere decir que has  escondido un 
;       un diamante en �l. El valor cambiar� siempre a 3 y el 19 pasar� a ix+2
; ejemplo
;	db	 0,1,19, 7, 72,3	;
;	db	 0,1,19,20, 56,0	;
;	db	 0,1,19,21, 88,1	;
;	db	 0,1,19,26, 88,2	;
;	db	 0,0, 0, 0,  0,0	; cofre
;	db	 0,0,24,25,104,0	; puerta abierta


