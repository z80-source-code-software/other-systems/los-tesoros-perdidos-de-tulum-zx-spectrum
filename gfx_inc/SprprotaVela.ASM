;	
; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      (  8,   8)
;Char Size:       (  1,   1)
;Frames:             4
;Sort Priorities: Mask, Char line, Frame number
;Attributes:      At the end
;Mask:            Yes, mask before


gfxprotavelader:
; Vela a derecha 1x1

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	128,  0
	DEFB	192,  0
	DEFB	 96,  0
	DEFB	 48,128
	DEFB	 48,128

	DEFB	  0,  0
	DEFB	112,  0
	DEFB	248,  0
	DEFB	220,  0
	DEFB	140, 32
	DEFB	  4,112
	DEFB	  4,112
	DEFB	136, 32

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 28,  0
	DEFB	 54,  0
	DEFB	 34,  8
	DEFB	 34,  8

	DEFB	  0,  0
	DEFB	  3,  0
	DEFB	  7,  0
	DEFB	 13,  0
	DEFB	  8,  2
	DEFB	 16,  7
	DEFB	 16,  7
	DEFB	 24,  2


gfxprotavelaizq:
; Vela a la izquierda

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  1,  0
	DEFB	  3,  0
	DEFB	  6,  0
	DEFB	 12,  1
	DEFB	 12,  1

	DEFB	  0,  0
	DEFB	 14,  0
	DEFB	 31,  0
	DEFB	 59,  0
	DEFB	 49,  4
	DEFB	 32, 14
	DEFB	 32, 14
	DEFB	 17,  4

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 56,  0
	DEFB	108,  0
	DEFB	 68, 16
	DEFB	 68, 16

	DEFB	  0,  0
	DEFB	192,  0
	DEFB	224,  0
	DEFB	176,  0
	DEFB	 16, 64
	DEFB	  8,224
	DEFB	  8,224
	DEFB	 24, 64
