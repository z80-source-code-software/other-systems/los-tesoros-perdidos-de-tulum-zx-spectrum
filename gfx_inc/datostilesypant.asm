;----------------------------------------------------------------------------------------
	; He dejado 4kb totales para la tabla de objetos y la de trampas.
	; El resto otros 12kb para la definición de las pantallas
;----------------------------------------------------------------------------------------

	
	org	49152
;----------------------------------------------------------------------------------------
; definición de los Objetos y trampas de las pantallas
	include "tabObjetos.asm"		; hoy 50601 (20/06/17) fichero que hay que modificar es el .sym
						; 1551 bytes
	include "tabTrampas.asm"		; 225 bytes

menutulumpi:
	incbin "menutulumpi2.zx7"	; 2253bytes

	org	53248				; $d000
;----------------------------------------------------------------------------------------
; definición de las pantallas
	include "dataspant.asm"		; 5255bytes	
menutulumpar2:
	incbin "menutulumpar2.zx7"	; 1208bytes
menutulumpd2:
	incbin "menutulumpd2.zx7"	; 412bytes
menutulumpab:
	incbin "menutulumpab.zx7"	; 796bytes
theend:
	incbin "final.zx7"		; 3364bytes
