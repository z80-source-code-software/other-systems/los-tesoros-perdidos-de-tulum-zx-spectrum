; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      (  8,   8)
;Char Size:       (  1,   1)
;Frames:             4
;Sort Priorities: Char line, Mask, Frame number
;Attributes:      At the end
;Mask:            Yes
	org	32300		; $7E2C
; (Insert graphic label here if needed)


	DEFB	 40,130
	DEFB	 68, 17
	DEFB	134, 48
	DEFB	 15, 96
	DEFB	 15, 96
	DEFB	143, 32
	DEFB	219,  0
	DEFB	241,  4

	DEFB	195,  8
	DEFB	131, 56
	DEFB	  3,112
	DEFB	 15, 64
	DEFB	183,  0
	DEFB	227,  8
	DEFB	225, 12
	DEFB	227,  8

	DEFB	135, 48
	DEFB	 11, 96
	DEFB	 17, 68
	DEFB	161, 12
	DEFB	225, 12
	DEFB	227,  8
	DEFB	231,  0
	DEFB	199, 16

	DEFB	  1, 76
	DEFB	 48,134
	DEFB	120,  2
	DEFB	241,  4
	DEFB	155,  0
	DEFB	 15, 96
	DEFB	135, 48
	DEFB	199, 16