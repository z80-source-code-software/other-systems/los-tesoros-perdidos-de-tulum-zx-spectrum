;----------------------------------------------------------------------------------------
; Frames Protagonista
;----------------------------------------------------------------------------------------
	org	49152
; Tama�o frame 2x2x8x2=64 bytes o $40h:
;	spr_der_0= $c000	spr_izq_0= $c100		Derecha e Izquierda
;	spr_der_1= $c040	spr_izq_1= $c140		D e I y   tambi�n   Salto

;	spr_lanza_der_0	= $c080		spr_lanza_izq_0	= $c180		disparando la luz
;	spr_lanza_der_1	= $c0c0		spr_lanza_izq_1 = $c1c0

;	spr_ArB_0= $c200					Arriba / Abajo
;	spr_ArB_1= $c240

;	spr_der_parado	= $c280					En parado
;	spr_izq_parado	= $c2c0				

;	spr_der_OJOS	= $c300 	$c340
;	spr_izq_OJOS	= $c380		$c3c0	

gfxprotader0:

	DEFB	129,  0,255,  0,  0, 56,255,  0
	DEFB	  0, 86,127,  0,  0,124,255,  0
	DEFB	  0,194,127,  0,  0, 59, 63,  0
	DEFB	  0, 62,127,  0,  0, 92,255,  0
	DEFB	  1,192,255,  0,  1,184,255,  0
	DEFB	  0,108,255,  0,  0, 28,255,  0
	DEFB	  0, 88,255,  0,  1,  0,255,  0
	DEFB	128, 24,255,  0,  0, 62,127,  0
gfxprotader1:
	DEFB	240,  3, 15,128,224,  5,  7, 96
	DEFB	224,  7, 15,192,192, 12,  7, 32
	DEFB	224,  3,  3,176,192, 11,  7,224
	DEFB	128, 29, 15,192,192, 12,  7,  0
	DEFB	192,  9,  3,208,224,  3,  3,208
	DEFB	240,  0,  7,  0,224,  4,  3, 64
	DEFB	192, 15,  1, 40,128, 22,  3, 80
	DEFB	192,  0,  7, 32,233,  0,143,  0



; Sprite lanzando a derechas 2x2 , 2frames
gfxprotalanzader0:

	DEFB	252,  1
	DEFB	 31,128

	DEFB	248,  3
	DEFB	 15, 96

	DEFB	240,  5
	DEFB	 31,128

	DEFB	240,  7
	DEFB	 31,  0

	DEFB	224, 12
	DEFB	 15, 32

	DEFB	240,  1
	DEFB	  7,176

	DEFB	232,  0
	DEFB	 15,224

	DEFB	192, 22
	DEFB	 31, 64


	DEFB	192, 27
	DEFB	 63,  0

	DEFB	224, 13
	DEFB	 63,128

	DEFB	224,  9
	DEFB	 31,192

	DEFB	244,  1
	DEFB	 31,192

	DEFB	252,  1
	DEFB	 63,128

	DEFB	252,  0
	DEFB	 63,  0

	DEFB	240,  2
	DEFB	 15,192

	DEFB	225,  8
	DEFB	  7,112


gfxprotalanzader1:

	DEFB	255,  0
	DEFB	255,  0

	DEFB	253,  0
	DEFB	127,  0

	DEFB	248,  2
	DEFB	 63,128

	DEFB	240,  5
	DEFB	 31, 64

	DEFB	224, 15
	DEFB	 15,224

	DEFB	224,  0
	DEFB	 15,  0

	DEFB	240,  3
	DEFB	  7,176

	DEFB	224,  8
	DEFB	 15,224


	DEFB	192, 16
	DEFB	 31,  0

	DEFB	224, 13
	DEFB	  3,232

	DEFB	224, 10
	DEFB	  3,104

	DEFB	240,  3
	DEFB	 31,128

	DEFB	240,  1
	DEFB	 63,  0

	DEFB	224, 14
	DEFB	 15, 64

	DEFB	241,  4
	DEFB	135, 16

	DEFB	248,  2
	DEFB	143, 32




; Prota a izquierdas de 2x2
gfxprotaizq0:

	DEFB	255,  0,129,  0,255,  0,  0, 28
	DEFB	254,  0,  0,106,255,  0,  0, 62
	DEFB	254,  0,  0, 67,252,  0,  0,220
	DEFB	254,  0,  0,124,255,  0,  0, 58
	DEFB	255,  0,128,  3,255,  0,128, 29
	DEFB	255,  0,  0, 54,255,  0,  0, 56
	DEFB	255,  0,  0, 26,255,  0,128,  0
	DEFB	255,  0,  1, 24,254,  0,  0,124

gfxprotaizq1:

	DEFB	240,  1, 15,192,224,  6,  7,160
	DEFB	240,  3,  7,224,224,  4,  3, 48
	DEFB	192, 13,  7,192,224,  7,  3,208
	DEFB	240,  3,  1,184,224,  0,  3, 48
	DEFB	192, 11,  3,144,192, 11,  7,192
	DEFB	224,  0, 15,  0,192,  2,  7, 32
	DEFB	128, 20,  3,240,192, 10,  1,104
	DEFB	224,  4,  3,  0,241,  0,151,  0














; Sprite lanzando a izquierdas 2x2, 2 frames
gfxprotalanzaizq0:

	DEFB	248,  1
	DEFB	 63,128

	DEFB	240,  6
	DEFB	 31,192

	DEFB	248,  1
	DEFB	 15,160

	DEFB	248,  0
	DEFB	 15,224

	DEFB	240,  4
	DEFB	  7, 48

	DEFB	224, 13
	DEFB	 15,128

	DEFB	240,  7
	DEFB	 23,  0

	DEFB	248,  2
	DEFB	  3,104


	DEFB	252,  0
	DEFB	  3,216

	DEFB	252,  1
	DEFB	  7,176

	DEFB	248,  3
	DEFB	  7,144

	DEFB	248,  3
	DEFB	 47,128

	DEFB	252,  1
	DEFB	 63,128

	DEFB	252,  0
	DEFB	 63,  0

	DEFB	240,  3
	DEFB	 15, 64

	DEFB	224, 14
	DEFB	135, 16


gfxprotalanzaizq1:

	DEFB	255,  0
	DEFB	255,  0

	DEFB	254,  0
	DEFB	191,  0

	DEFB	252,  1
	DEFB	 31, 64

	DEFB	248,  2
	DEFB	 15,160

	DEFB	240,  7
	DEFB	  7,240

	DEFB	240,  0
	DEFB	  7,  0

	DEFB	224, 13
	DEFB	 15,192

	DEFB	240,  7
	DEFB	  7, 16


	DEFB	248,  0
	DEFB	  3,  8

	DEFB	192, 23
	DEFB	  7,176

	DEFB	192, 22
	DEFB	  7, 80

	DEFB	248,  1
	DEFB	 15,192

	DEFB	252,  0
	DEFB	 15,128

	DEFB	240,  2
	DEFB	  7,112

	DEFB	225,  8
	DEFB	143, 32

	DEFB	241,  4
	DEFB	 31, 64



; Prota Arriba/Abajo 2x2, 2frames
gfxprotaarrba0:

	DEFB	247,  0
	DEFB	255,  0

	DEFB	224,  8
	DEFB	255,  0

	DEFB	192, 27
	DEFB	127,  0

	DEFB	128, 53
	DEFB	 55,128

	DEFB	128, 43
	DEFB	  3,200

	DEFB	128, 39
	DEFB	  1,156

	DEFB	128, 40
	DEFB	  1, 68

	DEFB	128, 55
	DEFB	  3,144


	DEFB	192, 16
	DEFB	  3,120

	DEFB	224,  7
	DEFB	  7,176

	DEFB	240,  4
	DEFB	 15,128

	DEFB	240,  3
	DEFB	 15, 96

	DEFB	224, 12
	DEFB	 15,224

	DEFB	224, 14
	DEFB	 31,192

	DEFB	240,  2
	DEFB	 31, 64

	DEFB	248,  0
	DEFB	191,  0



gfxprotaarrba1:

	DEFB	255,  0
	DEFB	223,  0

	DEFB	254,  0
	DEFB	 15, 32

	DEFB	252,  1
	DEFB	  7,176

	DEFB	216,  2
	DEFB	  3,216

	DEFB	128, 37
	DEFB	  3,232

	DEFB	  0,115
	DEFB	  3,200

	DEFB	  0, 68
	DEFB	  3, 40

	DEFB	128, 19
	DEFB	  3,216


	DEFB	128, 60
	DEFB	  7, 16

	DEFB	192, 27
	DEFB	 15,192

	DEFB	224,  2
	DEFB	 31, 64

	DEFB	224, 13
	DEFB	 31,128

	DEFB	224, 14
	DEFB	 15, 96

	DEFB	240,  4
	DEFB	 15,224

	DEFB	250,  0
	DEFB	 31,128

	DEFB	254,  0
	DEFB	 63,  0





; Sprite Prota parado a derechas 2x2, 1 frame
gfxprotaparadoder:

	DEFB	252,  0
	DEFB	127,  0

	DEFB	248,  3
	DEFB	 31,128

	DEFB	240,  5
	DEFB	 15, 96

	DEFB	240,  7
	DEFB	 31,192

	DEFB	224, 12
	DEFB	 15, 32

	DEFB	240,  3
	DEFB	  7,176

	DEFB	248,  3
	DEFB	 15,224

	DEFB	240,  5
	DEFB	 31,192


	DEFB	224, 12
	DEFB	 63,  0

	DEFB	224, 11
	DEFB	 63,128

	DEFB	240,  6
	DEFB	 31,192

	DEFB	248,  1
	DEFB	 31,192

	DEFB	240,  5
	DEFB	 63,128

	DEFB	250,  0
	DEFB	127,  0

	DEFB	252,  1
	DEFB	 31,128

	DEFB	248,  3
	DEFB	 15,224




; Sprite Prota parado a izquierdas 2x2, 1 frame
gfxprotaparadoizq:

	DEFB	254,  0
	DEFB	 63,  0

	DEFB	248,  1
	DEFB	 31,192

	DEFB	240,  6
	DEFB	 15,160

	DEFB	248,  3
	DEFB	 15,224

	DEFB	240,  4
	DEFB	  7, 48

	DEFB	224, 13
	DEFB	 15,192

	DEFB	240,  7
	DEFB	 31,192

	DEFB	248,  3
	DEFB	 15,160


	DEFB	252,  0
	DEFB	  7, 48

	DEFB	252,  1
	DEFB	  7,208

	DEFB	248,  3
	DEFB	 15, 96

	DEFB	248,  3
	DEFB	 31,128

	DEFB	252,  1
	DEFB	 15,160

	DEFB	254,  0
	DEFB	 95,  0

	DEFB	248,  1
	DEFB	 63,128

	DEFB	240,  7
	DEFB	 31,192





; Prota OJOS  a derechas
gfxprotaojosder0:


	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  3,  0
	DEFB	128,  0

	DEFB	  7,  0
	DEFB	192,  0

	DEFB	 14,  0
	DEFB	224,  0

	DEFB	 24,  1
	DEFB	 96,  0

	DEFB	 16,  4
	DEFB	192,  0

	DEFB	 27,  0
	DEFB	128,  0

	DEFB	 15,  0
	DEFB	  0,  0


	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0


gfxprotaojosder1:

	DEFB	  0,  0
	DEFB	 24,  0

	DEFB	  0,  0
	DEFB	124,  0

	DEFB	  0,  0
	DEFB	236,  0

	DEFB	  0,  0
	DEFB	132, 16

	DEFB	  1,  0
	DEFB	 12, 64

	DEFB	  1,  0
	DEFB	184,  0

	DEFB	  0,  0
	DEFB	240,  0

	DEFB	  0,  0
	DEFB	  0,  0


	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0


; Prota OJOS  a izquierdas
gfxprotaojosizq0:

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  1,  0
	DEFB	192,  0

	DEFB	  3,  0
	DEFB	224,  0

	DEFB	  7,  0
	DEFB	112,  0

	DEFB	  6,  0
	DEFB	 24,128

	DEFB	  3,  0
	DEFB	  8, 32

	DEFB	  1,  0
	DEFB	216,  0

	DEFB	  0,  0
	DEFB	240,  0


	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0


gfxprotaojosizq1:

	DEFB	 24,  0
	DEFB	  0,  0

	DEFB	 62,  0
	DEFB	  0,  0

	DEFB	 55,  0
	DEFB	  0,  0

	DEFB	 33,  8
	DEFB	  0,  0

	DEFB	 48,  2
	DEFB	128,  0

	DEFB	 29,  0
	DEFB	128,  0

	DEFB	 15,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0


	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0
