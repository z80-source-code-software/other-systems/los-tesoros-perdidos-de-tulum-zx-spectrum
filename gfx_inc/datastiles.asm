; ASM source file created by SevenuP v1.12
; SevenuP (C) Copyright 2002-2004 by Jaime Tejedor G�mez, aka Metalbrain

; N.tile, Alto Scannes, Ancho Char
	ORG	37104	;	$90f0


; Vacio para borrar.
db	0,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  6,  6
	DEFB	  6,  6


; Esquina suelo cortado a izquierdas
db	1,16,2
	DEFB	 85, 93
	DEFB	167,150
	DEFB	 76,248
	DEFB	150, 33
	DEFB	208,  0
	DEFB	 80,  0
	DEFB	  0,  0
	DEFB	 80,  0

	DEFB	  0, 36
	DEFB	 64,128
	DEFB	 32,  0
	DEFB	 64,  0
	DEFB	  0,  0
	DEFB	 64,  0
	DEFB	128,  0
	DEFB	 32,  0
; (Attributes here, insert label if needed)
	DEFB	 67, 67
	DEFB	 67,  1



; Esquina techo cortado a derechas
db	2,16,2
	DEFB	  0,  4
	DEFB	  0,  1
	DEFB	  0,  2
	DEFB	  0,  0
	DEFB	  0,  2
	DEFB	  0,  4
	DEFB	  4,  2
	DEFB	 65,  0

	DEFB	  0, 10
	DEFB	  0,  0
	DEFB	  0, 10
	DEFB	  0, 11
	DEFB	132,105
	DEFB	 31, 50
	DEFB	105,229
	DEFB	186,170
; (Attributes here, insert label if needed)
	DEFB	  1, 67
	DEFB	 67, 67


; Esquina techo cortado a izquierdas
db	3,16,2
	DEFB	 32,  0
	DEFB	128,  0
	DEFB	 64,  0
	DEFB	  0,  0
	DEFB	 64,  0
	DEFB	 32,  0
	DEFB	 64,  4
	DEFB	  0,162

	DEFB	 80,  0
	DEFB	  0,  0
	DEFB	 80,  0
	DEFB	208,  0
	DEFB	150, 33
	DEFB	 76,248
	DEFB	167,150
	DEFB	 85, 93
; (Attributes here, insert label if needed)
	DEFB	 67,  1
	DEFB	 67, 67


; conectorder
db	 4,16,2
	DEFB	 64,  0
	DEFB	136,  0
	DEFB	 96,  0
	DEFB	 40,  0
	DEFB	132,  0
	DEFB	 31,  0
	DEFB	 73,  0
	DEFB	 18,  0

	DEFB	  0,128
	DEFB	  0, 72
	DEFB	  0, 18
	DEFB	  0,104
	DEFB	  0, 22
	DEFB	  0,  1
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	 67,  6
	DEFB	  6, 67


; conectorizquierda
db	 5,16,2
	DEFB	  0,  0
	DEFB	  0, 17
	DEFB	  0,  0
	DEFB	  0, 18
	DEFB	  0, 33
	DEFB	  0,216
	DEFB	  0,146
	DEFB	  0, 72

	DEFB	  1,  0
	DEFB	 18,  0
	DEFB	 72,  0
	DEFB	 18,  0
	DEFB	104,  0
	DEFB	128,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  6, 67
	DEFB	 67,  6


; Fondillo
db	6,8,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  2,  0
	DEFB	 40, 90
; (Attributes here, insert label if needed)
	DEFB	  1,  1


; Liana Inicio y final
db	7,16,2
	DEFB	  0,  0
	DEFB	  5,  0
	DEFB	  2,  0
	DEFB	  0,128
	DEFB	  2,  0
	DEFB	  9,  0
	DEFB	  2,  0
	DEFB	  2,  0

	DEFB	  5,  0
	DEFB	  2,128
	DEFB	  0,  0
	DEFB	  1,  0
	DEFB	  2,  0
	DEFB	  0,  0
	DEFB	  2,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  4,  4
	DEFB	  4,  4


; liana
db	8,16,2
	DEFB	  1,  0
	DEFB	  2,128
	DEFB	  5,  0
	DEFB	  4,  0
	DEFB	  1,  0
	DEFB	  2, 64
	DEFB	  5, 32
	DEFB	  4, 32

	DEFB	  1, 64
	DEFB	  0,128
	DEFB	  1,128
	DEFB	  2,128
	DEFB	  0,128
	DEFB	  0,128
	DEFB	  1,  0
	DEFB	  1,  0
; (Attributes here, insert label if needed)
	DEFB	  4,  4
	DEFB	  4,  4


; Pared lateral
db	9, 32,1
	DEFB	 20,  0,  8,  0, 20, 24,  4,  8
	DEFB	  8,  8,  4,  8,  4,  8,  4, 12
	DEFB	  8,  0,  8,  0,  8,  0,  8,  8
	DEFB	  4,  4, 12,  0,  8,  4,  4,  8
; (Attributes here, insert label if needed)
	DEFB	  3+64,  3+64,  3+64,  3+64


; Puerta
db	10,16,2
	DEFB	 15,240
	DEFB	 48, 12
	DEFB	 64,  2
	DEFB	 27,216
	DEFB	 59,204
	DEFB	 91,218
	DEFB	 90, 74
	DEFB	 90, 88

	DEFB	 91,202
	DEFB	 91,210
	DEFB	 91,200
	DEFB	 91,210
	DEFB	 91,200
	DEFB	 91,208
	DEFB	  9,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2


; Suelo 0
db	11, 8,4
	DEFB	173,219,182,221
	DEFB	118,173, 75, 86
	DEFB	152,  8,164, 32
	DEFB	  0, 64,  1, 10
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 3+64, 3+64, 3+64, 3+64


; Suelo 1
db	12, 8,4
	DEFB	171,109,155,213
	DEFB	 90,154, 37,110
	DEFB	  5,  5, 16, 25
	DEFB	  0, 64,  4,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  3+64,  3+64,  3+64,  3+64


; Techo 0
db	13, 8,4
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	 42,170,148,170
	DEFB	 93,247,111,119
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  3+64,  3+64,  3+64,  3+64


; Techo 1
db	14, 8,4
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	170, 84,170,170
	DEFB	119,234,119, 84
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  3+64,  3+64,  3+64,  3+64


; Trampa pinchos en el suelo
db	15,16,2
	DEFB	  0, 64
	DEFB	  0, 64
	DEFB	 32,160
	DEFB	112, 64
	DEFB	 36,  0
	DEFB	 14, 68
	DEFB	 36,  4
	DEFB	 32, 74

	DEFB	  5, 68
	DEFB	  4,  4
	DEFB	  5, 32
	DEFB	  0,  0
	DEFB	  4,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  5,  7
	DEFB	  5,  5


; Esquina suelo cortado a derechas
db	16,16,2
	DEFB	186,170
	DEFB	105,229
	DEFB	 31, 50
	DEFB	132,105
	DEFB	  0, 11
	DEFB	  0, 10
	DEFB	  0,  0
	DEFB	  0, 10

	DEFB	 20,  0
	DEFB	  0,  2
	DEFB	  1,  4
	DEFB	  0,  2
	DEFB	  0,  0
	DEFB	  0,  2
	DEFB	  0,  1
	DEFB	  0,  4
; (Attributes here, insert label if needed)
	DEFB	 67, 67
	DEFB	  1, 67


; Cofre. Frame 0
db	17,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 38,100
	DEFB	115,206
	DEFB	 95,250
	DEFB	 51,204
	DEFB	 68, 34
	DEFB	253,191

	DEFB	133,161
	DEFB	253,191
	DEFB	132, 33
	DEFB	183,237
	DEFB	166,101
	DEFB	189,189
	DEFB	129,129
	DEFB	255,255
; (Attributes here, insert label if needed)
	DEFB	 70, 70
	DEFB	 70,  6


; Cofre. Frame 1
db	18,16,2
	DEFB	 23,232
	DEFB	108, 54
	DEFB	221,179
	DEFB	253,191
	DEFB	128,139
	DEFB	128,  5
	DEFB	192, 11
	DEFB	 96, 22

	DEFB	  1,  0
	DEFB	253,191
	DEFB	132, 33
	DEFB	183,237
	DEFB	166,101
	DEFB	189,189
	DEFB	129,129
	DEFB	255,255
; (Attributes here, insert label if needed)
	DEFB	 70, 70
	DEFB	  6,  6


; diamante
db	19,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 20,  0
	DEFB	  8,  0
	DEFB	 17,192
	DEFB	  6, 48
	DEFB	 13,216
	DEFB	  3,160

	DEFB	 13, 88
	DEFB	  6,176
	DEFB	  3, 96
	DEFB	  1,192
	DEFB	  0,128
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  7,  5
	DEFB	  5,  7


; luna
db	20,16,2
	DEFB	  0,  0
	DEFB	 23,128
	DEFB	 47,160
	DEFB	 23,112
	DEFB	  6,216
	DEFB	  1,232
	DEFB	  1,172
	DEFB	  1, 28

	DEFB	  0,252
	DEFB	  0,252
	DEFB	  0,  8
	DEFB	  1, 24
	DEFB	 28,248
	DEFB	 42, 80
	DEFB	 20,160
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  5,  5
	DEFB	  5,  5


; Sol
db	21,16,2
	DEFB	  1,128
	DEFB	  3,192
	DEFB	 48, 12
	DEFB	 59,220
	DEFB	 22,104
	DEFB	 10, 80
	DEFB	105,150
	DEFB	238,119

	DEFB	111,246
	DEFB	 12, 48
	DEFB	 21,168
	DEFB	 59,220
	DEFB	 48, 12
	DEFB	  3,192
	DEFB	  1,128
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  6,  6
	DEFB	  6,  6


; Llave 2
db	22,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  3, 96
	DEFB	  4,144
	DEFB	  4,  0
	DEFB	  2, 32
	DEFB	  1,128
	DEFB	  1, 64

	DEFB	  0,128
	DEFB	  2,128
	DEFB	  3,  0
	DEFB	  1,128
	DEFB	  3,  0
	DEFB	  2,128
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  7,  6
	DEFB	  6,  6


; Corazones
db	23,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 12,192
	DEFB	 31,224

	DEFB	 23,224
	DEFB	 23,224
	DEFB	 11,192
	DEFB	  5,154
	DEFB	  3, 30
	DEFB	  0, 12
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  3,  2
	DEFB	  2,  2


; Puerta de Nivel abierta
db	24,16,2
	DEFB	 15,240
	DEFB	 56, 12
	DEFB	 80,  2
	DEFB	 32,  0
	DEFB	 80,  0
	DEFB	 96,  2
	DEFB	 80,  0
	DEFB	 96,  2

	DEFB	 80,  0
	DEFB	 96,  2
	DEFB	 80,  0
	DEFB	 96,  2
	DEFB	 64,  0
	DEFB	 64,  0
	DEFB	  0,  0
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2


; Esquina de suelo para conectar tiles por la izquierda
db	25, 8,1
	DEFB	  2, 41, 84, 40,128, 32,128,  0
; (Attributes here, insert label if needed)
	DEFB	 67


; Esquina de suelo para conectar tiles por la derecha
db	26, 8,1
	DEFB	 64,148, 42, 20,  1,  4,  1,  0
; (Attributes here, insert label if needed)
	DEFB	 67


; fondo 0_0
db	27,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  2,170
	DEFB	  4,  5
	DEFB	  0,  2
	DEFB	  4,  1
	DEFB	  0,  2
	DEFB	  4,  1

	DEFB	  1,  2
	DEFB	  4,  1
	DEFB	  0,  2
	DEFB	  0,  1
	DEFB	  4,  4
	DEFB	  0,  9
	DEFB	  4, 68
	DEFB	  1, 80
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


;fondo 0_1
db	28,16,2
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 85, 80
	DEFB	128, 32

	DEFB	  0,  8
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  4
	DEFB	 64,  8
	DEFB	  4,  4
	DEFB	 42, 64
	DEFB	 84,164
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 0_2
db	29,16,2
	DEFB	  2,173
	DEFB	  0,  2
	DEFB	  4,  1
	DEFB	  0,  0
	DEFB	 13, 65
	DEFB	  2,128
	DEFB	 48, 65
	DEFB	 32, 32

	DEFB	  0, 64
	DEFB	 32, 32
	DEFB	  0, 72
	DEFB	 42, 34
	DEFB	  5, 84
	DEFB	 10, 32
	DEFB	 85, 21
	DEFB	 40,138
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 0_3
db	30,16,2
	DEFB	  8,  8
	DEFB	 65, 84
	DEFB	 34,162
	DEFB	128, 73
	DEFB	  0,  2
	DEFB	 64,  9
	DEFB	  0,  2
	DEFB	  0,  9

	DEFB	  0,  2
	DEFB	  0,  1
	DEFB	  4,  8
	DEFB	138,100
	DEFB	 69, 82
	DEFB	170, 38
	DEFB	 66,217
	DEFB	  5, 84
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 1_0
db	31,16,2
	DEFB	 21, 74
	DEFB	 72, 21
	DEFB	  0,  2
	DEFB	128,  5
	DEFB	  0,  0
	DEFB	128,  1
	DEFB	  0,  0
	DEFB	128,  1

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	 64, 33
	DEFB	 21, 74
	DEFB	  0, 32
	DEFB	 37, 80
	DEFB	 16,  2
	DEFB	 64,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 1_1
db	32,16,2
	DEFB	148,160
	DEFB	 33, 10
	DEFB	 64,  1
	DEFB	  0,128
	DEFB	 64,  1
	DEFB	  0,  0
	DEFB	128,  1
	DEFB	  0,  0

	DEFB	128,  1
	DEFB	  0,  0
	DEFB	 85,  1
	DEFB	  0,  1
	DEFB	128,128
	DEFB	  0,  2
	DEFB	128,128
	DEFB	  0,  4
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 1_2
db	33,16,2
	DEFB	  0,  1
	DEFB	128,  0
	DEFB	  0,  2
	DEFB	192,  0
	DEFB	  0,  1
	DEFB	128,  0
	DEFB	 90,128
	DEFB	  1,  0

	DEFB	 72,  0
	DEFB	  4,128
	DEFB	128,  1
	DEFB	  4,  4
	DEFB	130,  1
	DEFB	  0, 16
	DEFB	 32,138
	DEFB	 85, 85
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 1_3
db	34,16,2
	DEFB	  0,168
	DEFB	  0,  4
	DEFB	 65,  2
	DEFB	138,  9
	DEFB	 84,  2
	DEFB	  0,  5
	DEFB	160,  2
	DEFB	  0,  1

	DEFB	 64,  2
	DEFB	  0,  1
	DEFB	  0,  2
	DEFB	128,  1
	DEFB	  0,  2
	DEFB	 64, 16
	DEFB	168,170
	DEFB	 85, 84
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1



; fondo 2_0
db	35,16,2
	DEFB	  0, 86
	DEFB	  0,133
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0

	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	173, 85
	DEFB	  2, 40
	DEFB	 16,  2
	DEFB	  2,  0
; (Attributes here, insert label if needed)
	DEFB	  6,  1
	DEFB	  1,  1


; fondo 2_1
db	36,16,2
	DEFB	128,  0
	DEFB	 32,  0
	DEFB	  0,  0
	DEFB	 32,  0
	DEFB	128,  0
	DEFB	 32,  0
	DEFB	  0,  0
	DEFB	 32,  0

	DEFB	 64,  0
	DEFB	 32,  0
	DEFB	 64,  0
	DEFB	  0,  0
	DEFB	 88,  0
	DEFB	166,  0
	DEFB	 16,  0
	DEFB	  5,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  6
	DEFB	  1,  1


; fondo 2_2
db	37,16,2
	DEFB	  4,  0
	DEFB	  1,  0
	DEFB	  0,  0
	DEFB	  2,  0
	DEFB	  0,  1
	DEFB	  1,  0
	DEFB	  2,  0
	DEFB	 80,  0

	DEFB	  0,128
	DEFB	  8,  0
	DEFB	  0,128
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0,  0
	DEFB	  0, 32
	DEFB	  1, 84
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; fondo 2_3
db	38,16,2
	DEFB	  0, 64
	DEFB	  0,  0
	DEFB	 85, 32
	DEFB	136, 16
	DEFB	 66,168
	DEFB	169, 16
	DEFB	  0,  8
	DEFB	128, 16

	DEFB	  0,  4
	DEFB	  0,  8
	DEFB	  0,  2
	DEFB	  0,  8
	DEFB	  0,  4
	DEFB	  0,  0
	DEFB	128,  4
	DEFB	 80,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1
						; termina en 38552 a fecha 01/12/17

; Estatua fondo der
db	39,32,2
	DEFB	  8,  0
	DEFB	 37,  0
	DEFB	  2, 64
	DEFB	 20,160
	DEFB	  2, 72
	DEFB	 87, 60
	DEFB	 42, 84
	DEFB	  0,  4

	DEFB	 66,  0
	DEFB	225,  0
	DEFB	  5,  0
	DEFB	 68,  0
	DEFB	 66, 21
	DEFB	 75,  0
	DEFB	 74, 14
	DEFB	 83,  0

	DEFB	 11, 22
	DEFB	213,  8
	DEFB	 41,176
	DEFB	212, 64
	DEFB	 32,  0
	DEFB	 91,  0
	DEFB	 23,128
	DEFB	 95,192

	DEFB	 13,224
	DEFB	 82,160
	DEFB	  4,160
	DEFB	 72,  0
	DEFB	  4, 64
	DEFB	 72,  0
	DEFB	  0, 80
	DEFB	 94,232
; (Attributes here, insert label if needed)
	DEFB	  1,  1 
	DEFB	  1,  5
	DEFB	  1,  1 
	DEFB	  1,  1 




; Estatua fondo izq
db	40,32,2
	DEFB	  0, 16
	DEFB	  0,164
	DEFB	  2, 64
	DEFB	  5, 40
	DEFB	 18, 64
	DEFB	 60,234
	DEFB	 42, 84
	DEFB	 32,  0

	DEFB	  0, 66
	DEFB	  0,135
	DEFB	  0,160
	DEFB	  0, 34
	DEFB	168, 66
	DEFB	  0,210
	DEFB	112, 82
	DEFB	  0,202

	DEFB	104,208
	DEFB	 16,171
	DEFB	 13,148
	DEFB	  2, 43
	DEFB	  0,  4
	DEFB	  0,218
	DEFB	  1,232
	DEFB	  3,250

	DEFB	  7,176
	DEFB	  5, 74
	DEFB	  5, 32
	DEFB	  0, 18
	DEFB	  2, 32
	DEFB	  0, 18
	DEFB	 10,  0
	DEFB	 23,122
; (Attributes here, insert label if needed)
	DEFB	  1,  1 
	DEFB	  5,  1 
	DEFB	  1,  1 
	DEFB	  1,  1 



; fondo templo 0 4x4
db	41,32,4
	DEFB	  0,  0,  0,  0
	DEFB	 99,244,251,169
	DEFB	130, 42,  0,  0
	DEFB	 32,  0,205, 29
	DEFB	132,  1,109,186
	DEFB	  4, 68,181,169
	DEFB	  0, 42,128,  3
	DEFB	159, 64,118,241

	DEFB	 52,  9, 16,  2
	DEFB	 33, 32,217,166
	DEFB	176,137, 44,  4
	DEFB	 34,240,161, 86
	DEFB	 28, 72, 44, 34
	DEFB	137,  1,237,  0
	DEFB	  0, 32, 41,242
	DEFB	194,133, 34,103

	DEFB	128, 16, 10,  4
	DEFB	 34, 58, 93, 81
	DEFB	  8,  0, 22,228
	DEFB	128, 17, 16, 65
	DEFB	 40, 56,  0,  4
	DEFB	130, 18,  0, 65
	DEFB	  7, 16,  0,228
	DEFB	141,144,  4, 65

	DEFB	 23, 64, 14, 72
	DEFB	138,130,100, 93
	DEFB	133, 24,  5, 72
	DEFB	162,  2,133, 73
	DEFB	136, 20, 69, 72
	DEFB	 11,160,170, 64
	DEFB	128,  0,  0,  1
	DEFB	104,138,  5, 85
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1



; fondo templo 1 4x2
db	42,16,4
	DEFB	 13, 26,154,140
	DEFB	 44, 88, 89, 45
	DEFB	112,226,226,112
	DEFB	 58,112,117, 57
	DEFB	 84,186,186, 84
	DEFB	233, 81, 80,232
	DEFB	 67,131,165,193
	DEFB	  6, 23,  3,131

	DEFB	 42, 74, 23, 55
	DEFB	116,116,186, 58
	DEFB	 57, 57, 28,156
	DEFB	 64, 84, 38, 42
	DEFB	 42,169,116,116
	DEFB	129,194, 97, 33
	DEFB	  9,144,138,  4
	DEFB	164, 42, 36,202
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1


; fondo templo 2 4x2
db	43,16,4
	DEFB	  0,  0,  0,  0
	DEFB	214,170, 87,224
	DEFB	 87, 85, 88, 20
	DEFB	214,170,111,232
	DEFB	 85, 85,112, 20
	DEFB	210,170,127,250
	DEFB	209,255,127,244
	DEFB	 72,  0,127,234

	DEFB	231,245,127,245
	DEFB	112,  0,126,170
	DEFB	 63,250, 61, 85
	DEFB	 64,  0,  0,  0
	DEFB	236,162,120, 32
	DEFB	166,181,158,208
	DEFB	 74,162,120, 32
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1


; fondo templo 3 4x2
db	44,16,4
	DEFB	205,154,170,170
	DEFB	146, 68, 69, 85
	DEFB	109,179,175,162
	DEFB	109,182,221, 85
	DEFB	  0, 13,104,168
	DEFB	191,171,165, 85
	DEFB	 85, 87,210,168
	DEFB	158,147,168,  1

	DEFB	 95, 91,194,250
	DEFB	158,147,171,  0
	DEFB	 93, 91,240,213
	DEFB	158, 33, 75,  0
	DEFB	 79,170,160,250
	DEFB	173, 34,170,  1
	DEFB	 94,175,245, 84
	DEFB	169, 10,170,170
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1


; pared templo 0 4x1 der
db	45,32,1
	DEFB	 80, 64, 16, 64, 80, 64, 80, 32
	DEFB	 16, 96, 64, 64,  0, 32, 80, 96
	DEFB	  5, 72, 66, 64, 80, 64, 80, 32
	DEFB	 16, 64, 64, 96, 80, 96, 64, 64
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; pared templo 1 4x1 der
db	46,32,1
	DEFB	 80, 96, 69, 40, 84, 68, 80,  0
	DEFB	 80, 96, 64, 96, 64, 32, 64, 64
	DEFB	 80,  0, 64,  0, 80,  0, 80, 32
	DEFB	  0, 32, 64, 96, 84, 40, 69, 32
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; pared templo 0 4x1 izq
db	47,32,1
	DEFB	 10,  2,  8,  2, 10,  2, 10,  4
	DEFB	  8,  6,  2,  2,  0,  4, 10,  6
	DEFB	160, 18, 66,  2, 10,  2, 10,  4
	DEFB	  8,  2,  2,  6, 10,  6,  2,  2
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; pared templo 1 4x1 izq
db	48,32,1
	DEFB	 10,  6,162, 20, 42, 34, 10,  0
	DEFB	 10,  6,  2,  6,  2,  4,  2,  2
	DEFB	 10,  0,  2,  0, 10,  0, 10,  4
	DEFB	  0,  4,  2,  6, 42, 20,162,  4
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; piedras templo 1
db	49,8,4
	DEFB	127,254,255,127
	DEFB	 86,149, 82,194
	DEFB	  1, 33, 36,168
	DEFB	 10,  4, 66, 68
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; piedras templo 2
db	50,8,4
	DEFB	253,255,127,118
	DEFB	170,138,170,170
	DEFB	  0,  0,  0,  0
	DEFB	146, 65, 36,144
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
	DEFB	  0,  0,  0,  0
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; techo templo 1
db	51,8,4
	DEFB	  0,  0,  0,  0
	DEFB	 89, 84,154, 74
	DEFB	  0,  0,  0,  0
	DEFB	247, 41,146,222
	DEFB	151,123,210,210
	DEFB	243, 41,158, 26
	DEFB	  5,121,128,218
	DEFB	247,123,222,222
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; techo templo 2
db	52,8,4
	DEFB	  0,  0,  0,  0
	DEFB	164,178, 85, 84
	DEFB	 82,  5, 40,128
	DEFB	244,211, 85,218
	DEFB	166,  6,125,192
	DEFB	246,247,  1,222
	DEFB	246,  7,125,192
	DEFB	246,247,125,222
; (Attributes here, insert label if needed)
	DEFB	 67, 67, 67, 67


; simbolo Diosa
db	53,16,3
	DEFB	 10, 90, 80
	DEFB	 96,165,  6
	DEFB	146, 90, 73
	DEFB	229, 36,167
	DEFB	242,126, 79
	DEFB	 24,165,  8
	DEFB	106,165, 86
	DEFB	138,189, 81

	DEFB	117,153,238
	DEFB	 30, 90,120
	DEFB	 32, 44,  4
	DEFB	124, 52, 62
	DEFB	128, 44,  1
	DEFB	248, 52, 31
	DEFB	 64, 90,  2
	DEFB	 63,129,252
; (Attributes here, insert label if needed)

	DEFB	  1,  1,  1
	DEFB	  1,  1,  1


; gommy
db	54,16,2
	DEFB	 23,208, 41,232, 54,248, 52, 24
	DEFB	 59,184, 60,120, 51,152, 56, 56
	DEFB	 89, 52,235,174,236,110, 95,244
	DEFB	 63,248,  8, 32, 92,116,238,238
	
;	DEFB	  7,224
;	DEFB	 13,176
;	DEFB	 33,132
;	DEFB	 54,108
;	DEFB	 12, 48
;	DEFB	 95,248
;	DEFB	 25,152
;	DEFB	  0,  2

;	DEFB	 13,184
;	DEFB	126,124
;	DEFB	107,214
;	DEFB	  4, 34
;	DEFB	 71,224
;	DEFB	 11,208
;	DEFB	 29,185
;	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1
	DEFB	  1,  1


; nelo & quqo y el molo mielda
db	55,32,4
	DEFB	 84,  3,130,224
	DEFB	 32,  0, 13,224
	DEFB	128, 25, 11,208
	DEFB	128,248,  4,188
	DEFB	159,192, 27,126
	DEFB	 63,128, 36,158
	DEFB	122, 88, 70,156
	DEFB	202,164, 67, 36

	DEFB	201,  4, 64,  4
	DEFB	 96,  9, 39,  4
	DEFB	 96,113,155,152
	DEFB	 44,192,196, 34
	DEFB	 17, 48, 99,221
	DEFB	 78,168, 47,122
	DEFB	177,228, 25,240
	DEFB	 95,152,  1,182

	DEFB	 40,240,  8,204
	DEFB	 87,240,101,204
	DEFB	109,163,184,  0
	DEFB	 57,100,150,  0
	DEFB	  1,200, 97, 28
	DEFB	  0,  8,  1, 34
	DEFB	 60,  8, 10, 42
	DEFB	 64, 68,147, 26

	DEFB	 89,  3,  7,130
	DEFB	 84, 32,191,132
	DEFB	 68,226, 79, 96
	DEFB	 56,130,159,112
	DEFB	  0,132,112,224
	DEFB	  7, 38, 14,  0
	DEFB	  4, 66, 28,  0
	DEFB	  0,  3,128,  0
; (Attributes here, insert label if needed)
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1
	DEFB	  1,  1,  1,  1

; esq.suelo templo conec.izq 2x2
db	56,16,2
	DEFB	119,221
	DEFB	162,138
	DEFB	192,  0
	DEFB	137, 20
	DEFB	 64,  0
	DEFB	144,  0
	DEFB	192,  0
	DEFB	144,  0

	DEFB	  0,  4
	DEFB	144,  1
	DEFB	128,  0
	DEFB	192,  2
	DEFB	144,  0
	DEFB	 64,  1
	DEFB	128,  2
	DEFB	128, 80
; (Attributes here, insert label if needed)
	DEFB	 67, 67
	DEFB	 67,  1



; esqu.suelotemploder 2x2
db	57,16,2
	DEFB	187,238
	DEFB	 81, 69
	DEFB	  0,  3
	DEFB	164, 73
	DEFB	  0,  2
	DEFB	  0,  9
	DEFB	  0,  3
	DEFB	  0,  9

	DEFB	 32,  0
	DEFB	128,  1
	DEFB	  0,  9
	DEFB	 64,  3
	DEFB	  0,  1
	DEFB	128, 10
	DEFB	 64,  1
	DEFB	 10,  0
; (Attributes here, insert label if needed)
	DEFB	 67, 67
	DEFB	  1, 67

; esq. conector templo 1x1
db	58,8,1
	DEFB	  0, 44,106,125, 58, 20,  8,  0
; (Attributes here, insert label if needed)
	DEFB	 67


; puertaconprota0
db	59,16,2
	DEFB	 15,240
	DEFB	 56, 84
	DEFB	 80,170
	DEFB	 40,136
	DEFB	113,116
	DEFB	104,168
	DEFB	112,248
	DEFB	104,112

	DEFB	113,  0
	DEFB	106,116
	DEFB	114,250
	DEFB	104, 82
	DEFB	 96,  0
	DEFB	 64, 80
	DEFB	 64,216
	DEFB	  0,  0
; (Attributes here, insert label if needed)

	DEFB	  2,  2
	DEFB	  2,  2

; puertaconprota1
db	60,16,2
	DEFB	 15,240
	DEFB	 48,  4
	DEFB	 79, 42
	DEFB	 43,  8
	DEFB	122,116
	DEFB	 90, 40
	DEFB	 91,120
	DEFB	 91,112

	DEFB	 91,  0
	DEFB	 91,116
	DEFB	 91,122
	DEFB	 91, 82
	DEFB	 91,  0
	DEFB	 80, 80
	DEFB	 64, 88
	DEFB	  0,  0
; (Attributes here, insert label if needed)
	DEFB	  2,  2
	DEFB	  2,  2
